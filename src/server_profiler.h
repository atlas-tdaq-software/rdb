#ifndef RDB_SERVER_PROFILER_H
#define RDB_SERVER_PROFILER_H

#include <sys/resource.h>
#include <time.h>

namespace rdb {

  class ServerProfiler {

    private:

      bool m_use;
      const char * m_fname;
      struct rusage m_ru_1, m_ru_2;
      struct timespec m_ts_1, m_ts_2;

    public:

      ServerProfiler(bool use, const char * fname);
      ~ServerProfiler() { now(true); }

        /** Return number of microseconds passed since start of the function **/

      unsigned long now(bool stop);

  };

}

#define RDB_SERVER_PROFILING_INFO(X) rdb::ServerProfiler __rdb_server_profiling_info__( (ers::debug_level() >= 2), X );

#endif
