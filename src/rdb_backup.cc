#include <sstream>

#include <ipc/core.h>

#include <config/Configuration.h>

#include <sstream>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

#include <oks/file.h>
#include <oks/kernel.h>

#include "rdb_impl.h"
#include "server_utils.h"

#include "rdb-backup/Server.h"
#include "rdb-backup/Subscription.h"
#include "rdb-backup/ServerSubscription.h"
#include "rdb-backup/Object.h"
#include "rdb-backup/PoolNode.h"


struct BackupThreadCommand {
  RDBServer * m_server;
  size_t m_sleep_interval;
  size_t m_count;
  static std::thread * m_thread;
  static bool m_stop;

  BackupThreadCommand(RDBServer * p, size_t n) :  m_server (p), m_sleep_interval (n), m_count(0) { ; }

  void operator()() {
    while (!m_stop) {
      sleep(1);
      m_count++;
      if(m_count%m_sleep_interval == 0) {
        m_server->try_backup();
      }
    }
  }
};

bool BackupThreadCommand::m_stop(false);
std::thread * BackupThreadCommand::m_thread(nullptr);

void
RDBServer::start_backup_thread(size_t backup_interval)
{
  if(!m_backup_file.empty()) {
    BackupThreadCommand command(this, backup_interval);
    BackupThreadCommand::m_thread = new std::thread(command);
  }
  else {
    ERS_DEBUG(2, "the \'backup\' mode is disabled" );
  }
}

void
RDBServer::stop_backup_thread()
{
  if(BackupThreadCommand::m_thread) {
    BackupThreadCommand::m_stop = true;
    BackupThreadCommand::m_thread->join();
    delete BackupThreadCommand::m_thread;
    BackupThreadCommand::m_thread = nullptr;
  }
}


static size_t s_backup_requests_counter = 0;
static std::mutex s_backup_requests_mutex;

void
RDBServer::request_backup() const
{
  std::lock_guard lock(s_backup_requests_mutex);
  s_backup_requests_counter++;
}

void
RDBServer::try_backup()
{
  {
    std::lock_guard lock(s_backup_requests_mutex);
    if(s_backup_requests_counter == 0) {
      ERS_DEBUG(2, "there are no changes for backup" );
      return;
    }
  }

  make_backup();
}

void
RDBServer::make_backup()
{
  std::ostringstream log_text;
  log_text << "create backup file \'" << m_backup_file << '\'';

  {
    std::lock_guard lock(s_backup_requests_mutex);

    if (s_backup_requests_counter)
      {
        log_text << " (requests counter " << s_backup_requests_counter << ')';
        s_backup_requests_counter = 0;
      }
  }

  ers::log(ers::Message(ERS_HERE, log_text.str()));

  std::lock_guard lock(p_mutex);

  static std::mutex s_backup_mutex;
  std::lock_guard b_lock(s_backup_mutex);

  try {
    ::Configuration db(rdb::get_backup_spec());

    std::ostringstream s;
    s << m_backup_file << '.' << getpid();

    std::list<std::string> includes;

    if (const char *tdaq_dir = getenv("TDAQ_INST_PATH"))
      includes.push_back(std::string(tdaq_dir) + "/share/data/rdb/backup.schema.xml");
    else
      includes.push_back("rdb/backup.schema.xml");

    db.create(s.str(), includes);


      // Create description of main server parameters

    rdb::backup::Server * server = const_cast<rdb::backup::Server *>(db.create<rdb::backup::Server>(s.str(), name()));

    server->set_PushMirroredPartitionRetryInterval(m_push_mirror_partition ? m_push_mirror_partition_retry_interval : 0);

    if (!m_kernel->schema_files().empty())
      {
        std::vector<std::string> files;

        for (const auto &i : m_kernel->schema_files())
          if (i.second->get_parent() == nullptr)
            files.push_back(i.second->get_well_formed_name());

        server->set_Schema_Files(files);
      }

    if (!m_kernel->data_files().empty())
      {
        std::vector<std::string> files;

        for (const auto &i : m_kernel->data_files())
          if (i.second->get_parent() == 0)
            files.push_back(i.second->get_well_formed_name());

        server->set_Data_Files(files);
      }

    if (m_schema_fh) {
      server->set_Master_Server_Name(m_master_server_name);
    }

    server->set_Database_Version(m_kernel->get_repository_version());


      // backup information about pool of servers, if any

    if(!m_pool_nodes.empty()) {
      std::vector<const rdb::backup::PoolNode*> nodes;

      for(unsigned int i = 0; i < m_pool_nodes.size(); ++i) {
        rdb::backup::PoolNode * obj = const_cast<rdb::backup::PoolNode *>(db.create<rdb::backup::PoolNode>(*server, m_pool_nodes[i].p_name));
	obj->set_Host(m_pool_nodes[i].p_node);
	obj->set_NumberOfClients(m_pool_nodes[i].p_number);
	nodes.push_back(obj);
      }

      server->set_PoolNodes(nodes);
    }


    for(std::map<std::string, rdb::rdb_callback_var>::const_iterator i = m_server_subscriptions.begin(); i != m_server_subscriptions.end(); ++i) {
      rdb::backup::ServerSubscription * obj = const_cast<rdb::backup::ServerSubscription *>(db.create<rdb::backup::ServerSubscription>(*server, i->first));
      obj->set_SubscriptionObject(IPCCore::objectToString(i->second, IPCCore::Corbaloc));
    }


      // Create description of clients subscriptions

    for(std::map<int32_t, rdb::Subscription *>::const_iterator j = m_subscriptions.begin(); j != m_subscriptions.end(); ++j) {
      rdb::Subscription * s = j->second;

      std::ostringstream str;
      str << j->first;

      rdb::backup::Subscription * obj = const_cast<rdb::backup::Subscription *>(db.create<rdb::backup::Subscription>(*server, str.str()));

      obj->set_SubscriptionObject(IPCCore::objectToString(s->m_cb, IPCCore::Corbaloc ));
      obj->set_LookInSubclasses(s->m_look_in_subclasses);
      obj->set_Parameter(s->m_parameter);
      obj->set_UserID(s->m_user_id);
      obj->set_Host(s->m_host);
      obj->set_ProcessID(s->m_process_id);
      obj->set_AppName(s->m_app_name);
      obj->set_CreationTimeStamp(s->m_creation_ts);

      if(s->m_class_names.size()) {
        std::vector<std::string> classes;

        for(std::set<std::string>::const_iterator i = s->m_class_names.begin(); i != s->m_class_names.end(); ++i) {
          classes.push_back(*i);
        }

        obj->set_Classes(classes);
      }

      if(s->m_objects_names.size()) {
        std::vector<const rdb::backup::Object*> objs;

        for(rdb::Subscription::ObjectsMap::const_iterator i = s->m_objects_names.begin(); i != s->m_objects_names.end(); ++i) {
          std::string id = i->first + '@' + str.str();
          rdb::backup::Object * o = const_cast<rdb::backup::Object *>(db.create<rdb::backup::Object>(*server, id));

	  objs.push_back(o);
	
          o->set_ClassName(i->first);

          std::vector<std::string> ids;

          for(std::set<std::string>::const_iterator l = i->second.begin(); l != i->second.end(); ++l) {
	    ids.push_back(*l);
  	  }

          o->set_ObjectIDs(ids);
        }

        obj->set_Objects(objs);
      }
    }

    db.commit();

    rename(s.str().c_str(), m_backup_file.c_str());
  }
  catch (ers::Issue & ex) {
    ers::error( daq::rdb::FailedCreateBackupFile( ERS_HERE, m_backup_file.c_str(), ex) );
  }
}


void
RDBServer::load_backup(const char * backup_file)
{
  ERS_LOG( "restore state from backup file \'" << backup_file << '\'' );

  try {
    ::Configuration db(rdb::get_backup_spec(backup_file));

      // get server object

    std::vector<const rdb::backup::Server *> servers;
    db.get(servers);

    if(servers.size() > 1)
      throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "backup file contains more than one \'Server\' object" );
    else if(servers.empty())
      throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "backup file does not contain \'Server\' object" );

      // checkout correct git version

    const std::string& version = servers[0]->get_Database_Version();

    if (!version.empty())
      {
        std::string version_param, version_value;

        try
          {
            OksKernel::parse_config_version(getenv("TDAQ_DB_VERSION"), version_param, version_value);
          }
        catch (const std::exception &ex)
          {
            ers::error(daq::rdb::BadBackupData(ERS_HERE, backup_file, "failed to parse TDAQ_DB_VERSION", ex));
          }

        ERS_DEBUG(0, "version = " << version << ", TDAQ_DB_VERSION: " << version_param << ':' << version_value);

        if (version != version_value || version_param != "hash")
          m_kernel->update_repository("hash", version, OksKernel::RepositoryUpdateType::DiscardChanges);
      }


      // get schema and data files

    {
      const std::vector<std::string>& files = servers[0]->get_Schema_Files();

      if(servers[0]->get_Master_Server_Name().empty()) {
        for (const auto& i : files)
          m_kernel->load_schema(i);
      }
      else {
        unlink(files[0].c_str());
      }
    }

    m_push_mirror_partition_retry_interval = servers[0]->get_PushMirroredPartitionRetryInterval();

    if(m_push_mirror_partition_retry_interval != 0) {
      ERS_LOG( "restore server push-mirror-partition mode" );
      m_push_mirror_partition = true;
    }
    else {
      m_push_mirror_partition = false;
    }

    {
      const std::vector<std::string>& files = servers[0]->get_Data_Files();

      if(servers[0]->get_Master_Server_Name().empty()) {
        for (const auto& i : files)
          m_kernel->load_data(i);
      }
      else {
        unlink(files[0].c_str());
      }
    }

      // if necessary, read complete configuration from master RDB server

    if(!servers[0]->get_Master_Server_Name().empty())
      read_complete_db(servers[0]->get_Master_Server_Name(), true);


      // restore information about pool objects

    if(!servers[0]->get_PoolNodes().empty()) {
      for(unsigned int i = 0; i < servers[0]->get_PoolNodes().size(); ++i) {
        const rdb::backup::PoolNode * node = servers[0]->get_PoolNodes()[i];
	PoolNode obj(node->UID(), node->get_Host());
	obj.p_number = node->get_NumberOfClients();
	m_pool_nodes.push_back(obj);
      }
    }


      // get servers subscription objects

    {
      std::vector<const rdb::backup::ServerSubscription *> subs;
      db.get(subs);

      for(std::vector<const rdb::backup::ServerSubscription *>::const_iterator i = subs.begin(); i != subs.end(); ++i) {
        rdb::rdb_callback_var cb;

        std::ostringstream issue;
  
        try {
          CORBA::Object_var object = IPCCore::stringToObject( (*i)->get_SubscriptionObject() );
          cb = rdb::rdb_callback::_narrow( object );
          if ( CORBA::is_nil( cb ) || cb -> _non_existent() ) {
	    issue << "failed to restore server callback \'" << (*i)->get_SubscriptionObject() << "\': object is null or non-existent";
          }
        }
        catch( const CORBA::SystemException & ex) {
	  issue << "failed to restore server callback \'" << (*i)->get_SubscriptionObject() << "\': caught CORBA exception \'" << &ex << '\'';
        }
        catch(...) {
	  issue << "failed to restore server callback \'" << (*i)->get_SubscriptionObject() << "\': caught unknown exception";
        }

        if(!issue.str().empty()) {
	  ers::warning(daq::rdb::BadBackupData(ERS_HERE, backup_file, issue.str().c_str()));
          continue;
        }

        m_server_subscriptions[(*i)->UID()] = cb;

        ERS_LOG( "restore subscription for server \'" << (*i)->UID() << '\'' );
      }
    }


      // get clients subscription objects

    {
      std::vector<const rdb::backup::Subscription *> subs;
      db.get(subs);

      for(std::vector<const rdb::backup::Subscription *>::const_iterator i = subs.begin(); i != subs.end(); ++i) {
        rdb::callback_var cb;
  
        std::ostringstream issue;

        try {
          CORBA::Object_var object = IPCCore::stringToObject( (*i)->get_SubscriptionObject() );
          cb = rdb::callback::_narrow( object );
          if ( CORBA::is_nil( cb ) || cb -> _non_existent() ) {
	    issue << "failed to restore client callback \'" << (*i)->get_SubscriptionObject() << "\': object is null or non-existent";
          }
        }
        catch( const CORBA::SystemException & ex) {
	  issue << "failed to restore client callback \'" << (*i)->get_SubscriptionObject() << "\': caught CORBA exception \'" << &ex << '\'';
        }
        catch(...) {
	  issue << "failed to restore client callback \'" << (*i)->get_SubscriptionObject() << "\': caught unknown exception";
        }

        if(!issue.str().empty()) {
	  ers::warning(daq::rdb::BadBackupData(ERS_HERE, backup_file, issue.str().c_str()));
          continue;
        }

        rdb::Subscription::ObjectsMap objs;

        for(std::vector<const rdb::backup::Object *>::const_iterator j = (*i)->get_Objects().begin(); j != (*i)->get_Objects().end(); ++j) {
          for(std::vector<std::string>::const_iterator l = (*j)->get_ObjectIDs().begin(); l != (*j)->get_ObjectIDs().end(); ++l) {
            objs[(*j)->get_ClassName()].insert(*l);
          }
        }

        rdb::Subscription * s = new rdb::Subscription(
          (*i)->get_UserID(),
          (*i)->get_Host(),
          (*i)->get_ProcessID(),
          (*i)->get_AppName(),
          (*i)->get_CreationTimeStamp(),
          (*i)->get_Classes(),
          (*i)->get_LookInSubclasses(),
          objs,
          cb,
          (*i)->get_Parameter()
        );

        int cnt = atoi((*i)->UID().c_str());
        if(m_subscription_count < (cnt + 1)) m_subscription_count = cnt + 1;

        m_subscriptions[cnt] = s;

        ERS_LOG( "restore client subscription " << cnt );
      }
    }

    subscribe_oks_changes();
  }
  catch (oks::exception & ex) {
    std::ostringstream text;
    text << "caught oks exception\n" << ex.what() ;
    throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, text.str().c_str() );
  }
  catch (ers::Issue & ex) {
    throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "caught exception", ex );
  }
}

