#ifndef RDB_SERVER_UTILS_H
#define RDB_SERVER_UTILS_H

#include <iostream>

#include <AccessManager/client/ServerInterrogator.h>
#include <AccessManager/xacml/impl/RDBResource.h>

#include "rdb/rdb.hh"

#include <config/map.h>
#include <oks/object.h>

#include "src/common_utils.h"

std::ostream&
operator<<(std::ostream& s, const rdb::RDBObject& obj);


namespace rdb {

  std::string get_user(const rdb::ProcessInfo&);
  std::string get_user_safe(const rdb::ProcessInfo&) noexcept;

  std::string to_str(const rdb::RDBNameList& names, bool use_brackets = true);

      /**
       *  Describes subscription made by a client.
       */

  struct Subscription {

    typedef std::map< std::string,std::set<std::string> > ObjectsMap;

    Subscription(const rdb::ProcessInfo& info, const rdb::RDBNameList&, bool, const rdb::RDBObjectList&, rdb::callback_ptr, int64_t);
    Subscription(const std::string& user_id, const std::string& host,  long process_id, const std::string& app_name, const std::string& creation_ts, const std::vector<std::string>&, bool, const ObjectsMap&, rdb::callback_var&, int64_t);

    std::set<std::string>  m_class_names;
    ObjectsMap             m_objects_names;
    bool                   m_look_in_subclasses;
    int64_t                m_parameter;
    rdb::callback_var      m_cb;

    const std::string      m_user_id;        ///< User ID
    const std::string      m_host;           ///< Host name
    const long             m_process_id;     ///< ID of user's process
    const std::string      m_app_name;       ///< application name
    const std::string      m_creation_ts;    ///< subscription time

  };


    /// convert single-value data from oks to rdb representation

  void oks2rdb(rdb::DataUnion& rd, const OksData * d);


    /// converts multi-value data from oks to rdb representation

  void oks2rdb(rdb::DataListUnion& rd, const OksData * d, const OksData::Type type);


    /// converts data from rdb to oks representation

  void rdb2oks(const rdb::DataUnion& rd, OksData * d, const OksObject * o, const OksAttribute * a);
  void rdb2oks (const rdb::DataListUnion& rd, OksData * d, const OksObject * o, const OksAttribute * a);

  void copy(rdb::RDBClass& c, const OksClass * oks_class);

    // if at is -1, then this is a first call; otherwise it is a recursive call

  void append_obj_to_list(const OksData * d, rdb::RDBObjectList& l, int at = -1);

  void object2values(const OksObject * o, rdb::RDBAttributeValueList& s_attr_values,
                     rdb::RDBAttributeValuesList& m_attr_values, rdb::RDBRelationshipValueList& rel_values);


  void class2names(const OksClass *c, rdb::RDBNameList& s_attr_names, rdb::RDBNameList& m_attr_names, rdb::RDBNameList& rel_names);


  void object2values(const OksObject * o, CORBA::ULong s_len, CORBA::ULong m_len, rdb::RDBAttributeCompactValueList& s_attr_values,
                     rdb::RDBAttributeCompactValuesList& m_attr_values, rdb::RDBRelationshipCompactValueList& rel_values);

  void object2values(const OksObject * o, CORBA::ULong s_len, CORBA::ULong m_len, rdb::RDBAttributeCompactValueList& s_attr_values,
                     rdb::RDBAttributeCompactValuesList& m_attr_values);

  void oks2rdb(const OksAttribute * oa, rdb::RDBAttribute& ra, bool is_direct);


  void oks2rdb(const OksRelationship * oks_rel, rdb::RDBRelationship& rdb_rel, bool is_direct);

  void fill_values(OksObject::FSet& refs, rdb::RDBClassValueList*& values);
  void fill_values(std::map<const OksClass *, std::list<const OksObject *> >& refs, rdb::RDBClassValueList*& values);
  void fill_values(const OksKernel * k, rdb::RDBClassValueList*& values);

  void fill_class_description(const OksClass* c, rdb::RDBClassDescription& d);
  void fill_schema(OksKernel* kernel, rdb::RDBClassDescriptionList*& l);
  void fill_inheritance_hierarchy (OksKernel& kernel, rdb::RDBInheritanceHierarchyList*& l);

  void get_object_values(OksKernel *kernel, const char *cid, const char *oid, rdb::RDBAttributeValueList_out s_attr_values, rdb::RDBAttributeValuesList_out m_attr_values, rdb::RDBRelationshipValueList_out rel_values);
  void cget_object_values(OksKernel *kernel, const char *cid, const char *oid, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values);
  void xget_object_values(OksKernel *kernel, const char *cid, const char *oid, rdb::RDBNameList_out s_attr_names, rdb::RDBNameList_out m_attr_names, rdb::RDBNameList_out r_names, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values);

  void get_files_of_all_objects(OksKernel * kernel, rdb::RDBFileObjectsList*& info);

  OksFile * get_data_file(const OksKernel * kernel, const std::string& db_name);

  void check_am_permission(daq::am::ServerInterrogator& am_server_int, const std::string& requestor, daq::am::RDBResource::ActionID action, const std::string& partition);

  std::string get_name_if_schema_reload_exception(const std::exception& ex);

  struct GetOksObjectException
  {
    const char *m_class_name;
    const char *m_object_id;
    const bool m_look_in_subclasses;

    GetOksObjectException() :
        m_class_name(nullptr), m_object_id(nullptr), m_look_in_subclasses(false)
    {
      ;
    }
    GetOksObjectException(const char *class_name) :
        m_class_name(class_name), m_object_id(nullptr), m_look_in_subclasses(false)
    {
      ;
    }
    GetOksObjectException(const char *class_name, const char *object_id, bool look_in_subclasses) :
        m_class_name(class_name), m_object_id(object_id), m_look_in_subclasses(look_in_subclasses)
    {
      ;
    }
  };


#define throw_rdb_NotFound(EX)                                                                                                            \
if (EX.m_object_id == nullptr)                                                                                                            \
  throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, EX.m_class_name)));                                            \
else                                                                                                                                      \
  throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ObjectNotFound(ERS_HERE, EX.m_object_id, EX.m_class_name, EX.m_look_in_subclasses)));


  OksObject * get_oks_object(const OksKernel * kernel, const char * cid, const char * oid, bool look_in_sclasses = false);

  rdb::RDBRepositoryVersionList* get_changes(OksKernel& kernel);
  rdb::RDBRepositoryVersionList* get_versions(OksKernel& kernel, CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until);



  struct ClassChanges {
    std::list<const char *> m_created;
    std::list<const char *> m_changed;
    std::list<const char *> m_deleted;
  };


    /// check if the object satisfies the subscription and adds to changes;
    /// the actions are: '+' - created; '~' - changed; '-' - deleted

  void check(config::map<ClassChanges *> & changes_list,
    Subscription * s,
    const OksClass * obj_class,
    const std::string& obj_id,
    const char action
  );

    /// used by above or directly, if subscription satifies any change

  void add(config::map<rdb::ClassChanges *> & changes_list,
    const std::string& class_name,
    const std::string& obj_name,
    const char action
  );

    /// convert and free temporary collection created for fast inserts into CORBA structures

  void cvt_and_free(config::map<ClassChanges *> & from, rdb::RDBClassChangesList& to);

  void oks2rdb_list(
    OksObject::List * objs,
    rdb::RDBObjectList * rlist,
    OksObject::FSet * refs,
    unsigned long recursion_depth,
    oks::ClassSet * names4refs
  );


  std::string mk_bad_query_text(const char * class_name, const char * query, const char * reason);

  std::string xget_objects_by_path_mk_text(const OksObject * o, const char* query, const char * reason);

  void clear_list(OksData::List * dlist, OksObject * obj, const OksRelationship * r);

  void get_databases( const OksKernel& kernel, rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles );

  void get_externally_modified_databases( OksKernel& kernel, rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles );

  void fill_referenced_by(const OksObject * o, const char * r, bool composite_only, rdb::RDBObjectList_out l);

  void sequence2classes(const OksKernel * kernel, const rdb::RDBNameList& in, oks::ClassSet& out);
  
  void fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, OksObject::FSet& refs);

  bool create_dir_for_backup_file(const std::string& file);

  std::string get_backup_spec(const std::string& file = "");


  class ChangesHolder {

    public:

      OksKernel * m_kernel;
      bool m_notification_started;

      std::list<OksObject *> m_created;
      std::set<OksObject *> m_modified;
      std::map<const OksClass *, std::set<std::string> > m_removed;

      ChangesHolder(OksKernel * kernel) : m_kernel(kernel), m_notification_started(false) { ; }

      void start_oks_notification();
      void stop_oks_notification();
      
      void clear();

      void reset(OksKernel * kernel);


    private:

      std::mutex m_mutex;

      static void create_notify(OksObject *, void *);  /// Callback function for object creation

      static void change_notify(OksObject *, void *);  /// Callback function for object modification

      static void delete_notify(OksObject *, void *);  /// Callback function for object deletion


  };

  class GuardOksNotification {

    private:

      ChangesHolder& p_changes_holder;
      bool p_restart;

    public:

      GuardOksNotification(ChangesHolder& holder, bool restart) : p_changes_holder(holder), p_restart(restart) {
        p_changes_holder.stop_oks_notification();
      }

      ~GuardOksNotification() {
        if(p_restart) {
          p_changes_holder.start_oks_notification();
        }
      }

  };

}

#endif
