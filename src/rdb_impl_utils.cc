#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <sstream>
#include <string>

#include <boost/date_time/posix_time/time_parsers.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/gregorian/parsers.hpp>

#include <ers/ers.h>
#include <ers2idl/ers2idl.h>

#include <daq_tokens/verify.h>
#include <daq_tokens/issues.h>

#include <oks/kernel.h>
#include <oks/class.h>
#include <oks/relationship.h>

#include "rdb/rdb.hh"
#include "rdb/errors.h"

#include "server_utils.h"

std::string
rdb::get_user(const rdb::ProcessInfo &info)
{
  static bool s_tokens_enabled = daq::tokens::enabled();

  const char * user_id = static_cast<const char *>(info.user_id);

  if (s_tokens_enabled)
    return daq::tokens::verify(user_id).get_subject();
  else
    return user_id;
}

std::string
rdb::get_user_safe(const rdb::ProcessInfo &info) noexcept
{
  try
    {
      return get_user(info);
    }
  catch (const ers::Issue& ex)
    {
      ers::warning(ex);
      return "(unknown)";
    }
}

std::string
rdb::to_str(const rdb::RDBNameList &names, bool use_brackets)
{
  std::string out;

  if(use_brackets)
    out.push_back('(');

  for (unsigned int i = 0; i < names.length(); i++)
    {
      if (i != 0)
        out.append(", ");

      out.push_back('\"');
      out.append(names[i]);
      out.push_back('\"');
    }

  if(use_brackets)
    out.push_back(')');

  return out;
}


  /**
   *   convert single-value data from oks to rdb representation
   */

void
rdb::oks2rdb(rdb::DataUnion& rd, const OksData * d)
{
  if(d == 0) {
    rd._default();
    throw std::runtime_error( "internal problem: OksData references (null) object" );
  }
  else switch (d->type) {

    case OksData::string_type:  rd.du_string(d->data.STRING->c_str());             break;
    case OksData::s32_int_type: rd.du_s32_int(d->data.S32_INT);                    break;
    case OksData::u32_int_type: rd.du_u32_int(d->data.U32_INT);                    break;
    case OksData::bool_type:    rd.du_bool(d->data.BOOL);                          break;
    case OksData::s16_int_type: rd.du_s16_int(d->data.S16_INT);                    break;
    case OksData::u16_int_type: rd.du_u16_int(d->data.U16_INT);                    break;
    case OksData::s64_int_type: rd.du_s64_int(d->data.S64_INT);                    break;
    case OksData::u64_int_type: rd.du_u64_int(d->data.U64_INT);                    break;
    case OksData::enum_type:    rd.du_string(d->data.ENUMERATION->c_str());        break;
    case OksData::class_type:   rd.du_string(d->data.CLASS->get_name().c_str());   break;
    case OksData::s8_int_type:  rd.du_s8_int(d->data.S8_INT);                      break;
    case OksData::u8_int_type:  rd.du_u8_int(d->data.U8_INT);                      break;
    case OksData::float_type:   rd.du_float(d->data.FLOAT);                        break;
    case OksData::double_type:  rd.du_double(d->data.DOUBLE);                      break;
    case OksData::time_type: {  std::string s = d->str(); rd.du_string(s.c_str()); break; }
    case OksData::date_type: {  std::string s = d->str(); rd.du_string(s.c_str()); break; }

    default:
      rd._default();
      std::ostringstream text;
      text << "internal problem: got unexpected data type for data " << *d;
      throw std::runtime_error( text.str().c_str() );
  }
}


  /**
   *   converts multi-value data from oks to rdb representation
   */

void
rdb::oks2rdb(rdb::DataListUnion& rd, const OksData * d, const OksData::Type type)
{
  if(d == 0) {
    rd._default();
    throw std::runtime_error( "internal problem: OksData references (null) object" );
  }

  unsigned long idx = 0;

  switch (type) {

    case OksData::string_type: {
      rdb::DataListUnion::_du_strings_seq dummu;
      rd.du_strings(dummu);
      rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.STRING->c_str(); ++i; }
      }
      break;
    }

    case OksData::s32_int_type: {
      rdb::DataListUnion::_du_s32_ints_seq dummu;
      rd.du_s32_ints(dummu);
      rdb::DataListUnion::_du_s32_ints_seq & seq = rd.du_s32_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.S32_INT; ++i; }
      }
      break;
    }

    case OksData::u32_int_type: {
      rdb::DataListUnion::_du_u32_ints_seq dummu;
      rd.du_u32_ints(dummu);
      rdb::DataListUnion::_du_u32_ints_seq & seq = rd.du_u32_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.U32_INT; ++i; }
      }
      break;
    }

    case OksData::bool_type: {
      rdb::DataListUnion::_du_bools_seq dummu;
      rd.du_bools(dummu);
      rdb::DataListUnion::_du_bools_seq & seq = rd.du_bools();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.BOOL; ++i; }
      }
      break;
    }

    case OksData::s16_int_type: {
      rdb::DataListUnion::_du_s16_ints_seq dummu;
      rd.du_s16_ints(dummu);
      rdb::DataListUnion::_du_s16_ints_seq & seq = rd.du_s16_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.S16_INT; ++i; }
      }
      break;
    }

    case OksData::u16_int_type: {
      rdb::DataListUnion::_du_u16_ints_seq dummu;
      rd.du_u16_ints(dummu);
      rdb::DataListUnion::_du_u16_ints_seq & seq = rd.du_u16_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.U16_INT; ++i; }
      }
      break;
    }

    case OksData::s64_int_type: {
      rdb::DataListUnion::_du_s64_ints_seq dummu;
      rd.du_s64_ints(dummu);
      rdb::DataListUnion::_du_s64_ints_seq & seq = rd.du_s64_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.S64_INT; ++i; }
      }
      break;
    }

    case OksData::u64_int_type: {
      rdb::DataListUnion::_du_u64_ints_seq dummu;
      rd.du_u64_ints(dummu);
      rdb::DataListUnion::_du_u64_ints_seq & seq = rd.du_u64_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.U64_INT; ++i; }
      }
      break;
    }

    case OksData::enum_type: {
      rdb::DataListUnion::_du_strings_seq dummu;
      rd.du_strings(dummu);
      rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.ENUMERATION->c_str(); ++i; }
      }
      break;
    }

    case OksData::class_type: {
      rdb::DataListUnion::_du_strings_seq dummu;
      rd.du_strings(dummu);
      rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.CLASS->get_name().c_str(); ++i; }
      }
      break;
    }

    case OksData::s8_int_type: {
      rdb::DataListUnion::_du_s8_ints_seq dummu;
      rd.du_s8_ints(dummu);
      rdb::DataListUnion::_du_s8_ints_seq & seq = rd.du_s8_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.S8_INT; ++i; }
      }
      break;
    }

    case OksData::u8_int_type: {
      rdb::DataListUnion::_du_u8_ints_seq dummu;
      rd.du_u8_ints(dummu);
      rdb::DataListUnion::_du_u8_ints_seq & seq = rd.du_u8_ints();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.U8_INT; ++i; }
      }
      break;
    }

    case OksData::float_type: {
      rdb::DataListUnion::_du_floats_seq dummu;
      rd.du_floats(dummu);
      rdb::DataListUnion::_du_floats_seq & seq = rd.du_floats();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.FLOAT; ++i; }
      }
      break;
    }
    
    case OksData::double_type: {
      rdb::DataListUnion::_du_doubles_seq dummu;
      rd.du_doubles(dummu);
      rdb::DataListUnion::_du_doubles_seq & seq = rd.du_doubles();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { seq[idx++] = (*i)->data.DOUBLE; ++i; }
      }
      break;
    }

    case OksData::time_type: {
      rdb::DataListUnion::_du_strings_seq dummu;
      rd.du_strings(dummu);
      rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { std::string s = (*i)->str(); seq[idx++] = s.c_str(); ++i; }
      }
      break;
    }

    case OksData::date_type: {
      rdb::DataListUnion::_du_strings_seq dummu;
      rd.du_strings(dummu);
      rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();
      if(OksData::List * l = d->data.LIST) {
        OksData::List::const_iterator i = l->begin();
        seq.length(l->size());
        while(i != l->end()) { std::string s = (*i)->str(); seq[idx++] = s.c_str(); ++i; }
      }
      break;
    }

    default:
      rd._default();
      std::ostringstream text;
      text << "internal problem: got unexpected data type for data " << *d;
      throw std::runtime_error( text.str().c_str() );
  }
}


  /**
   *   rdb2oks() converts data from rdb to oks representation
   *
   *   NOTE: the 'd' is out parameter. However it's type is
   *         right since it is filled by the GetAttributeValue()
   *         call, which is invoked before given rdb2oks() call.
   */

void
rdb::rdb2oks(const rdb::DataUnion& rd, OksData * d, const OksObject * o, const OksAttribute * a)
{
  OksData::Type type = d->type;

  d->Clear();

  switch (type) {

    case OksData::string_type:  d->Set(rd.du_string()); break;
    case OksData::s32_int_type: d->Set((int32_t)rd.du_s32_int()); break;
    case OksData::u32_int_type: d->Set((uint32_t)rd.du_u32_int()); break;
    case OksData::bool_type:    d->Set(rd.du_bool()); break;
    case OksData::s16_int_type: d->Set(rd.du_s16_int()); break;
    case OksData::u16_int_type: d->Set(rd.du_u16_int()); break;
    case OksData::s64_int_type: d->Set(rd.du_s64_int()); break;
    case OksData::u64_int_type: d->Set(rd.du_u64_int()); break;
    case OksData::s8_int_type:  d->Set(rd.du_s8_int()); break;
    case OksData::u8_int_type:  d->Set(rd.du_u8_int()); break;
    case OksData::float_type:   d->Set(rd.du_float()); break;
    case OksData::double_type:  d->Set(rd.du_double()); break;

    case OksData::enum_type:
      try {
        d->data.ENUMERATION = a->get_enum_value(rd.du_string(), strlen(rd.du_string()));
        d->type = OksData::enum_type;
        break;
      }
      catch(std::exception& ex) {
        std::ostringstream text;
        text << "failed to set \"enum\" value of attribute \'" << a->get_name() << "\' of " << o << " from \'" << rd.du_string() << "\':\n" << ex.what();
        throw std::runtime_error( text.str().c_str() );
      }

    case OksData::time_type:
      try {
        d->Set(boost::posix_time::time_from_string(rd.du_string()));
      }
      catch(std::exception& ex) {
        std::ostringstream text;
        text << "failed to set \"time\" value of attribute \'" << a->get_name() << "\' of " << o << " from \'" << rd.du_string() << "\':\n" << ex.what();
        throw std::runtime_error( text.str().c_str() );
      }
      break;

    case OksData::date_type:
      try {
        d->Set(boost::gregorian::from_string(rd.du_string())); break;
      }
      catch(std::exception& ex) {
        std::ostringstream text;
        text << "failed to set \"date\" value of attribute \'" << a->get_name() << "\' of " << o << " from \'" << rd.du_string() << "\':\n" << ex.what();
        throw std::runtime_error( text.str().c_str() );
      }
      break;

    case OksData::class_type: {
      try {
        d->type = OksData::class_type;
        d->data.CLASS = 0;
        d->SetValue(rd.du_string(), a);
      }
      catch(oks::exception & ex) {
        std::ostringstream text;
        text << "failed to set \"class\" value of attribute \'" << a->get_name() << "\' of " << o << ":\n" << ex.what();
        throw std::runtime_error( text.str().c_str() );
      }
      break;
    }

    default: {
      std::ostringstream text;
      text << "internal problem: got unexpected data type " << (int)type;
      throw std::runtime_error( text.str().c_str() );
    }
  }
}


void
rdb::rdb2oks (const rdb::DataListUnion& rd, OksData * d, const OksObject * o, const OksAttribute * a)
{
  switch (rd._d()) {

    case rdb::pk_s8_int: {
      const rdb::DataListUnion::_du_s8_ints_seq & seq = rd.du_s8_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }

    case rdb::pk_u8_int: {
      const rdb::DataListUnion::_du_u8_ints_seq & seq = rd.du_u8_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }

    case rdb::pk_s16_int: {
      const rdb::DataListUnion::_du_s16_ints_seq & seq = rd.du_s16_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_u16_int: {
      const rdb::DataListUnion::_du_u16_ints_seq & seq = rd.du_u16_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_s32_int: {
      const rdb::DataListUnion::_du_s32_ints_seq & seq = rd.du_s32_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData((int32_t)seq[i])); }
      break;
    }
    
    case rdb::pk_u32_int: {
      const rdb::DataListUnion::_du_u32_ints_seq & seq = rd.du_u32_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData((uint32_t)seq[i])); }
      break;
    }
    
    case rdb::pk_s64_int: {
      const rdb::DataListUnion::_du_s64_ints_seq & seq = rd.du_s64_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_u64_int: {
      const rdb::DataListUnion::_du_u64_ints_seq & seq = rd.du_u64_ints();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_float: {
      const rdb::DataListUnion::_du_floats_seq & seq = rd.du_floats();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_double: {
      const rdb::DataListUnion::_du_doubles_seq & seq = rd.du_doubles();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_boolean: {
      const rdb::DataListUnion::_du_bools_seq & seq = rd.du_bools();
      for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
      break;
    }
    
    case rdb::pk_string: {
      const rdb::DataListUnion::_du_strings_seq & seq = rd.du_strings();

      OksData::Type oks_data_type = a->get_data_type();

      switch (oks_data_type) {

        case OksData::string_type:
	  for(size_t i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
          break;

        case OksData::enum_type:
	  for(size_t i = 0; i < seq.length(); ++i) {
	    OksData * d2 = new OksData();
            try {
	      d2->data.ENUMERATION = a->get_enum_value(seq[i], strlen(seq[i]));
	      d2->type = OksData::enum_type;
	      d->data.LIST->push_back(d2);
            }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"enum\" value of attribute \'" << a->get_name() << "\' of " << o << " for token " << i << " \'" << seq[i] << "\':\n" << ex.what();
              throw std::runtime_error( text.str().c_str() );
            }
	  }
          break;

        case OksData::class_type:
          for(size_t i = 0; i < seq.length(); ++i) {
            OksData * d2 = new OksData();
            try {
              d2->data.CLASS = 0;
              d2->type = OksData::class_type;
              d2->SetValue(seq[i], a);
            }
            catch(oks::exception & ex) {
              std::ostringstream text;
              text << "failed to set \"class\" value of attribute \'" << a->get_name() << "\' of " << o << ":\n" << ex.what();
              throw std::runtime_error( text.str().c_str() );
            }
            d->data.LIST->push_back(d2);
          }
          break;

        case OksData::date_type:
	  for(size_t i = 0; i < seq.length(); ++i) {
            try {
              d->data.LIST->push_back(new OksData(boost::gregorian::from_string(static_cast<const char *>(seq[i]))));
            }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"date\" value of attribute \'" << a->get_name() << "\' of " << o << " from \'" << seq[i] << "\':\n" << ex.what();
              throw std::runtime_error( text.str().c_str() );
            }
          }
          break;

        case OksData::time_type:
	  for(size_t i = 0; i < seq.length(); ++i) {
            try {
              d->data.LIST->push_back(new OksData(boost::posix_time::time_from_string(static_cast<const char *>(seq[i]))));
            }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"time\" value of attribute \'" << a->get_name() << "\' of " << o << " from \'" << seq[i] << "\':\n" << ex.what();
              throw std::runtime_error( text.str().c_str() );
            }
          }
          break;

        default: {
	  std::ostringstream text;
          text << "internal problem: got unexpected data type " << (int)oks_data_type << " instead of string for attribute \'" << a->get_name() << "\' of " << o;
          throw std::runtime_error( text.str().c_str() );
	}

      }
      break;
    }

    default: {
      std::ostringstream text;
      text << "internal problem: got unexpected data type " << (int)rd._d();
      throw std::runtime_error( text.str().c_str() );
    }
  }
}


void
rdb::copy(rdb::RDBClass& c, const OksClass * oks_class)
{
  c.name                     = rdb::string_dup(oks_class->get_name());
  c.isAbstract               = oks_class->get_is_abstract();
  c.description              = rdb::string_dup(oks_class->get_description());
  c.objectNumber             = oks_class->number_of_objects();
  c.attributeNumber          = oks_class->number_of_all_attributes();
  c.relationshipNumber       = oks_class->number_of_all_relationships();
  c.directAttributeNumber    = oks_class->number_of_direct_attributes();
  c.directRelationshipNumber = oks_class->number_of_direct_relationships();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

  // if at is -1, then this is a first call
  // otherwise it is a recursive call

void
rdb::append_obj_to_list(const OksData * d, rdb::RDBObjectList& l, int at)
{
  const char * cid = nullptr;
  const char * oid = nullptr;

  if(d->type == OksData::object_type) {
    if(OksObject * o = d->data.OBJECT) {
      cid = o->GetClass()->get_name().c_str();
      oid = o->GetId().c_str();
    }
  }
/*  else if(d->type == OksData::uid_type) {
    if(d->data.UID.class_id && d->data.UID.object_id) {
      cid = d->data.UID.class_id->get_name().c_str();
      oid = d->data.UID.object_id->c_str();
    }
  }
  else if(d->type == OksData::uid2_type) {
    if(d->data.UID2.class_id && d->data.UID2.object_id) {
      cid = d->data.UID2.class_id->c_str();
      oid = d->data.UID2.object_id->c_str();
    }
  }*/
  else if(d->type == OksData::list_type && d->data.LIST != 0) {
    int len = 0;
    for(const auto& i : *d->data.LIST) {
      if(i->type == OksData::object_type && i->data.OBJECT) len++;
    }
    l.length(len);
    unsigned int idx = 0;
    for(const auto& i : *d->data.LIST) {
      if(i->type == OksData::object_type && i->data.OBJECT) rdb::append_obj_to_list(i, l, idx++);
    }
  }
  else {
    return;
  }

  if(cid && oid && *cid != 0 && *oid != 0) {
    if(at == -1) { l.length(1); at = 0; }
    l[at].classid = CORBA::string_dup(cid);
    l[at].name	  = CORBA::string_dup(oid);
  }
}


void
rdb::object2values(
  const OksObject * o,
  rdb::RDBAttributeValueList& s_attr_values,
  rdb::RDBAttributeValuesList& m_attr_values,
  rdb::RDBRelationshipValueList& rel_values)
{
  const OksClass * c = o->GetClass();


    // get numbers of single-value and multi-value attributes
    // and allocate sequences using such numbers
  
  {
    size_t s_len = 0;
    size_t m_len = 0;
    
    for(std::list<OksAttribute *>::const_iterator i = c->all_attributes()->begin(); i != c->all_attributes()->end(); ++i) {
      if((*i)->get_is_multi_values()) m_len++;
      else s_len++;
    }

    s_attr_values.length(s_len);
    m_attr_values.length(m_len);
  }

    // allocate sequence to store object values

  rel_values.length(c->number_of_all_relationships());


  OksDataInfo odi(0, (const OksAttribute *)0);

    // fill values of attributes

  if(c->number_of_all_attributes()) {

    unsigned long s_idx = 0;
    unsigned long m_idx = 0;

    std::list<OksAttribute *>::const_iterator i = c->all_attributes()->begin();
    while(i != c->all_attributes()->end()) {
      OksData * d(o->GetAttributeValue(&odi));
      odi.offset++;

      try {
        if(d->type != OksData::list_type) {
          rdb::RDBAttributeValue& value = s_attr_values[s_idx++];
          value.name = rdb::string_dup((*i)->get_name());
	  oks2rdb(value.data, d);
        }
        else {
          rdb::RDBAttributeValues& value = m_attr_values[m_idx++];
          value.name = rdb::string_dup((*i)->get_name());
	  oks2rdb(value.data, d, (*i)->get_data_type());
        }
      }
      catch (std::exception& ex) {
        std::ostringstream text;
        text << "caught problem reading value(s) of attribute \'" << (*i)->get_name() << "\' of object " << o << ": " << ex.what();
        throw std::runtime_error(text.str().c_str());
      }

      ++i;
    }
  }

    // fill values of relationships

  if(c->number_of_all_relationships()) {
    unsigned long idx = 0;
    std::list<OksRelationship *>::const_iterator i = c->all_relationships()->begin();
    while(i != c->all_relationships()->end()) {
      rdb::RDBRelationshipValue& value = rel_values[idx++];

      value.name = rdb::string_dup((*i)->get_name());

      OksData * d(o->GetRelationshipValue(&odi));
      odi.offset++;

      rdb::append_obj_to_list(d, value.data);
      value.isMultiValue = (d->type == OksData::list_type);

      ++i;
    }
  }
}


void
rdb::class2names(
  const OksClass *c,
  rdb::RDBNameList& s_attr_names,
  rdb::RDBNameList& m_attr_names,
  rdb::RDBNameList& rel_names)
{
    // fill attribute names

  if(const std::list<OksAttribute *> * attrs = c->all_attributes()) {
    unsigned long sv_idx = 0, mv_idx = 0;
    std::list<OksAttribute *>::const_iterator ai = attrs->begin();
    for(; ai != attrs->end(); ++ai) {
      if( (*ai)->get_is_multi_values() ) mv_idx++; else sv_idx++;
    }
    s_attr_names.length(sv_idx);
    m_attr_names.length(mv_idx);

    sv_idx = mv_idx = 0;

    for(ai = attrs->begin(); ai != attrs->end(); ++ai) {
      if( (*ai)->get_is_multi_values() ) {
        m_attr_names[mv_idx++] = rdb::string_dup((*ai)->get_name());
      }
      else {
        s_attr_names[sv_idx++] = rdb::string_dup((*ai)->get_name());
      }
    }
  }
  else {
    s_attr_names.length(0);
    m_attr_names.length(0);
  }


    // fill relationship names

  if(const std::list<OksRelationship *> * rels = c->all_relationships()) {
    rel_names.length(c->number_of_all_relationships());
    unsigned long r_idx = 0;
    for(std::list<OksRelationship*>::const_iterator ri = rels->begin(); ri != rels->end(); ++ri) {
      rel_names[r_idx++] = rdb::string_dup((*ri)->get_name());
    }
  }
  else {
    rel_names.length(0);
  }
}

void
rdb::object2values(
  const OksObject * o,
  CORBA::ULong s_len,
  CORBA::ULong m_len,
  rdb::RDBAttributeCompactValueList& s_attr_values,
  rdb::RDBAttributeCompactValuesList& m_attr_values
)
{
  const OksClass * c = o->GetClass();

    // access object's data by offset

  OksDataInfo odi(0, (const OksAttribute *)0);


    // fill compact attributes values

  s_attr_values.length(s_len);
  m_attr_values.length(m_len);

  if(const std::list<OksAttribute *> * attrs = c->all_attributes()) {
    unsigned long sv_idx = 0, mv_idx = 0;
    for(std::list<OksAttribute *>::const_iterator i = attrs->begin(); i != attrs->end(); ++i) {
      OksData * d(o->GetAttributeValue(&odi));
      odi.offset++;
      try {
        if(d->type != OksData::list_type) { oks2rdb(s_attr_values[sv_idx++], d); }
        else { oks2rdb(m_attr_values[mv_idx++], d, (*i)->get_data_type()); }
      }
      catch (std::exception& ex) {
        std::ostringstream text;
        text << "caught problem reading value(s) of attribute \'" << (*i)->get_name() << "\' of object " << o << ": " << ex.what();
        throw std::runtime_error(text.str().c_str());
      }
    }
  }
}

void
rdb::object2values(
  const OksObject * o,
  CORBA::ULong s_len,
  CORBA::ULong m_len,
  rdb::RDBAttributeCompactValueList& s_attr_values,
  rdb::RDBAttributeCompactValuesList& m_attr_values,
  rdb::RDBRelationshipCompactValueList& rel_values)
{
  object2values(o, s_len, m_len, s_attr_values, m_attr_values);

  const OksClass * c = o->GetClass();

    // access object's data by offset

  OksDataInfo odi(c->number_of_all_attributes(), (const OksRelationship *)0);


    // fill compact relationship values

  rel_values.length(c->number_of_all_relationships());
  if(const std::list<OksRelationship *> * rels = c->all_relationships()) {
    unsigned long r_idx = 0;
    for(std::list<OksRelationship *>::const_iterator ri = rels->begin(); ri != rels->end(); ++ri) {
      OksData * d(o->GetRelationshipValue(&odi));
      odi.offset++;
      rdb::append_obj_to_list(d, rel_values[r_idx++]);
    }
  }
}


void
rdb::oks2rdb(const OksAttribute * oa, rdb::RDBAttribute& ra, bool is_direct)
{
  ra.name = rdb::string_dup(oa->get_name());
  ra.type = rdb::string_dup(oa->get_type());
  ra.range = rdb::string_dup(oa->get_range());
  ra.initValue = rdb::string_dup(oa->get_init_value());
  ra.intFormat = (
    oa->is_integer() == false ? rdb::int_format_na :
      (
        oa->get_format() == OksAttribute::Dec ? rdb::int_format_dec :
        oa->get_format() == OksAttribute::Hex ? rdb::int_format_hex :
        rdb::int_format_oct
      )
  );
  ra.isMultiValue = oa->get_is_multi_values();
  ra.isNotNull = oa->get_is_no_null();
  ra.description = rdb::string_dup(oa->get_description());
  ra.isDirect = is_direct;
}


void
rdb::oks2rdb(const OksRelationship * oks_rel, rdb::RDBRelationship& rdb_rel, bool is_direct)
{
  rdb_rel.name = rdb::string_dup(oks_rel->get_name());
  rdb_rel.classid = rdb::string_dup(oks_rel->get_type());
  rdb_rel.description = rdb::string_dup(oks_rel->get_description());
  rdb_rel.isMultiValue = (oks_rel->get_high_cardinality_constraint() == OksRelationship::Many);
  rdb_rel.isNotNull = (oks_rel->get_low_cardinality_constraint() == OksRelationship::One);
  rdb_rel.isAggregation = oks_rel->get_is_composite();
  rdb_rel.isExclusive = oks_rel->get_is_exclusive();
  rdb_rel.isDependent = oks_rel->get_is_dependent();
  rdb_rel.isOrdered = oks_rel->get_is_ordered();
  rdb_rel.isDirect = is_direct;
}


void
rdb::fill_values(OksObject::FSet& refs, rdb::RDBClassValueList*& values)
{
  std::map<const OksClass *, std::list<const OksObject *> > v;

  for (const auto& i : refs)
    v[i->GetClass()].push_back(i);

  fill_values(v, values);
}

void
rdb::fill_values(const OksKernel * k, rdb::RDBClassValueList*& values)
{
  std::map<const OksClass *, std::list<const OksObject *> > v;

  for (const auto& x : k->classes())
    {
      std::list<const OksObject *>& l = v[x.second];

      for (const auto& i : *x.second->objects())
        {
          l.push_back(i.second);
        }
    }

  fill_values(v, values);
}

void
rdb::fill_values(std::map<const OksClass *, std::list<const OksObject *> >& v, rdb::RDBClassValueList*& values)
  {
  values = new rdb::RDBClassValueList();
  values->length(v.size());

  unsigned long idx = 0;

  for(std::map<const OksClass *, std::list<const OksObject *> >::const_iterator j = v.begin(); j != v.end(); ++j) {
    const OksClass * c = j->first;
    rdb::RDBClassValue& cv = (*values)[idx++];

      // fill class name
    cv.name = rdb::string_dup(c->get_name());

      // fill names of attributes and relationships
    rdb::class2names(c, cv.sv_attributes, cv.mv_attributes, cv.relationships);

      // fill compact object values
    unsigned long o_idx = 0;
    cv.objects.length(j->second.size());

    for(std::list<const OksObject *>::const_iterator oi = j->second.begin(); oi != j->second.end(); ++oi) {
      rdb::RDBObjectCompactValue& ov = cv.objects[o_idx++];

        // fill object ID
      ov.name = rdb::string_dup((*oi)->GetId());

        // fill object values
      rdb::object2values(*oi, cv.sv_attributes.length(), cv.mv_attributes.length(), ov.av, ov.avs, ov.rv);
    }
  }
}


  // add object to the changes list; new RDBClassChanges object is created if required

void
rdb::add(config::map<rdb::ClassChanges *> & changes_list,
    const std::string& class_name,
    const std::string& obj_name,
    const char action
)
{
    // points to changes for given class and create new if needed

  rdb::ClassChanges*& class_changes(changes_list[class_name]);

  if(class_changes == 0) {
    class_changes = new rdb::ClassChanges();
  }

    // find collection with required changes

  std::list<const char *>& clist = (
    action == '+' ? class_changes->m_created :
    action == '-' ? class_changes->m_deleted :
    class_changes->m_changed
  );

  clist.push_back(obj_name.c_str());
}

void
rdb::cvt_and_free(config::map<ClassChanges *> & from, rdb::RDBClassChangesList& to)
{
  to.length(from.size());

  unsigned int idx(0);
  for(config::map<ClassChanges *>::iterator i = from.begin(); i != from.end(); ++i) {
    rdb::RDBClassChanges& class_changes(to[idx++]);

    class_changes.name = rdb::string_dup(i->first);

    std::list<const char *> * flists[3] = {&i->second->m_created, &i->second->m_changed, &i->second->m_deleted};
    rdb::RDBNameList * tlists[3] = {&class_changes.created, &class_changes.modified, &class_changes.removed};

    for(unsigned int x = 0; x < 3; ++x) {
      rdb::RDBNameList& t(*tlists[x]);

      t.length(flists[x]->size());
      unsigned int idx2(0);
      for(std::list<const char *>::iterator j = flists[x]->begin(); j != flists[x]->end(); ++j) {
        t[idx2++] = CORBA::string_dup(*j);
      }
    }

    delete i->second;
  }
}



  /**
   *  The function checks if the object satisfies the subscription and adds to changes.
   *  The actions can be:
   *   '+' - created
   *   '~' - changed
   *   '-' - deleted
   */

void
rdb::check(config::map<ClassChanges *> & changes_list,
           rdb::Subscription * s,
           const OksClass * obj_class,
           const std::string& obj_id,
           const char action
) {

  bool any = (s->m_class_names.empty() && s->m_objects_names.empty());


    // check subscription on objects
    // omi iterator in non-null, if for an object of class 'obj_class' there is explicit subscription

  rdb::Subscription::ObjectsMap::iterator omi = s->m_objects_names.end();

  if(action != '+') {
    omi = s->m_objects_names.find(obj_class->get_name());
    if(omi != s->m_objects_names.end()) {
      std::set<std::string>::iterator i = omi->second.find(obj_id);
      if(i != omi->second.end()) {
        add(changes_list, obj_class->get_name(), obj_id, action);
      }
    }
  }


    // process the class

  if(action == '+' || omi == s->m_objects_names.end()) {
    if(any || s->m_class_names.find(obj_class->get_name()) != s->m_class_names.end()) {
      add(changes_list, obj_class->get_name(), obj_id, action);
    }
  }


    // process subclasses if required

  if(s->m_look_in_subclasses) {
    if(const OksClass::FList * slist = obj_class->all_super_classes()) {
      for(OksClass::FList::const_iterator j = slist->begin(); j != slist->end(); ++j) {
	omi = (action != '+' ? s->m_objects_names.find((*j)->get_name()) : s->m_objects_names.end());

          // 1. add objects if criteria is 'any'
	  // 2. add object of class which has subscription and has no explicit object subscription
	  // 3. add explicitly subscribed object

        if(
	  any ||
	  (
	    omi == s->m_objects_names.end() &&
	    s->m_class_names.find((*j)->get_name()) != s->m_class_names.end()
	  ) ||
	  (
	    omi != s->m_objects_names.end() &&
	    omi->second.find(obj_id) != omi->second.end()
	  )
	) {
            add(changes_list, (*j)->get_name(), obj_id, action);
        }
      }
    }
  }
}


void
rdb::oks2rdb_list(OksObject::List * objs, rdb::RDBObjectList * rlist, OksObject::FSet * refs, unsigned long recursion_depth, oks::ClassSet * names4refs)
{
  unsigned long idx = 0;
  rlist->length(objs->size());
  for(OksObject::List::iterator i = objs->begin(); i != objs->end(); ++i) {
    (*rlist)[idx].classid = rdb::string_dup((*i)->GetClass()->get_name());
    (*rlist)[idx++].name  = rdb::string_dup((*i)->GetId());

    if(refs) {
      (*i)->references(*refs, recursion_depth, true, names4refs);
    }
  }
  delete objs;
}

std::string
rdb::mk_bad_query_text(const char * class_name, const char * query, const char * reason)
{
  std::string s = std::string("cannot execute query \'") + query + "\' on class \'" + class_name + "\': " + reason;
  return s;
}

std::string
rdb::xget_objects_by_path_mk_text(const OksObject * o, const char* query, const char * reason)
{
  std::ostringstream text;
  text << "cannot get path \'" << query << "\' from object " << o << " since " << reason;
  return text.str();
}

void
rdb::clear_list(OksData::List * dlist, OksObject * obj, const OksRelationship * r)
{
  while(!dlist->empty()) {
    OksData * d = dlist->front();
    dlist->pop_front();
    if(obj && d->type == OksData::object_type && d->data.OBJECT) {
      d->data.OBJECT->remove_RCR(obj, r);
    }
    delete d;
  }
}

std::ostream&
operator<<(std::ostream& s, const rdb::RDBObject& obj)
{
  s << '\'' << static_cast<const char *>(obj.name) << '@' << static_cast<const char *>(obj.classid) << '\'';
  return s;
}

rdb::Subscription::Subscription(const rdb::ProcessInfo& info,
                                      const rdb::RDBNameList& lc,
                                      bool look_in_sc,
                                      const rdb::RDBObjectList& lo,
				      rdb::callback_ptr cb,
				      int64_t p) :
 m_look_in_subclasses (look_in_sc),
 m_parameter          (p),
 m_cb                 (rdb::callback::_duplicate(cb)),
 m_user_id            ("unknown"),
 m_host               (info.host),
 m_process_id         (info.process_id),
 m_app_name           (info.app_name),
 m_creation_ts        (boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time()))
{
  try
    {
      const_cast<std::string&>(m_user_id) = rdb::get_user(info);
    }
  catch(const daq::tokens::Issue& ex)
    {
      const std::string user_id(strlen(info.user_id.in()) < 40 ? info.user_id.in() : m_user_id);
      ers::log(ers::Message(ERS_HERE, std::string("subscription failed for \"") + m_app_name + "\" application started on \"" + m_host + "\" with " + std::to_string(info.process_id) + " by \"" + user_id + '\"', ex));
      throw rdb::CannotProceed(daq::ers2idl_issue(ex));
    }

  unsigned long idx;

  for(idx = 0; idx < lc.length(); idx++) {
    m_class_names.insert(lc[idx].in());
  }

  for(idx = 0; idx < lo.length(); idx++) {
    m_objects_names[lo[idx].classid.in()].insert(lo[idx].name.in());
  }
}


rdb::Subscription::Subscription(const std::string& user_id,
                                      const std::string& host,
                                      long process_id,
                                      const std::string& app_name,
                                      const std::string& creation_ts,
                                      const std::vector<std::string>& lc,
                                      bool look_in_sc,
				      const ObjectsMap& lo,
				      rdb::callback_var& cb,
				      int64_t p) :
 m_look_in_subclasses (look_in_sc),
 m_parameter          (p),
 m_cb                 (cb),
 m_user_id            (user_id),
 m_host               (host),
 m_process_id         (process_id),
 m_app_name           (app_name),
 m_creation_ts        (creation_ts)
{
  for(std::vector<std::string>::const_iterator i = lc.begin(); i != lc.end(); ++i) {
    m_class_names.insert(*i);
  }

  for(ObjectsMap::const_iterator j = lo.begin(); j != lo.end(); ++j) {
    for(std::set<std::string>::const_iterator l = j->second.begin(); l != j->second.end(); ++l) {
      m_objects_names[j->first].insert(*l);
    }
  }
}


void
rdb::get_databases(const OksKernel& kernel, rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles)
{
  schemafiles = new rdb::RDBNameList();
  datafiles = new rdb::RDBNameList();

  schemafiles->length(kernel.schema_files().size());
  datafiles->length(kernel.data_files().size());

  // fill list of schema files
  OksFile::Map::const_iterator i = kernel.schema_files().begin();
  for (unsigned int idx = 0; i != kernel.schema_files().end(); ++i)
    (*schemafiles)[idx++] = rdb::string_dup(i->first);

  // fill list of data files
  i = kernel.data_files().begin();
  for (unsigned int idx = 0; i != kernel.data_files().end(); ++i)
    (*datafiles)[idx++] = rdb::string_dup(i->first);
}


void
rdb::get_externally_modified_databases(OksKernel& kernel, rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles)
{
  std::set<OksFile *> mfs, rfs;

  kernel.get_modified_files(mfs, rfs, "origin/master");

  // process updated files
  unsigned int idx = 0;
  externally_updated_datafiles = new rdb::RDBNameList();
  externally_updated_datafiles->length(mfs.size());
  for (const auto& i : mfs)
    (*externally_updated_datafiles)[idx++] = rdb::string_dup(i->get_well_formed_name());

  // process removed files
  idx = 0;
  externally_removed_datafiles = new rdb::RDBNameList();
  externally_removed_datafiles->length(rfs.size());
  for (const auto& i : rfs)
    (*externally_removed_datafiles)[idx++] = rdb::string_dup(i->get_well_formed_name());
}



  /**
   *  Is called when new object is created.
   *  Adds object to the list of created objects.
   */

void
rdb::ChangesHolder::create_notify(OksObject * o, void * p)
{
  rdb::ChangesHolder * h = reinterpret_cast<rdb::ChangesHolder *>(p);
  std::lock_guard lock(h->m_mutex);
  h->m_created.push_back(o);
}


  /**
   *  Is called when object is modified.
   *  Adds object to the list of changed objects.
   */

void
rdb::ChangesHolder::change_notify(OksObject * o, void * p)
{
  rdb::ChangesHolder * h = reinterpret_cast<rdb::ChangesHolder *>(p);
  std::lock_guard lock(h->m_mutex);
  h->m_modified.insert(o);
}


  /**
   *  Is called when object is deleted.
   *  Adds object's class-name and identity to the map of deleted objects.
   */

void
rdb::ChangesHolder::delete_notify(OksObject * o, void * p)
{
  rdb::ChangesHolder * h = reinterpret_cast<rdb::ChangesHolder *>(p);
  std::lock_guard lock(h->m_mutex);
  h->m_removed[o->GetClass()].insert(o->GetId());
}


void
rdb::ChangesHolder::start_oks_notification()
{
  ERS_DEBUG( 2, "start oks_notification()" );
  m_kernel->subscribe_create_object(create_notify, reinterpret_cast<void *>(this));
  m_kernel->subscribe_change_object(change_notify, reinterpret_cast<void *>(this));
  m_kernel->subscribe_delete_object(delete_notify, reinterpret_cast<void *>(this));
  m_notification_started = true;
}


void
rdb::ChangesHolder::stop_oks_notification()
{
  ERS_DEBUG( 2, "stop oks_notification()" );
  m_kernel->subscribe_create_object(0, 0);
  m_kernel->subscribe_change_object(0, 0);
  m_kernel->subscribe_delete_object(0, 0);
  m_notification_started = false;
}


void
rdb::ChangesHolder::clear()
{
  m_created.clear();
  m_modified.clear();
  m_removed.clear();
}


void
rdb::ChangesHolder::reset(OksKernel * kernel)
{
  ERS_DEBUG( 2, "reset kernel" );

  std::lock_guard lock(m_mutex);

  clear();

  bool started(m_notification_started);

  if(started) {
    stop_oks_notification();
  }

  m_kernel = kernel;

  if(started) {
    start_oks_notification();
  }
}


void
rdb::fill_inheritance_hierarchy (OksKernel& kernel, rdb::RDBInheritanceHierarchyList*& l)
{
  l = new rdb::RDBInheritanceHierarchyList();

  unsigned long idx = 0;

  std::shared_lock rlock(kernel.get_mutex());

  l->length(kernel.classes().size());

  for(OksClass::Map::const_iterator i = kernel.classes().begin(); i != kernel.classes().end(); ++i, ++idx) {
    if(const OksClass::FList * scl = i->second->all_super_classes()) {
      if(!scl->empty()) {
        (*l)[idx].name = CORBA::string_dup(i->first);
        (*l)[idx].all_parents.length(scl->size());
        unsigned long idx2 = 0;
        for(OksClass::FList::const_iterator j = scl->begin(); j != scl->end(); ++j) {
          (*l)[idx].all_parents[idx2++] = rdb::string_dup((*j)->get_name());
        }
      }
      else {
        (*l)[idx].name = CORBA::string_dup(i->first);
        (*l)[idx].all_parents.length(0);
      }
    }
  }
}

void
rdb::fill_class_description(const OksClass* c, rdb::RDBClassDescription& cd)
{
    // set name, isAbstract and description
  cd.name = rdb::string_dup(c->get_name());
  cd.isAbstract = c->get_is_abstract();
  cd.description = rdb::string_dup(c->get_description());

  // set direct superclasses
  if (auto list = c->direct_super_classes())
    {
      cd.direct_superclasses.length(list->size());
      unsigned long idx = 0;
      for (const auto& x : *list)
        cd.direct_superclasses[idx++] = rdb::string_dup(x);
    }
  else
    {
      cd.direct_superclasses.length(0);
    }

  // set all superclasses
  if (auto list = c->all_super_classes())
    {
      cd.all_superclasses.length(list->size());
      unsigned long idx = 0;
      for (const auto& x : *list)
        cd.all_superclasses[idx++] = rdb::string_dup(x->get_name());
    }
  else
    {
      cd.all_superclasses.length(0);
    }

  // set all subclasses
  if (auto list = c->all_sub_classes())
    {
      cd.all_subclasses.length(list->size());
      unsigned long idx = 0, len = 0;
      for (const auto& x : *list)
        {
          cd.all_subclasses[idx++] = rdb::string_dup(x->get_name());
          if (x->has_direct_super_class(c->get_name()) == true)
            len++;
        }

      // set direct subclasses
      cd.direct_subclasses.length(len);
      idx = 0;
      for (const auto& x : *list)
        if (x->has_direct_super_class(c->get_name()) == true)
          cd.direct_subclasses[idx++] = rdb::string_dup(x->get_name());
    }
  else
    {
      cd.all_superclasses.length(0);
      cd.direct_subclasses.length(0);
    }

  if (const auto list = c->all_attributes())
    {
      unsigned long idx = 0;
      cd.attributes.length(list->size());
      for (const auto& x : *list)
        rdb::oks2rdb(x, cd.attributes[idx++], (c->source_class(x) == c));
    }
  else
    {
      cd.attributes.length(0);
    }

  if (const auto list = c->all_relationships())
    {
      unsigned long idx = 0;
      cd.relationships.length(list->size());
      for (const auto& x : *list)
        rdb::oks2rdb(x, cd.relationships[idx++], (c->source_class(x) == c));
    }
  else
    {
      cd.relationships.length(0);
    }
}

void
rdb::fill_schema(OksKernel* kernel, rdb::RDBClassDescriptionList*& l)
{
  l = new rdb::RDBClassDescriptionList();

  l->length(kernel->classes().size());

  unsigned long idx = 0;
  for (const auto&c : kernel->classes())
    rdb::fill_class_description(c.second, (*l)[idx++]);
}

void
rdb::get_object_values(OksKernel * kernel, const char *cid, const char *oid, rdb::RDBAttributeValueList_out s_attr_values, rdb::RDBAttributeValuesList_out m_attr_values, rdb::RDBRelationshipValueList_out rel_values)
{
  rdb::RDBAttributeValueList *av = new rdb::RDBAttributeValueList();
  rdb::RDBAttributeValuesList *mv = new rdb::RDBAttributeValuesList();
  rdb::RDBRelationshipValueList *rv = new rdb::RDBRelationshipValueList();

  std::shared_lock read_kernel_lock(kernel->get_mutex());

  rdb::object2values(rdb::get_oks_object(kernel, cid, oid), *av, *mv, *rv);

  s_attr_values = av;
  m_attr_values = mv;
  rel_values = rv;
}

void
rdb::cget_object_values(OksKernel * kernel, const char *cid, const char *oid, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
{
  rdb::RDBAttributeCompactValueList *sv = new rdb::RDBAttributeCompactValueList();
  rdb::RDBAttributeCompactValuesList *mv = new rdb::RDBAttributeCompactValuesList();
  rdb::RDBRelationshipCompactValueList *rv = new rdb::RDBRelationshipCompactValueList();

  std::shared_lock read_kernel_lock(kernel->get_mutex());

  OksObject *o = rdb::get_oks_object(kernel, cid, oid);

  unsigned long sv_idx = 0, mv_idx = 0;

  if (const std::list<OksAttribute*> *attrs = o->GetClass()->all_attributes())
    for (const auto &i : *attrs)
      {
        if (i->get_is_multi_values())
          mv_idx++;
        else
          sv_idx++;
      }

  rdb::object2values(o, sv_idx, mv_idx, *sv, *mv, *rv);

  s_attr_values = sv;
  m_attr_values = mv;
  rel_values = rv;
}

void
rdb::xget_object_values(OksKernel * kernel, const char *cid, const char *oid, rdb::RDBNameList_out s_attr_names, rdb::RDBNameList_out m_attr_names, rdb::RDBNameList_out r_names,
                              rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
{
  rdb::RDBNameList * an = new rdb::RDBNameList();
  rdb::RDBNameList * mn = new rdb::RDBNameList();
  rdb::RDBNameList * rn = new rdb::RDBNameList();

  rdb::RDBAttributeCompactValueList * sv = new rdb::RDBAttributeCompactValueList();
  rdb::RDBAttributeCompactValuesList * mv = new rdb::RDBAttributeCompactValuesList();
  rdb::RDBRelationshipCompactValueList * rv = new rdb::RDBRelationshipCompactValueList();

  std::shared_lock read_kernel_lock(kernel->get_mutex());

  OksObject * o = rdb::get_oks_object(kernel, cid, oid);

  rdb::class2names(o->GetClass(), *an, *mn, *rn);
  rdb::object2values(o, an->length(), mn->length(), *sv, *mv, *rv);

  s_attr_names = an;
  m_attr_names = mn;
  r_names = rn;

  s_attr_values = sv;
  m_attr_values = mv;
  rel_values = rv;
}



void
rdb::get_files_of_all_objects(OksKernel * kernel, rdb::RDBFileObjectsList*& info)
{
  config::map<std::set<const OksObject *>> mapping;

  for (const auto &x : kernel->objects())
    mapping[x->get_file()->get_well_formed_name()].insert(x);

  info = new rdb::RDBFileObjectsList();

  info->length(mapping.size());

  unsigned long file_idx = 0;
  for(const auto& x : mapping)
    {
      (*info)[file_idx].filename = rdb::string_dup(x.first);
      (*info)[file_idx].objects.length(x.second.size());

      unsigned long obj_idx = 0;
      for(const auto& o : x.second)
        {
          (*info)[file_idx].objects[obj_idx].classid = rdb::string_dup(o->GetClass()->get_name());
          (*info)[file_idx].objects[obj_idx++].name  = rdb::string_dup(o->GetId());
        }

      file_idx++;
    }
}


void
rdb::fill_referenced_by(const OksObject * o, const char * r, bool composite_only, rdb::RDBObjectList_out l)
{
  l = new rdb::RDBObjectList();

  if(composite_only) {
    bool any_name = ((r != 0) && (r[0] == '*') && (r[1] == 0));

    if(const std::list<OksRCR *> * rcr = o->reverse_composite_rels()) {
      std::list<OksRCR *>::const_iterator i = rcr->begin();

        // calculate length of out list

      size_t len = 0;
      for(; i != rcr->end(); ++i) if(any_name || (*i)->relationship->get_name() == r) len++;
      l->length(len);

        // fill out list

      for(len = 0, i = rcr->begin(); i != rcr->end(); ++i) {
        if(any_name || (*i)->relationship->get_name() == r) {
          l[len].classid = rdb::string_dup((*i)->obj->GetClass()->get_name());
          l[len].name = rdb::string_dup((*i)->obj->GetId());
          ++len;
        }
      }
    }
    else {
      l->length(0);
    }
  }
  else {
    if(OksObject::FList * objs = o->get_all_rels(r)) {
      l->length(objs->size());
      size_t len = 0;
      for(OksObject::FList::const_iterator i = objs->begin(); i != objs->end(); ++i, ++len)  {
        l[len].classid = rdb::string_dup((*i)->GetClass()->get_name());
        l[len].name = rdb::string_dup((*i)->GetId());
      }

      delete objs;
    }
    else {
      l->length(0);
    }
  }
}


void
rdb::sequence2classes(const OksKernel * kernel, const rdb::RDBNameList& cnames_in, oks::ClassSet& classes_out)
{
  std::vector<std::string> names;

  for(unsigned int i = 0; i < cnames_in.length(); i++) {
    names.push_back(static_cast<const char *>(cnames_in[i]));
  }

  kernel->get_all_classes(names, classes_out);
}


void
rdb::fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, OksObject::FSet& refs)
{
  oks::ClassSet names4refs;
  rdb::sequence2classes(c->get_kernel(), cnames, names4refs);

  rlist = new rdb::RDBObjectList();

  if(look_in_sclasses == false) {
    if(const OksObject::Map * omap = c->objects()) {
      rlist->length(omap->size());
      unsigned long idx = 0;
      for(OksObject::Map::const_iterator i = omap->begin(); i != omap->end(); ++i) {
        (*rlist)[idx].classid = rdb::string_dup(c->get_name());
        (*rlist)[idx++].name  = rdb::string_dup((*i).first);
        i->second->references(refs, recursion_depth, true, &names4refs);
      }
    }
  }
  else {
    if(std::list<OksObject*> * objs = c->create_list_of_all_objects()) {
      rlist->length(objs->size());
      unsigned long idx = 0;
      for(std::list<OksObject*>::iterator i = objs->begin(); i != objs->end(); ++i) {
        (*rlist)[idx].classid = rdb::string_dup((*i)->GetClass()->get_name());
        (*rlist)[idx++].name  = rdb::string_dup((*i)->GetId());
        (*i)->references(refs, recursion_depth, true, &names4refs);
      }
      delete objs;
    }
  }
}

bool
rdb::create_dir_for_backup_file(const std::string& file)
{
  std::filesystem::path path( std::filesystem::path(file).parent_path() );

  try {
    if( !std::filesystem::exists( path ) ) {
       std::filesystem::create_directories( path );
    }
  }
  catch( std::exception & ex ) {
    ers::fatal( daq::rdb::CannotCreateDirectory( ERS_HERE, path.string(), ex ) );
    return false;
  }

  return true;
}

std::string
rdb::get_backup_spec(const std::string& file)
{
  std::string spec("oksconfig:");

  if (!file.empty())
    spec.append(file);

  spec.append("&norepo");

  return spec;
}


static void
fill_versions(const std::vector<OksRepositoryVersion>& versions, rdb::RDBRepositoryVersionList* out)
{
  out->length(versions.size());

  unsigned long idx = 0;

  for (const auto& x : versions)
    {
      rdb::RDBRepositoryVersion& v = (*out)[idx++];

      v.id = rdb::string_dup(x.m_commit_hash);
      v.user = rdb::string_dup(x.m_user);
      v.timestamp = x.m_date;
      v.comment = rdb::string_dup(x.m_comment);
      v.files.length(x.m_files.size());
      unsigned long idx2 = 0;
      for (const auto& f : x.m_files)
        v.files[idx2++] = rdb::string_dup(f);
    }
}

rdb::RDBRepositoryVersionList*
rdb::get_changes(OksKernel& kernel)
{
  rdb::RDBRepositoryVersionList* out = new rdb::RDBRepositoryVersionList();

  if (!kernel.get_repository_root().empty())
    {
      try
        {
          fill_versions(kernel.get_repository_versions_by_hash(true, kernel.read_repository_version()), out);
        }
      catch (const oks::exception& ex)
        {
          ers::Message issue(ERS_HERE, ex.what());
          ers::log(issue);
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }
    }
  else
    {
      std::set<OksFile *> mfs, rfs;
      kernel.get_modified_files(mfs, rfs, "");

      if(auto len = mfs.size() + rfs.size())
        {
          out->length(1);

          rdb::RDBRepositoryVersion& v = (*out)[0];

          v.id = CORBA::string_dup("");
          v.user = CORBA::string_dup("");
          v.timestamp = 0;
          v.comment = CORBA::string_dup("");
          v.files.length(len);

          unsigned int idx = 0;
          for (const auto& f : mfs)
            v.files[idx++] = rdb::string_dup(f->get_well_formed_name());
          for (const auto& f : rfs)
            v.files[idx++] = rdb::string_dup(f->get_well_formed_name());
        }
      else
        out->length(0);
    }

  return out;
}

rdb::RDBRepositoryVersionList*
rdb::get_versions(OksKernel& kernel, CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until)
{
  rdb::RDBRepositoryVersionList *out = new rdb::RDBRepositoryVersionList();

  if (!kernel.get_repository_root().empty())
    {
      try
        {
          fill_versions((query_type == rdb::query_by_date ? kernel.get_repository_versions_by_date(skip_irrelevant, since, until) : kernel.get_repository_versions_by_hash(skip_irrelevant, since, until)), out);
        }
      catch (const oks::exception &ex)
        {
          ers::Message issue(ERS_HERE, ex.what());
          ers::log(issue);
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }
    }
  else
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot get versions, repository is not set")));
    }

  return out;
}

OksFile*
rdb::get_data_file(const OksKernel *kernel, const std::string &db_name)
{
  if (OksFile *f = kernel->find_data_file(db_name))
    return f;
  else
    throw daq::rdb::NoDataFile(ERS_HERE, db_name);
}


OksObject *
rdb::get_oks_object(const OksKernel * kernel, const char * cid, const char * oid, bool look_in_sclasses)
{
  if (const OksClass * c = kernel->find_class(cid))
    {
      if (OksObject * o = c->get_object(oid))
        return o;

      if (look_in_sclasses)
        if (const OksClass::FList *slist = c->all_sub_classes())
          for (const auto &i : *slist)
            if (OksObject *o = i->get_object(oid))
              return o;

      throw GetOksObjectException(cid, oid, look_in_sclasses);
    }
  else
    throw GetOksObjectException(cid);
}

void
rdb::check_am_permission(daq::am::ServerInterrogator& am_server_int, const std::string& requestor, daq::am::RDBResource::ActionID action, const std::string& partition)
{
  if (requestor != OksKernel::get_user_name())
    {
      const daq::am::RequestorInfo am_requestor(requestor);
      const std::string& am_action(daq::am::RDBResource::ACTION_VALUES[action]);

      const daq::am::RDBResource resource(am_action, partition);

      if (am_server_int.isAuthorizationGranted(resource, am_requestor) == false)
        throw daq::rdb::NoAccessManagerPermission(ERS_HERE, requestor, am_action);
    }
}

std::string
rdb::get_name_if_schema_reload_exception(const std::exception& ex)
{
  static std::string s_empty;

  const std::string reason = ex.what();
  std::string::size_type idx1 = reason.find(oks::FileReloadSchemaFile::exception_start_text);
  std::string::size_type idx2 = std::string::npos;

  if (idx1 != std::string::npos)
    {
      idx1 += oks::FileReloadSchemaFile::exception_start_text.size();
      idx2 = reason.find(oks::FileReloadSchemaFile::exception_end_text, idx1);
    }

  if (idx2 != std::string::npos)
    return reason.substr(idx1, idx2 - idx1);
  else
    return s_empty;
}
