/**
 * Defines interfaces to the RDB server and RDB-server subscriber.
 *
 * @author  <Serguei.Kolos@cern.ch> and <Igor.Soloviev@cern.ch>
 * @version 12/11/03
 * @since   
 */

#ifndef RDBIMPL_H
#define RDBIMPL_H

#include <stdint.h>

#include <map>
#include <mutex>
#include <shared_mutex>
#include <set>
#include <stdexcept>

#include <ipc/object.h>
#include <AccessManager/client/ServerInterrogator.h>

#include <oks/object.h>

#include "rdb/rdb.hh"
#include "rdb/errors.h"
#include "server_utils.h"

class RDBServer;


class RDBServerCallbackInfo : public ::IPCObject<POA_rdb::rdb_callback>
{

  friend class RDBServer;

  private:

    RDBServerCallbackInfo( const RDBServerCallbackInfo & );
    RDBServerCallbackInfo& operator= ( const RDBServerCallbackInfo & );

    RDBServerCallbackInfo( RDBServer * s ) : m_server(s) {;}

    CORBA::Long inform (const rdb::RDBDataChanges& data_changes);

    RDBServer * m_server;

};


class RDBServer: public IPCNamedObject<POA_rdb::cursor>
{
  friend class RDBServerCallbackInfo;

  public:

      /**
       *  The method opens database files.
       */

    void open_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names);


      /**
       *  The method closes given files.
       *  If the list is empty, it closes all files.
       */

    void close_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names);


      /**
       *  The method returns list of schema and data files loaded on the server.
       */

    void get_databases( rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles );


      /**
       *  The method returns list of modified data files loaded by the server.
       */

    void get_modified_databases( rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles );


      /**
       *  The method updates timestamp of loaded file (only for Read/Write server mode).
       *  This is the only way to overwrite externally modified file.
       */

    void update_database_status(const rdb::ProcessInfo& info, const char * db_name );


      /**
       *  The method reloads database files which were modified. 
       *  It may result notification of users subscribed on data modification.
       *  Throw exception rdb::CannotProceed in case if failed.
       */

    void reload_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names);


      /**
       *  The method returns repository versions created after current HEAD version available on remote origin .
       *  Throw exception rdb::CannotProceed in case if failed.
       */

    rdb::RDBRepositoryVersionList* get_changes();


      /**
       *  The method returns repository versions in interval.
       *  Throw exception rdb::CannotProceed in case if failed.
       */

    rdb::RDBRepositoryVersionList* get_versions(CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until);


      /**
       *  The method merges loaded schema and data files.
       */

    void merge_database(const char * data_file_name, const char * schema_file_name, const char * class_name, const char * query, CORBA::Long recursion_depth, bool ignore_dangling);


      /**
       *  The method to request parent server.
       */

    void request_parent(const char * name, const char * host, CORBA::String_out parent);


      /**
       *  The method to register as parent server.
       */

    void register_parent(const char * name, const char * host);


      /**
       *  Read complete database in one call and subscribe server "server_name" on database updates.
       */

    void get_complete_db(rdb::RDBCompleteDB_out db, const char * server_name, rdb::rdb_callback_ptr server_cb);


      /**
       *  The method removes subscription made with get_complete_db() method.
       */

    void unsubscribe_rdb(const char * server_name);


      /**
       *  Push complete database in one call.
       */

    void push_complete_db ( const char * server_name, const char * partition_name, const rdb::RDBCompleteDB& db );


      /**
       *  Push complete database changes in one call.
       */

    void push_db_changes ( const char * server_name, const char * partition_name, const rdb::RDBDataChanges& data_changes);


      /**
       *  The method returns list of files included by given one. 
       */

    void database_get_files(const char * db_name, rdb::RDBNameList_out file_names);


      /**
       *  method used by rdbproxy to check if information on server was updated
       */

    CORBA::Long get_validity_key() { return m_validity_key; }


      /**
       *  The method gets class by name.
       */

    void get_class (const char * class_name, rdb::RDBClass_out c);


      /**
       *  The method gets all classes defined on the server.
       */

    void get_all_classes (rdb::RDBClassList_out l);


      /**
       *  The method gets list of direct superclasses defined for given class.
       */

    void get_direct_super_classes (const char * class_name, rdb::RDBClassList_out l);


      /**
       *  The method gets list of all superclasses defined for given class.
       */

    void get_all_super_classes (const char * class_name, rdb::RDBClassList_out l);


      /**
       *  The method gets list of all subclasses derived from given class.
       */

    void get_sub_classes (const char * class_name, rdb::RDBClassList_out l);


      /**
       *  The method gets list of direct subclasses derived from given class.
       */

    void get_direct_sub_classes (const char * class_name, rdb::RDBClassList_out l);


      /**
       *  The method gets list of all attributes defined for given class.
       */

    void get_attributes (const char * class_name, rdb::RDBAttributeList_out l);


      /**
       *  The method gets list of all relationships defined for given class.
       */

    void get_relationships (const char * class_name, rdb::RDBRelationshipList_out l);


      /**
       *  The method gets inheritance hierarchy for all classes.
       */

    CORBA::Long get_inheritance_hierarchy (rdb::RDBInheritanceHierarchyList_out l);


      /**
       *  The method gets object of given class by object identity.
       *  If the boolean parameter 'look_in_sclasses' is true, search also in subclasses.
       */

    void get_object (const char * class_name, bool look_in_sclasses, const char * object_id, rdb::RDBObject_out o);

   
      /**
       *  Return which file this object is located in.
       */

    void get_file_of_object ( const rdb::RDBObject& o, CORBA::String_out file );


      /**
       *  The method gets information about location of all objects.
       */

    void get_files_of_all_objects(rdb::RDBFileObjectsList_out info);


      /**
       *  The method gets all objects of given class. 
       *  If the boolean parameter 'look_in_sclasses' is true, return also all objects of subclasses.
       */

    void get_all_objects (const char * class_name, bool look_in_sclasses, rdb::RDBObjectList_out o);


      /**
       *  The method gets objects of given class executing query.
       */

    void get_objects_by_query (const char * class_name, const char * query, rdb::RDBObjectList_out o );


      /**
       *  The method gets path between objects
       */

    void get_objects_by_path(const rdb::RDBObject& obj_from, const char* query, rdb::RDBObjectList_out l);


    void get_object_values(const char *cid, const char *oid, rdb::RDBAttributeValueList_out s_attr_values, rdb::RDBAttributeValuesList_out m_attr_values, rdb::RDBRelationshipValueList_out rel_values);
    void cget_object_values(const char *cid, const char *oid, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values);
    void xget_object_values(const char *cid, const char *oid, rdb::RDBNameList_out s_attr_names, rdb::RDBNameList_out m_attr_names, rdb::RDBNameList_out r_names, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values);


    void get_objects_of_relationship (const rdb::RDBObject& o, const char *, rdb::RDBObjectList_out objs);
    void get_referenced_by (const rdb::RDBObject& o, const char * r, bool composite_only, rdb::RDBObjectList_out objs);
    void get_attribute_value (const rdb::RDBObject& o, const char *, rdb::DataUnion_out d);
    void get_attribute_values (const rdb::RDBObject& o, const char *, rdb::DataListUnion_out l);


      /**
       *  The method gets object of given class by object identity.
       *  If the boolean parameter 'look_in_sclasses' is true, search also in subclasses.
       *  It also fills object's and referenced objects' values in accordance with recursion-depth parameter.
       */

    CORBA::Long xget_object ( const char * class_name, bool look_in_sclasses, const char * object_id, rdb::RDBObject_out o, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values);

    CORBA::Long xget_all_objects ( const char * class_name, bool look_in_sclasses, rdb::RDBObjectList_out o, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values);

    CORBA::Long xget_objects_by_query ( const char * class_name, const char * query, rdb::RDBObjectList_out o, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values);

    void xget_objects_by_path ( const rdb::RDBObject& obj_from, const char* query, rdb::RDBObjectList_out l, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values);


     /** prefetch all data into rdbconfig client cache (used by DBE) */

    void prefetch_data(rdb::RDBClassValueList_out values);

    void prefetch_schema(rdb::RDBClassDescriptionList_out values);


      /**
       *  The method subscribes on changes of any object of class with
       *  given name or it's subclasses and/or by explicit list of objects.
       *  The notification can be invoked by the reload_database() method.
       */

    CORBA::Long subscribe (const rdb::ProcessInfo& info, const rdb::RDBNameList& l1, bool, const rdb::RDBObjectList& l2, rdb::callback_ptr, CORBA::LongLong);


      /**
       *  The method unsubscribes on changes made with the subscribe() method.
       *  The parameter id shall be equal to the returned by the subscribe() method.
       */

    void unsubscribe (const rdb::ProcessInfo& info, CORBA::Long id);



  public:

      /**
       *  Run remote database server with name 'server_name' in the partition 'partition'.
       */

    RDBServer (const char * server_name, IPCPartition & partition, bool use_cache, bool allow_repository);


      /**
       *  Destroy database server.
       */

    ~RDBServer ();


      /**
       *  Shutdown remote database server.
       */

    void shutdown ( );


      /**
       *  The method loads database file and returns true in case of success.
       */

    void add_file(const char *);


      /** Read complete OKS database from master RDB server. */

    void read_complete_db(const std::string& name, bool try_unsubscribe);


      /** The method runs server in push mirror partition mode. */

    void set_push_mirror_partition(uint32_t push_interval) {
      m_push_mirror_partition = true;
      m_push_mirror_partition_retry_interval = push_interval;
    }


      /** Return true if server is running in push mirror partition mode. */

    bool get_push_mirror_partition() const { return m_push_mirror_partition; }


      /** Try to push mirror data. */

    void try_push_mirror_data() noexcept;


      /** The method sets name of the backup file. */

    void set_backup_file(const std::string& s) {m_backup_file = s;}


      /** The method informs server about request to make backup file. */

    void request_backup() const;


      /** The method produces backup file if there are requests. */

    void try_backup();


      /** The method makes backup. */

    void make_backup();


      /** The method sets remote database parameters stored in the backup file 'backup_file'. */

    void load_backup(const char * backup_file);


      /** If server is running in the 'push-mirror-partition' mode, send command to mirror data. */

    void push_data(bool * confirmation = 0);


      /** The method starts backup thread. */

    void start_backup_thread(size_t backup_interval);


      /** The method stops backup thread. */

    static void stop_backup_thread();


      /** Run thread to push data */

    void start_push_mirror_data_thread();


      /** The method to report error. */

    static std::ostream& report_error(const std::string&);


      /** The method to print out a message. */

    static std::ostream& print_message();


  public:

    static std::mutex p_message_mutex;


  public:

    const OksKernel * get_kernel() { return m_kernel; }


      /// Holds subscriptions and their counter

  private:

    std::map<int32_t, rdb::Subscription *> m_subscriptions;

    int32_t m_subscription_count;


  public:

    /**
     *  \throw std::exception if failed.
     */
    void delete_subscription(CORBA::Long id, long process_id, const std::string& host);
    void delete_server_subscription(const std::string& server_name);


  private:

      /// Holds server subscriptions and their counter

    std::map<std::string, rdb::rdb_callback_var> m_server_subscriptions;

    void add_server_subscription(const std::string& server_name, rdb::rdb_callback_ptr server_cb);


      /// Callback info for server subscribed on changes from master server

    RDBServerCallbackInfo * m_server_callback_info;


    bool m_subscribed_oks_changes;
    std::mutex p_subscribed_oks_changes_mutex;


      /// If there is first subscription by client or server, register OKS callbacks

    void subscribe_oks_changes();


      /// If there is no subscription by client or server, unregister OKS callbacks

    void unsubscribe_oks_changes();


      /// Invoke notification

    unsigned long notify(bool push_changes_to_mirror = true, const rdb::ProcessInfo * ignore_subscriber = 0);


      /**
       *  Holds database files which were created.
       *  The set to be cleaned after successful commit.
       *  In case of abort all created files to be destroyed.
       */

    std::set<OksFile *> m_created_files;


      /** Holds name of backup file */

    std::string m_backup_file;


  protected:

    OksKernel * m_kernel;
    rdb::ChangesHolder m_changes_holder;
    bool m_push_mirror_partition;
    std::string m_mirror_partition_name;
    uint32_t m_push_mirror_partition_retry_interval;
    std::string m_master_server_name;
    OksFile * m_schema_fh;
    OksFile * m_data_fh;

    daq::am::ServerInterrogator m_am_server;

    std::mutex p_mutex;

    std::mutex p_db_reload_mutex;

    std::mutex p_push_data_mutex;



  private:

    int32_t m_validity_key;

    bool m_use_cache;

    void clean_cache(bool final = false);


      // inheritance hierarchy cache

    std::shared_mutex m_cache_inheritance_hierarchy_mutex;
    rdb::RDBInheritanceHierarchyList * m_cache_inheritance_hierarchy_list;
    rdb::RDBInheritanceHierarchyList * m_cache_inheritance_hierarchy_list2;

    void get_inheritance_hierarchy_from_cache (rdb::RDBInheritanceHierarchyList_out l);


      // query & get_all objects cache

    std::shared_mutex m_cache_query_mutex;
    rdb::QueryCache * m_cache_query_map;
    rdb::QueryCache * m_cache_query_map2;

    void get_query_from_cache (rdb::QueryCache::iterator& i, const char * class_name, const char * query, rdb::RDBObjectList_out rlist, rdb::RDBClassValueList_out values);
    unsigned long fill_query_result (const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values);
    unsigned long fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values);


      // get obj cache

    std::shared_mutex m_cache_object_id_mutex;
    rdb::ObjectIDCache * m_cache_object_id_map;
    rdb::ObjectIDCache * m_cache_object_id_map2;

    rdb::RDBObject * get_object_id_from_cache (rdb::ObjectIDCache::iterator& i, const char * class_name, const char * object_id, bool look_in_sclasses, rdb::RDBClassValueList_out values);
    unsigned long fill_object_id_result (const char * classid, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, std::string& oks_obj_class_name, rdb::RDBClassValueList*& values);


      // complete db cache

    std::shared_mutex m_cache_complete_db_mutex;
    rdb::RDBCompleteDB * m_cache_complete_db;
    rdb::RDBCompleteDB * m_cache_complete_db2;

    void get_complete_db(rdb::RDBCompleteDB*& db);
    void get_complete_db_from_cache (rdb::RDBCompleteDB*& l);
    void fill_complete_db (rdb::RDBCompleteDB*& l);

    void create_oks_data ( const rdb::RDBCompleteDB& db );
    void remove_temporal_db_files ();

    CORBA::Long apply_changes (const rdb::RDBDataChanges& data_changes);

    void unbind_references(const OksObject::FSet& rm_objs);
    void drop_RCRs(const OksObject::FSet& rm_objs);


  private:
  
    struct PoolNode {
      std::string p_name;
      std::string p_node;
      unsigned int p_number;

      PoolNode(const std::string& name, const std::string& node) : p_name(name), p_node(node), p_number(0) { ; }
    };

    std::vector<PoolNode> m_pool_nodes;
    unsigned int m_pool_idx;
    std::mutex m_pool_mutex;

};


#endif
