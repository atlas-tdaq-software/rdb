#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <thread>
#include <unordered_map>

#include <boost/date_time/posix_time/time_parsers.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/gregorian/parsers.hpp>

#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/util/ErsIssues.h>
#include <AccessManager/xacml/impl/RDBResource.h>

#include <ers2idl/ers2idl.h>
#include <ipc/signal.h>

#include <oks/kernel.h>
#include <oks/file.h>
#include <oks/object.h>

#include <oks/class.h>
#include <oks/relationship.h>

#include "rdb_impl.h"
#include "rdb/rdb_info.h"

#include "server_profiler.h"
#include "server_utils.h"


static std::mutex s_unsubscribe_mutex; // shared by client and server unsubscribe methods to avoid simultaneous make_backup() calls


///////////////////////////////////////////////////////////////////////////////

static int get_timestamp()
{
  struct timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec;
}

///////////////////////////////////////////////////////////////////////////////

static std::ostream&
operator<<(std::ostream& s, const rdb::RDBDataChanges& data_changes)
{
  s << "changes in " << data_changes.length() << " class(es):" << std::endl;

  for(unsigned int idx(0); idx < data_changes.length(); idx++) {
    const rdb::RDBChanges& ch(data_changes[idx]);
    const char * class_name(static_cast<const char *>(ch.class_name));

    s << " - [" << idx << "] class \'" << class_name << '\''  << std::endl;

    s << "  * " << ch.created_objects.length() << " created object(s)" << std::endl;
    for(unsigned long idx2 = 0; idx2 < ch.created_objects.length(); ++idx2) {
      const rdb::RDBObjectCompactValue& cv(ch.created_objects[idx2]);
      s << "   - [" << idx2 << "] object \'" << cv.name << "\'" << std::endl;
    }

    s << "  * " << ch.updated_objects.length() << " updated object(s)" << std::endl;
    for(unsigned long idx3 = 0; idx3 < ch.updated_objects.length(); ++idx3) {
      const rdb::RDBObjectCompactValue& cv(ch.updated_objects[idx3]);
      s << "   - [" << idx3 << "] object \'" << cv.name << "\'" << std::endl;
    }

    s << "  * " << ch.removed_objects.length() << " removed object(s)" << std::endl;
    for(unsigned long idx4 = 0; idx4 < ch.removed_objects.length(); ++idx4) {
      s << "   - [" << idx4 << "] object \'" << ch.removed_objects[idx4] << "\'" << std::endl;
    }
  }

  return s;
}

///////////////////////////////////////////////////////////////////////////////

RDBServer::RDBServer(const char * name, IPCPartition &p, bool use_cache, bool allow_repository) :
  IPCNamedObject<POA_rdb::cursor>        (p, name),
  m_subscription_count                   (1),
  m_server_callback_info                 (0),
  m_subscribed_oks_changes               (false),
  m_kernel                               (new OksKernel(false, false, false, allow_repository)),
  m_changes_holder                       (m_kernel),
  m_push_mirror_partition                (false),
  m_push_mirror_partition_retry_interval (0),
  m_schema_fh                            (0),
  m_data_fh                              (0),
  m_validity_key                         (0),
  m_use_cache                            (use_cache),
  m_cache_inheritance_hierarchy_list     (0),
  m_cache_inheritance_hierarchy_list2    (0),
  m_cache_query_map                      (0),
  m_cache_query_map2                     (0),
  m_cache_object_id_map                  (0),
  m_cache_object_id_map2                 (0),
  m_cache_complete_db                    (0),
  m_cache_complete_db2                   (0),
  m_pool_idx                             (0)
{
  try {
    publish();
  }
  catch (ers::Issue & ex) {
    delete m_kernel;
    throw daq::rdb::CannotPublish(ERS_HERE, name, p.name().c_str(), ex);
  }
}

void
RDBServer::add_file( const char * file )
{
  clean_cache();

  try {
    m_kernel->load_file(file);
  }
  catch (oks::exception & e) {
    throw daq::rdb::FailedLoadDatabaseFile( ERS_HERE, e.what());
  }
  catch (...) {
    std::ostringstream text;
    text << "file \'" << file << "\' cannot be loaded by unknown reasons";
    throw daq::rdb::FailedLoadDatabaseFile( ERS_HERE, text.str().c_str() );
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////


struct SendServerPushCommand {

  const std::string& m_partition_name;
  const std::string& m_mirror_partition_name;
  const std::string& m_server_name;
  rdb::RDBDataChanges m_data_changes;

  SendServerPushCommand(const std::string& partition_name, const std::string& mirror_partition_name, const std::string& server_name, CORBA::Long data_changes_size, rdb::RDBChanges* data_changes_data) :
    m_partition_name        (partition_name),
    m_mirror_partition_name (mirror_partition_name),
    m_server_name           (server_name),
    m_data_changes          (data_changes_size, data_changes_size, data_changes_data) { ; }

  void operator()() {

    ERS_DEBUG(2, "try to push data changes" );

    try {
      IPCPartition mirror_partition( m_mirror_partition_name );
      rdb::cursor_var c = mirror_partition.lookup<rdb::cursor>(m_server_name);
      c->push_db_changes ( m_server_name.c_str(), m_partition_name.c_str(), m_data_changes );
      ERS_LOG("push data changes on server \'" << m_server_name << "\' running in \'" << m_mirror_partition_name << "\' partition" );
    }
    catch( daq::ipc::Exception & ex ) {
      ERS_LOG( "Mirror server \'" << m_server_name << "\' for partition \'" << m_partition_name << "\' is not found: " << ex );
    }
    catch ( CORBA::SystemException & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), daq::ipc::CorbaSystemException(ERS_HERE, &ex) ) );
    }
    catch ( const rdb::CannotProceed & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), *daq::idl2ers_issue(ex.issue) ) );
    }
    catch ( ers::Issue & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), ex ) );
    }

    delete this;
  }

};

struct SendServerNotifyCommand {

  std::string m_server_name;
  rdb::rdb_callback_var m_cb;
  rdb::RDBDataChanges m_data_changes;
  RDBServer * m_server;

  SendServerNotifyCommand(const std::string& name, rdb::rdb_callback_var& cb, CORBA::Long data_changes_size, rdb::RDBChanges* data_changes_data, RDBServer * v) :
    m_server_name       (name),
    m_cb                (cb),
    m_data_changes      (data_changes_size, data_changes_size, data_changes_data),
    m_server            (v) { ; }

  void operator()() {
    try {
      ERS_LOG("notify server \'" << m_server_name << '\'');

      unsigned long client_time = m_cb->inform(m_data_changes);
      ERS_DEBUG(2, "remote server spent " << client_time << " usec to process callback for server \'" << m_server_name << "\' subscription");
    }
    catch ( CORBA::SystemException & ex ) {
      std::ostringstream text;
      std::string corba_loc_name;
      try {
        corba_loc_name = IPCCore::objectToString(m_cb, IPCCore::Corbaloc );
      }
      catch(daq::ipc::Exception & ex) {
        corba_loc_name = "(IPCCore::objectToString() has failed: ";
        corba_loc_name += ex.what();
        corba_loc_name += ')';
      }
      text << "remote server \"" << corba_loc_name << "\" is not responding, removing subscription...\n(to recover non-responding server has to be restarted)";
      ers::fatal( daq::rdb::ServerNotificationFailed( ERS_HERE, m_server_name.c_str(), text.str().c_str(), daq::ipc::CorbaSystemException(ERS_HERE, &ex) ) );
      m_server->delete_server_subscription(m_server_name);
    }

    delete this;
  }

};


struct SendNotifyCommand {
  rdb::RDBClassChangesList m_changes_list;
  CORBA::Long m_id;
  rdb::Subscription * m_subscription;
  RDBServer * m_server;

  SendNotifyCommand(CORBA::Long id, rdb::Subscription * s, RDBServer * v) :
    m_id            (id),
    m_subscription  (s),
    m_server        (v) { ; }

  void operator()() {
    try {

      ERS_LOG( "notify subscriber " << m_id );

      if(ers::debug_level() > 1) {
	std::ostringstream text;
	text << " * subscription on:" << std::endl;
	text << "   - any changes in " << m_subscription->m_class_names.size() << " classes: ";
	for(std::set<std::string>::const_iterator ci = m_subscription->m_class_names.begin(); ci != m_subscription->m_class_names.end(); ++ci) {
	  if(ci != m_subscription->m_class_names.begin()) { text << ", "; } text << '\'' << *ci << '\'';
	}
	text << std::endl;
	for(rdb::Subscription::ObjectsMap::const_iterator si = m_subscription->m_objects_names.begin(); si != m_subscription->m_objects_names.end(); ++si) {
	  text << "   - changes in " << si->second.size() << " object(s) of class \'" << si->first << "\': ";
	  for(std::set<std::string>::const_iterator cl = si->second.begin(); cl != si->second.end(); ++cl) {
	    if(cl != si->second.begin()) { text << ", "; } text << '\'' << *cl << '\'';
	  }
	  text << std::endl;
	}

	for(unsigned long i = 0; i < m_changes_list.length(); i++) {
          rdb::RDBClassChanges& c = m_changes_list[i];
          text << " * report changes in class \'" << c.name << '\'' << std::endl;
	  text << "   a) " << c.modified.length() << " modified objects: ";
	  if(c.modified.length()) {
	    for(unsigned long j = 0; j < c.modified.length(); j++) {
              if(j != 0) { text << ", "; } text << '\'' << c.modified[j] << '\'';
	    }
	  }
	  text << std::endl;
	  text << "   b) " << c.created.length() << " created objects: ";
	  if(c.created.length()) {
	    for(unsigned long j = 0; j < c.created.length(); j++) {
	      if(j != 0) { text << ", "; } text << '\'' << c.created[j] << '\'';
	    }
	  }
	  text << std::endl;
	  text << "   c) " << c.removed.length() << " removed objects: ";
	  if(c.removed.length()) {
	    for(unsigned long j = 0; j < c.removed.length(); j++) {
              if(j != 0) { text << ", "; } text << '\'' << c.removed[j] << '\'';
	    }
	  }
	  text << std::endl;
        }

        ERS_DEBUG( 2, text.str() );
      }

      unsigned long client_time = m_subscription->m_cb->inform(m_changes_list, m_subscription->m_parameter);
      ERS_DEBUG(2, "client spent " << client_time << " usec to process callback for subscriber " << m_id);
    }
    catch ( CORBA::SystemException & ex ) {
      daq::rdb::NotificationFailed issue(
        ERS_HERE,
        m_id, m_subscription->m_app_name,
        m_subscription->m_creation_ts, m_subscription->m_user_id,
        m_subscription->m_host, m_subscription->m_process_id,
        daq::ipc::CorbaSystemException(ERS_HERE, &ex)
      );

      if(!strcmp("TRANSIENT", ex._name())) {
        ers::log(issue);
      }
      else {
        ers::error(issue);
      }

      m_server->delete_subscription(m_id, m_subscription->m_process_id, m_subscription->m_host);
    }

    delete this;
  }

};


  /**
   *  Invokes user notification if any.
   */

unsigned long
RDBServer::notify(bool push_changes_to_mirror, const rdb::ProcessInfo * ignore_subscriber)
{
  RDB_SERVER_PROFILING_INFO("notify")

  ERS_LOG("there are " << m_changes_holder.m_created.size() << " created and " << (m_changes_holder.m_removed.empty() ? "" : "up to ") << m_changes_holder.m_modified.size() << " updated objects; the objects are removed in " << m_changes_holder.m_removed.size() << " classes");

  std::list<SendNotifyCommand *> notify_list;
  std::list<SendServerNotifyCommand *> server_notify_list;
  SendServerPushCommand * send_srv_push_cmd(nullptr);

  // lock mutex to be sure no one [un]subscribes

    {
      std::unique_ptr<rdb::RDBDataChanges> data_changes;

      std::lock_guard lock(p_mutex);

      // notify RDB servers reading data from THIS MASTER RDB server and mirroring server

      if (!m_server_subscriptions.empty() || m_push_mirror_partition)
        {
          std::map<const OksClass *, std::set<const OksObject *> > created_objs_map;
          std::map<const OksClass *, std::set<const OksObject *> > updated_objs_map;

          for (const auto& i : m_changes_holder.m_created)
            {
              created_objs_map[i->GetClass()].insert(i);
              ERS_DEBUG(2, "add created object " << i);
            }

          for (std::set<OksObject *>::iterator i = m_changes_holder.m_modified.begin(); i != m_changes_holder.m_modified.end();)
            {
              if (m_kernel->is_dangling(*i))
                {
                  m_changes_holder.m_modified.erase(i++);
                  ERS_DEBUG(1, "skip dangling updated object " << (void *)(*i));
                }
              else
                {
                  updated_objs_map[(*i)->GetClass()].insert(*i);
                  ERS_DEBUG(2, "add updated object " << *i);
                  ++i;
                }
            }

          std::set<const OksClass *> all_changed_data;

          for (const auto& i : created_objs_map)
            {
              all_changed_data.insert(i.first);
            }

          for (const auto& i : updated_objs_map)
            {
              all_changed_data.insert(i.first);
            }

          for (const auto& i : m_changes_holder.m_removed)
            {
              all_changed_data.insert(i.first);
            }

          if (!all_changed_data.empty())
            {
              data_changes.reset(new rdb::RDBDataChanges());

              data_changes->length(all_changed_data.size());

              unsigned long idx = 0;

              for (const auto& i : all_changed_data)
                {
                  (*data_changes)[idx].class_name = rdb::string_dup(i->get_name());

                  unsigned long sv_idx = 0, mv_idx = 0;
                  if (const std::list<OksAttribute *> * attrs = i->all_attributes())
                    {
                      for (const auto& ai : *attrs)
                        {
                          if (ai->get_is_multi_values())
                            mv_idx++;
                          else
                            sv_idx++;
                        }
                    }

                    {
                      std::map<const OksClass *, std::set<const OksObject *> >::const_iterator j = created_objs_map.find(i);

                      if (j != created_objs_map.end())
                        {
                          (*data_changes)[idx].created_objects.length(j->second.size());
                          unsigned long idx2 = 0;
                          for (const auto& oi : j->second)
                            {
                              rdb::RDBObjectCompactValue& ov = (*data_changes)[idx].created_objects[idx2++];
                              ov.name = rdb::string_dup(oi->GetId());
                              rdb::object2values(oi, sv_idx, mv_idx, ov.av, ov.avs, ov.rv);
                            }
                        }
                      else
                        {
                          (*data_changes)[idx].created_objects.length(0);
                        }
                    }

                    {
                      std::map<const OksClass *, std::set<const OksObject *> >::const_iterator j = updated_objs_map.find(i);

                      if (j != updated_objs_map.end())
                        {
                          (*data_changes)[idx].updated_objects.length(j->second.size());
                          unsigned long idx2 = 0;
                          for (const auto& oi : j->second)
                            {
                              rdb::RDBObjectCompactValue& ov = (*data_changes)[idx].updated_objects[idx2++];
                              ov.name = rdb::string_dup(oi->GetId());
                              rdb::object2values(oi, sv_idx, mv_idx, ov.av, ov.avs, ov.rv);
                            }
                        }
                      else
                        {
                          (*data_changes)[idx].updated_objects.length(0);
                        }
                    }

                    {
                      std::map<const OksClass *, std::set<std::string> >::const_iterator j = m_changes_holder.m_removed.find(i);

                      if (j != m_changes_holder.m_removed.end())
                        {
                          (*data_changes)[idx].removed_objects.length(j->second.size());
                          unsigned long idx2 = 0;
                          for (const auto& oi : j->second)
                            {
                              (*data_changes)[idx].removed_objects[idx2++] = rdb::string_dup(oi);
                              ERS_DEBUG(2, "add removed object \'" << oi << '@' << i->get_name() << '\'');
                            }
                        }
                      else
                        {
                          (*data_changes)[idx].removed_objects.length(0);
                        }
                    }

                  ++idx;
                }

              ERS_DEBUG(2, "send " << *data_changes);

              for (auto& j : m_server_subscriptions)
                {
                  if (CORBA::is_nil(j.second))
                    {
                      ERS_LOG("server callback function is (null) for \'" << j.first << "\', ignoring...");
                      continue;
                    }

                  server_notify_list.push_back(new SendServerNotifyCommand(j.first, j.second, data_changes->length(), data_changes->get_buffer(), this));

                  ERS_DEBUG(2, "add notify call for server with subscription id " << j.first);
                }

              if (push_changes_to_mirror)
                {
                  if (m_push_mirror_partition)
                    {
                      send_srv_push_cmd = new SendServerPushCommand(partition().name(), m_mirror_partition_name, name(), data_changes->length(), data_changes->get_buffer());
                    }
                  else
                    {
                      ERS_DEBUG(2, "the \'push-mirror-partition\' mode is disabled");
                    }
                }
              else
                {
                  ERS_DEBUG(0, "do not try to push data on mirror");
                }

            }
        }

      // notify clients

      for (const auto& j : m_subscriptions)
        {
          if (CORBA::is_nil(j.second->m_cb))
            {
              ERS_LOG("callback function is (null) for \'" << j.first << "\', ignoring...");
              continue;
            }

          // do not notify client, that originated the changes

          if (ignore_subscriber)
            {
              if (j.second->m_process_id == ignore_subscriber->process_id && j.second->m_host == ignore_subscriber->host)
                {
                  ERS_LOG("ignore subscription from originator process " << j.second->m_process_id << '@' << j.second->m_host);
                  continue;
                }
            }

          SendNotifyCommand * obj = new SendNotifyCommand(j.first, j.second, this);
          config::map<rdb::ClassChanges *> cl_changes;

          for (const auto& i : m_changes_holder.m_created)
            {
              rdb::check(cl_changes, obj->m_subscription, i->GetClass(), i->GetId(), '+');

              std::set<OksObject *>::iterator x = m_changes_holder.m_modified.find(i);
              if (x != m_changes_holder.m_modified.end())
                {
                  ERS_DEBUG(1, "created object " << i << " appears in \'modified\' set, removing...");
                  m_changes_holder.m_modified.erase(x);
                }
            }

          for (const auto& i : m_changes_holder.m_modified)
            {
              if (m_kernel->is_dangling(i))
                {
                  ERS_DEBUG(1, "object " << (void *)i << " is dangling, ignore...");
                }
              else
                {
                  rdb::check(cl_changes, obj->m_subscription, i->GetClass(), i->GetId(), '~');
                }
            }

          for (const auto& i : m_changes_holder.m_removed)
            {
              for (const auto& j : i.second)
                {
                  rdb::check(cl_changes, obj->m_subscription, i.first, j, '-');
                }
            }

          if (cl_changes.size())
            {
              rdb::cvt_and_free(cl_changes, obj->m_changes_list);
              notify_list.push_back(obj);
              ERS_DEBUG(2, "add notify call for client with subscription id " << j.first);
            }
          else
            {
              delete obj;
              ERS_DEBUG(1, "no changes for subscriber " << j.first);
            }
        }

      m_changes_holder.clear();

      if (notify_list.empty() && server_notify_list.empty() && !send_srv_push_cmd)
        {
          ERS_LOG("there are no mirrored servers, client or server subscribers to be informed");
          return 0;
        }

      // prepare pool of threads to simultaneously notify all RDB servers from given partition
      std::vector<std::shared_ptr<std::thread> > threads;

      // run client reload commands in pool of threads
      for (const auto& i : notify_list)
        {
          std::shared_ptr<std::thread> thread(new std::thread(std::ref(*i)));
          threads.push_back(thread);
        }

      // run server reload commands in pool of threads
      for (const auto& i : server_notify_list)
        {
          std::shared_ptr<std::thread> thread(new std::thread(std::ref(*i)));
          threads.push_back(thread);
        }

      // notify mirroring server
      if (send_srv_push_cmd)
        {
          std::shared_ptr<std::thread> thread(new std::thread(std::ref(*send_srv_push_cmd)));
          threads.push_back(thread);
        }

      // wait for all threads in the pool to exit
      for (std::size_t j = 0; j < threads.size(); ++j)
        {
          threads[j]->join();
        }

      ERS_LOG("notified " << notify_list.size() << " clients, " << (send_srv_push_cmd ? 1 : 0) << " mirror and " << server_notify_list.size() << " client servers");

      return threads.size();
    }
}


void
RDBServer::reload_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names)
{
  RDB_SERVER_PROFILING_INFO("reload_database")

  try
    {
      const std::string requestor(rdb::get_user(info));

      ers::info(ers::Message( ERS_HERE, std::string("call reload database ") + to_str(names, true) + " by " + requestor + " on " + name() + '@' + partition().name()));

      rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_RELOAD, partition().name());
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseReloadFailed(ERS_HERE, "", ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseReloadFailed(ERS_HERE, "", ex)));
    }

  std::lock_guard lock(p_db_reload_mutex);

  if (!m_kernel->get_user_repository_root().empty() && names.length() != 1)
    {
      daq::rdb::DatabaseReloadFailed issue(ERS_HERE, ": version parameter is required to reload repository database");
      ers::info(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }

  if (m_schema_fh)
    {
      std::ostringstream text;
      text << ": invalid request (master RDB server \'" << m_master_server_name << "\' is used)";
      daq::rdb::DatabaseReloadFailed issue(ERS_HERE, text.str());
      ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }

  std::set<OksFile *> mfs, rfs;
  std::string param, value;

  if (m_kernel->get_user_repository_root().empty() && names.length() > 0)
    {
      for (unsigned int i = 0; i < names.length(); i++)
        {
          const char *file_name = names[i];

          if (OksFile *f = m_kernel->find_data_file(file_name))
            {
              ERS_LOG("add data file \'" << file_name << "\' to be reloaded");
              mfs.insert(f);
            }
          else if (OksFile *f = m_kernel->find_schema_file(file_name))
            {
              ERS_LOG("found schema file \'" << file_name << "\' to be reloaded");
              mfs.insert(f);
            }
          else
            {
              ERS_LOG("no file \'" << file_name << '\'');
            }
        }
    }
  else
    {
      try
        {
          if (!m_kernel->get_user_repository_root().empty())
            OksKernel::parse_config_version (static_cast<const char*> (names[0]), param, value);
        }
      catch (const std::exception &ex)
        {
          daq::rdb::DatabaseReloadFailed issue(ERS_HERE, "", ex);
          ers::info(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

      try
        {
          m_kernel->get_modified_files(mfs, rfs, value);
        }
      catch(const oks::exception& ex)
        {
          daq::rdb::DatabaseReloadFailed issue(ERS_HERE, ": cannot read names of modified files", ex);
          ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }
    }

  for (const auto& x : mfs)
    {
      if (x->get_oks_format() == "schema")
        {
          daq::rdb::SchemaReloadNotAllowed issue(ERS_HERE, x->get_well_formed_name());
          ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }
    }


  unsigned long notify_num = 0;


    // process updated files

  if (mfs.size())
    {
      std::ostringstream log;

      log << mfs.size() << " files to be reloaded";

      if (!m_kernel->get_user_repository_root().empty())
        log << " modified between revisions " << m_kernel->read_repository_version() << " and " << value;

      log << ":\n";

      for (const auto& i : mfs)
          log << "  - \"" << i->get_full_file_name() << "\"\n";

      ERS_LOG(log.str());

      try
        {
          if(!m_kernel->get_user_repository_root().empty())
            m_kernel->update_repository(param, value, OksKernel::RepositoryUpdateType::DiscardChanges);

          m_kernel->reload_data(mfs, false);
        }
      catch (std::exception& ex)
        {
          const std::string schema_ex_name = rdb::get_name_if_schema_reload_exception(ex);

          if (schema_ex_name.empty() == false)
            {
              daq::rdb::SchemaReloadNotAllowed issue(ERS_HERE, schema_ex_name);
              ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
              throw rdb::CannotProceed(daq::ers2idl_issue(issue));
            }
          else
            {
              daq::rdb::DatabaseReloadFailed issue(ERS_HERE, ": cannot reload updated files", ex);
              ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
              throw rdb::CannotProceed(daq::ers2idl_issue(issue));
            }
        }
    }
  else if(m_schema_fh == nullptr)
    {
      if (!m_kernel->get_user_repository_root().empty())
        {
          ERS_LOG("update revision without reload, no files changed");

          try
            {
              m_kernel->update_repository(param, value, OksKernel::RepositoryUpdateType::DiscardChanges);
            }
          catch (oks::exception &ex)
            {
              daq::rdb::DatabaseReloadFailed issue(ERS_HERE, "", ex);
              ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
              throw rdb::CannotProceed(daq::ers2idl_issue(issue));
            }
        }
      else
        ERS_LOG("there are no files changed");
    }

  if (mfs.size() || rfs.size())
    clean_cache();

  if (m_schema_fh == nullptr && !m_kernel->get_repository_version().empty() && !m_backup_file.empty())
    make_backup();


    // notify, if there were changes

  if (mfs.size())
    notify_num = notify();

  if (m_schema_fh == nullptr)
    ERS_INFO(
      "RDB Server \'" << name() << '@' << partition().name() << "\' has been reloaded. "
      "Number of reloaded files: " << mfs.size() << ". "
      "Number of closed files: " << rfs.size() << ". "
      "Number of notified clients: " << notify_num << '.'
    );
}

rdb::RDBRepositoryVersionList*
RDBServer::get_changes()
{
  return rdb::get_changes(*m_kernel);
}

rdb::RDBRepositoryVersionList*
RDBServer::get_versions(CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until)
{
  return rdb::get_versions(*m_kernel, skip_irrelevant, query_type, since, until);
}

void
RDBServer::merge_database(const char * data_file_name, const char * schema_file_name, const char * class_name, const char * query, CORBA::Long recursion_depth, bool ignore_dangling)
{
  ERS_LOG( "data: \'" << data_file_name << "\', schema \'" << schema_file_name
                      << "\', class: \'" << class_name << "\', query: \'" << query
                      << "\', ignore-dangling-references: " << ignore_dangling );

  std::lock_guard lock(p_mutex);

  try {
    OksKernel merge_kernel;

    OksFile * schema_file_h = merge_kernel.new_schema(schema_file_name);
    OksFile * data_file_h = merge_kernel.new_data(data_file_name);

    OksObject::FSet refs;

    if(*class_name == 0 && *query == 0) {
      for (const auto& i : m_kernel->objects()) {
        refs.insert(i);
      }
    }
    else if(OksClass * c = m_kernel->find_class(class_name)) {
      std::unique_ptr<OksQuery> q( new OksQuery(c, query) );
      if(q->good()) {
        OksObject::List * objs = c->execute_query(q.get());
        size_t num = (objs ? objs->size() : 0);
        ERS_LOG("found " << num << " matching query \"" << query << "\" in class \"" << class_name << '\"');

        if(num) {
          while(!objs->empty()) {
            OksObject * o = objs->front();
            objs->pop_front();
            if(recursion_depth > 0) {
              o->references(refs, recursion_depth, true);
            }
            else {
              refs.insert(o);
            }
          }
          delete objs;
        }
        else {
          std::ostringstream text;
          text << "query \'" << query << "\' for class \'" << class_name << "\' returned 0 objects";
          ers::Message issue(ERS_HERE, text.str());
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));

        }
      }
      else {
        std::ostringstream text;
        text << "cannot parse query \'" << query << "\' for class \'" << class_name << '\'';
        ers::Message issue(ERS_HERE, text.str());
        throw rdb::CannotProceed(daq::ers2idl_issue(issue));
      }
    }
    else {
      std::ostringstream text;
      text << "cannot find query class \'" << class_name << '\'';
      ers::Message issue(ERS_HERE, text.str());
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }

      // save merged schema

    m_kernel->save_schema(schema_file_h, true, m_kernel->classes());


      // save merged data

    data_file_h->add_include_file(schema_file_name);
    m_kernel->save_data(data_file_h, refs);
  }
  catch(std::exception & ex) {
    unlink(schema_file_name);
    unlink(data_file_name);
    ERS_LOG("remove created \'" << schema_file_name << "\' and \'" << data_file_name << "\' files caused by:\n" << ex.what());
    throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, ex.what())));
  }
}

void
RDBServer::open_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names)
{
  try
    {
      const std::string requestor(rdb::get_user(info));

      ers::info(ers::Message( ERS_HERE, std::string("call open database ") + to_str(names, false) + " by " + requestor + " on " + name() + '@' + partition().name()));

      rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_OPEN, partition().name());

      for (unsigned int i = 0; i < names.length(); i++)
        add_file(names[i]);

      push_data();
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseOpenFailed(ERS_HERE, ex)));
    }
  catch (const daq::rdb::FailedLoadDatabaseFile& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseOpenFailed(ERS_HERE, ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseOpenFailed(ERS_HERE, ex)));
    }
}


void
RDBServer::close_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names)
{
  try
    {
       const std::string requestor(rdb::get_user(info));

       ers::info(ers::Message( ERS_HERE, std::string("call close database ") + to_str(names, true) + " by " + requestor + " on " + name() + '@' + partition().name()));

       rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_CLOSE, partition().name());
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseCloseFailed(ERS_HERE, "", ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseCloseFailed(ERS_HERE, "", ex)));
    }

  rdb::GuardOksNotification g(m_changes_holder, m_subscriptions.size() > 0 || m_server_subscriptions.size() > 0 || m_push_mirror_partition);

  m_changes_holder.clear(); // FIXME: write code removing notify info only about closed schema and data files

  if (names.length() == 0)
    {
      for (OksFile::Map::const_iterator i = m_kernel->data_files().begin(); i != m_kernel->data_files().end(); ++i)
        {
          if (i->second->is_updated())
            {
              std::ostringstream text;
              text << ": file \'" << i->second->get_full_file_name() << "\' has uncommitted changes";
              throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseCloseFailed(ERS_HERE, text.str())));
            }
        }

      m_kernel->close_all_data();
      m_kernel->close_all_schema();
    }
  else
    {
      for (unsigned int i = 0; i < names.length(); i++)
        {
          const char * file_name = names[i];
          OksFile * f = m_kernel->find_data_file(file_name);
          if (f == nullptr)
            {
              f = m_kernel->find_schema_file(file_name);
              if (f == nullptr)
                {
                  std::ostringstream text;
                  text << ": file \'" << file_name << "\' is not loaded";
                  throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseCloseFailed(ERS_HERE, text.str())));
               }
              else
                {
                  m_kernel->close_schema(f);
                }
            }
          else
            {
              if (f->is_updated())
                {
                  std::ostringstream text;
                  text << ": file \'" << f->get_full_file_name() << "\' since it has uncommitted changes";
                  throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseCloseFailed(ERS_HERE, text.str())));
                }

              m_kernel->close_data(f);
            }
        }
    }

  clean_cache();
}


void
RDBServer::get_databases( rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles )
{
  std::lock_guard lock(p_mutex);

  rdb::get_databases( *m_kernel, schemafiles, datafiles );
}


void
RDBServer::get_modified_databases( rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles )
{
  std::lock_guard lock(p_mutex);

  rdb::get_externally_modified_databases( *m_kernel, externally_updated_datafiles, externally_removed_datafiles );
}


void
RDBServer::update_database_status(const rdb::ProcessInfo& info, const char * db_name )
{
  try
    {
       const std::string requestor(rdb::get_user(info));

       ers::info(ers::Message( ERS_HERE, std::string("update database file \"") + db_name + "\" status by " + requestor + " on " + name() + '@' + partition().name()));

       rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_UPDATE, partition().name());

       std::lock_guard lock(p_mutex);

       rdb::get_data_file(m_kernel, db_name)->update_status_of_file();
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, 0, ex)));
    }
  catch (const daq::rdb::NoDataFile& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, 0, ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, 0, ex)));
    }
}


void
RDBServer::database_get_files(const char * db_name, rdb::RDBNameList_out file_names)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  file_names = new rdb::RDBNameList();

  // get includes of file
  if (*db_name != 0)
    {
      try
        {
          OksFile* file_h = rdb::get_data_file(m_kernel, db_name);
          file_names->length(file_h->get_include_files().size());
          unsigned int idx(0);
          for (const auto& i : file_h->get_include_files())
            (*file_names)[idx++] = rdb::string_dup(i);
        }
      catch (const daq::rdb::Exception& ex)
        {
          throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedGetFileIncludes(ERS_HERE, db_name, 0, ex)));
        }
    }

  // get top-level files
  else
    {
      unsigned int idx(0);
      for (auto& i : m_kernel->data_files())
        {
          if (i.second->get_parent() == nullptr)
            idx++;
        }

      file_names->length(idx);

      idx = 0;
      for (auto& i : m_kernel->data_files())
        {
          if (i.second->get_parent() == nullptr)
            (*file_names)[idx++] = rdb::string_dup(i.first);
        }
    }
}


struct SendServerExitsCommand {
  CORBA::Long m_id;
  rdb::Subscription * m_subscription;

  SendServerExitsCommand(CORBA::Long id, rdb::Subscription * s) :
    m_id            (id),
    m_subscription  (s) { ; }

  void operator()() {
    try {
      ERS_LOG( "send server-exit command to subscriber " << m_id );
      m_subscription->m_cb->server_exits(m_subscription->m_parameter);
    }
    catch ( CORBA::SystemException & ex ) {
      ers::error( daq::rdb::NotificationFailed( ERS_HERE, m_id, m_subscription->m_app_name, m_subscription->m_creation_ts,
        m_subscription->m_user_id, m_subscription->m_host, m_subscription->m_process_id, daq::ipc::CorbaSystemException(ERS_HERE, &ex))
      );
    }

    delete this;
  }

};


RDBServer::~RDBServer()
{
  clean_cache(true);

  m_changes_holder.stop_oks_notification();

  delete m_kernel;
  m_kernel = nullptr;

  if(m_server_callback_info) {
    try {
      rdb::cursor_var c = partition().lookup<rdb::cursor>(m_master_server_name);
      c->unsubscribe_rdb(name().c_str());
    }
    catch ( CORBA::SystemException & ex ) {
      daq::rdb::CannotUnsubscribeServer issue(ERS_HERE, name(), m_master_server_name, partition().name(), daq::ipc::CorbaSystemException(ERS_HERE, &ex));
      if(!strcmp("TRANSIENT", ex._name()) || !strcmp("OBJECT_NOT_EXIST", ex._name())) {
        ers::info(issue); // parent server has exit normally or exiting ...
      }
      else {
        ers::error(issue);
      }
    }
    catch( const rdb::CannotProceed& ex ) {
      ers::error(daq::rdb::CannotUnsubscribeServer(ERS_HERE, name(), m_master_server_name, partition().name(), *daq::idl2ers_issue(ex.issue)));
    }
    catch ( ers::Issue & ex ) {
      ers::error(daq::rdb::CannotUnsubscribeServer(ERS_HERE, name(), m_master_server_name, partition().name(), ex));
    }

    m_server_callback_info->_destroy();
    m_server_callback_info = nullptr;
  }


  // force unsubscribe clients
  if (!m_subscriptions.empty()) {
    std::vector<std::shared_ptr<std::thread> > threads;

    for(const auto& j : m_subscriptions) {
      if( CORBA::is_nil(j.second->m_cb) ) {
        ERS_LOG("callback function is (null) for \'" << j.first << "\', ignoring...");
        continue;
      }

      std::shared_ptr<std::thread> thread(new std::thread(std::ref(*(new SendServerExitsCommand(j.first, j.second)))));
      threads.push_back(thread);
    }

    // wait for all threads in the pool to exit
    for (std::size_t j = 0; j < threads.size(); ++j)
      threads[j]->join();

    ERS_LOG("notify about server exit " << threads.size() << " clients");

    for (auto &j : m_subscriptions)
      delete j.second;

    m_subscriptions.clear();
  }

  try {
    withdraw();
  }
  catch(ers::Issue & ex) {
    ers::error( daq::rdb::CannotUnpublish( ERS_HERE, name(), partition().name(), ex ) );
  }

  ERS_LOG( "Terminated" );
}

void
RDBServer::shutdown()
{
  daq::ipc::signal::raise (SIGTERM);
}

void
RDBServer::get_class(const char *classid, rdb::RDBClass_out c)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass *rc = m_kernel->find_class(classid))
    {
      c = new rdb::RDBClass();
      rdb::copy(*c.ptr(), rc);
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBServer::get_all_classes(rdb::RDBClassList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  l = new rdb::RDBClassList();
  l->length(m_kernel->classes().size());

  unsigned long idx = 0;
  for (const auto& i : m_kernel->classes())
    rdb::copy(l[idx++], i.second);
}

void
RDBServer::get_all_super_classes(const char *classid, rdb::RDBClassList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass *c = m_kernel->find_class(classid))
    {
      l = new rdb::RDBClassList();
      if (const OksClass::FList *clist = c->all_super_classes())
        {
          l->length(clist->size());
          unsigned long idx = 0;
          for (const auto& i : *clist)
            rdb::copy(l[idx++], i);
        }
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}


void
RDBServer::get_direct_super_classes(const char *classid, rdb::RDBClassList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass *c = m_kernel->find_class(classid))
    {
      l = new rdb::RDBClassList();
      if (const std::list<std::string*> *clist = c->direct_super_classes())
        {
          l->length(clist->size());
          unsigned long idx = 0;
          for (const auto& i : *clist)
            rdb::copy(l[idx++], m_kernel->find_class(*i));
        }
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBServer::get_sub_classes(const char *classid, rdb::RDBClassList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass *c = m_kernel->find_class(classid))
    {
      l = new rdb::RDBClassList();
      if (const OksClass::FList *clist = c->all_sub_classes())
        {
          l->length(clist->size());
          OksClass::FList::const_iterator i = clist->begin();
          for (unsigned long idx = 0; i != clist->end(); ++i)
            rdb::copy(l[idx++], *i);
        }
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}
	
void
RDBServer::get_direct_sub_classes(const char * classid, rdb::RDBClassList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass * c = m_kernel->find_class(classid)) {
    l = new rdb::RDBClassList();
    if(const OksClass::FList * clist = c->all_sub_classes()) {
      unsigned long len = 0;
      OksClass::FList::const_iterator i = clist->begin();

      for(; i != clist->end(); ++i) {
        if((*i)->has_direct_super_class(classid) == true) len++;
      }

      l->length(len);

      i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        if((*i)->has_direct_super_class(classid) == true) {
          rdb::copy(l[idx++], *i);
	}
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}


void
RDBServer::get_attributes(const char * classid, rdb::RDBAttributeList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass * c = m_kernel->find_class(classid)) {
    l = new rdb::RDBAttributeList();
    if(const std::list<OksAttribute*> * clist = c->all_attributes()) {
      l->length(clist->size());
      std::list<OksAttribute*>::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        const OksAttribute * oa = *i;
        rdb::oks2rdb(oa, l[idx++], (c->source_class(oa) == c));
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}


void
RDBServer::get_relationships(const char * classid, rdb::RDBRelationshipList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass * c = m_kernel->find_class(classid)) {
    l = new rdb::RDBRelationshipList();
    if(const std::list<OksRelationship*> * clist = c->all_relationships()) {
      l->length(clist->size());
      std::list<OksRelationship*>::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        const OksRelationship * rr = *i;
	rdb::oks2rdb(rr, l[idx++], (c->source_class(rr) == c));
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}


void
RDBServer::get_inheritance_hierarchy_from_cache (rdb::RDBInheritanceHierarchyList_out l)
{
  l = new rdb::RDBInheritanceHierarchyList(m_cache_inheritance_hierarchy_list->length(), m_cache_inheritance_hierarchy_list->length(), m_cache_inheritance_hierarchy_list->get_buffer());

  ERS_DEBUG( 3 , "get_inheritance_hierarchy() returns from cache description for " << m_cache_inheritance_hierarchy_list->length() << " classes" );
}


CORBA::Long
RDBServer::get_inheritance_hierarchy (rdb::RDBInheritanceHierarchyList_out l)
{
  RDB_SERVER_PROFILING_INFO("get_inheritance_hierarchy")

    // if cache is used

  if(m_use_cache) {

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_inheritance_hierarchy_mutex);

      if(m_cache_inheritance_hierarchy_list) {
        get_inheritance_hierarchy_from_cache (l);
        return m_validity_key;
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_inheritance_hierarchy_mutex);

      if(!m_cache_inheritance_hierarchy_list) {
        rdb::fill_inheritance_hierarchy(*m_kernel, m_cache_inheritance_hierarchy_list);
      }

      ERS_DEBUG( 3 , "get_inheritance_hierarchy() puts to cache description of " << m_cache_inheritance_hierarchy_list->length() << " classes (v-key: " << m_validity_key << ')' );

      get_inheritance_hierarchy_from_cache (l);
      return m_validity_key;
    }
  }
  else {
    rdb::fill_inheritance_hierarchy(*m_kernel, l);
    ERS_DEBUG( 3 , "get_inheritance_hierarchy() returns description for " << l->length() << " classes (v-key: " << m_validity_key << ')' );
  }

  return m_validity_key;
}


void
RDBServer::get_object(const char * classid, bool look_in_sclasses, const char * objectid, rdb::RDBObject_out object )
{
  RDB_SERVER_PROFILING_INFO("get_object")

  object = new rdb::RDBObject();

  try
    {
      std::shared_lock read_kernel_lock(m_kernel->get_mutex());

      const OksObject *o = rdb::get_oks_object(m_kernel, classid, objectid, look_in_sclasses);

      object->classid = rdb::string_dup(o->GetClass()->get_name());
      object->name = rdb::string_dup(o->GetId());
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::get_file_of_object ( const rdb::RDBObject& o, CORBA::String_out file )
{
  try
    {
      std::shared_lock read_kernel_lock(m_kernel->get_mutex());

      file = rdb::string_dup(rdb::get_oks_object(m_kernel, o.classid, o.name)->get_file()->get_well_formed_name());
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::get_files_of_all_objects(rdb::RDBFileObjectsList_out info)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  rdb::get_files_of_all_objects(m_kernel, info);
}


rdb::RDBObject *
RDBServer::get_object_id_from_cache (rdb::ObjectIDCache::iterator& i, const char * class_name, const char * object_id, bool look_in_sclasses, rdb::RDBClassValueList_out values)
{
  values = new rdb::RDBClassValueList(i->second.second->length(), i->second.second->length(), i->second.second->get_buffer());

  ERS_DEBUG( 3 , "xget_object() for for \'" << object_id << '@' << class_name << (look_in_sclasses ? "*" : "") << "\' returns from cache \'" << object_id << '@' << i->second.first << "\' object (preload referenced objects from " << i->second.second->length() << " class(es))" );

  return rdb::make_rdb_object(i->second.first, object_id);
}

unsigned long 
RDBServer::fill_object_id_result (const char * classid, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, std::string& oks_obj_class_name, rdb::RDBClassValueList*& values)
{
  std::shared_lock rlock(m_kernel->get_mutex());

  OksObject * oks_obj = rdb::get_oks_object(m_kernel, classid, objectid, look_in_sclasses);

  oks_obj_class_name = oks_obj->GetClass()->get_name();
  OksObject::FSet refs;
  oks::ClassSet names4refs;
  rdb::sequence2classes(m_kernel, cnames, names4refs);
  oks_obj->references(refs, recursion_depth, true, &names4refs);
  rdb::fill_values(refs, values);
  return refs.size();
}


CORBA::Long
RDBServer::xget_object(const char * classid, bool look_in_sclasses, const char * objectid, rdb::RDBObject_out object, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  RDB_SERVER_PROFILING_INFO("xget_object")

  try
    {
      if (m_use_cache)
        {
          std::string key = rdb::object_id_params_to_string(classid, look_in_sclasses, objectid, recursion_depth, cnames);

          // check the value is already in cache
            {
              std::shared_lock m(m_cache_object_id_mutex);

              if (m_cache_object_id_map)
                {
                  rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(key);
                  if (i != m_cache_object_id_map->end())
                    {
                      object = get_object_id_from_cache(i, classid, objectid, look_in_sclasses, values);
                      return m_validity_key;
                    }
                }
            }

          // put the value to cache
            {
              std::unique_lock m(m_cache_object_id_mutex);

              if (!m_cache_object_id_map)
                m_cache_object_id_map = new rdb::ObjectIDCache();

              rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(key);

              if (i == m_cache_object_id_map->end())
                {
                  std::string oks_obj_class_name;
                  rdb::RDBClassValueList *the_list = 0;
                  unsigned long size = fill_object_id_result(classid, look_in_sclasses, objectid, recursion_depth, cnames, oks_obj_class_name, the_list);

                  i = m_cache_object_id_map->insert(std::make_pair(key, std::make_pair(oks_obj_class_name, the_list))).first;

                  ERS_DEBUG(3, "xget_object() for \'" << objectid << '@' << classid << (look_in_sclasses ? "*" : "") << "\' returns from cache \'" << objectid << '@' << oks_obj_class_name<< "\' object (preload " << size << " referenced object(s) from " << the_list->length() << " class(es)) (v-key: " << m_validity_key << ')');
                }

              object = get_object_id_from_cache(i, classid, objectid, look_in_sclasses, values);

              return m_validity_key;
            }
        }

      std::string oks_obj_class_name;
      unsigned long size = fill_object_id_result(classid, look_in_sclasses, objectid, recursion_depth, cnames, oks_obj_class_name, values);
      object = rdb::make_rdb_object(oks_obj_class_name, objectid);

      ERS_DEBUG(2, "xget_object() for \'" << objectid << '@' << classid << (look_in_sclasses ? "*" : "") << "\' returns \'" << objectid << '@' << oks_obj_class_name<< "\' object (preload " << size << " referenced object(s) from " << values->length() << " class(es)) (v-key: " << m_validity_key << ')');

      return m_validity_key;
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBServer::get_all_objects(const char * class_name, bool look_in_sclasses, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_all_objects")

  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (const OksClass * c = m_kernel->find_class(class_name)) {
    rlist = new rdb::RDBObjectList();

    if(look_in_sclasses == false) {
      if(const OksObject::Map * omap = c->objects()) {
        rlist->length(omap->size());
	unsigned long idx = 0;
        for(const auto& i : *omap) {
	  (*rlist)[idx].classid = rdb::string_dup(c->get_name());
	  (*rlist)[idx++].name	= rdb::string_dup(i.first);
        }
      }
    }
    else {
      if(std::list<OksObject*> * objs = c->create_list_of_all_objects()) {
        rlist->length(objs->size());
	unsigned long idx = 0;
        for(const auto& i : *objs) {
	  (*rlist)[idx].classid = rdb::string_dup(i->GetClass()->get_name());
	  (*rlist)[idx++].name	= rdb::string_dup(i->GetId());
        }
        delete objs;
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  ERS_DEBUG( 2 , "get_all_objects (\"" << class_name << "\", look_in_sclasses = " << look_in_sclasses << ") returns " << rlist->length() << " object(s)" );
}

unsigned long
RDBServer::fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values)
{
  try
    {
      OksObject::FSet refs;
      rdb::fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, rlist, refs);
      rdb::fill_values(refs, values);
      return refs.size();
    }
  catch (std::exception &ex)
    {
      std::ostringstream text;
      text << "failed to fill structures to pass objects of class \'" << c->get_name() << '\'';
      ers::Message issue(ERS_HERE, text.str(), ex);
      ers::debug(issue, 1);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
}


CORBA::Long
RDBServer::xget_all_objects(const char * class_name, bool look_in_sclasses, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  RDB_SERVER_PROFILING_INFO("xget_all_objects")

  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  const OksClass * c = m_kernel->find_class(class_name);

  if (!c)
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  if(m_use_cache) {
    const char * query = (look_in_sclasses ? "*" : "T");
    std::string key = rdb::query_params_to_string(class_name, query, recursion_depth, cnames);

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_query_mutex);

      if(m_cache_query_map) {
        rdb::QueryCache::iterator i = m_cache_query_map->find(key);
	if(i != m_cache_query_map->end()) {
	  get_query_from_cache (i, class_name, query, rlist, values);
	  return m_validity_key;
	}
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_query_mutex);

      if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

      rdb::QueryCache::iterator i = m_cache_query_map->find(key);

      if(i == m_cache_query_map->end()) {
        i = m_cache_query_map->insert( std::make_pair(key, std::make_pair((rdb::RDBObjectList*)0,(rdb::RDBClassValueList*)0) ) ).first;
        unsigned long size = fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, i->second.first, i->second.second);

        ERS_DEBUG( 3 , "xget_all_objects() [\'" << key << "\'] => " << i->second.first->length() << " object(s) (preloading " << size << " referenced objects from " << i->second.second->length() << " class(es)) (v-key: " << m_validity_key << ')' );
      }

      get_query_from_cache (i, class_name, query, rlist, values);

      return m_validity_key;
    }
  }

  unsigned long size = fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, rlist, values);

  ERS_DEBUG( 2 , "xget_all_objects (\"" << class_name << (look_in_sclasses ? "*" : "") << ") returns " << rlist->length() << " object(s) (preload " << size << " referenced objects from " << values->length() << " class(es)) (v-key: " << m_validity_key << ')' );

  return m_validity_key;
}


void
RDBServer::get_objects_by_query(const char *class_name, const char *query, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_objects_by_query")

  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  if (OksClass *c = m_kernel->find_class(class_name))
    {
      rlist = new rdb::RDBObjectList();

      if (query == nullptr)
        query = "";

      std::unique_ptr<OksQuery> qe(new OksQuery(c, query));

      if (qe->good() == false)
        {
          ers::Message issue(ERS_HERE, rdb::mk_bad_query_text(class_name, query, "bad syntax"));
          ers::debug(issue, 1);
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

      if (OksObject::List *objs = c->execute_query(qe.get()))
        rdb::oks2rdb_list(objs, rlist, 0, 0, 0);
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  ERS_DEBUG(2, "query \"" << class_name << "\", \"" << query << "\" returns " << rlist->length() << " object(s)");
}

void
RDBServer::get_query_from_cache (rdb::QueryCache::iterator& i, const char * class_name, const char * query, rdb::RDBObjectList_out rlist, rdb::RDBClassValueList_out values)
{
  rlist = new rdb::RDBObjectList(i->second.first->length(), i->second.first->length(), i->second.first->get_buffer());
  values = new rdb::RDBClassValueList(i->second.second->length(), i->second.second->length(), i->second.second->get_buffer());

  ERS_DEBUG( 3 , "xget_objects_by_query() query \"" << class_name << "\", \"" << query << "\" returns from cache " << i->second.first->length() << " object(s) (preload referenced objects from " << i->second.second->length() << " class(es))" );
}


unsigned long 
RDBServer::fill_query_result (const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values)
{
  rlist = new rdb::RDBObjectList();
  OksObject::FSet refs;

  std::shared_lock rlock(m_kernel->get_mutex());

  if (OksClass *c = m_kernel->find_class(class_name))
    {
      if (query == nullptr)
        query = "";

      std::unique_ptr<OksQuery> qe(new OksQuery(c, query));

      if (qe->good() == false)
        {
          ers::Message issue(ERS_HERE, rdb::mk_bad_query_text(class_name, query, "bad syntax"));
          ers::debug(issue, 1);
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

      if (OksObject::List *objs = c->execute_query(qe.get()))
        {
          oks::ClassSet names4refs;
          rdb::sequence2classes(m_kernel, cnames, names4refs);
          rdb::oks2rdb_list(objs, rlist, &refs, recursion_depth, &names4refs);
        }
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  rdb::fill_values(refs, values);

  return refs.size();
}


CORBA::Long
RDBServer::xget_objects_by_query(const char * class_name, const char * query, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  RDB_SERVER_PROFILING_INFO("xget_objects_by_query")

  if(m_use_cache) {
    std::string key = rdb::query_params_to_string(class_name, query, recursion_depth, cnames);

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_query_mutex);

      if(m_cache_query_map) {
        rdb::QueryCache::iterator i = m_cache_query_map->find(key);
	if(i != m_cache_query_map->end()) {
	  get_query_from_cache (i, class_name, query, rlist, values);
	  return m_validity_key;
	}
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_query_mutex);

      if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

      rdb::QueryCache::iterator i = m_cache_query_map->find(key);

      if(i == m_cache_query_map->end()) {
        i = m_cache_query_map->insert( std::make_pair(key, std::make_pair((rdb::RDBObjectList*)0,(rdb::RDBClassValueList*)0) ) ).first;
        unsigned long size = fill_query_result(class_name, query, recursion_depth, cnames, i->second.first, i->second.second);

        ERS_DEBUG( 3 , "xget_objects_by_query() [\'" << key << "\'] => " << i->second.first->length() << " object(s) (preloading " << size << " referenced objects from " << i->second.second->length() << " class(es)) (v-key: " << m_validity_key << ')' );
      }

      get_query_from_cache (i, class_name, query, rlist, values);

      return m_validity_key;
    }
  }

  unsigned long size = fill_query_result(class_name, query, recursion_depth, cnames, rlist, values);

  ERS_DEBUG( 2 , "xget_objects_by_query() query \"" << class_name << "\", \"" << query << "\" returns " << rlist->length() << " object(s) (preload " << size << " referenced objects from " << values->length() << " class(es)) (v-key: " << m_validity_key << ')' );

  return m_validity_key;
}

void
RDBServer::get_objects_by_path(const rdb::RDBObject &obj_from, const char *query, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_objects_by_path")

  rlist = new rdb::RDBObjectList();

  try
    {
      std::shared_lock read_kernel_lock(m_kernel->get_mutex());

      const OksObject *oks_o = rdb::get_oks_object(m_kernel, obj_from.classid, obj_from.name);

      oks::QueryPath q(query, *m_kernel);

      if (OksObject::List *objs = oks_o->find_path(q))
        rdb::oks2rdb_list(objs, rlist, nullptr, 0, nullptr);

      ERS_DEBUG(2, "path \"" << query << "\" from " << oks_o << " returns " << rlist->length() << " object(s)");
    }
  catch (const oks::bad_query_syntax& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot get objects by path", ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::xget_objects_by_path(const rdb::RDBObject& obj_from, const char* query, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  RDB_SERVER_PROFILING_INFO("xget_objects_by_path")

  OksObject::FSet refs;

  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  try
    {
      const OksObject *oks_o = rdb::get_oks_object(m_kernel, obj_from.classid, obj_from.name);

      rlist = new rdb::RDBObjectList();

      oks::QueryPath q(query, *m_kernel);

      if (OksObject::List *objs = oks_o->find_path(q))
        {
          oks::ClassSet names4refs;
          rdb::sequence2classes(m_kernel, cnames, names4refs);
          rdb::oks2rdb_list(objs, rlist, &refs, recursion_depth, &names4refs);
        }

      rdb::fill_values(refs, values);

      ERS_DEBUG(2, "path \"" << query << "\" from " << oks_o << " returns " << rlist->length() << " object(s) (preload " << refs.size() << " referenced object(s))");
    }
  catch (const oks::bad_query_syntax& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot get objects by path", ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::prefetch_data(rdb::RDBClassValueList_out values)
{
  std::shared_lock rlock(m_kernel->get_mutex());
  fill_values(m_kernel, values);
}

void RDBServer::prefetch_schema(rdb::RDBClassDescriptionList_out values)
{
  std::shared_lock rlock(m_kernel->get_mutex());
  fill_schema(m_kernel, values);
}


void
RDBServer::get_object_values(const char *cid, const char *oid, rdb::RDBAttributeValueList_out s_attr_values, rdb::RDBAttributeValuesList_out m_attr_values, rdb::RDBRelationshipValueList_out rel_values)
{
  try
    {
      rdb::get_object_values(m_kernel, cid, oid, s_attr_values, m_attr_values, rel_values);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBServer::cget_object_values(const char *cid, const char *oid, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
{
  try
    {
      rdb::cget_object_values(m_kernel, cid, oid, s_attr_values, m_attr_values, rel_values);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBServer::xget_object_values(const char *cid, const char *oid, rdb::RDBNameList_out s_attr_names, rdb::RDBNameList_out m_attr_names, rdb::RDBNameList_out r_names,
                        rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
{
  try
    {
      rdb::xget_object_values(m_kernel, cid, oid, s_attr_names, m_attr_names, r_names, s_attr_values, m_attr_values, rel_values);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBServer::get_objects_of_relationship(const rdb::RDBObject& rdb_o, const char * r, rdb::RDBObjectList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  try
    {
      OksData *d(rdb::get_oks_object(m_kernel, rdb_o.classid, rdb_o.name)->GetRelationshipValue(r));
      l = new rdb::RDBObjectList(1);
      rdb::append_obj_to_list(d, *l.ptr());
    }
  catch (const oks::exception &ex)
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::RelationshipNotFound(ERS_HERE, r, rdb_o.classid, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::get_referenced_by (const rdb::RDBObject& rdb_o, const char * r, bool composite_only, rdb::RDBObjectList_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  try
    {
      rdb::fill_referenced_by(rdb::get_oks_object(m_kernel, rdb_o.classid, rdb_o.name), r, composite_only, l);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBServer::get_attribute_value(const rdb::RDBObject& rdb_o, const char * a, rdb::DataUnion_out data )
{
  OksData *oks_data = nullptr;

  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  try
    {
      oks_data = rdb::get_oks_object(m_kernel, rdb_o.classid, rdb_o.name)->GetAttributeValue(a);
    }
  catch (const oks::exception& ex)
    {
      throw daq::rdb::AttributeNotFound(ERS_HERE, a, rdb_o.classid, ex);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }

  if (oks_data->type == OksData::list_type)
    throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, 0, std::runtime_error("the attribute is multi-value"))));

  try
    {
      data = new rdb::DataUnion();
      rdb::oks2rdb(*data.ptr(), oks_data);
    }
  catch (std::exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, 0, ex)));
    }
}


void
RDBServer::get_attribute_values(const rdb::RDBObject &rdb_o, const char *a, rdb::DataListUnion_out l)
{
  std::shared_lock read_kernel_lock(m_kernel->get_mutex());

  try
    {
      const OksObject *oks_obj = rdb::get_oks_object(m_kernel, rdb_o.classid, rdb_o.name);

      if (OksDataInfo *di = oks_obj->GetClass()->data_info(a))
        {
          OksData *oks_data(oks_obj->GetAttributeValue(di));

          if (oks_data->type != OksData::list_type)
            throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, 0, std::runtime_error("the attribute is single value"))));

          try
            {
              l = new rdb::DataListUnion();
              rdb::oks2rdb(*l.ptr(), oks_data, di->attribute->get_data_type());
            }
          catch (std::exception& ex)
            {
              throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, 0, ex)));
            }
        }
      else
        throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::AttributeNotFound(ERS_HERE, a, rdb_o.classid)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBServer::subscribe_oks_changes()
{
  std::lock_guard lock(p_subscribed_oks_changes_mutex);

  if(
    (m_subscribed_oks_changes == false) &&
    (!m_subscriptions.empty() || !m_server_subscriptions.empty() || m_push_mirror_partition) &&
    (m_schema_fh == 0)
  ) {
    m_subscribed_oks_changes = true;
    ERS_LOG( "subscribe OKS changes" );
    m_changes_holder.start_oks_notification();
  }
}

void
RDBServer::unsubscribe_oks_changes()
{
  std::lock_guard lock(p_subscribed_oks_changes_mutex);

  if(m_subscriptions.empty() && m_server_subscriptions.empty() && !m_push_mirror_partition) {
    m_subscribed_oks_changes = false;
    ERS_LOG( "unsubscribe OKS changes" );
    m_changes_holder.stop_oks_notification();
  }
}


CORBA::Long
RDBServer::subscribe(const rdb::ProcessInfo& info, const rdb::RDBNameList& names, bool look_in_suclasses, const rdb::RDBObjectList& objs, rdb::callback_ptr cb, CORBA::LongLong parameter)
{
  std::lock_guard lock(p_mutex);

  rdb::Subscription * s = new rdb::Subscription(info, names, look_in_suclasses, objs, cb, parameter);
  m_subscriptions[m_subscription_count] = s;

  ERS_LOG( "create client subscription " << m_subscription_count << " for \"" << s->m_app_name << "\" application started on \"" << s->m_host << "\" with " << s->m_process_id << " by \"" << s->m_user_id << "\"");

  subscribe_oks_changes();

  request_backup();

  return m_subscription_count++;
}


void
RDBServer::delete_subscription(CORBA::Long id, long process_id, const std::string& host)
{
    // note: notify() may already get p_mutex
    // use this local one to protect from 2 threads deleting subscription in result of notify() 

  std::lock_guard lock(s_unsubscribe_mutex);

  std::map<int32_t, rdb::Subscription *>::iterator i = m_subscriptions.find(id);

  if(i == m_subscriptions.end()) {
    throw std::runtime_error("it does not exist");
  }
  else {
    rdb::Subscription * s = i->second;

    if(s->m_process_id != process_id) {
      throw std::runtime_error("process id does not match to subscriber one");
    }
    else if(s->m_host != host) {
      throw std::runtime_error("hostname does not match to subscriber one");
    }

    ERS_LOG( "removing subscription " << id << "..." );

    m_subscriptions.erase(i);

    delete s;

    unsubscribe_oks_changes();
  }

  request_backup();
}


void
RDBServer::unsubscribe(const rdb::ProcessInfo &info, CORBA::Long id)
{
  try
    {
      std::lock_guard lock(p_mutex);
      delete_subscription(id, info.process_id, static_cast<const char*>(info.host));
    }
  catch (std::exception &ex)
    {
      std::ostringstream text;
      text << "cannot remove subscription with id " << id << " (requested by application \'" << info.app_name << "\' run by \'" << rdb::get_user_safe(info) << "\' on \'" << info.host << "\' with pid " << info.process_id << ')';
      ers::Message issue(ERS_HERE, text.str(), ex);
      ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
}


void
RDBServer::clean_cache(bool final)
{
  ++m_validity_key;

  if(m_use_cache) {

      // clean inheritance hierarchy cache
    {
      std::unique_lock m(m_cache_inheritance_hierarchy_mutex);

      if(m_cache_inheritance_hierarchy_list2) {
        ERS_DEBUG( 3 , "clean_cache() destroys second-level description of inheritance hierarchy stored in cache" );
        delete m_cache_inheritance_hierarchy_list2;
        m_cache_inheritance_hierarchy_list2 = 0;
      }

      if(m_cache_inheritance_hierarchy_list) {
        if(final) {
          ERS_DEBUG( 3 , "clean_cache() destroys first-level description of inheritance hierarchy stored in cache" );
	  delete m_cache_inheritance_hierarchy_list;
	  m_cache_inheritance_hierarchy_list = 0;
	}
	else {
          m_cache_inheritance_hierarchy_list2 = m_cache_inheritance_hierarchy_list;
          m_cache_inheritance_hierarchy_list = 0;
	}
      }
    }

      // clean complete db cache
    {
      std::unique_lock m(m_cache_complete_db_mutex);
    
      if(m_cache_complete_db2) {
        ERS_DEBUG( 3 , "clean_cache() destroys second-level complete db stored in cache" );
        delete m_cache_complete_db2;
        m_cache_complete_db2 = 0;
      }

      if(m_cache_complete_db) {
        if(final) {
          ERS_DEBUG( 3 , "clean_cache() destroys first-level complete db stored in cache" );
	  delete m_cache_complete_db;
	  m_cache_complete_db = 0;
	}
	else {
          m_cache_complete_db2 = m_cache_complete_db;
          m_cache_complete_db = 0;
	}
      }
    }

      // clean query cache
    {
      std::unique_lock m(m_cache_query_mutex);

      if(m_cache_query_map2) {
        ERS_DEBUG( 3 , "clean_cache() destroys second-level query cache" );
	rdb::destroy(m_cache_query_map2);
      }

      if(m_cache_query_map) {
        if(final) {
	  ERS_DEBUG( 3 , "clean_cache() destroys first-level query cache" );
	  rdb::destroy(m_cache_query_map);
	}
	else {
          m_cache_query_map2 = m_cache_query_map;
          m_cache_query_map = 0;
	}
      }
    }


      // clean object id cache
    {
      std::unique_lock m(m_cache_object_id_mutex);

      if(m_cache_object_id_map2) {
        ERS_DEBUG( 3 , "clean_cache() destroys second-level object id cache" );
	rdb::destroy(m_cache_object_id_map2);
      }

      if(m_cache_object_id_map) {
        if(final) {
          ERS_DEBUG( 3 , "clean_cache() destroys first-level object id cache" );
	  rdb::destroy(m_cache_object_id_map);
	}
	else {
          m_cache_object_id_map2 = m_cache_object_id_map;
          m_cache_object_id_map = 0;
	}
      }
    }

  }
}

void
RDBServer::get_complete_db_from_cache (rdb::RDBCompleteDB*& db)
{
  db = new rdb::RDBCompleteDB(m_cache_complete_db->length(), m_cache_complete_db->length(), m_cache_complete_db->get_buffer());

  ERS_DEBUG( 3 , "get_complete_db_from_cache() returns from cache complete db composed of " << db->length() << " classes" );
}


class StringsDictionary {
  
  public:

    typedef std::unordered_map<const std::string *, uint32_t, oks::hash_str, oks::equal_str> Map;

    StringsDictionary(size_t n) : p_data(n), p_counter(0) { ; }

    Map& get_data() { return p_data; }

    uint32_t put(const std::string * s);


  private:

    Map p_data;
    uint32_t p_counter;

};

uint32_t StringsDictionary::put(const std::string * s)
{
  std::pair<Map::iterator,bool> ret = p_data.insert (std::pair<const std::string *,uint32_t>(s,p_counter) );

  if(ret.second == true) { p_counter++; }

  return (ret.first)->second;
}

void
RDBServer::fill_complete_db (rdb::RDBCompleteDB*& cdb)
{
  rdb::ServerProfiler profiler(true, "RDBServer::fill_complete_db");

  cdb = new rdb::RDBCompleteDB();
  
  cdb->length(1);
  
  rdb::RDBCompleteDBi& db = (*cdb)[0];
  
  StringsDictionary str_dict(m_kernel->objects().size());  // allocate 1 string bucket per object to avoid rehash of map


  unsigned long idx = 0;

  std::shared_lock rlock(m_kernel->get_mutex());

  db.classes.length(m_kernel->classes().size());

  unsigned long number_of_objects(0);
  
  unsigned long num_of_booleans(0), num_of_s8_ints(0), num_of_u8_ints(0), num_of_s16_ints(0),
                num_of_u16_ints(0), num_of_s32_ints(0), num_of_u32_ints(0), num_of_s64_ints(0),
                num_of_u64_ints(0), num_of_floats(0), num_of_doubles(0);

  unsigned long num_of_boolean_lists(0), num_of_s8_int_lists(0), num_of_u8_int_lists(0),
                num_of_s16_int_lists(0), num_of_u16_int_lists(0), num_of_s32_int_lists(0),
                num_of_u32_int_lists(0), num_of_s64_int_lists(0), num_of_u64_int_lists(0),
                num_of_float_lists(0), num_of_double_lists(0);

  unsigned long to_be_deleted_size(0);


  for(OksClass::Map::const_iterator i = m_kernel->classes().begin(); i != m_kernel->classes().end(); ++i, ++idx) {

    OksClass *c(i->second);

    rdb::fill_class_description(c, db.classes[idx].schema);

    if(const OksObject::Map * objs = c->objects()) {

      unsigned long num = objs->size();

      if(const std::list<OksAttribute *> * attrs = c->all_attributes()) {
        for(std::list<OksAttribute *>::const_iterator ai = attrs->begin(); ai != attrs->end(); ++ai) {
	  bool is_mv = (*ai)->get_is_multi_values();
	  
	  switch((*ai)->get_data_type()) {

	    case OksData::date_type:
	    case OksData::time_type:
	      to_be_deleted_size++;
	      /* no break */
	    case OksData::u32_int_type:
	    case OksData::string_type:
	    case OksData::class_type:
	      if(is_mv == false) num_of_u32_ints += num;  else num_of_u32_int_lists += num; break;

	    case OksData::s32_int_type:
	      if(is_mv == false) num_of_s32_ints += num;  else num_of_s32_int_lists += num; break;

	    case OksData::bool_type:
	      if(is_mv == false) num_of_booleans += num;  else num_of_boolean_lists += num; break;

	    case OksData::s16_int_type:
	      if(is_mv == false) num_of_s16_ints += num;  else num_of_s16_int_lists += num; break;

	    case OksData::u16_int_type:
	    case OksData::enum_type:
	      if(is_mv == false) num_of_u16_ints += num;  else num_of_u16_int_lists += num; break;

	    case OksData::s64_int_type:
	      if(is_mv == false) num_of_s64_ints += num;  else num_of_s64_int_lists += num; break;

	    case OksData::u64_int_type:
	      if(is_mv == false) num_of_u64_ints += num;  else num_of_u64_int_lists += num; break;

	    case OksData::s8_int_type:
	      if(is_mv == false) num_of_s8_ints += num;   else num_of_s8_int_lists += num;  break;

	    case OksData::u8_int_type:
	      if(is_mv == false) num_of_u8_ints += num;   else num_of_u8_int_lists += num;  break;

	    case OksData::float_type:
	      if(is_mv == false) num_of_floats += num;    else num_of_float_lists += num;   break;

	    case OksData::double_type:
	      if(is_mv == false) num_of_doubles += num;   else num_of_double_lists += num;  break;

            default:
	      {
                std::ostringstream text;
                text << "got unexpected type of attribute " << (*ai)->get_name() << '@' << c->get_name();
		ers::fatal(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), ers::Message(ERS_HERE, text.str())));
 	        abort();
	      }
	  }
	  
        }
      }

      if(const std::list<OksRelationship *> * rels = c->all_relationships()) {
        for(std::list<OksRelationship *>::const_iterator ri = rels->begin(); ri != rels->end(); ++ri) {
	  if((*ri)->get_high_cardinality_constraint() == OksRelationship::Many) {
	    num_of_s32_int_lists += num;
	  }
	  else {
	    num_of_s32_ints += num;
	  }
        }
      }

      db.classes[idx].objects.length(objs->size());
      unsigned long idx2 = 0;
      for(OksObject::Map::const_iterator oi = objs->begin(); oi != objs->end(); ++oi) {
        oi->second->set_int32_id(number_of_objects++);
	db.classes[idx].objects[idx2++] = rdb::string_dup(oi->second->GetId());
      }
    }
    else {
      db.classes[idx].objects.length(0);
    }
  }
  
  idx = 0;
  
  db.booleans.length(num_of_booleans);
  db.s8_ints.length(num_of_s8_ints);
  db.u8_ints.length(num_of_u8_ints);
  db.s16_ints.length(num_of_s16_ints);
  db.u16_ints.length(num_of_u16_ints);
  db.s32_ints.length(num_of_s32_ints);
  db.u32_ints.length(num_of_u32_ints);
  db.s64_ints.length(num_of_s64_ints);
  db.u64_ints.length(num_of_u64_ints);
  db.floats.length(num_of_floats);
  db.doubles.length(num_of_doubles);

  db.boolean_lists.length(num_of_boolean_lists);
  db.s8_int_lists.length(num_of_s8_int_lists);
  db.u8_int_lists.length(num_of_u8_int_lists);
  db.s16_int_lists.length(num_of_s16_int_lists);
  db.u16_int_lists.length(num_of_u16_int_lists);
  db.s32_int_lists.length(num_of_s32_int_lists);
  db.u32_int_lists.length(num_of_u32_int_lists);
  db.s64_int_lists.length(num_of_s64_int_lists);
  db.u64_int_lists.length(num_of_u64_int_lists);
  db.float_lists.length(num_of_float_lists);
  db.double_lists.length(num_of_double_lists);

  num_of_booleans = num_of_s8_ints = num_of_u8_ints = num_of_s16_ints = num_of_u16_ints = num_of_s32_ints =
  num_of_u32_ints = num_of_s64_ints = num_of_u64_ints = num_of_floats = num_of_doubles = num_of_boolean_lists =
  num_of_s8_int_lists = num_of_u8_int_lists = num_of_s16_int_lists = num_of_u16_int_lists = num_of_s32_int_lists =
  num_of_u32_int_lists = num_of_s64_int_lists = num_of_u64_int_lists = num_of_float_lists = num_of_double_lists = 0;


  std::vector<std::string *> to_be_deleted(to_be_deleted_size);


  for(OksClass::Map::const_iterator i = m_kernel->classes().begin(); i != m_kernel->classes().end(); ++i, ++idx) {

    OksClass * c(i->second);

    if(const OksObject::Map * objs = c->objects()) {
      for(OksObject::Map::const_iterator oi = objs->begin(); oi != objs->end(); ++oi) {
        OksObject * o = oi->second;
        OksDataInfo odi(0, (const OksAttribute *)0);

        for(std::list<OksAttribute *>::const_iterator ai = c->all_attributes()->begin(); ai != c->all_attributes()->end(); ++ai) {
	  OksData * d(o->GetAttributeValue(&odi));
	  odi.offset++;

	  bool is_mv = (*ai)->get_is_multi_values();

	  switch((*ai)->get_data_type()) {

	    case OksData::string_type:
	      if(is_mv == false) {
	        db.u32_ints[num_of_u32_ints++] = str_dict.put(d->data.STRING);
	      }
	      else {
	        rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = str_dict.put((*x)->data.STRING);
                }
	      }
	      break;

	    case OksData::u32_int_type:
	      if(is_mv == false) {
	        db.u32_ints[num_of_u32_ints++] = d->data.U32_INT;
	      }
	      else {
	        rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.U32_INT;
                }
	      }
	      break;

	    case OksData::date_type:
	    case OksData::time_type:
	      if(is_mv == false) {
	        std::string * tmp = new std::string(d->str());
	        db.u32_ints[num_of_u32_ints++] = str_dict.put(tmp);
		to_be_deleted.push_back(tmp);
	      }
	      else {
	        rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
		std::string * tmp = new std::string((*x)->str());
                  seq[len] = str_dict.put(tmp);
		  to_be_deleted.push_back(tmp);
                }
	      }
	      break;

	    case OksData::class_type:
	      if(is_mv == false) {
	        db.u32_ints[num_of_u32_ints++] = str_dict.put(&d->data.CLASS->get_name());
	      }
	      else {
	        rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = str_dict.put(&(*x)->data.CLASS->get_name());
                }
	      }
	      break;

	    case OksData::s32_int_type:
	      if(is_mv == false) {
	        db.s32_ints[num_of_s32_ints++] = d->data.S32_INT;
	      }
	      else {
	        rdb::RDB_s32_ints& seq(db.s32_int_lists[num_of_s32_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.S32_INT;
                }
	      }
	      break;

	    case OksData::bool_type:
	      if(is_mv == false) {
	        db.booleans[num_of_booleans++] = d->data.BOOL;
	      }
	      else {
	        rdb::RDB_booleans& seq(db.boolean_lists[num_of_boolean_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.BOOL;
                }
	      }
	      break;

	    case OksData::s16_int_type:
	      if(is_mv == false) {
	        db.s16_ints[num_of_s16_ints++] = d->data.S16_INT;
	      }
	      else {
	        rdb::RDB_s16_ints& seq(db.s16_int_lists[num_of_s16_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.S16_INT;
                }
	      }
	      break;

	    case OksData::u16_int_type:
	      if(is_mv == false) {
	        db.u16_ints[num_of_u16_ints++] = d->data.U16_INT;
	      }
	      else {
	        rdb::RDB_u16_ints& seq(db.u16_int_lists[num_of_u16_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.U16_INT;
                }
	      }
	      break;


	    case OksData::enum_type:
	      if(is_mv == false) {
	        db.u16_ints[num_of_u16_ints++] = (*ai)->get_enum_value(*d);
	      }
	      else {
	        rdb::RDB_u16_ints& seq(db.u16_int_lists[num_of_u16_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*ai)->get_enum_value(**x);
                }
	      }
	      break;

	    case OksData::s64_int_type:
	      if(is_mv == false) {
	        db.s64_ints[num_of_s64_ints++] = d->data.S64_INT;
	      }
	      else {
	        rdb::RDB_s64_ints& seq(db.s64_int_lists[num_of_s64_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.S64_INT;
                }
	      }
	      break;

	    case OksData::u64_int_type:
	      if(is_mv == false) {
	        db.u64_ints[num_of_u64_ints++] = d->data.U64_INT;
	      }
	      else {
	        rdb::RDB_u64_ints& seq(db.u64_int_lists[num_of_u64_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.U64_INT;
                }
	      }
	      break;

	    case OksData::s8_int_type:
	      if(is_mv == false) {
	        db.s8_ints[num_of_s8_ints++] = d->data.S8_INT;
	      }
	      else {
	        rdb::RDB_s8_ints& seq(db.s8_int_lists[num_of_s8_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.S8_INT;
                }
	      }
	      break;

	    case OksData::u8_int_type:
	      if(is_mv == false) {
	        db.u8_ints[num_of_u8_ints++] = d->data.U8_INT;
	      }
	      else {
	        rdb::RDB_u8_ints& seq(db.u8_int_lists[num_of_u8_int_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.U8_INT;
                }
	      }
	      break;

	    case OksData::float_type:
	      if(is_mv == false) {
	        db.floats[num_of_floats++] = d->data.FLOAT;
	      }
	      else {
	        rdb::RDB_floats& seq(db.float_lists[num_of_float_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.FLOAT;
                }
	      }
	      break;

	    case OksData::double_type:
	      if(is_mv == false) {
	        db.doubles[num_of_doubles++] = d->data.DOUBLE;
	      }
	      else {
	        rdb::RDB_doubles& seq(db.double_lists[num_of_double_lists++]);
	        seq.length(d->data.LIST->size());
                int len = 0;
                for(OksData::List::const_iterator x = d->data.LIST->begin(); x != d->data.LIST->end(); ++x, ++len) {
                  seq[len] = (*x)->data.DOUBLE;
                }
	      }
	      break;

            default:
	      {
                std::ostringstream text;
                text << "got unexpected type of attribute " << (*ai)->get_name() << '@' << c->get_name();
                ers::fatal(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), ers::Message(ERS_HERE, text.str())));
 	        abort();
	      }
          }
	}

        for(std::list<OksRelationship *>::const_iterator ri = c->all_relationships()->begin(); ri != c->all_relationships()->end(); ++ri) {
	  OksData * d(o->GetRelationshipValue(&odi));
	  odi.offset++;

          if((*ri)->get_high_cardinality_constraint() != OksRelationship::Many) {
	    int32_t id = -1;

            if(d->type == OksData::object_type) {
	      if(OksObject * o2 = d->data.OBJECT) {
	        id = o2->get_int32_id();
              }
            }

	    db.s32_ints[num_of_s32_ints++] = id;
	  }
	  else {
	    rdb::RDB_s32_ints& seq(db.s32_int_lists[num_of_s32_int_lists++]);

	    if(OksData::List * l = d->data.LIST) {
              int list_len = 0;
              for(OksData::List::const_iterator li = l->begin(); li != l->end(); ++li) {
                if((*li)->type == OksData::object_type && (*li)->data.OBJECT) list_len++;
              }
              seq.length(list_len);
              unsigned int list_idx = 0;
              for(OksData::List::const_iterator li = l->begin(); li != l->end(); ++li) {
                if((*li)->type == OksData::object_type) {
		  if(OksObject * o2 = (*li)->data.OBJECT) {
		    seq[list_idx++] = o2->get_int32_id();
		  }
		}
              }
	    }
	    else {
              seq.length(0);
	    }
	  }
	}
      }
    }
  }


    // fill strings dictionary

  StringsDictionary::Map& strings_map(str_dict.get_data());

  db.strings_dictionary.length(strings_map.size());

  for(StringsDictionary::Map::const_iterator i = strings_map.begin(); i != strings_map.end(); ++i) {
    db.strings_dictionary[i->second] = rdb::string_dup(i->first);
  }


    // destroy temporal strings (created from date / time)

  for(unsigned int i = 0; i < to_be_deleted.size(); ++i) {
    delete to_be_deleted[i];
  }

  unsigned long t = profiler.now(true);

  ERS_LOG("\n"
    "complete in:             " << std::setprecision(4) << (float)t / 1000000 << " seconds\n"
    "number of classes:       " << m_kernel->classes().size() << "\n"
    "number of objects:       " << m_kernel->objects().size() << "\n"
    "number of data items:    " <<
      num_of_booleans + num_of_s8_ints + num_of_u8_ints + num_of_s16_ints + num_of_u16_ints + num_of_s32_ints +
      num_of_u32_ints + num_of_s64_ints + num_of_u64_ints + num_of_floats + num_of_doubles + num_of_boolean_lists +
      num_of_s8_int_lists + num_of_u8_int_lists + num_of_s16_int_lists + num_of_u16_int_lists + num_of_s32_int_lists +
      num_of_u32_int_lists + num_of_s64_int_lists + num_of_u64_int_lists + num_of_float_lists + num_of_double_lists << "\n"
    "strings dictionary size: " << strings_map.size()
  );
}


void
RDBServer::get_complete_db(rdb::RDBCompleteDB*& db)
{
  if(m_use_cache) {

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_complete_db_mutex);

      if(m_cache_complete_db) {
        get_complete_db_from_cache(db);
        return;
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_complete_db_mutex);

      if(!m_cache_complete_db) {
        fill_complete_db(m_cache_complete_db);
      }

      ERS_DEBUG( 3 , "get_complete_db() puts to cache complete db composed of " << m_cache_complete_db->length() << " classes" );

      get_complete_db_from_cache (db);
      return;
    }
  }
  else {
    fill_complete_db(db);
    ERS_DEBUG( 3 , "get_complete_db() returns complete db composed of " << db->length() << " classes" );
  }
}


void
RDBServer::get_complete_db(rdb::RDBCompleteDB_out db, const char * server_name, rdb::rdb_callback_ptr server_cb)
{
  RDB_SERVER_PROFILING_INFO("get_complete_db")

  rdb::RDBCompleteDB * db_ptr(0);
  get_complete_db(db_ptr);
  db = db_ptr;

  return add_server_subscription(server_name, server_cb);
}


void
RDBServer::push_complete_db ( const char * server_name, const char * partition_name, const rdb::RDBCompleteDB& db )
{
  ERS_LOG( "got push-complete-database request from server \"" << server_name << "\" running in partition \"" << partition_name << '\"' );

  std::lock_guard push_lock(p_push_data_mutex);

  OksKernel * old_kernel(m_kernel);
  m_kernel = new OksKernel(false, false, false, false);


    // create schema and data files

  {
    std::ostringstream basename;
    basename << "/tmp/rdb:push:" << server_name << '@' << partition_name << ':' << getpid() << ':' << get_timestamp() << ':' << (void *)m_kernel << '.';

    std::string tmp_schema_name = basename.str() + "schema.xml";
    std::string tmp_data_name = basename.str() + "data.xml";

    try {
      m_schema_fh = m_kernel->new_schema(tmp_schema_name);
      m_data_fh = m_kernel->new_data(tmp_data_name);
    }
    catch(oks::exception& ex) {
      std::string text(ex.what());
      delete m_kernel;
      m_kernel = old_kernel;
      throw daq::rdb::FailedReadDB(ERS_HERE, server_name, partition_name, text.c_str());
    }
  }

  m_changes_holder.reset(m_kernel);

  create_oks_data(db);

  ERS_DEBUG( 2 , "complete push of database from rdb server \'" << server_name << '@' << partition_name << "\' was successful" );

    // calculate changes between old and new databases

  if(!m_subscriptions.empty()) {
    std::shared_lock rlock(m_kernel->get_mutex());

    ERS_DEBUG( 3 , "calculate changes and notify subscribers on complete DB reload" );

    m_changes_holder.clear();

    if(!old_kernel->objects().empty()) {
      for(OksClass::Map::const_iterator i = old_kernel->classes().begin(); i != old_kernel->classes().end(); ++i) {
        OksClass * c1(i->second);
	
	if(const OksObject::Map * objects1 = c1->objects()) {
          if(OksClass * c2 = m_kernel->find_class(c1->get_name())) {
            bool all_updated(*c1 != *c2);
            for(OksObject::Map::const_iterator j = objects1->begin(); j != objects1->end(); ++j) {
              OksObject * o1(j->second);
	      if(OksObject * o2 = c2->get_object(j->first)) {
	        if(all_updated || !(*o1 == *o2)) {
                  ERS_DEBUG(3, "add " << o2 << " to \'modified\'" );
		  m_changes_holder.m_modified.insert(o2);
		}
	      }
	      else {
                ERS_DEBUG(3, "add " << j->second << " to \'removed\'" );
	        m_changes_holder.m_removed[c1].insert(*j->first);
	      }
	    }
	    
	    if(const OksObject::Map * objects2 = c2->objects()) {
              for(OksObject::Map::const_iterator j = objects2->begin(); j != objects2->end(); ++j) {
	        if(c1->get_object(j->first) == 0) {
                  ERS_DEBUG(3, "add " << j->second << " to \'created\'" );
		  m_changes_holder.m_created.push_back(j->second);
		}
	      }
	    }
	  }
	  else {
	    ERS_DEBUG(3, "new database has no class \'" << c1->get_name() << "\', add all objects to \'removed\'");
            for(OksObject::Map::const_iterator j = objects1->begin(); j != objects1->end(); ++j) {
              ERS_DEBUG(3, " - add " << j->second << " to \'removed\'" );
	      m_changes_holder.m_removed[c1].insert(*j->first);
	    }
	  }
	}
      }
    }
    else if(!m_kernel->objects().empty()) {
      for(OksClass::Map::const_iterator i = m_kernel->classes().begin(); i != m_kernel->classes().end(); ++i) {
        OksClass * c2(i->second);
        if(old_kernel->find_class(c2->get_name()) == 0) {
	  if(const OksObject::Map * objects = c2->objects()) {
	    ERS_DEBUG(3, "old database has no class \'" << c2->get_name() << "\', add all objects to \'created\'");
            for(OksObject::Map::const_iterator j = objects->begin(); j != objects->end(); ++j) {
              ERS_DEBUG(3, " - add " << j->second << " to \'created\'" );
	      m_changes_holder.m_created.push_back(j->second);
	    }
	  }
	}
      }
    }

    notify(false);
  }

  old_kernel->close_all_data();
  old_kernel->close_all_schema();
  delete old_kernel;

  if(m_push_mirror_partition) {
    ERS_LOG("push complete database on mirror");
    push_data();
  }
}

void
RDBServer::push_db_changes(const char * server_name, const char * partition_name, const rdb::RDBDataChanges& data_changes)
{
  ERS_LOG( "got push-database-changes request from server \"" << server_name << "\" running in partition \"" << partition_name << '\"' );

  std::lock_guard push_lock(p_push_data_mutex);

  apply_changes(data_changes);
}


void
RDBServer::add_server_subscription(const std::string& server_name, rdb::rdb_callback_ptr server_cb)
{
  std::lock_guard lock(p_mutex);

  if(m_server_subscriptions.find(server_name) != m_server_subscriptions.end()) {
    std::ostringstream text;
    text << "server \'" << server_name << "\' was already subscribed";
    ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), ers::Message(ERS_HERE, text.str())));
  }

  m_server_subscriptions[server_name] = rdb::rdb_callback::_duplicate(server_cb);

  ERS_LOG( "create subscription for server \'" << server_name << '\'' );

  subscribe_oks_changes();

  request_backup();
}


void
RDBServer::delete_server_subscription(const std::string &server_name)
{
  // note: notify() may already get p_mutex
  // use this local one to protect from 2 threads deleting subscription in result of notify()

  std::lock_guard lock(s_unsubscribe_mutex);

  std::map<std::string, rdb::rdb_callback_var>::iterator i = m_server_subscriptions.find(server_name);

  if (i == m_server_subscriptions.end())
    {
      std::ostringstream text;
      text << "cannot remove subscription for server \'" << server_name << "\' since it does not exist";
      ers::Message issue(ERS_HERE, text.str());
      ers::log(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  else
    {
      m_server_subscriptions.erase(i);
      unsubscribe_oks_changes();
    }

  request_backup();
}


void
RDBServer::unsubscribe_rdb(const char * server_name)
{
  std::lock_guard lock(p_mutex);
  delete_server_subscription(server_name);
}


void
RDBServer::read_complete_db(const std::string& master_rdb_server_name, bool try_unsubscribe)
{
  RDB_SERVER_PROFILING_INFO("read_complete_db")

  m_master_server_name = master_rdb_server_name;

  const char * pn = partition().name().c_str();    // partition name
  const char * mn = m_master_server_name.c_str();  // master server name


    // search master RDB server

  ERS_LOG( "reading complete database from rdb server \'" << m_master_server_name << "\' (in partition \'" << pn << "\')..." );

  rdb::cursor_var c;

  try {
    c = partition().lookup<rdb::cursor>(m_master_server_name);
  }
  catch ( CORBA::SystemException & ex ) {
    throw daq::rdb::FailedReadDB(ERS_HERE, mn, pn, "lookup failed", daq::ipc::CorbaSystemException(ERS_HERE, &ex));
  }
  catch ( ers::Issue & ex ) {
    throw daq::rdb::FailedReadDB(ERS_HERE, mn, pn, "lookup failed", ex);
  }

  if (try_unsubscribe) {
    try {
      c->unsubscribe_rdb(name().c_str());
    }
    catch ( CORBA::SystemException & ex ) {
      ers::error(daq::rdb::CannotUnsubscribeServer(ERS_HERE, name(), m_master_server_name, partition().name(), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
    }
    catch( rdb::CannotProceed& ex ) {
      ers::warning(daq::rdb::CannotUnsubscribeServer(ERS_HERE, name(), m_master_server_name, partition().name(), *daq::idl2ers_issue(ex.issue)));
    }
  }

  rdb::RDBCompleteDB_var db;
  std::lock_guard lock(p_mutex);  // TEST

  try {
    m_server_callback_info = new RDBServerCallbackInfo(this);
    c->get_complete_db(db, name().c_str(), m_server_callback_info->_this());
  }
  catch ( CORBA::SystemException & ex ) {
    throw daq::rdb::FailedReadDB(ERS_HERE, mn, pn, "remote method failed", daq::ipc::CorbaSystemException(ERS_HERE, &ex));
  }


    // create schema and data files
    
  {
    std::ostringstream basename;
    basename << "/tmp/rdb:" << m_master_server_name << '@' << partition().name() << ':' << getpid() << ':' << get_timestamp() << '.';

    std::string tmp_schema_name = basename.str() + "schema.xml";
    std::string tmp_data_name = basename.str() + "data.xml";

    try {
      m_schema_fh = m_kernel->new_schema(tmp_schema_name);
      m_data_fh = m_kernel->new_data(tmp_data_name);
    }
    catch(oks::exception& ex) {
      throw daq::rdb::FailedReadDB(ERS_HERE, mn, pn, ex.what());
    }
  }

  create_oks_data(db.in());

  ERS_LOG( "read complete database was successful" );
}

inline void set_string(const rdb::RDBNameList& strings_dictionary, std::vector<OksString *>& all_strings, uint32_t index, OksData *d)
{
  if(all_strings[index]) {
    d->data.STRING->assign(*all_strings[index]);
  }
  else {
    d->data.STRING->assign(static_cast<const char *>(strings_dictionary[index]));
    all_strings[index] = d->data.STRING;
  }
}

static void
remove_temporal_db(const OksFile * file)
{
  int status = unlink(file->get_full_file_name().c_str());

  if (status != 0)
    {
      std::ostringstream text;
      text << "failed to remove temporal database file \'" << file->get_full_file_name() << "\':\n" << oks::strerror(errno);
      ers::error(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
    }
}

void
RDBServer::remove_temporal_db_files()
{
  // remove lock files to fix issue https://its.cern.ch/jira/browse/ADTCC-127
  try
    {
      m_data_fh->unlock();
      m_schema_fh->unlock();
    }
  catch (oks::exception& ex)
    {
      ers::error(daq::rdb::FailedCreateData(ERS_HERE, "failed unlock active file", ex));
    }

  remove_temporal_db(m_schema_fh);
  remove_temporal_db(m_data_fh);
}

void
RDBServer::create_oks_data ( const rdb::RDBCompleteDB& cdb )
{
  rdb::ServerProfiler profiler(true, "RDBServer::create_oks_data");

  clean_cache();

  unsigned long idx;

  const rdb::RDBCompleteDBi& db = cdb[0];

  std::unique_lock wlock(m_kernel->get_mutex());


    // create schema

  try {

    for(idx = 0; idx < db.classes.length(); ++idx) {
      OksClass * c = new OksClass(
        m_kernel,
        static_cast<const char *>(db.classes[idx].schema.name),
        static_cast<const char *>(db.classes[idx].schema.description),
        db.classes[idx].schema.isAbstract
      );

      if(db.classes[idx].schema.direct_superclasses.length() > 0) {
        for(unsigned long idx2 = 0; idx2 < db.classes[idx].schema.direct_superclasses.length(); ++idx2) {
          c->k_add_super_class(static_cast<const char *>(db.classes[idx].schema.direct_superclasses[idx2]));
        }
      }

      for(unsigned long idx2 = 0; idx2 < db.classes[idx].schema.attributes.length(); ++idx2) {
        const rdb::RDBAttribute& a = db.classes[idx].schema.attributes[idx2];
        if (a.isDirect)
          c->k_add(
            new OksAttribute(
              static_cast<const char *>(a.name),
              static_cast<const char *>(a.type),
              a.isMultiValue,
              static_cast<const char *>(a.range),
              static_cast<const char *>(a.initValue),
              static_cast<const char *>(a.description),
              a.isNotNull,
              (
                a.intFormat == rdb::int_format_hex ? OksAttribute::Hex :
                a.intFormat == rdb::int_format_oct ? OksAttribute::Oct :
                OksAttribute::Dec
              )
            )
          );
      }

      for(unsigned long idx3 = 0; idx3 < db.classes[idx].schema.relationships.length(); ++idx3) {
        const rdb::RDBRelationship& r = db.classes[idx].schema.relationships[idx3];
        if (r.isDirect)
          c->k_add(
            new OksRelationship(
              static_cast<const char *>(r.name),
              static_cast<const char *>(r.classid),
              (r.isNotNull ? OksRelationship::One : OksRelationship::Zero),
              (r.isMultiValue ? OksRelationship::Many : OksRelationship::One),
              r.isAggregation,
              r.isExclusive,
              r.isDependent,
              r.isOrdered,
              static_cast<const char *>(r.description)
            )
          );
      }
    }

    m_kernel->registrate_all_classes(true);
  }
  catch(oks::exception& ex) {
    ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, "internal error reading complete database", ex));
    abort();
  }

    // create array to store pointers to all objects

  int32_t number_of_objects(0);

  for(idx = 0; idx < db.classes.length(); ++idx) {
    number_of_objects += db.classes[idx].objects.length();
  }

  if(number_of_objects == 0) {
    remove_temporal_db_files();

    unsigned long t = profiler.now(true);

    ERS_LOG("\n"
      "complete in:       " << std::setprecision(4) << (float)t / 1000000 << " seconds\n"
      "number of classes: " << m_kernel->classes().size()
    );

    return;
  }

  std::unique_ptr<OksObject*[]> objects(new OksObject * [number_of_objects]);

    // create empty objects

  number_of_objects = 0;

  for(idx = 0; idx < db.classes.length(); ++idx) {
    if(const size_t len = db.classes[idx].objects.length()) {
      OksClass * c = m_kernel->find_class(static_cast<const char *>(db.classes[idx].schema.name));

      if(c->objects()->bucket_count() < len) {
        const_cast<OksObject::Map*>(c->objects())->rehash(len);
      }

      for(unsigned long idx2 = 0; idx2 < len; ++idx2) {
	objects[number_of_objects++] = new OksObject(c, static_cast<const char *>(db.classes[idx].objects[idx2]), true);
      }
    }
  }


    // create strings dictionary

  const rdb::RDBNameList& strings_dictionary(db.strings_dictionary);
  std::vector<OksString *> all_strings(strings_dictionary.length(), 0);


    // fill attributes and link relationships

  unsigned long num_of_booleans(0), num_of_s8_ints(0), num_of_u8_ints(0), num_of_s16_ints(0),
                num_of_u16_ints(0), num_of_s32_ints(0), num_of_u32_ints(0), num_of_s64_ints(0),
                num_of_u64_ints(0), num_of_floats(0), num_of_doubles(0);

  unsigned long num_of_boolean_lists(0), num_of_s8_int_lists(0), num_of_u8_int_lists(0),
                num_of_s16_int_lists(0), num_of_u16_int_lists(0), num_of_s32_int_lists(0),
                num_of_u32_int_lists(0), num_of_s64_int_lists(0), num_of_u64_int_lists(0),
                num_of_float_lists(0), num_of_double_lists(0);


  for(int32_t idx2 = 0; idx2 < number_of_objects; ++idx2) {
    OksObject * o = objects[idx2];
    const OksClass * c = o->GetClass();
    
    OksDataInfo di(0, (const OksAttribute *)(0));
    OksData * d(o->GetAttributeValue(&di));

    for(std::list<OksAttribute *>::const_iterator ai = c->all_attributes()->begin(); ai != c->all_attributes()->end(); ++ai, ++d) {

      bool is_mv = (*ai)->get_is_multi_values();

      switch((*ai)->get_data_type()) {

        case OksData::string_type:
          if(is_mv == false) {
	    uint32_t v = db.u32_ints[num_of_u32_ints++];
	    if(all_strings[v]) {
	      d->data.STRING->assign(*all_strings[v]);
	    }
	    else {
	      d->data.STRING->assign(static_cast<const char *>(strings_dictionary[v]));
	      all_strings[v] = d->data.STRING;
	    }
          }
	  else {
	    const rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) {
	      uint32_t v = seq[i];
	      if(all_strings[v]) {
	        d->data.LIST->push_back(new OksData(*all_strings[v]));
	      }
	      else {
		d->data.LIST->push_back(new OksData(all_strings[v] = new OksString(static_cast<const char *>(strings_dictionary[v]))));
	      }
	    }
	  }
	  break;

	case OksData::u32_int_type:
          if(is_mv == false) {
            d->data.U32_INT = db.u32_ints[num_of_u32_ints++];
          }
	  else {
	    const rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<uint32_t>(seq[i]))); }
          }
          break;

	case OksData::date_type:
	  if(is_mv == false) {
	    uint32_t v = db.u32_ints[num_of_u32_ints++];
	    try {
	      d->Set(boost::gregorian::from_string(static_cast<const char *>(strings_dictionary[v])));
	    }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"date\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << " from \'" << static_cast<const char *>(strings_dictionary[v]) << "\' (idx: " << v << "):\n" << ex.what();
              ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	      abort();
            }
	  }
	  else {
	    const rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) {
	      uint32_t v = seq[i];
	      try {
	        d->data.LIST->push_back(new OksData(boost::gregorian::from_string(static_cast<const char *>(strings_dictionary[v]))));
	      }
              catch(std::exception& ex) {
                std::ostringstream text;
                text << "failed to set \"date\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << " from \'" << static_cast<const char *>(strings_dictionary[v]) << "\' (idx: " << v << "):\n" << ex.what();
                ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	        abort();
              }
	    }
	  }
	  break;
	
	case OksData::time_type:
	  if(is_mv == false) {
	    uint32_t v = db.u32_ints[num_of_u32_ints++];
	    try {
	      d->Set(boost::posix_time::time_from_string(static_cast<const char *>(strings_dictionary[v])));
	    }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"time\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << " from \'" << static_cast<const char *>(strings_dictionary[v]) << "\' (idx: " << v << "):\n" << ex.what();
              ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	      abort();
            }
	  }
	  else {
	    const rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) {
	      uint32_t v = seq[i];
	      try {
	        d->data.LIST->push_back(new OksData(boost::posix_time::time_from_string(static_cast<const char *>(strings_dictionary[v]))));
	      }
              catch(std::exception& ex) {
                std::ostringstream text;
                text << "failed to set \"time\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << " from \'" << static_cast<const char *>(strings_dictionary[v]) << "\' (idx: " << v << "):\n" << ex.what();
                ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	        abort();
              }
	    }
	  }
	  break;


	case OksData::class_type:
	  if(is_mv == false) {
	    uint32_t v = db.u32_ints[num_of_u32_ints++];
	    try {
              d->type = OksData::class_type;
              d->data.CLASS = 0;
              d->SetValue(static_cast<const char *>(strings_dictionary[v]), *ai);
	    }
            catch(oks::exception& ex) {
              std::ostringstream text;
              text << "failed to set \"class\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << ":\n" << ex.what();
              ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	      abort();
            }
	  }
	  else {
	    const rdb::RDB_u32_ints& seq(db.u32_int_lists[num_of_u32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) {
	      uint32_t v = seq[i];
	      try {
	        OksData * d2  = new OksData();
                d2->type = OksData::class_type;
                d2->data.CLASS = 0;
                d2->SetValue(static_cast<const char *>(strings_dictionary[v]), *ai);
	        d->data.LIST->push_back(d2);
	      }
              catch(oks::exception& ex) {
                std::ostringstream text;
                text << "failed to set \"class\" value of attribute \'" << (*ai)->get_name() << "\' of " << o << " (idx: " << v << "):\n" << ex.what();
                ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
 	        abort();
              }
	    }
	  }
	  break;

	case OksData::s32_int_type:
          if(is_mv == false) {
            d->data.S32_INT = db.s32_ints[num_of_s32_ints++];
          }
	  else {
	    const rdb::RDB_s32_ints& seq(db.s32_int_lists[num_of_s32_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<int32_t>(seq[i]))); }
          }
          break;


	case OksData::bool_type:
          if(is_mv == false) {
            d->data.BOOL = db.booleans[num_of_booleans++];
          }
	  else {
	    const rdb::RDB_booleans& seq(db.boolean_lists[num_of_boolean_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<bool>(seq[i]))); }
          }
          break;


	case OksData::s16_int_type:
          if(is_mv == false) {
            d->data.S16_INT = db.s16_ints[num_of_s16_ints++];
          }
	  else {
	    const rdb::RDB_s16_ints& seq(db.s16_int_lists[num_of_s16_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<int16_t>(seq[i]))); }
          }
          break;


	case OksData::u16_int_type:
          if(is_mv == false) {
            d->data.U16_INT = db.u16_ints[num_of_u16_ints++];
          }
	  else {
	    const rdb::RDB_u16_ints& seq(db.u16_int_lists[num_of_u16_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<uint16_t>(seq[i]))); }
          }
          break;


	case OksData::enum_type:
          if(is_mv == false) {
            d->data.ENUMERATION = (*ai)->get_enum_string(db.u16_ints[num_of_u16_ints++]);
            d->type = OksData::enum_type;
          }
	  else {
	    const rdb::RDB_u16_ints& seq(db.u16_int_lists[num_of_u16_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) {
	      OksData * d2  = new OksData();
              d2->type = OksData::enum_type;
              d2->data.ENUMERATION = (*ai)->get_enum_string(seq[i]);
	      d->data.LIST->push_back(d2);
	    }
          }
          break;


	case OksData::s64_int_type:
          if(is_mv == false) {
            d->data.S64_INT = db.s64_ints[num_of_s64_ints++];
          }
	  else {
	    const rdb::RDB_s64_ints& seq(db.s64_int_lists[num_of_s64_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<int64_t>(seq[i]))); }
          }
          break;


	case OksData::u64_int_type:
          if(is_mv == false) {
            d->data.U64_INT = db.u64_ints[num_of_u64_ints++];
          }
	  else {
	    const rdb::RDB_u64_ints& seq(db.u64_int_lists[num_of_u64_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<uint64_t>(seq[i]))); }
          }
          break;


	case OksData::s8_int_type:
          if(is_mv == false) {
            d->data.S8_INT = db.s8_ints[num_of_s8_ints++];
          }
	  else {
	    const rdb::RDB_s8_ints& seq(db.s8_int_lists[num_of_s8_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<int8_t>(seq[i]))); }
          }
          break;


	case OksData::u8_int_type:
          if(is_mv == false) {
            d->data.U8_INT = db.u8_ints[num_of_u8_ints++];
          }
	  else {
	    const rdb::RDB_u8_ints& seq(db.u8_int_lists[num_of_u8_int_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(static_cast<uint8_t>(seq[i]))); }
          }
          break;


	case OksData::float_type:
          if(is_mv == false) {
            d->data.FLOAT = db.floats[num_of_floats++];
          }
	  else {
	    const rdb::RDB_floats& seq(db.float_lists[num_of_float_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
          }
          break;


	case OksData::double_type:
          if(is_mv == false) {
            d->data.DOUBLE = db.doubles[num_of_doubles++];
          }
	  else {
	    const rdb::RDB_doubles& seq(db.double_lists[num_of_double_lists++]);
	    for(unsigned int i = 0; i < seq.length(); ++i) { d->data.LIST->push_back(new OksData(seq[i])); }
          }
          break;

        default:
	  {
            std::ostringstream text;
            text << "got unexpected type of attribute " << (*ai)->get_name() << '@' << c->get_name();
            ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str()));
            abort();
	  }
      }
    }


    for(std::list<OksRelationship *>::const_iterator ri = c->all_relationships()->begin(); ri != c->all_relationships()->end(); ++ri, ++d) {
      if((*ri)->get_high_cardinality_constraint() != OksRelationship::Many) {
        int32_t v = db.s32_ints[num_of_s32_ints++];
	if(v >= 0) {
          OksObject * robj = objects[v];
	
	  try {
	    robj->add_RCR(o, *ri);
            d->Set(robj);
	  }
          catch(oks::exception& ex) {
            std::ostringstream text;
            text << "cannot modify relationship value \'" << (*ri)->get_name() << "\' of object " << o;
            ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str(), ex));
	    abort();
          }
	}
      }
      else {
        const rdb::RDB_s32_ints& seq(db.s32_int_lists[num_of_s32_int_lists++]);
        for(unsigned int i = 0; i < seq.length(); ++i) {
	  OksObject * robj = objects[seq[i]];

          try {
            robj->add_RCR(o, *ri);
            d->data.LIST->push_back(new OksData(robj));
          }
          catch(oks::exception& ex) {
            std::ostringstream text;
            text << "cannot modify relationship value \'" << (*ri)->get_name() << "\' of object " << o;
            ers::fatal(daq::rdb::FailedCreateData(ERS_HERE, text.str().c_str(), ex));
	    abort();
          }
	}
      }
    }
  }

  remove_temporal_db_files();

  unsigned long t = profiler.now(true);

  ERS_LOG("\n"
    "complete in:             " << std::setprecision(4) << (float)t / 1000000 << " seconds\n"
    "number of classes:       " << m_kernel->classes().size() << "\n"
    "number of objects:       " << m_kernel->objects().size() << "\n"
    "number of data items:    " <<
      num_of_booleans + num_of_s8_ints + num_of_u8_ints + num_of_s16_ints + num_of_u16_ints + num_of_s32_ints +
      num_of_u32_ints + num_of_s64_ints + num_of_u64_ints + num_of_floats + num_of_doubles + num_of_boolean_lists +
      num_of_s8_int_lists + num_of_u8_int_lists + num_of_s16_int_lists + num_of_u16_int_lists + num_of_s32_int_lists +
      num_of_u32_int_lists + num_of_s64_int_lists + num_of_u64_int_lists + num_of_float_lists + num_of_double_lists << "\n"
    "strings dictionary size: " << all_strings.size()
  );
}


CORBA::Long
RDBServerCallbackInfo::inform (const rdb::RDBDataChanges& data_changes)
{
  return m_server->apply_changes(data_changes);
}


CORBA::Long
RDBServer::apply_changes (const rdb::RDBDataChanges& data_changes)
{
  bool notify_clients = (m_changes_holder.m_created.empty() && m_changes_holder.m_modified.empty() && m_changes_holder.m_removed.empty()) ? true : false;

  ERS_DEBUG( 2, "got server callback with " << data_changes );

    // clean cache of queries and set OksKernel lock

  clean_cache();

  std::unique_lock wlock(m_kernel->get_mutex());

  rdb::ServerProfiler profiler(true, "RDBServerCallbackInfo::inform");

  unsigned long idx;


    // calculate set of removing objects

  OksObject::FSet to_be_removed;

  for(idx = 0; idx < data_changes.length(); idx++) {
    const rdb::RDBChanges& ch(data_changes[idx]);
    if(ch.removed_objects.length() > 0) {
      if(OksClass * c = m_kernel->find_class(static_cast<const char *>(ch.class_name))) {
        for(unsigned long idx2 = 0; idx2 < ch.removed_objects.length(); ++idx2) {
          const char * obj_name(static_cast<const char *>(ch.removed_objects[idx2]));

          if(OksObject * o = c->get_object(obj_name)) {
	    if(notify_clients) m_changes_holder.m_removed[c].insert(obj_name);
            to_be_removed.insert(o);
          }
          else {
            std::ostringstream text;
            text << "internal error: cannot find object \'" << obj_name << '@' << c->get_name() << '\'';
            ers::error(daq::rdb::ReloadCommandFailed(ERS_HERE, name(), partition().name(), text.str()));
            continue;
          }
        }
      }
    }
  }


    // unset RCRs on removing objects

  if(!to_be_removed.empty()) {
    drop_RCRs(to_be_removed);
  }


    // 1. create new objects and set their attributes
    // 2. update attributes of modified objects
    // 3. clean RCR of modified objects

  for(idx = 0; idx < data_changes.length(); idx++) {
    const rdb::RDBChanges& ch(data_changes[idx]);
    const char * class_name(static_cast<const char *>(ch.class_name));

    if(OksClass * c = m_kernel->find_class(class_name)) {
      OksDataInfo di(0, (const OksAttribute *)(0));

        // 1. process created objects

      for(unsigned long idx2 = 0; idx2 < ch.created_objects.length(); ++idx2) {
        const rdb::RDBObjectCompactValue& cv(ch.created_objects[idx2]);
	OksObject * o = new OksObject(c, static_cast<const char *>(cv.name), true);
        OksData * d(o->GetAttributeValue(&di));

	unsigned long sv_idx = 0, mv_idx = 0;
	
	for(std::list<OksAttribute *>::const_iterator ia = c->all_attributes()->begin(); ia != c->all_attributes()->end(); ++ia, ++d) {
	  if((*ia)->get_is_multi_values()) {
            rdb::rdb2oks(cv.avs[mv_idx++], d, o, *ia);
	  }
	  else {
            rdb::rdb2oks(cv.av[sv_idx++], d, o, *ia);
	  }
	}

	if(notify_clients) { m_changes_holder.m_created.push_back(o); }
      }

        // 2,3. process modified objects

      for(unsigned long idx3 = 0; idx3 < ch.updated_objects.length(); ++idx3) {
        const rdb::RDBObjectCompactValue& cv(ch.updated_objects[idx3]);
	const char * obj_name(static_cast<const char *>(cv.name));

	if(OksObject * o = c->get_object(obj_name)) {
          OksData * d(o->GetAttributeValue(&di));

	  unsigned long sv_idx = 0, mv_idx = 0;

	  for(std::list<OksAttribute *>::const_iterator ia = c->all_attributes()->begin(); ia != c->all_attributes()->end(); ++ia, ++d) {
	    if((*ia)->get_is_multi_values()) {
	      rdb::clear_list(d->data.LIST, 0, 0);
              rdb::rdb2oks(cv.avs[mv_idx++], d, o, *ia);
	    }
	    else {
              rdb::rdb2oks(cv.av[sv_idx++], d, o, *ia);
	    }
	  }

          for(std::list<OksRelationship *>::const_iterator ir = c->all_relationships()->begin(); ir != c->all_relationships()->end(); ++ir, ++d) {
            if((*ir)->get_is_composite()) {
              ERS_DEBUG( 0 , "clean composite relationship \"" << (*ir)->get_name() << "\" of updated object " << o << ": " << *d );
            }
            else {
              ERS_DEBUG( 3 , "clean non-composite relationship \"" << (*ir)->get_name() << "\" of updated object " << o << ": " << *d );
            }

            if(d->type == OksData::list_type) {
	      rdb::clear_list(d->data.LIST, o, *ir);
            }
            else if(d->type == OksData::object_type && d->data.OBJECT) {
	      d->data.OBJECT->remove_RCR(o, *ir);
	      d->data.OBJECT = 0;
            }
          }

	  if(notify_clients) { m_changes_holder.m_modified.insert(o); }
	}
	else {
          std::ostringstream text;
          text << "internal error: cannot find object \'" << obj_name << '@' << class_name << '\'';
          ers::error(daq::rdb::ReloadCommandFailed(ERS_HERE, name(), partition().name(), text.str()));

          if(notify_clients) m_changes_holder.clear();

          return -1;
	}
      }
    }
    else {
      std::ostringstream text;
      text << "internal error: cannot find class \'" << class_name << '\'';
      ers::error(daq::rdb::ReloadCommandFailed(ERS_HERE, name(), partition().name(), text.str()));

      if(notify_clients) m_changes_holder.clear();

      return -1;
    }
  }


    // set relationships of created and updated objects

  for (idx = 0; idx < data_changes.length(); idx++)
    {
      const rdb::RDBChanges &ch(data_changes[idx]);
      const char *class_name(static_cast<const char*>(ch.class_name));
      OksClass *c = m_kernel->find_class(class_name);
      OksDataInfo di(c->all_attributes()->size(), (const OksRelationship*) (0));

      const rdb::RDBObjectCompactValueList *cobjs[2] =
        { &ch.created_objects, &ch.updated_objects };

      for (unsigned int i = 0; i < 2; ++i)
        {
          for (unsigned long idx2 = 0; idx2 < cobjs[i]->length(); ++idx2)
            {
              const rdb::RDBObjectCompactValue &cv((*cobjs[i])[idx2]);
              OksObject *o = c->get_object(static_cast<const char*>(cv.name));
              OksData *d(o->GetRelationshipValue(&di));

              unsigned long r_idx = 0;

              for (std::list<OksRelationship*>::const_iterator ir = c->all_relationships()->begin(); ir != c->all_relationships()->end(); ++ir, ++d)
                {
                  const rdb::RDBObjectList &objs(cv.rv[r_idx++]);

                  for (unsigned i = 0; i < objs.length(); ++i)
                    {
                      const char *it_obj_id(static_cast<const char*>(objs[i].name));
                      const char *it_class_name(static_cast<const char*>(objs[i].classid));

                      const OksClass *rclass = m_kernel->find_class(it_class_name);
                      OksObject *robj = rclass ? rclass->get_object(it_obj_id) : nullptr;

                      if (d->type == OksData::list_type)
                        {
                          if (robj)
                            {
                              d->data.LIST->push_back(new OksData(robj));
                              robj->add_RCR(o, *ir);
                            }
                          else if (rclass)
                            d->data.LIST->push_back(new OksData(rclass, it_obj_id));
                          else
                            d->data.LIST->push_back(new OksData(it_class_name, it_obj_id));
                        }
                      else
                        {
                          if (robj)
                            {
                              d->Set(robj);
                              robj->add_RCR(o, *ir);
                            }
                          else if (rclass)
                            d->Set(rclass, it_obj_id);
                          else
                            d->Set(it_class_name, it_obj_id);

                          break;
                        }
                    }
                }
            }
        }
    }


  unsigned long created_objs_count(m_changes_holder.m_created.size());
  unsigned long modified_objs_count(m_changes_holder.m_modified.size());
  unsigned long removed_objs_count = 0;


    // process removing objects

  if(!to_be_removed.empty()) {

      // unlink removing objects (to avoid exceptions, when call OksObject::destroy() methods)

    unbind_references(to_be_removed);


      // remove deleted objects
    
    for(OksObject::FSet::iterator i = to_be_removed.begin(); i != to_be_removed.end(); ++i) {
      try {
        OksObject::destroy(*i, true);
        removed_objs_count++;
      }
      catch(const oks::exception& ex) {
        ers::error(daq::rdb::ReloadCommandFailed(ERS_HERE, name(), partition().name(), "", ex));
      }
    }
  }


  unsigned long num_of_notified_clients = 0;

  if(notify_clients) {
    num_of_notified_clients = notify();
  }

  const char * msg1(0);
  std::string msg2;

  if(m_master_server_name.empty()) {
    msg1 = "Mirrored ";
    msg2 = "main server";
  }
  else {
    msg1 = "";
    msg2 = "master server \'";
    msg2 += m_master_server_name;
    msg2 += '\'';
  }

  ERS_LOG(
    msg1 << "RDB Server \'" << name() << '@' << partition().name() << "\' has been reloaded by " << msg2 << ". "
    "Number of created objects: " << created_objs_count << "; updated objects: " << modified_objs_count <<
    "; removed objects " << removed_objs_count << ". Number of notified clients: " << num_of_notified_clients << '.'
  );

  return profiler.now(true);
}


struct PushCommand {
  rdb::RDBCompleteDB * m_db_ptr;
  std::string m_server_name;
  std::string m_partition_name;
  bool * m_confirmation;
  const std::string& m_mirror_partition_name;

  PushCommand(rdb::RDBCompleteDB * p, const std::string& server_name, const std::string& p_name, bool * confirmation, const std::string& mirror_p_name) :
    m_db_ptr                (p),
    m_server_name           (server_name),
    m_partition_name        (p_name),
    m_confirmation          (confirmation),
    m_mirror_partition_name (mirror_p_name) { ; }

  void operator()() {
    try {
      IPCPartition mirror_partition( m_mirror_partition_name );
      rdb::cursor_var c = mirror_partition.lookup<rdb::cursor>(m_server_name);
      c->push_complete_db ( m_server_name.c_str(), m_partition_name.c_str(), *m_db_ptr );
      if(m_confirmation) {
        *m_confirmation = false;
      }
      ERS_LOG("push data on server \'" << m_server_name << "\' running in \'" << m_mirror_partition_name << "\' partition" );
    }
    catch( daq::ipc::Exception & ex ) {
      ERS_LOG( "Mirror server \'" << m_server_name << "\' for partition \'" << m_partition_name << "\' is not found: " << ex );
    }
    catch ( CORBA::SystemException & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), daq::ipc::CorbaSystemException(ERS_HERE, &ex) ) );
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), *daq::idl2ers_issue(ex.issue) ) );
    }
    catch ( ers::Issue & ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str(), ex ) );
    }
    catch( ... ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, m_server_name.c_str(), m_mirror_partition_name.c_str() ) );
    }

    delete m_db_ptr;
  }

};


void
RDBServer::push_data(bool * confirmation)
{
  if(m_push_mirror_partition) {
    ERS_DEBUG(2, "try to push data" );

    try {
      subscribe_oks_changes();

      rdb::RDBCompleteDB * db_ptr(0);
      get_complete_db(db_ptr);

      PushCommand command(db_ptr, name(), partition().name(), confirmation, m_mirror_partition_name);
      std::thread thrd(command);
      thrd.detach();
    }
    catch ( std::exception& ex ) {
      ers::error( daq::rdb::PushCommandFailed( ERS_HERE, name().c_str(), m_mirror_partition_name.c_str(), ex ) );
    }
  }
  else {
    ERS_DEBUG(2, "the \'push-mirror-partition\' mode is disabled" );
  }
}

void
RDBServer::try_push_mirror_data() noexcept
{
  if(size_t number_of_classes = m_kernel->number_of_classes()) {
    try {
      static bool is_first_push(true);

      IPCPartition mirror_partition( m_mirror_partition_name );
      rdb::cursor_var c = mirror_partition.lookup<rdb::cursor>(name());

        // check if it is a first push

      if(is_first_push) {
        ERS_LOG("push complete database on mirror \'" << m_mirror_partition_name << '\'' );
        push_data(&is_first_push);  // in case of success is_first_push will be "false"
      }

        // check number of classes in mirror partition

      else {
        rdb::RDBClassList_var l;
        c->get_all_classes(l);

        if(l->length() != number_of_classes) {
          ERS_LOG("push complete database to sync number of classes on this server (" << number_of_classes << ") and mirror \'" << m_mirror_partition_name << "\' partition server (" << l->length() << ')' );
          push_data();
        }
      }
    }
    catch( const daq::ipc::Exception & ex ) {
      ERS_LOG( "Mirror server \'" << name() << "\' for partition \'" << partition().name() << "\' is not found: " << ex );
    }
    catch ( CORBA::SystemException & ex ) {
      ers::error( daq::rdb::GetAllClassesCommandFailed( ERS_HERE, name().c_str(), m_mirror_partition_name.c_str(), daq::ipc::CorbaSystemException(ERS_HERE, &ex) ) );
    }
  }
}


struct PushThreadCommand {
  RDBServer * m_server;
  size_t m_sleep_interval;

  PushThreadCommand(RDBServer * p, size_t n) :  m_server (p), m_sleep_interval (n) { ; }

  void operator()() {
    while (true) {
      m_server->try_push_mirror_data();
      sleep(m_sleep_interval);
    }
  }

};

void
RDBServer::start_push_mirror_data_thread()
{
  if(m_push_mirror_partition) {
    m_mirror_partition_name = partition().name() + ipc::partition::mirror_postfix;
    PushThreadCommand command(this, m_push_mirror_partition_retry_interval);
    std::thread thrd(command);
    thrd.detach();
  }
  else {
    ERS_DEBUG(2, "the \'push-mirror-partition\' mode is disabled" );
  }
}

void
RDBServer::unbind_references(const OksObject::FSet& rm_objs)
{
  const OksClass::Map& all_classes(m_kernel->classes());

  for(OksClass::Map::const_iterator i = all_classes.begin(); i != all_classes.end(); ++i) {
    OksClass * c(i->second);

    const unsigned short L1 = c->number_of_all_attributes();
    const unsigned short L2 = L1 + c->number_of_all_relationships();

    if(L1 == L2) continue; // class has no relationships  

    if(const OksObject::Map * objs = c->objects()) {
      for(OksObject::Map::const_iterator j = objs->begin(); j != objs->end(); ++j) {
        OksObject *o(j->second);
        bool to_be_removed = (rm_objs.find(o) != rm_objs.end());
        unsigned short l = L1;

        while(l < L2) {
          OksDataInfo odi(l, (OksRelationship *)0);
          OksData * d(o->GetRelationshipValue(&odi));

          if(d->type == OksData::object_type) {
            if(to_be_removed) {
              ERS_DEBUG( 3 , "clean relationship of removing object " << o << ": " << *d );
              d->Set((OksObject *)0);
            }
            else {
              if(rm_objs.find(d->data.OBJECT) != rm_objs.end()) {
                const OksClass * __c(d->data.OBJECT->GetClass());
                const OksString& __id(d->data.OBJECT->GetId());
                d->Set(__c,__id);
                ERS_DEBUG( 0 , "internal problem: set relationship of " << o << " => " << *d );
              }
            }
          }
          else if(d->type == OksData::list_type) {
            if(to_be_removed) {
              ERS_DEBUG( 3 , "clean relationship of removing object " << o << ": " << *d );
              d->data.LIST->clear();
            }
            else {
              for(OksData::List::const_iterator li = d->data.LIST->begin(); li != d->data.LIST->end(); ++li) {
                OksData * lid(*li);
                if(lid->type == OksData::object_type) {
                  if(rm_objs.find(lid->data.OBJECT) != rm_objs.end()) {
                    const OksClass * __c(lid->data.OBJECT->GetClass());
                    const OksString& __id(lid->data.OBJECT->GetId());
		    lid->Set(__c,__id);
                    ERS_DEBUG( 0 , "internal problem: set relationship of " << o << " => " << *d );
                  }
                }
              }
            }
          }

          ++l;
        }
      }
    }
  }
}


void
RDBServer::drop_RCRs(const OksObject::FSet& rm_objs)
{
  const OksClass::Map& all_classes(m_kernel->classes());

  for(OksClass::Map::const_iterator i = all_classes.begin(); i != all_classes.end(); ++i) {
    OksClass * c(i->second);

    if(const OksObject::Map * objs = c->objects()) {
      for(OksObject::Map::const_iterator j = objs->begin(); j != objs->end(); ++j) {
        if(std::list<OksRCR *> * refs = const_cast<std::list<OksRCR *> *>(j->second->reverse_composite_rels())) {
          for(std::list<OksRCR *>::iterator x = refs->begin(); x != refs->end();) {
            OksRCR * rcr = *x;
            OksObject * o = rcr->obj;
            if(rm_objs.find(o) != rm_objs.end()) {
              ERS_DEBUG( 0 , "clean RCR \"" << rcr->relationship->get_name() << "\"@" << j->second << " => " << o << " (removing this object)" );
              x = refs->erase(x);
              delete rcr;
            }
            else {
              ++x;
            }
          }
        }
      }
    }
  }
}


void
RDBServer::request_parent(const char * pool_server_name, const char * node, CORBA::String_out parent)
{
  static unsigned int p_count(0);

  std::lock_guard lock(m_pool_mutex);

  size_t size = m_pool_nodes.size();


    // if pool is empty, return self as a pool server name

  if(size == 0) {
    ERS_LOG( "pool is empty, assign self as pool server to client server \'" << pool_server_name << "\' (node: \'" << node << "\'), number-of-clients: " << ++p_count );
    parent = rdb::string_dup(name());
    return;
  }


    // once per pool cycle return self as a pool server name

  if(m_pool_idx == size) {
    m_pool_idx++;
    ERS_LOG( "assign self as pool server to client server \'" << pool_server_name << "\' (node: \'" << node << "\'), number-of-clients: " << ++p_count );
    parent = rdb::string_dup(name());
    return;
  }


    // reset pool

  if(m_pool_idx > size) {
    m_pool_idx = 0;
  }

  PoolNode& n(m_pool_nodes[m_pool_idx++]);

  parent = rdb::string_dup(n.p_name);
  n.p_number++;

  ERS_LOG( "assign pool server \'" << n.p_name << "\' (node: \'" << n.p_node << "\', number-of-clients: " << n.p_number << ") to client server \'" << pool_server_name << "\' (node: \'" << node << "\')" );

  request_backup();
}

void
RDBServer::register_parent(const char * pool_server_name, const char * _node)
{
  std::lock_guard lock(m_pool_mutex);

  std::string node(_node);

  if(!node.empty()) {
    for(unsigned int i = 0; i < m_pool_nodes.size(); ++i) {
      if(node == m_pool_nodes[i].p_node) {
        std::ostringstream text;
	text << "cannot register pool server \'" << pool_server_name << "\' on node \'" << _node << "\': server \'" << m_pool_nodes[i].p_name << "\' is already running on this node";
        ers::Message issue(ERS_HERE, text.str());
        ers::log(issue);
        throw rdb::CannotProceed(daq::ers2idl_issue(issue));
      }
    }

    bool moved = false;
    for(unsigned int i = 0; i < m_pool_nodes.size(); ++i) {
      if (pool_server_name == m_pool_nodes[i].p_name) {
        ERS_LOG( "move pool server \'" << pool_server_name << "\' from node \'" << m_pool_nodes[i].p_node << "\' to \'" << node << "\')" );
        m_pool_nodes[i].p_node = node;
        moved = true;
        break;
      }
    }

    if (!moved) {
      ERS_LOG( "add pool server \'" << pool_server_name << "\' (node: \'" << node << "\')" );
      m_pool_nodes.push_back(PoolNode(pool_server_name, node));
    }
    
    request_backup();
  }
  else {
    for(unsigned int i = 0; i < m_pool_nodes.size(); ++i) {
      if(pool_server_name == m_pool_nodes[i].p_name) {
        m_pool_nodes.erase (m_pool_nodes.begin()+i);

        ERS_LOG( "remove pool server \'" << pool_server_name << '\'' );

	  // decrement pool_idx, if it points to a node after removed one
	if(m_pool_idx > i) {
	  m_pool_idx--;
	}
	  // reset pool_idx, if removed removed node was last
	else if(m_pool_idx > m_pool_nodes.size()) {
	  m_pool_idx=0;
	}

	request_backup();

	return;
      }
    }

    std::ostringstream text;
    text << "cannot find pool server \'" << pool_server_name << '\'';
    ers::Message issue(ERS_HERE, text.str());
    ers::log(issue);
    throw rdb::CannotProceed(daq::ers2idl_issue(issue));
  }
}
