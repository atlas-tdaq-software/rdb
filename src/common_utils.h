#ifndef RDB_COMMON_UTILS_H
#define RDB_COMMON_UTILS_H

#include <map>
#include <string>

#include "rdb/rdb.hh"

      /**
       *  Describe functions shared by servers and proxy
       */

namespace rdb {

  /// more efficient than CORBA::string_dup, since the length of string is known

  char*
  string_dup(const std::string *str);

  inline char*
  string_dup(const std::string &str)
  {
    return string_dup(&str);
  }

  std::string query_params_to_string(const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames);

  std::string object_id_params_to_string(const char * class_name, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames);

  rdb::RDBObject * make_rdb_object(const std::string& class_name, const char * object_id);

  typedef std::map< std::string , std::pair<std::string, rdb::RDBClassValueList*> > ObjectIDCache;
  void destroy(ObjectIDCache *&);

  typedef std::map< std::string , std::pair<rdb::RDBObjectList*, rdb::RDBClassValueList*> > QueryCache;
  void destroy(QueryCache *&);
}

#endif
