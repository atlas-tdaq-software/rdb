#include <sstream>

#include <ipc/core.h>

#include <config/Configuration.h>

#include <sstream>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

#include <oks/file.h>
#include <oks/kernel.h>

#include "rdb_writer_impl.h"
#include "server_utils.h"

#include "rdb-writer-backup/Server.h"
#include "rdb-writer-backup/Object.h"
#include "rdb-writer-backup/Session.h"


struct BackupThreadCommand
{
  RDBWriter * m_server;
  size_t m_sleep_interval;
  size_t m_count;
  static std::thread * m_thread;
  static bool m_stop;

  BackupThreadCommand(RDBWriter * p, size_t n) :
      m_server(p), m_sleep_interval(n), m_count(0)
  {
    ;
  }

  void
  operator()()
  {
    while (!m_stop)
      {
        sleep(1);
        m_count++;
        if (m_count % m_sleep_interval == 0)
          {
            m_server->try_backup();
          }
      }
  }
};

bool BackupThreadCommand::m_stop(false);
std::thread * BackupThreadCommand::m_thread(0);

void
RDBWriter::start_backup_thread(size_t backup_interval)
{
  if (!m_backup_file.empty())
    {
      BackupThreadCommand command(this, backup_interval);
      BackupThreadCommand::m_thread = new std::thread(command);
    }
  else
    {
      ERS_DEBUG(2, "the \'backup\' mode is disabled");
    }
}

void
RDBWriter::stop_backup_thread()
{
  if (BackupThreadCommand::m_thread)
    {
      BackupThreadCommand::m_stop = true;
      BackupThreadCommand::m_thread->join();
      delete BackupThreadCommand::m_thread;
      BackupThreadCommand::m_thread = 0;
    }
}


static size_t s_backup_requests_counter = 0;
static std::mutex s_backup_requests_mutex;

void
RDBWriter::request_backup() const
{
  std::lock_guard lock(s_backup_requests_mutex);
  s_backup_requests_counter++;
}

void
RDBWriter::try_backup()
{
  {
    std::lock_guard lock(s_backup_requests_mutex);
    if(s_backup_requests_counter == 0) {
      ERS_DEBUG(2, "there are no changes for backup" );
      return;
    }
  }

  make_backup();
}

void
RDBWriter::make_backup()
{
  std::ostringstream log_text;
  log_text << "create backup file \'" << m_backup_file << '\'';

  {
    std::lock_guard lock(s_backup_requests_mutex);

    if (s_backup_requests_counter)
      {
        log_text << " (requests counter " << s_backup_requests_counter << ')';
        s_backup_requests_counter = 0;
      }
  }

  ers::log(ers::Message(ERS_HERE, log_text.str()));

  static std::mutex s_backup_mutex;
  std::lock_guard b_lock(s_backup_mutex);

  try
    {
      ::Configuration db(rdb::get_backup_spec());

      std::ostringstream s;
      s << m_backup_file << '.' << getpid();

      std::list<std::string> includes;

      if (const char * tdaq_dir = getenv("TDAQ_INST_PATH"))
        includes.push_back(std::string(tdaq_dir) + "/share/data/rdb/writer_backup.schema.xml");
      else
        includes.push_back("rdb/writer_backup.schema.xml");

      db.create(s.str(), includes);

      // prevent changes of the server structures

      std::shared_lock lock(m_mutex);

      // create description of server schema and data files

      rdb::writer_backup::Server * server = const_cast<rdb::writer_backup::Server *>(db.create<rdb::writer_backup::Server>(s.str(), name()));

      if (!m_kernel.schema_files().empty())
        {
          std::vector<std::string> files;

          for (const auto &i : m_kernel.schema_files())
            if (i.second->get_parent() == 0)
              files.push_back(i.second->get_well_formed_name());

          server->set_Schema_Files(files);
        }

      if (!m_kernel.data_files().empty())
        {
          std::vector<std::string> files;

          for (const auto &i : m_kernel.data_files())
            if (i.second->get_parent() == 0)
              files.push_back(i.second->get_well_formed_name());

          server->set_Data_Files(files);
        }

      server->set_Database_Version(m_kernel.get_repository_version());


      //  create descriptions of sessions

      for (const auto& j : m_sessions)
        {
          std::ostringstream str;
          str << j.first;

          std::lock_guard s_lock(j.second->m_session_mutex);

          rdb::writer_backup::Session * obj = const_cast<rdb::writer_backup::Session *>(db.create<rdb::writer_backup::Session>(*server, str.str()));

          obj->set_UserID(j.second->m_session_user_id);
          obj->set_Host(j.second->m_session_host);
          obj->set_ProcessID(j.second->m_session_process_id);
          obj->set_AppName(j.second->m_session_app_name);
          obj->set_CreationTimeStamp(j.second->m_session_creation_ts);
          obj->set_PingCallbackObject(IPCCore::objectToString(j.second->m_ping_cb, IPCCore::Corbaloc));
          obj->set_ServerUserName(j.second->m_user_id);
          obj->set_ServerUserPassword("");  // FIXME

          if (OksKernel * sk = j.second->m_session_kernel)
            {
              obj->set_RepositoryVersion(sk->get_repository_version());
              obj->set_RepositoryRoot(sk->get_user_repository_root());

              if (!sk->schema_files().empty())
                {
                  std::vector<std::string> files;

                  for (const auto &i : sk->schema_files())
                    if (i.second->get_parent() == nullptr)
                      files.push_back(i.second->get_well_formed_name());

                  obj->set_Schema_Files(files);
                }

              if (!sk->data_files().empty())
                {
                  std::vector<std::string> files;

                  for (const auto& i : sk->data_files())
                    if (i.second->get_parent() == nullptr)
                      files.push_back(i.second->get_well_formed_name());

                  obj->set_Data_Files(files);
                }

            }

          if (rdb::Subscription * s = j.second->m_subscription)
            {
              obj->set_SubscriptionObject(IPCCore::objectToString(s->m_cb, IPCCore::Corbaloc));
              obj->set_SubscriptionLookInSubclasses(s->m_look_in_subclasses);
              obj->set_SubscriptionParameter(s->m_parameter);
              obj->set_SubscriptionUserID(s->m_user_id);
              obj->set_SubscriptionHost(s->m_host);
              obj->set_SubscriptionProcessID(s->m_process_id);
              obj->set_SubscriptionAppName(s->m_app_name);
              obj->set_SubscriptionCreationTimeStamp(s->m_creation_ts);

              if (s->m_class_names.size())
                {
                  std::vector<std::string> classes;

                  for (std::set<std::string>::const_iterator i = s->m_class_names.begin(); i != s->m_class_names.end(); ++i)
                    {
                      classes.push_back(*i);
                    }

                  obj->set_SubscriptionClasses(classes);
                }

              if (s->m_objects_names.size())
                {
                  std::vector<const rdb::writer_backup::Object*> objs;

                  for (const auto& i : s->m_objects_names)
                    {
                      std::string id = i.first + '@' + str.str();
                      rdb::writer_backup::Object * o = const_cast<rdb::writer_backup::Object *>(db.create<rdb::writer_backup::Object>(*server, id));

                      objs.push_back(o);

                      o->set_ClassName(i.first);

                      std::vector<std::string> ids;

                      for (const auto& l : i.second)
                        ids.push_back(l);

                      o->set_ObjectIDs(ids);
                    }

                  obj->set_SubscriptionObjects(objs);
                }
            }
        }

      db.commit();

      rename(s.str().c_str(), m_backup_file.c_str());
    }
  catch (ers::Issue & ex)
    {
      ers::error(daq::rdb::FailedCreateBackupFile( ERS_HERE, m_backup_file.c_str(), ex) );
    }
}


void
RDBWriter::load_backup(const char * backup_file)
{
  ERS_LOG("restore state from backup file \'" << backup_file << '\'');

  try
    {
      ::Configuration db(rdb::get_backup_spec(backup_file));

      // get server object

      std::vector<const rdb::writer_backup::Server *> servers;
      db.get(servers);

      if (servers.size() > 1)
        {
          throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "backup file contains more than one \'Server\' object" );
        }
      else if(servers.empty())
        {
          throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "backup file does not contain \'Server\' object" );
        }

      const std::string& version = servers[0]->get_Database_Version();

      if (!version.empty())
        {
          std::string version_param, version_value;

          try
            {
              OksKernel::parse_config_version(getenv("TDAQ_DB_VERSION"), version_param, version_value);
            }
          catch (const std::exception &ex)
            {
              ers::error(daq::rdb::BadBackupData(ERS_HERE, backup_file, "failed to parse TDAQ_DB_VERSION", ex));
            }

          ERS_DEBUG(0, "version = " << version << ", TDAQ_DB_VERSION: " << version_param << ':' << version_value);

          if (version != version_value || version_param != "hash")
            m_kernel.update_repository("hash", version, OksKernel::RepositoryUpdateType::DiscardChanges);
        }

      // get schema and data files

      for (const auto& f : servers[0]->get_Schema_Files())
        m_kernel.load_schema(f);

      for (const auto& f : servers[0]->get_Data_Files())
        m_kernel.load_data(f);

      // restore sessions

      unsigned int subscription_count(0);

        {
          std::vector<const rdb::writer_backup::Session *> sessions;
          db.get(sessions);

          for (const auto& i : sessions)
            {
              rdb::ping_var cb;

              std::ostringstream issue;

              try
                {
                  CORBA::Object_var object = IPCCore::stringToObject(i->get_PingCallbackObject());
                  cb = rdb::ping::_narrow(object);
                  if (CORBA::is_nil(cb) || cb->_non_existent())
                    {
                      issue << "failed to restore client callback \'" << i->get_PingCallbackObject() << "\' (session " << i->UID() << "): object is null or non-existent";
                    }
                }
              catch (const CORBA::SystemException & ex)
                {
                  issue << "failed to restore client callback \'" << i->get_PingCallbackObject() << "\' (session " << i->UID() << "): caught CORBA exception \'" << &ex << '\'';
                }
              catch (...)
                {
                  issue << "failed to restore client callback \'" << i->get_PingCallbackObject() << "\' (session " << i->UID() << "): caught unknown exception";
                }

              if (!issue.str().empty())
                {
                  ers::warning(daq::rdb::BadBackupData(ERS_HERE, backup_file, issue.str().c_str()));
                  continue;
                }

              std::unique_ptr<RDBWriter::Session> session(new RDBWriter::Session(name(), partition().name(), i->get_UserID(), i->get_Host(), i->get_ProcessID(), i->get_AppName(), cb));
              session->m_session_creation_ts = i->get_CreationTimeStamp();

              session->m_user_id = i->get_ServerUserName();
              session->m_user_password = i->get_ServerUserPassword();


              // init session if needed

              if (!i->get_Schema_Files().empty())
                {
                  session->m_session_kernel = new OksKernel(false, false, false, false);

                  session->m_session_kernel->set_user_repository_root(i->get_RepositoryRoot(), i->get_RepositoryVersion());

                  if (!session->m_session_kernel->get_user_repository_root().empty())
                    {
                      struct stat buf;
                      if (int code = stat(session->m_session_kernel->get_user_repository_root().c_str(), &buf))
                        {
                          std::ostringstream issue;
                          issue << "cannot restore user repository dir (session " << i->UID() << "): stat(\'" << session->m_session_kernel->get_user_repository_root() << "\') returned " << code << " => \'" << strerror(errno) << '\'';
                          ers::warning(daq::rdb::BadBackupData(ERS_HERE, backup_file, issue.str().c_str()));
                          session->m_session_kernel->set_user_repository_root("");
                          errno = 0;
                          continue;
                        }
                    }

                  for (const auto& f : i->get_Schema_Files())
                    session->m_session_kernel->load_schema(f);

                  for (const auto& f : i->get_Data_Files())
                    session->m_session_kernel->load_data(f);

                  session->m_changes_holder = new rdb::ChangesHolder(session->m_session_kernel);
                }

              if (!i->get_SubscriptionObject().empty())
                {
                  rdb::callback_var s_cb;

                  std::ostringstream issue;

                  try
                    {
                      CORBA::Object_var object = IPCCore::stringToObject(i->get_SubscriptionObject());
                      s_cb = rdb::callback::_narrow(object);
                      if (CORBA::is_nil(s_cb) || s_cb->_non_existent())
                        {
                          issue << "failed to restore client subscription callback \'" << i->get_SubscriptionObject() << "\' (session " << i->UID() << "): object is null or non-existent";
                        }
                    }
                  catch (const CORBA::SystemException & ex)
                    {
                      issue << "failed to restore client subscription callback \'" << i->get_SubscriptionObject() << "\' (session " << i->UID() << "): caught CORBA exception \'" << &ex << '\'';
                    }
                  catch (...)
                    {
                      issue << "failed to restore client subscription callback \'" << i->get_SubscriptionObject() << "\' (session " << i->UID() << "): caught unknown exception";
                    }

                  if (!issue.str().empty())
                    {
                      ers::warning(daq::rdb::BadBackupData(ERS_HERE, backup_file, issue.str().c_str()));
                      continue;
                    }

                  rdb::Subscription::ObjectsMap objs;

                  for (const auto &j : i->get_SubscriptionObjects())
                    for (const auto &l : j->get_ObjectIDs())
                      objs[j->get_ClassName()].insert(l);

                  session->m_subscription = new rdb::Subscription(i->get_SubscriptionUserID(), i->get_SubscriptionHost(), i->get_SubscriptionProcessID(), i->get_SubscriptionAppName(), i->get_SubscriptionCreationTimeStamp(), i->get_SubscriptionClasses(),
                      i->get_SubscriptionLookInSubclasses(), objs, s_cb, i->get_SubscriptionParameter());

                  subscription_count++;
                }

              int64_t id = session->m_id = static_cast<int64_t>(strtoll(i->UID().c_str(), 0, 0));

              ERS_LOG("restore session " << session->m_id);

              m_sessions[id] = session.release();
            }
        }

      if (subscription_count > 0)
        set_oks_changes_subscriptions();
    }
  catch (oks::exception & ex)
    {
      std::ostringstream text;
      text << "caught oks exception\n" << ex.what();
      throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, text.str().c_str() );
    }
  catch (ers::Issue & ex)
    {
      throw daq::rdb::BadBackupFile( ERS_HERE, backup_file, "caught exception", ex );
    }
}

