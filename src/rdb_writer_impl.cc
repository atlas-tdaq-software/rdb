#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>
#include <memory>
#include <thread>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <ers2idl/ers2idl.h>
#include <ipc/signal.h>

#include <oks/access.h>
#include <oks/file.h>
#include <oks/object.h>
#include <oks/class.h>
#include <oks/relationship.h>

#include "rdb_writer_impl.h"

#include "server_utils.h"
#include "server_profiler.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ERS_DECLARE_ISSUE(
  rdb_writer,
  CannotRemoveDirectory,
  "cannot remove directory \'" << path << "\': caught Boost exception \"" << error << '\"',
  ((const char*)path)
  ((const char*)error)
)

ERS_DECLARE_ISSUE(
  rdb_writer,
  CannotCreateDirectory,
  "mkdir(\'" << path << "\') failed with code " << code << ": " << error,
  ((const char*)path)
  ((int)code)
  ((const char*)error)
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern RDBWriter * s_rdb_server;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RDBWriter::Session::Session(const std::string& server, const std::string& partition, const std::string& user_id, const std::string& host, long pid, const std::string& app_name, rdb::ping_ptr cb) :
  m_server_name          (server),
  m_partition_name       (partition),
  m_session_user_id      (user_id),
  m_session_host         (host),
  m_session_process_id   (pid),
  m_session_app_name     (app_name),
  m_session_creation_ts  (boost::posix_time::to_simple_string(boost::posix_time::second_clock::local_time())),
  m_session_kernel       (0),
  m_subscription         (0),
  m_ping_cb              (rdb::ping::_duplicate(cb)),
  m_changes_holder       (0)
{
  ;
}

RDBWriter::Session::~Session()
{
  clear();

  if(m_subscription) {
    delete m_subscription;
  }
}


  // this method creates OKS kernel of session if it was not yet created

OksKernel *
RDBWriter::Session::init(const OksKernel& kernel)
{
  if (!m_session_kernel)
    {
      try
        {
          m_session_kernel = new OksKernel(kernel, !kernel.get_user_repository_root().empty());
        }
      catch (const oks::exception& ex)
        {
          std::ostringstream text;
          text << "cannot initialise session \'" << m_id << "\' for update";
          ers::Message issue(ERS_HERE, text.str(), ex);
          ers::error(daq::rdb::UserRequestFailed(ERS_HERE, m_server_name, m_partition_name, issue));

          delete m_session_kernel;
          m_session_kernel = nullptr;

          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

      m_changes_holder = new rdb::ChangesHolder(m_session_kernel);

      // FIXME: check if backup is needed
      s_rdb_server->request_backup();

      s_rdb_server->set_oks_changes_subscriptions();
    }

  return m_session_kernel;
}

void
RDBWriter::Session::clear()
{
  if(m_session_kernel) {
    if(m_session_kernel->get_silence_mode() == false && ers::debug_level() < 1) {
      ERS_LOG( "destroy OKS kernel of session " << m_id << " in silent mode");
      m_session_kernel->set_silence_mode(true);
    }
    delete m_session_kernel;
    delete m_changes_holder;
    m_session_kernel = nullptr;
    m_changes_holder = nullptr;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


RDBWriter::RDBWriter(const char * name, IPCPartition &p) :
  IPCNamedObject<POA_rdb::writer>        (p, name),
  m_changes_holder                       (&m_kernel),
  m_cache_inheritance_hierarchy_list     (0),
  m_cache_inheritance_hierarchy_list2    (0),
  m_cache_query_map                      (0),
  m_cache_query_map2                     (0),
  m_cache_object_id_map                  (0),
  m_cache_object_id_map2                 (0)
{
  try {
    publish();
  }
  catch (ers::Issue & ex) {
    ers::fatal(daq::rdb::CannotPublish(ERS_HERE, name, p.name().c_str(), ex));
  }
}

struct SendServerExitsCommand {
  int64_t m_id;
  rdb::Subscription * m_subscription;

  SendServerExitsCommand(int64_t id, rdb::Subscription * s) :
    m_id            (id),
    m_subscription  (s) { ; }

  void operator()() {
    try {
      ERS_LOG( "send server-exit command to subscriber of session " << m_id );
      m_subscription->m_cb->server_exits(m_subscription->m_parameter);
    }
    catch ( CORBA::SystemException & ex ) {
      daq::rdb::SessionNotificationFailed issue(
        ERS_HERE,
        m_id, m_subscription->m_app_name,
        m_subscription->m_creation_ts, m_subscription->m_user_id,
        m_subscription->m_host, m_subscription->m_process_id,
        daq::ipc::CorbaSystemException(ERS_HERE, &ex)
      );

      if(!strcmp("TRANSIENT", ex._name())) {
        ers::log(issue);
      }
      else {
        ers::error(issue);
      }
    }

    delete this;
  }

};

RDBWriter::~RDBWriter()
{
  clean_cache(true);

  m_kernel.close_all_data();
  m_kernel.close_all_schema();


    // force unsubscribe clients

  if(!m_sessions.empty()) {
    std::vector<std::shared_ptr<std::thread> > threads;

    for(std::map<int64_t, Session*>::iterator j = m_sessions.begin(); j != m_sessions.end(); ++j) {
      if(rdb::Subscription * s = j->second->m_subscription) {
        if( CORBA::is_nil(s->m_cb) ) {
          ERS_LOG("callback function is (null) for \'" << j->first << "\', ignoring...");
          continue;
        }

        std::shared_ptr<std::thread> thread(new std::thread(std::ref(*(new SendServerExitsCommand(j->first, s)))));
        threads.push_back(thread);
      }
    }

    if(!threads.empty()) {
      for (std::size_t j = 0; j < threads.size(); ++j) {
        threads[j]->join();
      }

      ERS_LOG("notify about server exit " << threads.size() << " clients");
    }
  }

  for(std::map<int64_t, Session*>::iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
    delete i->second;
  }


  try {
    withdraw();
  }
  catch(ers::Issue & ex) {
    ers::error( daq::rdb::CannotUnpublish( ERS_HERE, name().c_str(), partition().name().c_str(), ex ) );
  }
}

void
RDBWriter::shutdown()
{
  daq::ipc::signal::raise (SIGTERM);
}

rdb::RDBSessionId
RDBWriter::open_session(const rdb::ProcessInfo& info, rdb::ping_ptr cb)
{
  std::string requestor;

  try
    {
       requestor = rdb::get_user(info);
       rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_OPEN_SESSION, partition().name());
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "failed to open session", ex)));
    }


    // create session and fill it's properties

  RDBWriter::Session * session = new RDBWriter::Session(name(), partition().name(), requestor, static_cast<const char *>(info.host), info.process_id, static_cast<const char *>(info.app_name), cb);


    // create unique session id

  int64_t id(session->m_session_process_id);

  {
    std::string __str = IPCCore::objectToString(cb, IPCCore::Ior );
    __str.append(session->m_session_creation_ts);

    const char * p = __str.c_str();
    int c;

    while ( (c = *p++) ) {
      id = c + (id << 6) + (id << 16) - id;
    }
  }

  if(id < 0) {
    id = -id;
    id = id >> 1;
  }

  {
    std::unique_lock lock(m_mutex);

    RDBWriter::Session *& x(m_sessions[id]);
    while(x != 0) x = m_sessions[++id];

    x = session;
  }

  session->m_id = id;

  ERS_LOG("open session " << id << " for application \'" << info.app_name << "\' run by \'" << requestor << "\' on \'" << info.host << "\' with pid " << info.process_id);

  request_backup();

  return id;
}


void
RDBWriter::set_commit_credentials(rdb::RDBSessionId session_id, const char *user, const char *password)
{
  {
    std::shared_lock lock(m_mutex);

    Session *session = get_session(session_id);

    std::lock_guard s_lock(session->m_session_mutex);

    try
      {
        OksKernel::validate_credentials(user, password);
      }
    catch (oks::exception &ex)
      {
        throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, ex.what())));
      }

    session->m_user_id = user;
    session->m_user_password = password;
  }

  request_backup();
}


void
RDBWriter::close_session(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info)
{
  Session * session(0);

  {
    std::unique_lock lock(m_mutex);

    std::map<int64_t, Session*>::iterator i = m_sessions.find(session_id);

    if(i != m_sessions.end()) {
      session = i->second;
      m_sessions.erase(i);
    }
  }

  if(session) {
    ERS_LOG("close session " << session_id);
    delete session;
    request_backup();
  }
  else {
    ERS_LOG(
      "cannot close session " << session_id << " since it does not exist (requested by application \'" <<
      info.app_name << "\' started by \'" << rdb::get_user_safe(info) << "\' on \'" << info.host << "\' with pid " << info.process_id << ')'
    );
    throw rdb::SessionNotFound(session_id);
  }
}


void
RDBWriter::list_sessions(const rdb::ProcessInfo& info, rdb::SessionInfoList_out data)
{
  try
    {
       const std::string requestor(rdb::get_user(info));

       ers::log(ers::Message( ERS_HERE, std::string("call list sessions by ") + requestor + " on " + name() + '@' + partition().name()));

       rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_LIST_SESSIONS, partition().name());
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot list sessions", ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot list sessions", ex)));
    }

  std::shared_lock lock(m_mutex);

  data = new rdb::SessionInfoList();
  data->length(m_sessions.size());

  unsigned int idx(0);
  for(std::map<int64_t, Session*>::const_iterator j = m_sessions.begin(); j != m_sessions.end(); ++j, ++idx) {
    (*data)[idx].id = j->first;
    (*data)[idx].user_id = rdb::string_dup(j->second->m_session_user_id);
    (*data)[idx].host = rdb::string_dup(j->second->m_session_host);
    (*data)[idx].process_id = j->second->m_session_process_id;
    (*data)[idx].app_name = rdb::string_dup(j->second->m_session_app_name);
    (*data)[idx].creation_ts = rdb::string_dup(j->second->m_session_creation_ts);
  }
}


RDBWriter::Session *
RDBWriter::get_session(int64_t session_id)
{
  std::map<int64_t, Session*>::iterator i = m_sessions.find(session_id);
  if(i != m_sessions.end()) return i->second;
  throw rdb::SessionNotFound(session_id);
}


void
RDBWriter::create_database(rdb::RDBSessionId session_id, const char * db_name)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

  ERS_DEBUG( 1 , "create_database(\'" << db_name << "\') in session " << session_id );

  try {
    session->m_created_files.insert(kernel->new_data(db_name));
  }
  catch (oks::exception & ex) {
    std::ostringstream text;
    text << "cannot create database \'" << db_name << "\' in session \'" << session_id << '\'';
    ers::Message issue(ERS_HERE, text.str(), ex);
    ers::debug(issue, 1);
    throw rdb::CannotProceed(daq::ers2idl_issue(issue));
  }
}

void
RDBWriter::get_databases(rdb::RDBSessionId session_id, rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  rdb::get_databases( (session->m_session_kernel ? *session->m_session_kernel : m_kernel), schemafiles, datafiles );
}

void
RDBWriter::get_modified_databases(rdb::RDBSessionId session_id, rdb::RDBNameList_out updated_datafiles, rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->m_session_kernel;


    // fill list of files updated by RDB server

  updated_datafiles = new rdb::RDBNameList();

  if(kernel) {
    unsigned int num_of_updated_files = 0;
    OksFile::Map::const_iterator i = kernel->data_files().begin();

    for(; i != kernel->data_files().end(); ++i) {
      if(i->second->is_updated()) num_of_updated_files++;
    }

    updated_datafiles->length(num_of_updated_files);

    i = kernel->data_files().begin();
    for(unsigned int idx = 0; i != kernel->data_files().end(); ++i) {
      if(i->second->is_updated()) {
        (*updated_datafiles)[idx++] = rdb::string_dup(i->second->get_well_formed_name());
      }
    }
  }
  else {
    kernel = &m_kernel;
    updated_datafiles->length(0);
  }


    // fill lists of externally updated files (direct and repository ones)

  rdb::get_externally_modified_databases( *kernel, externally_updated_datafiles, externally_removed_datafiles );
}


void
RDBWriter::update_database_status(const rdb::ProcessInfo& info, rdb::RDBSessionId session_id, const char *db_name)
{
  try
    {
       const std::string requestor(rdb::get_user(info));

       ers::info(ers::Message( ERS_HERE, std::string("update database file \"") + db_name + "\" status by " + requestor + " on " + name() + '@' + partition().name()));

       rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_UPDATE, partition().name());

       std::shared_lock lock(m_mutex);

       const OksKernel* kernel(nullptr);

       if (session_id == 0)
         {
           kernel = &m_kernel;
         }
       else
         {
           kernel = get_session(session_id)->m_session_kernel;
           if (!kernel)
             {
               ERS_DEBUG(0, "skip request for update_database_status(" << session_id << ", \'" << db_name << "\') since the session kernel is not inited");
               return;
             }
         }

       rdb::get_data_file(kernel, db_name)->update_status_of_file();
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, session_id, ex)));
    }
  catch (const daq::rdb::NoDataFile& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, session_id, ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedUpdateFileStatus(ERS_HERE, db_name, session_id, ex)));
    }
}


void
RDBWriter::commit_changes(rdb::RDBSessionId session_id, const char *token, const char *log_message)
{
  std::string from;

  if (strlen(token) > 40)
    from = std::string(" using daq token ") + std::string(std::string_view(token, 40)) + "...";

  ERS_LOG("got \'commit\' request for session " << session_id << from << " with log message: \"" << log_message << '\"');

  RDB_SERVER_PROFILING_INFO("commit_changes")

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->m_session_kernel;

  if (!kernel)
    {
      ERS_LOG("nothing to commit, session " << session_id << " has no changes");
      return;
    }

  std::ostringstream info_text;   // prepare text for ERS_INFO message
  unsigned int num_of_saved_files = 0;

  const bool commit2repo(!kernel->get_user_repository_root().empty() && !getenv("TDAQ_DB_USER_REPOSITORY"));

  // check files which were modified or located in user repository
  for (const auto &i : kernel->data_files())
    if (i.second->is_updated())
      try
        {
          info_text << " - \"" << *i.first << '\"';

          if (commit2repo)
            info_text << " (+U)";
          else if (*log_message)
            i.second->add_comment(log_message, kernel->get_user_name());

          info_text << std::endl;

          if (i.second->is_updated())
            {
              ERS_LOG("saving file \'" << *i.first << "\'...");
              kernel->save_data(i.second);
              num_of_saved_files++;
            }
        }
      catch (const oks::exception &ex)
        {
          ers::Message issue(ERS_HERE, "cannot commit changes into data file", ex);
          ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

  if (num_of_saved_files && commit2repo)
    try
      {
        kernel->commit_repository(log_message, token);
      }
    catch (const oks::exception &ex)
      {
        ers::Message issue(ERS_HERE, "cannot commit changes into repository", ex);
        ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
        throw rdb::CannotProceed(daq::ers2idl_issue(issue));
      }

  session->m_created_files.clear();

  // print ERS_INFO, if there were committed changes
  if (num_of_saved_files > 0)
    ERS_INFO("RDB Server \'" << name() << '@' << partition().name() << "\' has committed changes in " << num_of_saved_files << " file(s):\n" << info_text.str());
}

void
RDBWriter::abort_changes(rdb::RDBSessionId session_id)
{
  ERS_LOG("got \'abort\' request for session " << session_id);

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->m_session_kernel;

  if (!kernel)
    {
      ERS_LOG("nothing to abort, session " << session_id << " has no changes");
      return;
    }

  // unsubscribe any changes
  rdb::GuardOksNotification g(*session->m_changes_holder, (session->m_subscription != 0));

  // destroy created files
  for (const auto &i : session->m_created_files)
    {
      const std::string file_name = i->get_full_file_name();

      ERS_LOG("destroy created file \'" << file_name << "\' (session " << session_id << ')');

      kernel->close_data(i);

      if (int result = unlink(file_name.c_str()))
        {
          std::ostringstream text;
          text << "abort changes failed in session " << session_id << "\n\twas caused by: unlink(\'" << file_name << "\') failed with code " << result << ": " << strerror(errno);
          ers::Message issue(ERS_HERE, text.str());
          ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }
    }

  session->m_created_files.clear();

  // release check-out files, destroy kernel and changes holder
  session->clear();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SendReloadCommand {

  int64_t m_session_id;
  OksKernel& m_kernel;
  const rdb::RDBNameList& m_names;

  enum Severity : uint8_t {
    Info = 1 << 0,  // binary 0001
    Error = 1 << 1, // binary 0010
    Fatal = 1 << 2  // binary 0100
  };

  static std::ostringstream s_info;
  static std::ostringstream s_errors;
  static std::string s_schema_file;
  static uint8_t s_severity;
  static std::mutex s_mutex;
  static bool s_were_reloads;

  SendReloadCommand(int64_t session_id, OksKernel& kernel, const rdb::RDBNameList& names) :
    m_session_id   (session_id),
    m_kernel       (kernel),
    m_names        (names) { ; }
  
  void operator()() {
    ERS_LOG( "reload files of session " << m_session_id );

    std::set<OksFile *> mfs, rfs;
    std::string param, value;

    bool found_error = false;

    if (m_kernel.get_user_repository_root().empty() && m_names.length() > 0)
      {
        for (unsigned int i = 0; i < m_names.length(); i++)
          {
            const char *file_name = m_names[i];

            if (OksFile *f = m_kernel.find_data_file(file_name))
              {
                ERS_LOG("add data file \'" << file_name << "\' to be reloaded (session " << m_session_id << ')');
                mfs.insert(f);
              }
            else if (OksFile *f = m_kernel.find_schema_file(file_name))
              {
                ERS_LOG("found schema file \'" << file_name << "\' to be reloaded (session " << m_session_id << ')');
                mfs.insert(f);
              }
            else
              {
                ERS_LOG("no file \'" << file_name << '\'');
              }
          }
      }
    else
      {
        try
          {
            if (!m_kernel.get_user_repository_root().empty())
              OksKernel::parse_config_version (static_cast<const char*> (m_names[0]), param, value);
          }
        catch (std::exception &ex)
          {
            std::lock_guard lock(s_mutex);
            s_errors << "cannot parse config version to reload updated files (session " << m_session_id << "):\n" << ex.what();
            s_severity |= Severity::Info;
          }

        try
          {
            m_kernel.get_modified_files(mfs, rfs, value);
          }
        catch(const oks::exception& ex)
          {
            std::lock_guard lock(s_mutex);
            if (m_kernel.get_user_repository_root().empty())
              s_errors << "cannot read names of modified files: " << ex.what() << '\n';
            else
              s_errors << "failed to get differences between revisions " << m_kernel.read_repository_version() << " and " << value << " (session " << m_session_id << "): " << ex.what() << '\n';
            found_error = true;
            s_severity |= Severity::Error;
          }
      }

    for (const auto& x : mfs)
      {
        if (x->get_oks_format() == "schema")
          {
            std::lock_guard lock(s_mutex);
            s_schema_file = x->get_well_formed_name();
            s_errors << "reload of schema file (\'" << s_schema_file << "\')";
            if (!m_kernel.get_user_repository_root().empty())
              s_errors << " modified between revisions " << m_kernel.read_repository_version() << " and " << value;
            s_errors << " is not allowed (session " << m_session_id << ")\n";
            found_error = true;
            s_severity |= Severity::Info;
          }
      }


      // process updated files

    if (!found_error)
      {
        std::ostringstream log;

        if (mfs.size())
          {
            log << mfs.size() << " files to be reloaded";

            if (!m_kernel.get_user_repository_root().empty())
              log << " modified between revisions " << m_kernel.read_repository_version() << " and " << value;

            log << " (session " << m_session_id << "):\n";

            for (const auto &i : mfs)
              log << "  - \"" << i->get_full_file_name() << "\"\n";

            ERS_LOG(log.str());
          }
        else if (!m_kernel.get_user_repository_root().empty())
          {
            ERS_LOG("update revision without reload, no files changed (session " << m_session_id << ')');
          }

        try
          {
            if (!m_kernel.get_user_repository_root().empty())
              m_kernel.update_repository(param, value, OksKernel::RepositoryUpdateType::DiscardChanges);

            if (mfs.size())
              {
                m_kernel.reload_data(mfs, false);

                std::lock_guard lock(s_mutex);
                s_were_reloads = true;
                s_info << "* session " << m_session_id << " => number of reloaded files: " << mfs.size() << ", number of closed files: " << rfs.size() << std::endl;
              }
            else if (!m_kernel.get_user_repository_root().empty())
              {
                std::lock_guard lock(s_mutex);
                s_info << "* session " << m_session_id << " update revision without reload\n";
              }
          }
        catch (oks::exception &ex)
          {
            const std::string schema_ex_name = rdb::get_name_if_schema_reload_exception(ex);

            std::lock_guard lock(s_mutex);

            s_errors << "cannot reload updated files (session " << m_session_id << "):\n" << ex.what();

            if (schema_ex_name.empty() == false)
              {
                s_schema_file = schema_ex_name;
                s_severity |= Severity::Info;
              }
            else
              {
                s_severity |= Severity::Fatal;
              }
          }
      }

    if(!m_session_id && (mfs.size() || rfs.size())) {
      s_rdb_server->clean_cache();
    }

    delete this;
  }

};

std::ostringstream SendReloadCommand::s_info;
std::ostringstream SendReloadCommand::s_errors;
std::string SendReloadCommand::s_schema_file;
std::mutex SendReloadCommand::s_mutex;
uint8_t SendReloadCommand::s_severity;
bool SendReloadCommand::s_were_reloads;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void
RDBWriter::reload_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names)
{
  try
    {
      const std::string requestor(rdb::get_user(info));

      ers::info(ers::Message( ERS_HERE, std::string("call reload database ") + to_str(names, true) + " by " + requestor + " on " + name() + '@' + partition().name()));

      rdb::check_am_permission(m_am_server, requestor, daq::am::RDBResource::ACTION_ID_RELOAD, partition().name());
    }
  catch (const daq::rdb::NoAccessManagerPermission& ex)
    {
      ers::log(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseReloadFailed(ERS_HERE, "", ex)));
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::DatabaseReloadFailed(ERS_HERE, "", ex)));
    }


  if (!m_kernel.get_user_repository_root().empty() && names.length() != 1)
    {
      daq::rdb::DatabaseReloadFailed issue(ERS_HERE, ": version parameter is required to reload repository database");
      ers::info(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }


    // reload data in MAIN and session kernels in multiple threads

    {
      std::unique_lock lock(m_mutex);

      SendReloadCommand::s_info.str("");
      SendReloadCommand::s_errors.str("");
      SendReloadCommand::s_schema_file.clear();
      SendReloadCommand::s_were_reloads = false;
      SendReloadCommand::s_severity = 0;

      std::list<SendReloadCommand *> reload_list;

      reload_list.push_back( new SendReloadCommand(0, m_kernel, names) );

      for(auto& i : m_sessions) {
        if(i.second->m_session_kernel) {
          ERS_LOG("session " << i.first << " has updates");
          reload_list.push_back( new SendReloadCommand( i.first, *i.second->m_session_kernel, names) );
        }
        else {
          ERS_LOG("session " << i.first << " has no updates");
        }
      }

      std::vector<std::shared_ptr<std::thread> > threads;

      for(const auto& i : reload_list) {
        std::shared_ptr<std::thread> thread(new std::thread(std::ref(*i)));
        threads.push_back(thread);
      }

      for (std::size_t j = 0; j < threads.size(); ++j) {
        threads[j]->join();
      }

      if(!SendReloadCommand::s_errors.str().empty()) {
        std::ostringstream text;
        text << ":\n" << SendReloadCommand::s_errors.str();
        daq::rdb::DatabaseReloadFailed issue(ERS_HERE, text.str());
        daq::rdb::UserRequestFailed problem(ERS_HERE, name(), partition().name(), issue);

        if (SendReloadCommand::s_severity & SendReloadCommand::Severity::Fatal)
          ers::fatal(problem);
        else if (SendReloadCommand::s_severity & SendReloadCommand::Severity::Error)
          ers::error(problem);
        else if (SendReloadCommand::s_schema_file.empty() == false)
          {
            daq::rdb::SchemaReloadNotAllowed schema_reload_issue(ERS_HERE, SendReloadCommand::s_schema_file);
            ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), schema_reload_issue));
            throw rdb::CannotProceed(daq::ers2idl_issue(schema_reload_issue));
          }
        else
          ers::info(problem);

        throw rdb::CannotProceed(daq::ers2idl_issue(issue));
      }

      unsigned long notify_num = 0;

      if(SendReloadCommand::s_were_reloads) {
        notify_num = notify();
      }

      ERS_INFO(
        "RDB Server \'" << name() << '@' << partition().name() << "\' has been reloaded:\n" <<
        SendReloadCommand::s_info.str() <<
        "Number of notified clients: " << notify_num << '.'
      );
    }

  if (!m_kernel.get_repository_version().empty() && !m_backup_file.empty())
    make_backup();
}

rdb::RDBRepositoryVersionList*
RDBWriter::get_changes()
{
  return rdb::get_changes(m_kernel);
}

rdb::RDBRepositoryVersionList*
RDBWriter::get_versions(CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until)
{
  return rdb::get_versions(m_kernel, skip_irrelevant, query_type, since, until);
}

struct SendNotifyCommand {
  rdb::RDBClassChangesList m_changes_list;
  RDBWriter::Session * m_session;
  int64_t m_session_id;

  SendNotifyCommand(int64_t id, RDBWriter::Session * session) :
    m_session       (session),
    m_session_id    (id) { ; }

  void operator()() {
    rdb::Subscription * s = m_session->m_subscription;

    try {

      ERS_LOG( "notify subscriber of session " << m_session_id );

      if(ers::debug_level() > 1) {
	std::ostringstream text;
	text << " * subscription on:" << std::endl;
	text << "   - any changes in " << s->m_class_names.size() << " classes: ";
	for(std::set<std::string>::const_iterator ci = s->m_class_names.begin(); ci != s->m_class_names.end(); ++ci) {
	  if(ci != s->m_class_names.begin()) { text << ", "; } text << '\'' << *ci << '\'';
	}
	text << std::endl;
	for(rdb::Subscription::ObjectsMap::const_iterator si = s->m_objects_names.begin(); si != s->m_objects_names.end(); ++si) {
	  text << "   - changes in " << si->second.size() << " object(s) of class \'" << si->first << "\': ";
	  for(std::set<std::string>::const_iterator cl = si->second.begin(); cl != si->second.end(); ++cl) {
	    if(cl != si->second.begin()) { text << ", "; } text << '\'' << *cl << '\'';
	  }
	  text << std::endl;
	}

	for(unsigned long i = 0; i < m_changes_list.length(); i++) {
          rdb::RDBClassChanges& c = m_changes_list[i];
          text << " * report changes in class \'" << c.name << '\'' << std::endl;
	  text << "   a) " << c.modified.length() << " modified objects: ";
	  if(c.modified.length()) {
	    for(unsigned long j = 0; j < c.modified.length(); j++) {
              if(j != 0) { text << ", "; } text << '\'' << c.modified[j] << '\'';
	    }
	  }
	  text << std::endl;
	  text << "   b) " << c.created.length() << " created objects: ";
	  if(c.created.length()) {
	    for(unsigned long j = 0; j < c.created.length(); j++) {
	      if(j != 0) { text << ", "; } text << '\'' << c.created[j] << '\'';
	    }
	  }
	  text << std::endl;
	  text << "   c) " << c.removed.length() << " removed objects: ";
	  if(c.removed.length()) {
	    for(unsigned long j = 0; j < c.removed.length(); j++) {
              if(j != 0) { text << ", "; } text << '\'' << c.removed[j] << '\'';
	    }
	  }
	  text << std::endl;
        }

        ERS_DEBUG( 2, text.str() );
      }

      unsigned long client_time = s->m_cb->inform(m_changes_list, s->m_parameter);
      ERS_DEBUG(2, "client spent " << client_time << " usec to process callback for subscriber " << m_session_id);
    }
    catch ( CORBA::SystemException & ex ) {
      daq::rdb::SessionNotificationFailed issue(
        ERS_HERE,
        m_session_id, s->m_app_name,
        s->m_creation_ts, s->m_user_id,
        s->m_host, s->m_process_id,
        daq::ipc::CorbaSystemException(ERS_HERE, &ex)
      );

      if(!strcmp("TRANSIENT", ex._name())) {
        ers::log(issue);
      }
      else {
        ers::error(issue);
      }

      delete s;
      m_session->m_subscription = 0;
    }

    delete this;
  }

};


unsigned long
RDBWriter::notify()
{
  RDB_SERVER_PROFILING_INFO("notify")

  std::list<SendNotifyCommand *> notify_list;

  unsigned long subscribers_count(0);

  for(std::map<int64_t, Session*>::const_iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
     //std::lock_guard s_lock(session->m_session_mutex);

    if(rdb::Subscription * s = i->second->m_subscription) {
      subscribers_count++;

      if( CORBA::is_nil(s->m_cb) ) {
        ERS_LOG("callback function is (null) for \'" << i->first << "\', ignoring...");
        continue;
      }

      OksKernel * kernel = i->second->m_session_kernel;
      rdb::ChangesHolder * changes_holder = i->second->m_changes_holder;

      if(!kernel) {
        kernel = &m_kernel;
        changes_holder = &m_changes_holder;
      }

      SendNotifyCommand * obj = new SendNotifyCommand(i->first, i->second);
      config::map<rdb::ClassChanges *> cl_changes;

      {
        for(std::list<OksObject *>::iterator j = changes_holder->m_created.begin(); j != changes_holder->m_created.end(); ++j) {
          rdb::check(cl_changes, s, (*j)->GetClass(), (*j)->GetId(), '+');

          std::set<OksObject *>::iterator x = changes_holder->m_modified.find(*j);
          if(x != changes_holder->m_modified.end()) {
            ERS_DEBUG(1, "created object " << *j << " appears in \'modified\' set, removing...");
            changes_holder->m_modified.erase(x);
          }
        }
      }

      {
        for(std::set<OksObject *>::iterator j = changes_holder->m_modified.begin(); j != changes_holder->m_modified.end(); ++j) {
          if(kernel->is_dangling(*j)) {
            ERS_DEBUG(1, "object " << (void *)(*j) << " is dangling (session " << i->first << "), ignore...");
          }
          else {
            rdb::check(cl_changes, s, (*j)->GetClass(), (*j)->GetId(), '~');
          }
        }
      }

      {
        for(std::map<const OksClass *, std::set<std::string> >::iterator i2 = changes_holder->m_removed.begin(); i2 != changes_holder->m_removed.end(); ++i2) {
          for(std::set<std::string>::const_iterator j = i2->second.begin(); j != i2->second.end(); ++j) {
            rdb::check(cl_changes, s, i2->first, *j, '-');
	  }
        }
      }

      if(cl_changes.size()) {
        rdb::cvt_and_free(cl_changes, obj->m_changes_list);
        notify_list.push_back(obj);
        ERS_DEBUG(2, "add notify call for subscribed in session " << i->first);
      }
      else {
        delete obj;
        ERS_DEBUG( 1, "no changes for subscriber in session " << i->first );
      }
    }

  }


    // clear changes holders

  for(std::map<int64_t, Session*>::const_iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
    if(rdb::ChangesHolder * ch = i->second->m_changes_holder) {
      ch->clear();
    }
  }

  m_changes_holder.clear();

  if(notify_list.empty()) {
    ERS_LOG( "there are no subscribers to be informed" );
    return 0;
  }

    // prepare pool of threads to simultaneously notify all RDB servers from given partition
  std::vector<std::shared_ptr<std::thread> > threads;

    // run client reload commands in pool of threads
  {
    for(std::list<SendNotifyCommand *>::const_iterator i = notify_list.begin(); i != notify_list.end(); ++i) {
      std::shared_ptr<std::thread> thread(new std::thread(std::ref(*(*i))));
      threads.push_back(thread);
    }
  }

    // wait for all threads in the pool to exit
  for (std::size_t j = 0; j < threads.size(); ++j) {
    threads[j]->join();
  }

  ERS_LOG("notified " << notify_list.size() << " clients");

    // detect if there were removed subscribers

  for(std::map<int64_t, Session*>::const_iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
    if(i->second->m_subscription) {
      subscribers_count--;
    }
  }

  if(subscribers_count > 0) {
    set_oks_changes_subscriptions();
  }

  return threads.size();
}

void
RDBWriter::lock_file(OksFile *file_h)
{
  OksFile::FileStatus file_status = file_h->get_status_of_file();

  if (file_status == OksFile::FileModified)
    throw daq::rdb::FailedLockDataFile(ERS_HERE, file_h->get_full_file_name(), "the file was modified after initialisation of session (close session or reload database to continue)");
  else if (file_status == OksFile::FileRemoved)
    throw daq::rdb::FailedLockDataFile(ERS_HERE, file_h->get_full_file_name(), "the file was removed after initialisation of session (close session or reload database to continue)");
  else if (OksKernel::check_read_only(file_h))
    throw daq::rdb::FailedLockDataFile(ERS_HERE, file_h->get_full_file_name(), "the file is read-only");

  file_h->lock();
 }

void
RDBWriter::database_add_file(rdb::RDBSessionId session_id, const char *db_name, const char *file_name)
{
  std::shared_lock lock(m_mutex);

  Session* session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel* kernel = session->init(m_kernel);

  try
    {
      OksFile* file_h = rdb::get_data_file(kernel, db_name);
      lock_file(file_h);
      file_h->add_include_file(file_name);
    }
  catch (const daq::rdb::Exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedAddIncludeFile(ERS_HERE, file_name, db_name, session_id, ex)));
    }
  catch (oks::exception& ex)
    {
      // report oks exception as error since most probably there is a service issue
      daq::rdb::FailedAddIncludeFile issue(ERS_HERE, file_name, db_name, session_id, ex);
      ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
}

class DestroyGuard {

  public:

    DestroyGuard(rdb::ChangesHolder& active_ch, rdb::ChangesHolder * session_ch) : m_active_changes_holder(active_ch), m_session_changes_holder(session_ch) { ; }

    ~DestroyGuard() {
      m_active_changes_holder.stop_oks_notification();
      if(m_session_changes_holder && m_session_changes_holder->m_notification_started) {
	m_session_changes_holder->start_oks_notification();
      }
    }

  private:

    rdb::ChangesHolder& m_active_changes_holder;
    rdb::ChangesHolder* m_session_changes_holder;

};

void
RDBWriter::database_remove_file(rdb::RDBSessionId session_id, const char *db_name, const char *file_name, rdb::RDBClassChangesList_out removed_objects)
{
  std::shared_lock lock(m_mutex);

  Session *session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel *kernel = session->init(m_kernel);

  try
    {
      OksFile* file_h = rdb::get_data_file(kernel, db_name);

      lock_file(file_h);

      // remember correspondence between pointers on OKS classes and their names used in rdb::add() method,
      // since remove_include_file() may close some schema files and invalidate pointers on OKS classes

      std::map<const OksClass*, std::string> classptr2name;

      for (const auto& i : kernel->classes())
        classptr2name[i.second] = i.second->get_name();

      rdb::ChangesHolder ch(kernel);
      ch.start_oks_notification();
      DestroyGuard __dg__(ch, session->m_changes_holder);

      file_h->remove_include_file(file_name);

      config::map<rdb::ClassChanges*> cl_changes;

      for (const auto& i2 : ch.m_removed)
        for (const auto& j : i2.second)
          rdb::add(cl_changes, classptr2name[i2.first], j, '-');

      removed_objects = new rdb::RDBClassChangesList();
      rdb::cvt_and_free(cl_changes, *removed_objects);
    }
  catch (const daq::rdb::Exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedRemoveIncludeFile(ERS_HERE, file_name, db_name, session_id, ex)));
    }
  catch (oks::exception& ex)
    {
      // report oks exception as error since most probably there is a service issue
      daq::rdb::FailedRemoveIncludeFile issue(ERS_HERE, file_name, db_name, session_id, ex);
      ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
}

OksKernel *
RDBWriter::get_kernel(Session * session)
{
  return (session->m_session_kernel ? session->m_session_kernel : &m_kernel);
}

void
RDBWriter::database_get_files(rdb::RDBSessionId session_id, const char *db_name, rdb::RDBNameList_out file_names)
{
  std::shared_lock lock(m_mutex);

  Session* session = get_session(session_id);
  OksKernel* kernel = get_kernel(session);

  file_names = new rdb::RDBNameList();

  std::shared_lock rlock(kernel->get_mutex());

  // get includes of file
  if (*db_name != 0)
    {
      try
        {
          OksFile* file_h = rdb::get_data_file(kernel, db_name);
          file_names->length(file_h->get_include_files().size());
          unsigned int idx(0);
          for (const auto& i : file_h->get_include_files())
            (*file_names)[idx++] = rdb::string_dup(i);
        }
      catch (const daq::rdb::Exception& ex)
        {
          throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedGetFileIncludes(ERS_HERE, db_name, session_id, ex)));
        }
    }

  // get top-level files
  else
    {
      unsigned int idx(0);
      for (auto& i : kernel->data_files())
        {
          if (i.second->get_parent() == nullptr)
            idx++;
        }

      file_names->length(idx);

      idx = 0;
      for (auto& i : kernel->data_files())
        {
          if (i.second->get_parent() == nullptr)
            (*file_names)[idx++] = rdb::string_dup(i.first);
        }
    }
}

bool
RDBWriter::is_database_writable(rdb::RDBSessionId session_id, const char *db_name)
{
  std::shared_lock lock(m_mutex);

  Session* session = get_session(session_id);
  OksKernel* kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  try
    {
      OksFile* file_h = rdb::get_data_file(kernel, db_name);

      if (!kernel->get_user_repository_root().empty())
        return oks::access::is_writable(*file_h, session->m_user_id);
      else
        return !OksKernel::check_read_only(file_h);
    }
  catch (const daq::rdb::Exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::FailedCheckFileStatus(ERS_HERE, db_name, session_id, ex)));
    }
  catch (const std::exception& ex)
    {
      daq::rdb::FailedCheckFileStatus issue(ERS_HERE, db_name, session_id, ex);
      ers::warning(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
}

void
RDBWriter::get_all_classes(rdb::RDBSessionId session_id, rdb::RDBClassList_out l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);


  std::shared_lock rlock(kernel->get_mutex());

  OksClass::Map::const_iterator i = kernel->classes().begin();

  l = new rdb::RDBClassList();
  l->length(kernel->classes().size());

  for(unsigned long idx = 0; i != kernel->classes().end() ; ++i) {
    rdb::copy(l[idx++], i->second);
  }
}

void
RDBWriter::get_class(rdb::RDBSessionId session_id, const char* classid, rdb::RDBClass_out c)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * rc = kernel->find_class(classid)) {
    c = new rdb::RDBClass();
    rdb::copy(*c.ptr(), rc);
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_direct_super_classes(rdb::RDBSessionId session_id, const char* classid, rdb::RDBClassList_out l)
{
  l = new rdb::RDBClassList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const std::list<std::string *> * clist = c->direct_super_classes()) {
      l->length(clist->size());
      std::list<std::string *>::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        rdb::copy(l[idx++], kernel->find_class(**i));
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_all_super_classes(rdb::RDBSessionId session_id, const char* classid, rdb::RDBClassList_out l)
{
  l = new rdb::RDBClassList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const OksClass::FList * clist = c->all_super_classes()) {
      l->length(clist->size());
      OksClass::FList::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        rdb::copy(l[idx++], *i);
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_sub_classes(rdb::RDBSessionId session_id, const char* classid, rdb::RDBClassList_out l)
{
  l = new rdb::RDBClassList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const OksClass::FList * clist = c->all_sub_classes()) {
      l->length(clist->size());
      OksClass::FList::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        rdb::copy(l[idx++], *i);
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_direct_sub_classes(rdb::RDBSessionId session_id, const char* classid, rdb::RDBClassList_out l)
{
  l = new rdb::RDBClassList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const OksClass::FList * clist = c->all_sub_classes()) {
      unsigned long len = 0;
      OksClass::FList::const_iterator i = clist->begin();

      for(; i != clist->end(); ++i) {
        if((*i)->has_direct_super_class(classid) == true) len++;
      }

      l->length(len);

      i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        if((*i)->has_direct_super_class(classid) == true) {
          rdb::copy(l[idx++], *i);
	}
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_attributes(rdb::RDBSessionId session_id, const char* classid, rdb::RDBAttributeList_out l)
{
  l = new rdb::RDBAttributeList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const std::list<OksAttribute*> * clist = c->all_attributes()) {
      l->length(clist->size());
      std::list<OksAttribute*>::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        const OksAttribute * oa = *i;
        rdb::oks2rdb(oa, l[idx++], (c->source_class(oa) == c));
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}

void
RDBWriter::get_relationships(rdb::RDBSessionId session_id, const char* classid, rdb::RDBRelationshipList_out l)
{
  l = new rdb::RDBRelationshipList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if(const OksClass * c = kernel->find_class(classid)) {
    if(const std::list<OksRelationship*> * clist = c->all_relationships()) {
      l->length(clist->size());
      std::list<OksRelationship*>::const_iterator i = clist->begin();
      for(unsigned long idx = 0; i != clist->end(); ++i) {
        const OksRelationship * rr = *i;
	rdb::oks2rdb(rr, l[idx++], (c->source_class(rr) == c));
      }
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, classid)));
}


void
RDBWriter::get_inheritance_hierarchy_from_cache (rdb::RDBInheritanceHierarchyList_out l)
{
  l = new rdb::RDBInheritanceHierarchyList(m_cache_inheritance_hierarchy_list->length(), m_cache_inheritance_hierarchy_list->length(), m_cache_inheritance_hierarchy_list->get_buffer());

  ERS_DEBUG( 3 , "get_inheritance_hierarchy() returns from cache description for " << m_cache_inheritance_hierarchy_list->length() << " classes" );
}


void
RDBWriter::get_inheritance_hierarchy(rdb::RDBSessionId session_id, rdb::RDBInheritanceHierarchyList_out l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);


      // if session is inited, calculate and return inheritance hierarchy
      
  if(OksKernel * kernel = session->m_session_kernel) {
    RDB_SERVER_PROFILING_INFO("get_inheritance_hierarchy(no-cache)")

    std::shared_lock rlock(kernel->get_mutex());
    rdb::fill_inheritance_hierarchy(*kernel, l);
    ERS_DEBUG( 3 , "get_inheritance_hierarchy() returns description for " << l->length() << " classes" );
  }


      // if session is not inited, get inheritance hierarchy from cache

  else {
    RDB_SERVER_PROFILING_INFO("get_inheritance_hierarchy(cache)")
    
    {
      std::shared_lock m(m_cache_inheritance_hierarchy_mutex);

      if(m_cache_inheritance_hierarchy_list) {
        get_inheritance_hierarchy_from_cache (l);
        return;
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_inheritance_hierarchy_mutex);

      if(!m_cache_inheritance_hierarchy_list) {
        rdb::fill_inheritance_hierarchy(m_kernel, m_cache_inheritance_hierarchy_list);
      }

      ERS_DEBUG( 3 , "get_inheritance_hierarchy() puts to cache description of " << m_cache_inheritance_hierarchy_list->length() << " classes" );

      get_inheritance_hierarchy_from_cache (l);
      return;
    }
    
  }
}

void
RDBWriter::get_object(rdb::RDBSessionId session_id, const char* classid, ::CORBA::Boolean look_in_sclasses, const char* objectid, rdb::RDBObject_out object)
{
  RDB_SERVER_PROFILING_INFO("get_object")

  object = new rdb::RDBObject();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  try
    {
      std::shared_lock rlock(kernel->get_mutex());

      const OksObject *o = rdb::get_oks_object(kernel, classid, objectid, look_in_sclasses);

      object->classid = rdb::string_dup(o->GetClass()->get_name());
      object->name = rdb::string_dup(o->GetId());
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::get_file_of_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, ::CORBA::String_out file)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  try
    {
      std::shared_lock rlock(kernel->get_mutex());

      file = rdb::string_dup(rdb::get_oks_object(kernel, o.classid, o.name)->get_file()->get_well_formed_name());
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::get_files_of_all_objects(rdb::RDBSessionId session_id, rdb::RDBFileObjectsList_out info)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  rdb::get_files_of_all_objects(kernel, info);
}

void
RDBWriter::get_all_objects(rdb::RDBSessionId session_id, const char *class_name, bool look_in_sclasses, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_all_objects")

  std::shared_lock lock(m_mutex);

  Session *session = get_session(session_id);
  OksKernel *kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if (const OksClass *c = kernel->find_class(class_name))
    {
      rlist = new rdb::RDBObjectList();

      if (look_in_sclasses == false)
        {
          if (const OksObject::Map *omap = c->objects())
            {
              rlist->length(omap->size());
              unsigned long idx = 0;
              for (const auto &i : *omap)
                {
                  (*rlist)[idx].classid = rdb::string_dup(c->get_name());
                  (*rlist)[idx++].name = rdb::string_dup(i.first);
                }
            }
        }
      else
        {
          if (std::list<OksObject*> *objs = c->create_list_of_all_objects())
            {
              rlist->length(objs->size());
              unsigned long idx = 0;
              for (const auto &i : *objs)
                {
                  (*rlist)[idx].classid = rdb::string_dup(i->GetClass()->get_name());
                  (*rlist)[idx++].name = rdb::string_dup(i->GetId());
                }
              delete objs;
            }
        }
    }
  else
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));
    }

  ERS_DEBUG(2, "get_all_objects (session: " << session_id << ", \"" << class_name << "\", look_in_sclasses = " << look_in_sclasses << ") returns " << rlist->length() << " object(s)");
}

void
RDBWriter::get_objects_by_query(rdb::RDBSessionId session_id, const char *class_name, const char *query, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_objects_by_query")

  std::shared_lock lock(m_mutex);

  Session *session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  if (OksClass *c = kernel->find_class(class_name))
    {
      rlist = new rdb::RDBObjectList();

      if (query == nullptr)
        query = "";

      std::unique_ptr<OksQuery> qe(new OksQuery(c, query));

      if (qe->good() == false)
        {
          ers::Message issue(ERS_HERE, rdb::mk_bad_query_text(class_name, query, "bad syntax"));
          ers::debug(issue, 1);
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));
        }

      if (OksObject::List *objs = c->execute_query(qe.get()))
        rdb::oks2rdb_list(objs, rlist, 0, 0, 0);
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  ERS_DEBUG(2, "query \"" << class_name << "\", \"" << query << "\" returns " << rlist->length() << " object(s)");
}

void
RDBWriter::get_objects_by_path(rdb::RDBSessionId session_id, const rdb::RDBObject& obj_from, const char* query, rdb::RDBObjectList_out rlist)
{
  RDB_SERVER_PROFILING_INFO("get_objects_by_path")

  rlist = new rdb::RDBObjectList();

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  try
    {
      std::shared_lock rlock(kernel->get_mutex());

      const OksObject *oks_o = rdb::get_oks_object(kernel, obj_from.classid, obj_from.name);

      oks::QueryPath q(query, *kernel);

      if (OksObject::List *objs = oks_o->find_path(q))
        rdb::oks2rdb_list(objs, rlist, 0, 0, 0);

      ERS_DEBUG( 2 , "path \"" << query << "\" from " << oks_o << " returns " << rlist->length() << " object(s)" );
    }
  catch (const oks::bad_query_syntax &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot get objects by path", ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::get_objects_of_relationship(rdb::RDBSessionId session_id, const rdb::RDBObject& rdb_o, const char * r, rdb::RDBObjectList_out l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  try
    {
      std::shared_lock rlock(kernel->get_mutex());

      const OksObject *oks_o = rdb::get_oks_object(kernel, rdb_o.classid, rdb_o.name);

      OksData *d(oks_o->GetRelationshipValue(r));
      l = new rdb::RDBObjectList(1);
      rdb::append_obj_to_list(d, *l.ptr());
    }
  catch (const oks::exception& ex)
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::RelationshipNotFound(ERS_HERE, r, rdb_o.classid, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::get_referenced_by(rdb::RDBSessionId session_id, const rdb::RDBObject& rdb_o, const char * r, bool composite_only, rdb::RDBObjectList_out l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  try
    {
      std::shared_lock rlock(kernel->get_mutex());

      const OksObject *oks_o = rdb::get_oks_object(kernel, rdb_o.classid, rdb_o.name);

      rdb::fill_referenced_by(oks_o, r, composite_only, l);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::get_attribute_value(rdb::RDBSessionId session_id, const rdb::RDBObject& rdb_o, const char * a, rdb::DataUnion_out data)
{
  OksData * oks_data = nullptr;

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  try
    {
      oks_data = rdb::get_oks_object(kernel, rdb_o.classid, rdb_o.name)->GetAttributeValue(a);
    }
  catch (const oks::exception& ex)
    {
      throw daq::rdb::AttributeNotFound(ERS_HERE, a, rdb_o.classid, ex);
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }

  if (oks_data->type == OksData::list_type)
    throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, session_id, std::runtime_error("the attribute is multi-value"))));

  try
    {
      data = new rdb::DataUnion();
      rdb::oks2rdb(*data.ptr(), oks_data);
    }
  catch (std::exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, session_id, ex)));
    }
}

void
RDBWriter::get_attribute_values(rdb::RDBSessionId session_id, const rdb::RDBObject& rdb_o, const char * a, rdb::DataListUnion_out l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  try
    {
      const OksObject *oks_obj = rdb::get_oks_object(kernel, rdb_o.classid, rdb_o.name);

      if (OksDataInfo *di = oks_obj->GetClass()->data_info(a))
        {
          OksData *oks_data(oks_obj->GetAttributeValue(di));

          if (oks_data->type != OksData::list_type)
            throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, session_id, std::runtime_error("the attribute is single value"))));

          try
            {
              l = new rdb::DataListUnion();
              rdb::oks2rdb(*l.ptr(), oks_data, di->attribute->get_data_type());
            }
          catch (std::exception& ex)
            {
              throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotReadAttribute(ERS_HERE, a, rdb_o.name, rdb_o.classid, session_id, ex)));
            }
        }
      else
        throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::AttributeNotFound(ERS_HERE, a, rdb_o.classid)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

unsigned long 
RDBWriter::fill_object_id_result (OksKernel * kernel, const char * classid, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, std::string& oks_obj_class_name, rdb::RDBClassValueList*& values)
{
  std::shared_lock rlock(kernel->get_mutex());

  OksObject *oks_obj = rdb::get_oks_object(kernel, classid, objectid, look_in_sclasses);

  oks_obj_class_name = oks_obj->GetClass()->get_name();
  OksObject::FSet refs;
  oks::ClassSet names4refs;
  rdb::sequence2classes(kernel, cnames, names4refs);
  oks_obj->references(refs, recursion_depth, true, &names4refs);
  rdb::fill_values(refs, values);
  return refs.size();
}

rdb::RDBObject *
RDBWriter::get_object_id_from_cache (rdb::ObjectIDCache::iterator& i, const char * class_name, const char * object_id, bool look_in_sclasses, rdb::RDBClassValueList_out values)
{
  values = new rdb::RDBClassValueList(i->second.second->length(), i->second.second->length(), i->second.second->get_buffer());

  ERS_DEBUG( 3 , "xget_object() for for \'" << object_id << '@' << class_name << (look_in_sclasses ? "*" : "") << "\' returns from cache \'" << object_id << '@' << i->second.first << "\' object (preload referenced objects from " << i->second.second->length() << " class(es))" );

  return rdb::make_rdb_object(i->second.first, object_id);
}

void
RDBWriter::xget_object(rdb::RDBSessionId session_id, const char * classid, bool look_in_sclasses, const char * objectid, rdb::RDBObject_out object, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  try
    {
      std::shared_lock lock(m_mutex);

      Session *session = get_session(session_id);

      // if session is inited, calculate and return object's values

      if (OksKernel *kernel = session->m_session_kernel)
        {
          RDB_SERVER_PROFILING_INFO("xget_object(no-cache)")

          std::string oks_obj_class_name;
          unsigned long size = fill_object_id_result(kernel, classid, look_in_sclasses, objectid, recursion_depth, cnames, oks_obj_class_name, values);
          object = rdb::make_rdb_object(oks_obj_class_name, objectid);

          ERS_DEBUG(2, "xget_object(session: " << session_id << ") for \'" << objectid << '@' << classid << (look_in_sclasses ? "*" : "") << "\' returns \'" << objectid << '@' << oks_obj_class_name<< "\' object (preload " << size << " referenced object(s) from " << values->length() << " class(es))");
        }

      // if session is not inited, get object's values from cache

      else
        {
          RDB_SERVER_PROFILING_INFO("xget_object(cache)")

          std::string key = rdb::object_id_params_to_string(classid, look_in_sclasses, objectid, recursion_depth, cnames);

          // check the value is already in cache
            {
              std::shared_lock m(m_cache_object_id_mutex);

              if (m_cache_object_id_map)
                {
                  rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(key);
                  if (i != m_cache_object_id_map->end())
                    {
                      object = get_object_id_from_cache(i, classid, objectid, look_in_sclasses, values);
                      return;
                    }
                }
            }

          // put the value to cache
            {
              std::unique_lock m(m_cache_object_id_mutex);

              if (!m_cache_object_id_map)
                m_cache_object_id_map = new rdb::ObjectIDCache();

              rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(key);

              if (i == m_cache_object_id_map->end())
                {
                  std::string oks_obj_class_name;
                  rdb::RDBClassValueList *the_list = 0;
                  unsigned long size = fill_object_id_result(&m_kernel, classid, look_in_sclasses, objectid, recursion_depth, cnames, oks_obj_class_name, the_list);

                  i = m_cache_object_id_map->insert(std::make_pair(key, std::make_pair(oks_obj_class_name, the_list))).first;

                  ERS_DEBUG(3, "xget_object() for \'" << objectid << '@' << classid << (look_in_sclasses ? "*" : "") << "\' returns from cache \'" << objectid << '@' << oks_obj_class_name<< "\' object (preload " << size << " referenced object(s) from " << the_list->length() << " class(es))");
                }

              object = get_object_id_from_cache(i, classid, objectid, look_in_sclasses, values);

              return;
            }
        }
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

unsigned long
RDBWriter::fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values)
{
  try {
    OksObject::FSet refs;
    rdb::fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, rlist, refs);
    rdb::fill_values(refs, values);
    return refs.size();
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << "failed to fill structures to pass objects of class \'" << c->get_name() << '\'';
    ers::Message issue(ERS_HERE, text.str(), ex);
    ers::debug(issue, 1);
    throw rdb::CannotProceed(daq::ers2idl_issue(issue));
  }
}

void
RDBWriter::get_query_from_cache (rdb::QueryCache::iterator& i, const char * class_name, const char * query, rdb::RDBObjectList_out rlist, rdb::RDBClassValueList_out values)
{
  rlist = new rdb::RDBObjectList(i->second.first->length(), i->second.first->length(), i->second.first->get_buffer());
  values = new rdb::RDBClassValueList(i->second.second->length(), i->second.second->length(), i->second.second->get_buffer());

  ERS_DEBUG( 3 , "xget_objects_by_query() query \"" << class_name << "\", \"" << query << "\" returns from cache " << i->second.first->length() << " object(s) (preload referenced objects from " << i->second.second->length() << " class(es))" );
}

void
RDBWriter::xget_all_objects(rdb::RDBSessionId session_id, const char * class_name, bool look_in_sclasses, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  const OksClass * c = (session->m_session_kernel ? session->m_session_kernel : &m_kernel)->find_class(class_name);

  if (c == nullptr)
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));


      // if session is inited, calculate and return object's values
      
  if(session->m_session_kernel) {
    RDB_SERVER_PROFILING_INFO("xget_all_objects(no-cache)")

    unsigned long size = fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, rlist, values);

    ERS_DEBUG( 2 , "xget_all_objects (\"" << class_name << (look_in_sclasses ? "*" : "") << ") returns " << rlist->length() << " object(s) (preload " << size << " referenced objects from " << values->length() << " class(es))" );
  }


      // if session is not inited, get object's values from cache

  else {
    RDB_SERVER_PROFILING_INFO("xget_all_objects(cache)")

    const char * query = (look_in_sclasses ? "*" : "T");
    std::string key = rdb::query_params_to_string(class_name, query, recursion_depth, cnames);

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_query_mutex);

      if(m_cache_query_map) {
        rdb::QueryCache::iterator i = m_cache_query_map->find(key);
	if(i != m_cache_query_map->end()) {
	  get_query_from_cache (i, class_name, query, rlist, values);
	  return;
	}
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_query_mutex);

      if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

      rdb::QueryCache::iterator i = m_cache_query_map->find(key);

      if(i == m_cache_query_map->end()) {
        i = m_cache_query_map->insert( std::make_pair(key, std::make_pair((rdb::RDBObjectList*)0,(rdb::RDBClassValueList*)0) ) ).first;
        unsigned long size = fill_get_all_objects(c, look_in_sclasses, recursion_depth, cnames, i->second.first, i->second.second);

        ERS_DEBUG( 3 , "xget_all_objects() [\'" << key << "\'] => " << i->second.first->length() << " object(s) (preloading " << size << " referenced objects from " << i->second.second->length() << " class(es))" );
      }

      get_query_from_cache (i, class_name, query, rlist, values);

      return;
    }
  }
}

unsigned long 
RDBWriter::fill_query_result(OksKernel& kernel, const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values)
{
  rlist = new rdb::RDBObjectList();
  OksObject::FSet refs;

  std::shared_lock rlock(kernel.get_mutex());

  if(OksClass * c = kernel.find_class(class_name)) {
    if(query == 0) query = "";

    std::unique_ptr<OksQuery> qe( new OksQuery(c, query) );

    if(qe->good() == false) {
      ers::Message issue(ERS_HERE, rdb::mk_bad_query_text(class_name, query, "bad syntax"));
      ers::debug(issue, 1);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }

    if(OksObject::List * objs = c->execute_query(qe.get())) {
      oks::ClassSet names4refs;
      rdb::sequence2classes(&kernel, cnames, names4refs);
      rdb::oks2rdb_list(objs, rlist, &refs, recursion_depth, &names4refs);
    }
  }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, class_name)));

  rdb::fill_values(refs, values);

  return refs.size();
}

void
RDBWriter::xget_objects_by_query(rdb::RDBSessionId session_id, const char * class_name, const char * query, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  
       // if session is inited, calculate and return object's values
      
  if(OksKernel * kernel = session->m_session_kernel) {
    RDB_SERVER_PROFILING_INFO("xget_objects_by_query(no-cache)")

    unsigned long size = fill_query_result(*kernel, class_name, query, recursion_depth, cnames, rlist, values);

    ERS_DEBUG( 2 , "xget_objects_by_query() query \"" << class_name << "\", \"" << query << "\" returns " << rlist->length() << " object(s) (preload " << size << " referenced objects from " << values->length() << " class(es))" );
  }


      // if session is not inited, get object's values from cache

  else {
    RDB_SERVER_PROFILING_INFO("xget_objects_by_query(cache)")

    std::string key = rdb::query_params_to_string(class_name, query, recursion_depth, cnames);

      // check the value is already in cache
    {
      std::shared_lock m(m_cache_query_mutex);

      if(m_cache_query_map) {
        rdb::QueryCache::iterator i = m_cache_query_map->find(key);
	if(i != m_cache_query_map->end()) {
	  get_query_from_cache (i, class_name, query, rlist, values);
	  return;
	}
      }
    }

      // put the value to cache
    {
      std::unique_lock m(m_cache_query_mutex);

      if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

      rdb::QueryCache::iterator i = m_cache_query_map->find(key);

      if(i == m_cache_query_map->end()) {
        i = m_cache_query_map->insert( std::make_pair(key, std::make_pair((rdb::RDBObjectList*)0,(rdb::RDBClassValueList*)0) ) ).first;
        unsigned long size = fill_query_result(m_kernel, class_name, query, recursion_depth, cnames, i->second.first, i->second.second);

        ERS_DEBUG( 3 , "xget_objects_by_query() [\'" << key << "\'] => " << i->second.first->length() << " object(s) (preloading " << size << " referenced objects from " << i->second.second->length() << " class(es))" );
      }

      get_query_from_cache (i, class_name, query, rlist, values);

      return;
    }
  }
}

void
RDBWriter::xget_objects_by_path(rdb::RDBSessionId session_id, const rdb::RDBObject& obj_from, const char* query, rdb::RDBObjectList_out rlist, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBClassValueList_out values)
{
  RDB_SERVER_PROFILING_INFO("xget_objects_by_path")

  rlist = new rdb::RDBObjectList();
  OksObject::FSet refs;

  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());

  try
    {
      const OksObject *oks_o = rdb::get_oks_object(kernel, obj_from.classid, obj_from.name);

      oks::QueryPath q(query, *kernel);

      if (OksObject::List *objs = oks_o->find_path(q))
        {
          oks::ClassSet names4refs;
          rdb::sequence2classes(kernel, cnames, names4refs);
          rdb::oks2rdb_list(objs, rlist, &refs, recursion_depth, &names4refs);
        }

      rdb::fill_values(refs, values);

      ERS_DEBUG( 2 , "path \"" << query << "\" from " << oks_o << " returns " << rlist->length() << " object(s) (preload " << refs.size() << " referenced object(s))" );
    }
  catch (const oks::bad_query_syntax& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, "cannot get objects by path", ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}


void
RDBWriter::prefetch_data(rdb::RDBSessionId session_id, rdb::RDBClassValueList_out values)
{
  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());
  fill_values(kernel, values);
}

void
RDBWriter::prefetch_schema(rdb::RDBSessionId session_id, rdb::RDBClassDescriptionList_out values)
{
  Session * session = get_session(session_id);
  OksKernel * kernel = get_kernel(session);

  std::shared_lock rlock(kernel->get_mutex());
  fill_schema(kernel, values);
}

void
RDBWriter::clean_cache(bool final)
{
  ERS_LOG("final: " << std::boolalpha << final);

    // clean inheritance hierarchy cache
  {
    std::unique_lock m(m_cache_inheritance_hierarchy_mutex);

    if(m_cache_inheritance_hierarchy_list2) {
      ERS_DEBUG( 3 , "clean_cache(): destroy second-level description of inheritance hierarchy stored in cache" );
      delete m_cache_inheritance_hierarchy_list2;
      m_cache_inheritance_hierarchy_list2 = 0;
    }

    if(m_cache_inheritance_hierarchy_list) {
      if(final) {
        ERS_DEBUG( 3 , "clean_cache(): destroy first-level description of inheritance hierarchy stored in cache" );
        delete m_cache_inheritance_hierarchy_list;
        m_cache_inheritance_hierarchy_list = 0;
      }
      else {
        m_cache_inheritance_hierarchy_list2 = m_cache_inheritance_hierarchy_list;
        m_cache_inheritance_hierarchy_list = 0;
      }
    }
  }


      // clean query cache
  {
    std::unique_lock m(m_cache_query_mutex);

    if(m_cache_query_map2) {
      ERS_DEBUG( 3 , "clean_cache() destroys second-level query cache" );
      rdb::destroy(m_cache_query_map2);
    }

    if(m_cache_query_map) {
      if(final) {
        ERS_DEBUG( 3 , "clean_cache() destroys first-level query cache" );
        rdb::destroy(m_cache_query_map);
      }
      else {
        m_cache_query_map2 = m_cache_query_map;
        m_cache_query_map = 0;
      }
    }
  }


    // clean object id cache
  {
    std::unique_lock m(m_cache_object_id_mutex);

    if(m_cache_object_id_map2) {
      ERS_DEBUG( 3 , "clean_cache(): destroy second-level object id cache" );
      rdb::destroy(m_cache_object_id_map2);
    }

    if(m_cache_object_id_map) {
      if(final) {
        ERS_DEBUG( 3 , "clean_cache(): destroy first-level object id cache" );
	rdb::destroy(m_cache_object_id_map);
      }
      else {
        m_cache_object_id_map2 = m_cache_object_id_map;
        m_cache_object_id_map = 0;
      }
    }
  }
}

void
RDBWriter::create_object(Session& session, const char * cid, const char * oid, OksFile * file_h)
{
  if (OksClass * c = session.m_session_kernel->find_class(cid))
    {
      try
        {
          if (c->get_is_abstract())
            throw daq::rdb::ClassIsAbstract(ERS_HERE, cid);

          if (oid && *oid)
            if (const OksObject * existing_obj = c->get_object(oid))
              throw daq::rdb::ObjectAlreadyExist(ERS_HERE, oid, cid, existing_obj->get_file()->get_full_file_name());

          if (session.m_session_kernel->get_test_duplicated_objects_via_inheritance_mode() == false)
            {
              session.m_session_kernel->set_test_duplicated_objects_via_inheritance_mode(true);
              session.m_session_kernel->registrate_all_classes();
            }

          lock_file(file_h);
          session.m_session_kernel->k_set_active_data(file_h);

          new OksObject(c, oid);
        }
      catch (const daq::rdb::Exception &ex)
        {
          throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotCreateObject(ERS_HERE, oid, cid, file_h->get_full_file_name(), session.m_id, ex)));
        }
      catch (oks::exception &ex)
        {
          // report oks exception as error since most probably there is a service issue (most typical error conditions were tested already)
          daq::rdb::CannotCreateObject issue(ERS_HERE, oid, cid, file_h->get_full_file_name(), session.m_id, ex);
          ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
          throw rdb::CannotProceed(daq::ers2idl_issue(issue));

        }
    }
  else
    throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::ClassNotFound(ERS_HERE, cid)));
}

void
RDBWriter::create_object(rdb::RDBSessionId session_id, const char * cid, const char * oid, const char * file_name )
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

   try
    {
      std::unique_lock wlock(kernel->get_mutex());
      create_object(*session, cid, oid, rdb::get_data_file(kernel, file_name));
    }
  catch (const daq::rdb::Exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotCreateObject(ERS_HERE, oid, cid, file_name, session->m_id, ex)));
    }
}

void
RDBWriter::new_object(rdb::RDBSessionId session_id, const char *cid, const char *oid, const rdb::RDBObject &o)
{
  std::shared_lock lock(m_mutex);

  Session *session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel *kernel = session->init(m_kernel);

  try
    {
      std::unique_lock wlock(kernel->get_mutex()); // FIXME 2023-09-04: check if this lock is needed
      const OksObject *obj = rdb::get_oks_object(kernel, o.classid, o.name);
      create_object(*session, cid, oid, obj->get_file());
    }
  catch (const rdb::GetOksObjectException &ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::delete_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, rdb::RDBClassChangesList_out removed_objects)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

  try
    {
      std::unique_lock wlock(kernel->get_mutex());

      OksObject * oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);

      lock_file(oks_obj->get_file());

      rdb::ChangesHolder ch(kernel);
      ch.start_oks_notification();
      DestroyGuard __dg__(ch, session->m_changes_holder);
      OksObject::destroy(oks_obj);

      config::map<rdb::ClassChanges*> cl_changes;

      for (const auto& i2 : ch.m_removed)
        for (const auto& j : i2.second)
          rdb::add(cl_changes, i2.first->get_name(), j, '-');

      removed_objects = new rdb::RDBClassChangesList();
      rdb::cvt_and_free(cl_changes, *removed_objects);
    }
  catch (oks::exception &ex)
    {
      daq::rdb::CannotDeleteObject issue(ERS_HERE, o.classid, o.name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotDeleteObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::move_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* file_name)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

  try
    {
      std::unique_lock wlock(kernel->get_mutex());

      OksObject *oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);
      OksFile *new_file_h = rdb::get_data_file(kernel, file_name);
      OksFile *old_file_h = oks_obj->get_file();

      if (old_file_h != new_file_h)
        {
          lock_file(old_file_h);
          lock_file(new_file_h);
          oks_obj->set_file(new_file_h, false);
        }
    }
  catch (oks::exception &ex)
    {
      daq::rdb::CannotMoveObject issue(ERS_HERE, o.classid, o.name, file_name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotMoveObject(ERS_HERE, o.classid, o.name, file_name, session_id, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::rename_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* new_name)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

  try
    {
      std::unique_lock wlock(kernel->get_mutex());

      OksObject *oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);

      std::set<OksFile*> files;

      files.insert(oks_obj->get_file());

      if (OksObject::FList *objs = oks_obj->get_all_rels())
        {
          for (auto &f : *objs)
            files.insert(f->get_file());

          delete objs;
        }

      for (auto &f : files)
        lock_file(f);

      oks_obj->set_id(new_name);
    }
  catch (oks::exception &ex)
    {
      daq::rdb::CannotRenameObject issue(ERS_HERE, o.classid, o.name, new_name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotRenameObject(ERS_HERE, o.classid, o.name, new_name, session_id, ex)));
    }
  catch (const rdb::GetOksObjectException &ex)
    {
      throw_rdb_NotFound(ex);
    }
}

void
RDBWriter::set_objects_of_relationship(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* r_name, const rdb::RDBObjectList& objs, ::CORBA::Boolean skip_non_null_check)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

    // set write lock to avoid a situation when another thread will change
    // the relationship or destroy the object before this method will be completed

  OksObject * oks_obj;
  OksData * d2;

  std::unique_lock wlock(kernel->get_mutex());

  try
    {
      oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);
      d2 = oks_obj->GetRelationshipValue(r_name);
    }
  catch (const oks::exception &ex)
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::RelationshipNotFound(ERS_HERE, r_name, o.classid, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }

  try
    {
      lock_file(oks_obj->get_file());
    }
  catch (oks::exception &ex)
    {
      daq::rdb::CannotUpdateObject issue(ERS_HERE, o.classid, o.name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }

  OksData d;

  if (d2->type == OksData::list_type)
    d.Set(new OksData::List());
  else
    d.Set((OksObject*) nullptr);

  for (unsigned i = 0; i < objs.length(); ++i)
    try
      {
        OksObject *robj = rdb::get_oks_object(kernel, objs[i].classid, objs[i].name);
        if (d.type == OksData::list_type)
          {
            d.data.LIST->push_back(new OksData(robj));
          }
        else
          {
            d.Set(robj);
            break;
          }
      }
    catch (const rdb::GetOksObjectException& ex)
      {
        std::stringstream text;
        text << "object at index " << i << " = " << objs[i] << " does not exist";
        throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, std::runtime_error(text.str().c_str()))));
      }

  try
    {
      oks_obj->SetRelationshipValue(r_name, &d, skip_non_null_check);
    }
  catch (const oks::exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
}

void
RDBWriter::set_attribute_value(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char * a_name, const rdb::DataUnion& rd)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);


    // set write lock to avoid a situation when another thread will change
    // the attribute or destroy the object before this method will be completed

  std::unique_lock wlock(kernel->get_mutex());


  OksObject * oks_obj;
  OksData * d;
  OksAttribute * a;

  try
    {
      oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);
      a = oks_obj->GetClass()->find_attribute(a_name);
      d = oks_obj->GetAttributeValue(a_name);
    }
  catch (const oks::exception& ex)
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::AttributeNotFound(ERS_HERE, a_name, o.classid, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }

  try
    {
      rdb::rdb2oks(rd, d, oks_obj, a);
      lock_file(oks_obj->get_file());
    }
  catch (oks::exception& ex)
    {
      daq::rdb::CannotUpdateObject issue(ERS_HERE, o.classid, o.name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
  catch (std::exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }

  try
    {
      oks_obj->SetAttributeValue(a_name, d);
    }
  catch (oks::exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
}

void
RDBWriter::set_attribute_values(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* a_name, const rdb::DataListUnion& l)
{
  std::shared_lock lock(m_mutex);

  Session * session = get_session(session_id);

  std::lock_guard s_lock(session->m_session_mutex);

  OksKernel * kernel = session->init(m_kernel);

    // set write lock to avoid a situation when another thread will change
    // the attribute or destroy the object before this method will be completed

  std::unique_lock wlock(kernel->get_mutex());


  OksObject * oks_obj;
  OksData * d ;
  OksAttribute * a;

  try
    {
      oks_obj = rdb::get_oks_object(kernel, o.classid, o.name);
      a = oks_obj->GetClass()->find_attribute(a_name);
      d = oks_obj->GetAttributeValue(a_name);
    }
  catch (oks::exception& ex)
    {
      throw rdb::NotFound(daq::ers2idl_issue(daq::rdb::AttributeNotFound(ERS_HERE, a_name, o.classid, ex)));
    }
  catch (const rdb::GetOksObjectException& ex)
    {
      throw_rdb_NotFound(ex);
    }

  if (d->data.LIST)
    {
      while (!d->data.LIST->empty())
        {
          OksData *d2 = d->data.LIST->front();
          d->data.LIST->pop_front();
          delete d2;
        }
    }
  else
    {
      d->data.LIST = new OksData::List();
    }

  try
    {
      rdb::rdb2oks(l, d, oks_obj, a);
      lock_file(oks_obj->get_file());
    }
  catch (oks::exception& ex)
    {
      daq::rdb::CannotUpdateObject issue(ERS_HERE, o.classid, o.name, session_id, ex);
      ers::error(issue);
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }
  catch (const daq::rdb::Exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
  catch (std::exception& ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }

  try
    {
      oks_obj->SetAttributeValue(a_name, d);
    }
  catch (oks::exception &ex)
    {
      throw rdb::CannotProceed(daq::ers2idl_issue(daq::rdb::CannotUpdateObject(ERS_HERE, o.classid, o.name, session_id, ex)));
    }
}


void
RDBWriter::subscribe(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info, const rdb::RDBNameList& names, bool look_in_suclasses, const rdb::RDBObjectList& objs, rdb::callback_ptr cb, CORBA::LongLong parameter)
{
  {
    std::unique_lock lock(m_mutex);

    Session * session = get_session(session_id);

    std::lock_guard s_lock(session->m_session_mutex);

    if(session->m_subscription) {
      std::ostringstream text;
      text << "subscription in session " << session_id << " already exists: " << IPCCore::objectToString(session->m_subscription->m_cb, IPCCore::Ior );
      throw rdb::CannotProceed(daq::ers2idl_issue(ers::Message(ERS_HERE, text.str())));
    }

    session->m_subscription = new rdb::Subscription(info, names, look_in_suclasses, objs, cb, parameter);

    ERS_LOG( "create client subscription in session " << session_id << " for \"" << session->m_subscription->m_app_name << "\" application started on \"" << session->m_subscription->m_host << "\" with " << session->m_subscription->m_process_id << " by \"" << session->m_subscription->m_user_id << "\"");

    set_oks_changes_subscriptions();
  }

  request_backup();
}

void
RDBWriter::unsubscribe(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info)
{
  ERS_LOG( "removing subscription for session " << session_id );

  {
    std::unique_lock lock(m_mutex);

    Session * session = get_session(session_id);

    std::lock_guard s_lock(session->m_session_mutex);

    if(session->m_subscription) {
      delete session->m_subscription;
      session->m_subscription = nullptr;
    }
    else {
      std::ostringstream text;
      text << "cannot unsubscribe session " << session_id << " since it was not subscribed (requested by application \'"
           << info.app_name << "\' run by \'" << rdb::get_user_safe(info) << "\' on \'" << info.host << "\' with pid " << info.process_id << ')';
      ers::Message issue(ERS_HERE, text.str());
      ers::error(daq::rdb::UserRequestFailed(ERS_HERE, name(), partition().name(), issue));
      throw rdb::CannotProceed(daq::ers2idl_issue(issue));
    }

    set_oks_changes_subscriptions();
  }

  request_backup();
}

void
RDBWriter::set_oks_changes_subscriptions()
{
  bool subscibe_main(false);

  for(std::map<int64_t, Session*>::const_iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
    if(i->second->m_subscription) {
      if(i->second->m_changes_holder) {
        i->second->m_changes_holder->start_oks_notification();
        ERS_LOG( "[re-]start OKS changes subscription for session " << i->first );
      }
      else {
        subscibe_main = true;
      }
    }
    else {
      if(i->second->m_changes_holder) {
        i->second->m_changes_holder->stop_oks_notification();
        ERS_LOG( "stop OKS changes subscription for session " << i->first );
      }
    }
  }

  if(subscibe_main) {
    ERS_LOG( "[re-]start OKS changes subscription" );
    m_changes_holder.start_oks_notification();
  }
  else {
    ERS_LOG( "stop OKS changes subscription" );
    m_changes_holder.stop_oks_notification();
  }
}


struct SendPingCommand
{
  RDBWriter::Session * m_session;
  RDBWriter * m_server;

  SendPingCommand(RDBWriter::Session * session, RDBWriter * server) :
    m_session   (session),
    m_server    (server) { ; }

  void operator()() {
    try {
      ERS_LOG( "ping session " << m_session->m_id );

      rdb::RDBSessionId ret = m_session->m_ping_cb->get_session_id();
      m_server->clean_bad_session(m_session->m_id);

      if(ret != m_session->m_id) {
        std::cerr << "get_session_id() for session " << m_session->m_id << " returned " << ret << std::endl;
      }
    }
    catch ( CORBA::SystemException & ex ) {
      daq::ipc::CorbaSystemException error(ERS_HERE, &ex);
      m_server->bad_session(m_session->m_id, error);
    }

    delete this;
  }

};

void
RDBWriter::ping_sessions()
{
  RDB_SERVER_PROFILING_INFO("ping_sessions")

  std::vector<std::shared_ptr<std::thread> > threads;
  std::list<SendPingCommand *> objs;

  std::unique_lock lock(m_mutex);


    // the intermediate list is used instead of map of session, since thread may remove session from the map
  {
    for(std::map<int64_t, Session*>::const_iterator i = m_sessions.begin(); i != m_sessions.end(); ++i) {
      objs.push_back(new SendPingCommand(i->second, this));
    }
  }

  {
    for(std::list<SendPingCommand *>::const_iterator i = objs.begin(); i != objs.end(); ++i) {
      std::shared_ptr<std::thread> thread(new std::thread(std::ref(*(*i))));
      threads.push_back(thread);
    }
  }

    // wait for all threads in the pool to exit
  for (std::size_t j = 0; j < threads.size(); ++j) {
    threads[j]->join();
  }

  ERS_LOG("sent ping to " << threads.size() << " sessions");
}

static std::mutex s_bad_session_mutex; // shared by clean_bad_session and bad_session methods
static std::set<rdb::RDBSessionId> s_once_bad;

void
RDBWriter::clean_bad_session(rdb::RDBSessionId session_id)
{
  std::lock_guard lock(s_bad_session_mutex);

  if(s_once_bad.erase(session_id)) {
    ERS_LOG( "reset bad session " << session_id );
  }
}

void
RDBWriter::bad_session(rdb::RDBSessionId session_id, daq::ipc::CorbaSystemException& ex)
{
  std::map<int64_t, Session*>::iterator j = m_sessions.find(session_id);
  RDBWriter::Session * s = j->second;

  std::lock_guard lock(s_bad_session_mutex);

  std::set<rdb::RDBSessionId>::iterator i = s_once_bad.find(session_id);

  if(i == s_once_bad.end()) {
    s_once_bad.insert(session_id);

    daq::rdb::SessionPingFailed issue(
      ERS_HERE, session_id, s->m_session_app_name, s->m_session_user_id, s->m_session_host, s->m_session_process_id, "ignoring once...", ex
    );

    if(!strcmp("TRANSIENT", ex.get_exception()->_name())) {
      ers::log(issue);
    }
    else {
      ers::warning(issue);
    }
  }
  else {
    daq::rdb::SessionPingFailed issue(
      ERS_HERE, session_id, s->m_session_app_name, s->m_session_user_id, s->m_session_host, s->m_session_process_id, "close session", ex
    );

    m_sessions.erase(j);
    delete s;

    request_backup();

    if(!strcmp("TRANSIENT", ex.get_exception()->_name())) {
      ers::log(issue);
    }
    else {
      ers::error(issue);
    }
  }

}
