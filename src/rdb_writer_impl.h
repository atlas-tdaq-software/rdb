/**
 * Defines interfaces to the RDB writer server.
 *
 * @author  <Igor.Soloviev@cern.ch>
 * @version 08/06/10
 * @since   
 */

#ifndef RDB_WRITER_IMPL_H
#define RDB_WRITER_IMPL_H

#include <stdint.h>

#include <map>
#include <mutex>
#include <shared_mutex>
#include <set>
#include <stdexcept>

#include <ipc/object.h>
#include <AccessManager/client/ServerInterrogator.h>

#include <oks/object.h>

#include "rdb/rdb.hh"
#include "rdb/errors.h"
#include "server_utils.h"


class RDBWriter: public IPCNamedObject<POA_rdb::writer>
{
  friend class RDBWriterCallbackInfo;
  friend class SendReloadCommand;

    // Generated from IDL

  public:

    rdb::RDBSessionId open_session(const rdb::ProcessInfo& info, rdb::ping_ptr cb);
    void set_commit_credentials( rdb::RDBSessionId session_id, const char * user, const char * password );
    void close_session(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info);
    void list_sessions(const rdb::ProcessInfo& info, rdb::SessionInfoList_out data);

    void create_database(rdb::RDBSessionId session_id, const char * db_name);
    void get_databases(rdb::RDBSessionId session_id, rdb::RDBNameList_out schemafiles, rdb::RDBNameList_out datafiles);
    void get_modified_databases(rdb::RDBSessionId session_id, rdb::RDBNameList_out updated_datafiles, rdb::RDBNameList_out externally_updated_datafiles, rdb::RDBNameList_out externally_removed_datafiles);
    void update_database_status(const rdb::ProcessInfo& info, rdb::RDBSessionId session_id, const char* db_name);
    void commit_changes(rdb::RDBSessionId session_id, const char* token, const char* log);
    void abort_changes(rdb::RDBSessionId session_id);
    void reload_database(const rdb::ProcessInfo& info, const rdb::RDBNameList& names);

    rdb::RDBRepositoryVersionList* get_changes();
    rdb::RDBRepositoryVersionList* get_versions(CORBA::Boolean skip_irrelevant, rdb::VersionsQueryType query_type, const char * since, const char * until);

    void database_add_file(rdb::RDBSessionId session_id, const char* db_name, const char* file_name);
    void database_remove_file(rdb::RDBSessionId session_id, const char* db_name, const char* file_name, rdb::RDBClassChangesList_out removed_objects);
    void database_get_files(rdb::RDBSessionId session_id, const char* db_name, rdb::RDBNameList_out file_names);
    bool is_database_writable(rdb::RDBSessionId session_id, const char* db_name );

    void get_all_classes(rdb::RDBSessionId session_id, rdb::RDBClassList_out l);
    void get_class(rdb::RDBSessionId session_id, const char* cid, rdb::RDBClass_out c);
    void get_direct_super_classes(rdb::RDBSessionId session_id, const char* cid, rdb::RDBClassList_out l);
    void get_all_super_classes(rdb::RDBSessionId session_id, const char* cid, rdb::RDBClassList_out l);
    void get_sub_classes(rdb::RDBSessionId session_id, const char* cid, rdb::RDBClassList_out l);
    void get_direct_sub_classes(rdb::RDBSessionId session_id, const char* cid, rdb::RDBClassList_out l);
    void get_attributes(rdb::RDBSessionId session_id, const char* cid, rdb::RDBAttributeList_out l);
    void get_relationships(rdb::RDBSessionId session_id, const char* cid, rdb::RDBRelationshipList_out l);
    void get_inheritance_hierarchy(rdb::RDBSessionId session_id, rdb::RDBInheritanceHierarchyList_out l);

    void get_object(rdb::RDBSessionId session_id, const char* cid, ::CORBA::Boolean lis, const char* oid, rdb::RDBObject_out o);
    void get_file_of_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, ::CORBA::String_out file);
    void get_files_of_all_objects(rdb::RDBSessionId session_id, rdb::RDBFileObjectsList_out info);
    void get_all_objects(rdb::RDBSessionId session_id, const char* cid, ::CORBA::Boolean lis, rdb::RDBObjectList_out l);
    void get_objects_by_query(rdb::RDBSessionId session_id, const char* cid, const char* query, rdb::RDBObjectList_out l);
    void get_objects_by_path(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* query, rdb::RDBObjectList_out l);

    void get_object_values(rdb::RDBSessionId session_id, const char *cid, const char *oid, rdb::RDBAttributeValueList_out s_attr_values, rdb::RDBAttributeValuesList_out m_attr_values, rdb::RDBRelationshipValueList_out rel_values)
    {
      std::shared_lock lock(m_mutex);
      rdb::get_object_values(get_kernel(get_session(session_id)), cid, oid, s_attr_values, m_attr_values, rel_values);
    }

    void cget_object_values(rdb::RDBSessionId session_id, const char *cid, const char *oid, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
    {
      std::shared_lock lock(m_mutex);
      rdb::cget_object_values(get_kernel(get_session(session_id)), cid, oid, s_attr_values, m_attr_values, rel_values);
    }

    void xget_object_values(rdb::RDBSessionId session_id, const char *cid, const char *oid, rdb::RDBNameList_out s_attr_names, rdb::RDBNameList_out m_attr_names, rdb::RDBNameList_out r_names, rdb::RDBAttributeCompactValueList_out s_attr_values, rdb::RDBAttributeCompactValuesList_out m_attr_values, rdb::RDBRelationshipCompactValueList_out rel_values)
    {
      std::shared_lock lock(m_mutex);
      rdb::xget_object_values(get_kernel(get_session(session_id)), cid, oid, s_attr_names, m_attr_names, r_names, s_attr_values, m_attr_values, rel_values);
    }

    void get_objects_of_relationship(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* r_name, rdb::RDBObjectList_out objs);
    void get_referenced_by(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* r_name, ::CORBA::Boolean composite_only, rdb::RDBObjectList_out objs);
    void get_attribute_value(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* a_name, rdb::DataUnion_out d);
    void get_attribute_values(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* a_name, rdb::DataListUnion_out l);
    void xget_object(rdb::RDBSessionId session_id, const char* cid, ::CORBA::Boolean lis, const char* oid, rdb::RDBObject_out o, ::CORBA::Long recursion_depth, const rdb::RDBNameList& rcls, rdb::RDBClassValueList_out values);
    void xget_all_objects(rdb::RDBSessionId session_id, const char* cid, ::CORBA::Boolean lis, rdb::RDBObjectList_out l, ::CORBA::Long recursion_depth, const rdb::RDBNameList& rcls, rdb::RDBClassValueList_out values);
    void xget_objects_by_query(rdb::RDBSessionId session_id, const char* cid, const char* query, rdb::RDBObjectList_out l, ::CORBA::Long recursion_depth, const rdb::RDBNameList& rcls, rdb::RDBClassValueList_out values);
    void xget_objects_by_path(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* query, rdb::RDBObjectList_out l, ::CORBA::Long recursion_depth, const rdb::RDBNameList& rcls, rdb::RDBClassValueList_out values);
    void prefetch_data(rdb::RDBSessionId session_id, rdb::RDBClassValueList_out values);
    void prefetch_schema(rdb::RDBSessionId session_id, rdb::RDBClassDescriptionList_out values);

    void create_object(rdb::RDBSessionId session_id, const char* cid, const char* oid, const char* file_name);
    void new_object(rdb::RDBSessionId session_id, const char* cid, const char* oid, const rdb::RDBObject& o);
    void delete_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, rdb::RDBClassChangesList_out removed_objects);
    void move_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* file_name);
    void rename_object(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* new_name);

    void set_objects_of_relationship(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* r_name, const rdb::RDBObjectList& objs, ::CORBA::Boolean skip_non_null_check);
    void set_attribute_value(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* a_name, const rdb::DataUnion& d);
    void set_attribute_values(rdb::RDBSessionId session_id, const rdb::RDBObject& o, const char* a_name, const rdb::DataListUnion& l);

    void subscribe(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info, const rdb::RDBNameList& l1, ::CORBA::Boolean lis, const rdb::RDBObjectList& l2, rdb::callback_ptr cb, ::CORBA::LongLong parameter);
    void unsubscribe(rdb::RDBSessionId session_id, const rdb::ProcessInfo& info);


  public:

    struct Session {

      std::string m_server_name;
      std::string m_partition_name;

        // creation info

      const std::string  m_session_user_id;        ///< User ID
      const std::string  m_session_host;           ///< Host name
      const long         m_session_process_id;     ///< ID of user's process
      const std::string  m_session_app_name;       ///< application name
      std::string  m_session_creation_ts;          ///< subscription time


        // user info

      std::string  m_user_id;                      ///< User ID
      std::string  m_user_password;                ///< User password


        // session data

      OksKernel * m_session_kernel;
      rdb::Subscription * m_subscription;
      rdb::ping_var m_ping_cb;
      std::set<OksFile *> m_created_files;
      std::mutex m_session_mutex;
      std::set<std::string> m_check_out_files;
      rdb::ChangesHolder * m_changes_holder;
      rdb::RDBSessionId m_id;

      Session(const std::string& server, const std::string& partition, const std::string& user_id, const std::string& host, long pid, const std::string& app_name, rdb::ping_ptr cb);

      ~Session();

      OksKernel * init(const OksKernel& kernel);
      void clear();

    };



  public:

      /**
       *  Run remote database writer server with name 'server_name' in the partition 'partition'.
       */

    RDBWriter (const char * server_name, IPCPartition & partition);


      /**
       *  Destroy database server.
       */

    ~RDBWriter ();


      /**
       *  Shutdown remote database server.
       */

    void shutdown ( );


  private:

    OksKernel m_kernel;
    rdb::ChangesHolder m_changes_holder;
    std::map<int64_t, Session*> m_sessions;
    daq::am::ServerInterrogator m_am_server;


    std::shared_mutex m_mutex;


     // inheritance hierarchy cache

    std::shared_mutex m_cache_inheritance_hierarchy_mutex;
    rdb::RDBInheritanceHierarchyList * m_cache_inheritance_hierarchy_list;
    rdb::RDBInheritanceHierarchyList * m_cache_inheritance_hierarchy_list2;

    void get_inheritance_hierarchy_from_cache (rdb::RDBInheritanceHierarchyList_out l);


      // query & get_all objects cache

    std::shared_mutex m_cache_query_mutex;
    rdb::QueryCache * m_cache_query_map;
    rdb::QueryCache * m_cache_query_map2;

    void get_query_from_cache (rdb::QueryCache::iterator& i, const char * class_name, const char * query, rdb::RDBObjectList_out rlist, rdb::RDBClassValueList_out values);
    unsigned long fill_query_result (OksKernel& kernel, const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values);
    unsigned long fill_get_all_objects(const OksClass * c, bool look_in_sclasses, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, rdb::RDBObjectList*& rlist, rdb::RDBClassValueList*& values);


      // obj cache

    std::shared_mutex m_cache_object_id_mutex;
    rdb::ObjectIDCache * m_cache_object_id_map;
    rdb::ObjectIDCache * m_cache_object_id_map2;

    rdb::RDBObject * get_object_id_from_cache (rdb::ObjectIDCache::iterator& i, const char * class_name, const char * object_id, bool look_in_sclasses, rdb::RDBClassValueList_out values);
    unsigned long fill_object_id_result (OksKernel * kernel, const char * classid, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames, std::string& oks_obj_class_name, rdb::RDBClassValueList*& values);


      // clean cache

    void clean_cache(bool final = false);

    void create_object(Session& session, const char * cid, const char * oid, OksFile * file_h);


      /** Holds name of backup file */

    std::string m_backup_file;


  public:

    OksKernel& get_kernel() { return m_kernel; }


      /** The method sets name of the backup file. */

    void set_backup_file(const std::string& s) {m_backup_file = s;}


      /** The method informs server about request to make backup file. */

    void request_backup() const;


      /** The method produces backup file if there are requests. */

    void try_backup();


      /** The method makes backup. */

    void make_backup();


      /** The method sets remote database parameters stored in the backup file 'backup_file'. */

    void load_backup(const char * backup_file);


      /** The method starts backup thread. */

    void start_backup_thread(size_t backup_interval);

      /** The method stops backup thread. */

    static void stop_backup_thread();


      /** Ping sessions and remove dead ones. */
    
    void ping_sessions();

      /** Report bad session and take decision to remove session or not. */
    
    void bad_session(rdb::RDBSessionId session_id, daq::ipc::CorbaSystemException& ex);

      /** Reset possible bad session. */

    void clean_bad_session(rdb::RDBSessionId session_id);


  private:

    Session * get_session(int64_t id);  // throw rdb::NotFound
    OksKernel * get_kernel(Session *);

    unsigned long notify();

    void set_oks_changes_subscriptions();       // [un-]register OKS callbacks

    void lock_file(OksFile * file_h);

};


#endif
