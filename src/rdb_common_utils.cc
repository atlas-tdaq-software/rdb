#include <iostream>
#include <sstream>
#include <string>

#include "src/common_utils.h"

char*
rdb::string_dup(const std::string *str)
{
  char* r = new char[str->size() + 1];
  strcpy(r, str->c_str());
  return r;
}

static void
ref2stream(std::ostringstream& s, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames)
{
  if(recursion_depth) {
    s << '@' << recursion_depth;

    if(cnames.length()) {
      s << '@';
      for(unsigned long idx = 0; idx < cnames.length(); idx++) {
        if(idx) s << ':';
	s << cnames[idx];
      }
    }
  }
}

std::string
rdb::query_params_to_string(const char * class_name, const char * query, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames)
{
  std::ostringstream s;
  s << class_name << '@' << query;
  
  ref2stream(s, recursion_depth, cnames);

  return s.str();
}

std::string
rdb::object_id_params_to_string(const char * class_name, bool look_in_sclasses, const char * objectid, CORBA::Long recursion_depth, const rdb::RDBNameList& cnames)
{
  std::ostringstream s;
  s << class_name;

  if(look_in_sclasses == false) {
    s << "@N";
  }

  s << '@' << objectid;

  ref2stream(s, recursion_depth, cnames);

  return s.str();
}

rdb::RDBObject *
rdb::make_rdb_object(const std::string& class_name, const char * object_id)
{
  rdb::RDBObject * object = new rdb::RDBObject();
  object->classid = rdb::string_dup(class_name);
  object->name    = CORBA::string_dup(object_id);
  return object;
}

void
rdb::destroy(rdb::ObjectIDCache *&qc)
{
  for (auto& i : *qc)
    delete i.second.second;

  delete qc;
  qc = nullptr;
}

void
rdb::destroy(rdb::QueryCache *& qc)
{
  for(rdb::QueryCache::iterator i = qc->begin(); i != qc->end(); ++i) {
    delete i->second.first;
    delete i->second.second;
  }

  delete qc;
  qc = nullptr;
}
