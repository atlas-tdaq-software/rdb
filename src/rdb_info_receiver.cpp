#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>

#include <system/Host.h>
#include <system/User.h>

#include <ers2idl/ers2idl.h>
#include <daq_tokens/acquire.h>

#include "rdb/rdb_info.h"
#include "server_profiler.h"
#include "src/common_utils.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(__sun__) || defined(__rtems__)
  extern "C" int getrusage( int who, struct rusage *rusage );
  #define CLOCK_PROCESS_CPUTIME_ID	CLOCK_PROCESS_CPUTIME
#endif 

rdb::ServerProfiler::ServerProfiler(bool use, const char * fname) : m_use(use), m_fname(fname)
{
  if(m_use) {
    getrusage( RUSAGE_SELF, &m_ru_1 );
    clock_gettime( CLOCK_PROCESS_CPUTIME_ID , &m_ts_1);
  }
}

unsigned long
rdb::ServerProfiler::now(bool stop)
{
  if(m_use) {
    getrusage( RUSAGE_SELF, &m_ru_2 );
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &m_ts_2);

    unsigned long msec = (m_ts_2.tv_sec - m_ts_1.tv_sec) * 1000000 + (m_ts_2.tv_nsec - m_ts_1.tv_nsec) / 1000;

    ERS_DEBUG( 2, "time per function " << m_fname <<
        " -> real: " << msec << " us "
        "(user: " << (m_ru_2.ru_utime.tv_sec - m_ru_1.ru_utime.tv_sec) * 1000 + (m_ru_2.ru_utime.tv_usec - m_ru_1.ru_utime.tv_usec + 499)/1000 << " ms, "
        "sys: " << (m_ru_2.ru_stime.tv_sec - m_ru_1.ru_stime.tv_sec) * 1000 + (m_ru_2.ru_stime.tv_usec - m_ru_1.ru_stime.tv_usec + 499)/1000 << " ms)\n" );

    if(stop) m_use = false;

    return msec;
  }

  return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

namespace rdb {

  std::string ProcessInfoHelper::m_user_id;
  std::string ProcessInfoHelper::m_host;
  long ProcessInfoHelper::m_process_id;
  std::string ProcessInfoHelper::m_app_name;

  void ProcessInfoHelper::init() noexcept
  {
    if(!m_process_id) {

      System::User myself;
      m_user_id = myself.name_safe();

      m_host = System::LocalHost::instance()->full_local_name();

      m_process_id = getpid();

        // get application name
      if(m_app_name.empty()) {
        if(const char * v = getenv("TDAQ_APPLICATION_NAME")) m_app_name = v;
        else m_app_name = "(unknown)";
      }

    }
  }

  void ProcessInfoHelper::refresh(rdb::ProcessInfo& info)
  {
    static bool s_tokens_enabled = daq::tokens::enabled();

    if (s_tokens_enabled)
      info.user_id = rdb::string_dup(daq::tokens::acquire());
    else
      info.user_id = rdb::string_dup(get_user_id());
  }

  void ProcessInfoHelper::set(rdb::ProcessInfo& info)
  {
    refresh(info);

    info.host = rdb::string_dup(get_host());
    info.process_id = get_process_id();
    info.app_name = rdb::string_dup(get_app_name());
  }


  rdb::ProcessInfo
  ProcessInfoHelper::get()
    {
      rdb::ProcessInfo info;
      set(info);
      return info;
    }
}

std::ostream&
operator<<(std::ostream& s, const rdb::ProcessInfo& info)
{
  s << "user: \'" << info.user_id << "\' host: \'" << info.host << "\' process-id: " << info.process_id;
  return s;
}


void
RDBInfoReceiver::init()
{
  try {
    if(m_session_id == 0) {
      m_read_cursor = m_partition.lookup<rdb::cursor>(m_server_name.c_str());
    }
    else {
      m_write_cursor = m_partition.lookup<rdb::writer>(m_server_name.c_str());
    }
  }
  catch( daq::ipc::Exception & ex ) {
    throw daq::rdb::LookupFailed(ERS_HERE, m_server_name.c_str(), m_partition.name().c_str(), ex);
  }
}

RDBInfoReceiver::RDBInfoReceiver(const IPCPartition & p, const char * server_name, rdb::RDBSessionId session_id, const char * app_name) noexcept :
  m_partition   (p),
  m_server_name (server_name),
  m_cb_info     (nullptr),
  m_id          (0),
  m_session_id  (session_id)
{
  rdb::ProcessInfoHelper::set_app_name(app_name);
}

void
RDBInfoReceiver::subscribe (
  const std::set<std::string>& class_names,
  bool look_in_subclasses,
  const std::map< std::string , std::set<std::string> >& objects,
  RDBCallbackInfo::Callback cb,
  int64_t param)
{
  rdb::RDBNameList lc;
  rdb::RDBObjectList lo;

    // fill CORBA structures

  try {
      // subscribe on classes
    {
      unsigned int idx = 0;
      lc.length(class_names.size());
      for(const auto& i : class_names) {
        lc[idx++] = rdb::string_dup(i);
      }
    }

      // subscribe on objects
    {
      unsigned int idx = 0;
      std::map< std::string , std::set<std::string> >::const_iterator i = objects.begin();
      for(; i != objects.end(); ++i) { idx += i->second.size(); }
      lo.length(idx);
      idx = 0;
      for(i = objects.begin(); i != objects.end(); ++i) {
        for(std::set<std::string>::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
          lo[idx].classid = rdb::string_dup(i->first);
          lo[idx++].name = rdb::string_dup(*j);
        }
      }
    }
  }
  catch( CORBA::SystemException & ex ) {
    throw daq::ipc::CorbaSystemException(ERS_HERE, &ex);
  }

  init();

  try {
    rdb::ProcessInfo proc_info;
    rdb::ProcessInfoHelper::set(proc_info);
    m_cb_info = new RDBCallbackInfo( cb, param );
    if(!m_session_id) {
      m_id = m_read_cursor->subscribe( proc_info, lc, look_in_subclasses, lo, m_cb_info->_this(), reinterpret_cast<int64_t>(this));
    }
    else {
      m_write_cursor->subscribe( m_session_id, proc_info, lc, look_in_subclasses, lo, m_cb_info->_this(), reinterpret_cast<int64_t>(this));
    }
  }
  catch( CORBA::SystemException & ex ) {
    clear_cb_info();
    throw daq::ipc::CorbaSystemException(ERS_HERE, &ex);
  }
  catch ( rdb::SessionNotFound & ex ) {
    clear_cb_info();
    throw daq::rdb::SessionNotFound(ERS_HERE, ex.session_id);
  }
  catch ( rdb::CannotProceed & ex ) {
    clear_cb_info();
    throw daq::rdb::CannotSubscribe(ERS_HERE, *daq::idl2ers_issue(ex.issue));
  }

}

void
RDBInfoReceiver::unsubscribe()
{
  if(m_cb_info) {
    m_cb_info->_destroy();
    m_cb_info = 0;

    init();

    try {
      rdb::ProcessInfo proc_info;
      rdb::ProcessInfoHelper::set(proc_info);
      if(!m_session_id) {
        m_read_cursor->unsubscribe(proc_info, m_id);
      }
      else {
        m_write_cursor->unsubscribe(m_session_id, proc_info);
      }
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw daq::rdb::SessionNotFound(ERS_HERE, ex.session_id);
    }
    catch ( const rdb::NotFound & ex ) {
      throw daq::rdb::CannotUnsubscribe(ERS_HERE, *daq::idl2ers_issue(ex.issue));
    }
    catch ( rdb::CannotProceed & ex ) {
      clear_cb_info();
      throw daq::rdb::CannotUnsubscribe(ERS_HERE, *daq::idl2ers_issue(ex.issue));
    }
    catch( CORBA::SystemException & ex ) {
      throw daq::ipc::CorbaSystemException(ERS_HERE, &ex);
    }
  }
  else {
    ERS_LOG("there is no any subscription");
  }
}

CORBA::Long
RDBCallbackInfo::inform(const rdb::RDBClassChangesList& changes, CORBA::LongLong p)
{
  rdb::ServerProfiler profiler(true, "RDBCallbackInfo::inform");

  m_changes = &changes;

  ERS_DEBUG(3, "call RDBCallbackInfo::inform(" << (reinterpret_cast<RDBInfoReceiver *>(p))->get_id() << ')' );

  (*m_callback)(this);

  return profiler.now(true);
}

void
RDBCallbackInfo::server_exits(CORBA::LongLong p)
{
  RDBInfoReceiver * info_receiver(reinterpret_cast<RDBInfoReceiver *>(p));

  ERS_LOG( "got server_exits() request on subscriber " << info_receiver->get_id() );

  if(info_receiver->m_cb_info) {
    info_receiver->m_cb_info = 0;
  }

  _destroy();
}
