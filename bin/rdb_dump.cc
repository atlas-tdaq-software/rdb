/**	
 *	\file rdb/bin/rdb_dump.cc
 *	
 *	This file is part of the RDB package.
 *	Authors: <Sergei.Kolos@cern.ch> and <Igor.Soloviev@cern.ch>
 *	
 *	This file contains code to display the contents of a specific
 *      database server at different level of details.
 */

#include <string.h>
#include <stdlib.h>
#include <iostream>

#include <boost/program_options.hpp>

#include <ipc/exceptions.h>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/core.h>
#include <ers2idl/ers2idl.h>

#include "rdb/rdb.hh"
#include "rdb/rdb_info.h"
#include "rdb/errors.h"

ERS_DECLARE_ISSUE(
  rdb_dump,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_dump,
  RequestFailed,
  "method " << method << "(\"" << args <<"\") failed",
  ((std::string)method)
  ((std::string)args)
)

static const char*
session2str(rdb::RDBSessionId session)
{
  static std::string buf;

  if (buf.empty())
    {
      std::ostringstream s;
      s << "session " << session << " is not found";
      buf = s.str();
    }

  return buf.c_str();
}

  /// The cursor is used for communication with server.

static rdb::cursor_var cursor;        /// the 'cursor' is used for communication with read-only server
static rdb::writer_var writer;        /// the 'writer' is used for communication with read-write server
static rdb::RDBSessionId session;     /// the 'session' is identifier for all communications with writer server
bool is_writer;


/// Command line arguments used by many functions.
bool show_attributes, show_relationships, show_objects, show_data, show_data_old_style;

static const char*
bool2str(bool v)
{
  return (v ? "true" : "false");
}

static void
print_name(const char *prefix_str, size_t width, const char *name)
{
  std::cout << prefix_str;
  std::cout.width(width);
  std::cout << name << ' ';
}


    /// Print OKS data.

static void
print_value(const rdb::DataUnion& rd)
{
  switch (rd._d())
    {
      case rdb::pk_s8_int:  std::cout << "[8-bits integer]           : "   << static_cast<int16_t>(rd.du_s8_int());  break;
      case rdb::pk_u8_int:  std::cout << "[8-bits unsigned integer]  : "   << static_cast<uint16_t>(rd.du_u8_int()); break;
      case rdb::pk_s16_int: std::cout << "[16-bits integer]          : "   << rd.du_s16_int();                       break;
      case rdb::pk_u16_int: std::cout << "[16-bits unsigned integer] : "   << rd.du_u16_int();                       break;
      case rdb::pk_s32_int: std::cout << "[32-bits integer]          : "   << rd.du_s32_int();                       break;
      case rdb::pk_u32_int: std::cout << "[32-bits unsigned integer] : "   << rd.du_u32_int();                       break;
      case rdb::pk_s64_int: std::cout << "[64-bits integer]          : "   << rd.du_s64_int();                       break;
      case rdb::pk_u64_int: std::cout << "[64-bits unsigned integer] : "   << rd.du_u64_int();                       break;
      case rdb::pk_float:   std::cout << "[float]                    : "   << rd.du_float();                         break;
      case rdb::pk_double:  std::cout << "[double]                   : "   << rd.du_double();                        break;
      case rdb::pk_boolean: std::cout << "[boolean]                  : "   << bool2str(rd.du_bool());                break;
      case rdb::pk_string:  std::cout << "[string-based]             : \"" << rd.du_string() << '\"';                break;
      case rdb::pk_unknown: std::cout << "[unknown]                  : ERROR, bad type";                             break;
    }

  std::cout << std::endl;
}

static void
print_sep(size_t i)
{
  if (i != 0)
    std::cout << ", ";
}


    /// Print multi-value attribute.
    /**
     *  The function prints list of data items as {v1, v2, ..., vN}.
     */

static void
print_data(const rdb::DataListUnion& rd)
{
  switch (rd._d())
    {
      case rdb::pk_s8_int:  { std::cout << "[8-bits integer]           : {"; const auto & seq = rd.du_s8_ints();  for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << static_cast<int16_t>(seq[i]);  } break; }
      case rdb::pk_u8_int:  { std::cout << "[8-bits unsigned integer]  : {"; const auto & seq = rd.du_u8_ints();  for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << static_cast<uint16_t>(seq[i]); } break; }
      case rdb::pk_s16_int: { std::cout << "[16-bits integer]          : {"; const auto & seq = rd.du_s16_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_u16_int: { std::cout << "[16-bits unsigned integer] : {"; const auto & seq = rd.du_u16_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_s32_int: { std::cout << "[32-bits integer]          : {"; const auto & seq = rd.du_s32_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_u32_int: { std::cout << "[32-bits unsigned integer] : {"; const auto & seq = rd.du_u32_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_s64_int: { std::cout << "[64-bits integer]          : {"; const auto & seq = rd.du_s64_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_u64_int: { std::cout << "[64-bits unsigned integer] : {"; const auto & seq = rd.du_u64_ints(); for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_float:   { std::cout << "[float]                    : {"; const auto & seq = rd.du_floats();   for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_double:  { std::cout << "[double]                   : {"; const auto & seq = rd.du_doubles();  for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << seq[i];                        } break; }
      case rdb::pk_boolean: { std::cout << "[boolean]                  : {"; const auto & seq = rd.du_bools();    for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << bool2str(seq[i]);              } break; }
      case rdb::pk_string:  { std::cout << "[string-based]             : {"; const auto & seq = rd.du_strings();  for(size_t i = 0; i < seq.length(); ++i) { print_sep(i); std::cout << '\"' << seq[i] << '\"';        } break; }
      default:              { std::cout << "[unknown]                  : { ERROR, bad type "; break; }
    }

  std::cout << "}\n";
}


    /// Print OKS object class-name and identity.
    /**
     *  The function prints object as "object-class-name@object-id".
     *  If the parameter is 0, it prints "(null)".
     *  The parameter 'print_prefix' to be used to disable printing the type "object".
     */

static void
print_object_name(const rdb::RDBObject *obj)
{
  if (obj)
    std::cout << "\"" << obj->classid << '@' << obj->name << '\"';
  else
    std::cout << "(null)";
}


    /// Print a relationship value..
    /**
     *  The function prints relationship value using the same style as for attributes.
     *  The output is: "name_______[object]___: {obj1, obj2, ..., obj[N]}" for multi-value
     *  relationships and "name_______[object]___: obj" for single-value.
     *  
     *  The 'width' parameter is is used for alignment when an object contains several
     *  attributes and relationships with different lengths of their names.
     *
     *  The 'prefix_str' parameter is used as leading prefix depending if the objects
     *  is printed alone or as a part of class description.
     */

static void
print_rel_value(const rdb::RDBObjectList &l, const char *name, bool is_multi_value, size_t width, const char *prefix_str = "        ")
{
  print_name(prefix_str, width, name);

  if (is_multi_value)
    {
      std::cout << "[object]                   : {";

      for (unsigned long idx = 0; idx < l.length(); idx++)
        {
          print_sep(idx);
          print_object_name(&l[idx]);
        }

      std::cout << "}\n";
    }
  else
    {
      std::cout << "[object]                   : ";

      print_object_name((l.length() > 0) ? &l[0] : nullptr);

      std::cout << "\n";
    }
}


static const char *
format2str(rdb::IntegerFormat f)
{
  switch(f)
    {
      case rdb::int_format_oct: return "octal";
      case rdb::int_format_dec: return "decimal";
      case rdb::int_format_hex: return "hexadecimal";
      default: return "not applicable";
    }
}


    /// Print list of class attributes.
    /**
     *  The function prints list of class attributes and their properties.
     */

static void
print_attributes(const rdb::RDBAttributeList& l)
{
  std::cout << "\n    ATTRIBUTES are:\n";

  for (unsigned long idx = 0; idx < l.length(); ++idx)
    {
      std::cout << "      name           : \"" << l[idx].name << "\"\n"
                   "      type           : \"" << l[idx].type << "\"\n"
                   "      range          : \"" << l[idx].range << "\"\n"
                   "      init value     : \"" << l[idx].initValue << "\"\n";

      if (l[idx].intFormat != rdb::int_format_na)
        std::cout << "      integer format : \"" << format2str(l[idx].intFormat) << "\"\n";

      std::cout << "      is multi value : \"" << (l[idx].isMultiValue ? "Yes" : "No" ) << "\"\n"
                   "      is not null    : \"" << (l[idx].isNotNull ? "Yes" : "No" ) << "\"\n"
                   "      is direct      : \"" << (l[idx].isDirect ? "Yes" : "No" ) << "\"\n"
                   "      description    : \"" << l[idx].description << "\"\n"
                   "      ----------------------------------------------------------------\n";
    }

  std::cout << std::endl;
}


    /// Print list of class relationships.
    /**
     *  The function prints list of class relationships and their properties.
     */

void print_relationships(const rdb::RDBRelationshipList& l)
{
  std::cout << "\n    RELATIONSHIPS are:\n";

  for (unsigned long idx = 0; idx < l.length(); ++idx)
    std::cout << "      name           : \"" << l[idx].name << "\"\n"
                 "      class          : \"" << l[idx].classid << "\"\n"
                 "      is direct      : \"" << (l[idx].isDirect ? "Yes" : "No" ) << "\"\n"  
                 "      is multi value : \"" << (l[idx].isMultiValue ? "Yes" : "No" ) << "\"\n"  
                 "      is not null    : \"" << (l[idx].isNotNull ? "Yes" : "No" ) << "\"\n"  
                 "      is aggregation : \"" << (l[idx].isAggregation ? "Yes" : "No" ) << "\"\n"  
                 "      description    : \"" << l[idx].description << "\"\n"
                 "      ----------------------------------------------------------------\n";

  std::cout << std::endl;
}

static std::string
obj2str(const rdb::RDBObject& o)
{
  return std::string(static_cast<const char *>(o.name)) + '@' + static_cast<const char *>(o.classid);
}


    /// Print an object.
    /**
     *  The function prints values of object attributes and relationships.
     *  It uses single call to the server.
     *
     *  The 'max_width' parameter is used for alignment when the object has several
     *  attributes and relationships with different lengths of their names. If the
     *  parameter is 0, it is calculated inside the function, otherwise it should
     *  be calculated otside the function (e.g. for whole class) to improve
     *  performance.
     */

static void
print_object(const rdb::RDBObject &o, size_t max_width, const char *prefix_str = "        ")
{
  rdb::RDBAttributeValueList_var s_avlist;
  rdb::RDBAttributeValuesList_var m_avlist;
  rdb::RDBRelationshipValueList_var rvlist;

  try
    {
      if (is_writer)
        writer->get_object_values(session, o.classid, o.name, s_avlist, m_avlist, rvlist);
      else
        cursor->get_object_values(o.classid, o.name, s_avlist, m_avlist, rvlist);
    }
  catch (CORBA::SystemException& ex)
    {
      ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_object_values", obj2str(o), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
      return;
    }
  catch (const rdb::NotFound& ex)
    {
      ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_object_values", obj2str(o), *daq::idl2ers_issue(ex.issue)));
      return;
    }
  catch (const rdb::CannotProceed& ex)
    {
      ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_object_values", obj2str(o), *daq::idl2ers_issue(ex.issue)));
      return;
    }
  catch (rdb::SessionNotFound& ex)
    {
      ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_object_values", "", session2str(ex.session_id)));
      return;
    }

  if (max_width == 0)
    {
      for (unsigned long idx = 0; idx < s_avlist->length(); ++idx)
        {
          size_t len = strlen(s_avlist[idx].name);
          if (len > max_width)
            max_width = len;
        }

      for (unsigned long idx = 0; idx < m_avlist->length(); ++idx)
        {
          size_t len = strlen(m_avlist[idx].name);
          if (len > max_width)
            max_width = len;
        }

      for (unsigned long idx = 0; idx < rvlist->length(); ++idx)
        {
          size_t len = strlen(rvlist[idx].name);
          if (len > max_width)
            max_width = len;
        }
    }

  for (unsigned long idx = 0; idx < s_avlist->length(); ++idx)
    {
      print_name(prefix_str, max_width, s_avlist[idx].name);
      print_value(s_avlist[idx].data);
    }

  for (unsigned long idx = 0; idx < m_avlist->length(); ++idx)
    {
      print_name(prefix_str, max_width, m_avlist[idx].name);
      print_data(m_avlist[idx].data);
    }

  for (unsigned long idx = 0; idx < rvlist->length(); ++idx)
    {
      print_rel_value(rvlist[idx].data, rvlist[idx].name, rvlist[idx].isMultiValue, max_width, prefix_str);
    }
}


    /// Print list of objects.
    /**
     *  The function prints list of objects. The are two different styles:
     *   - OLD STYLE:
     *       value of each attribute and relationships is read in a separate
     *       call to server
     *   - NEW STYLE:
     *       all such values are read in a single call
     *
     *  The 'max_width' parameter is used for alignment when the object has several
     *  attributes and relationships with different lengths of their names.
     */

static void
print_objects(const rdb::RDBObjectList &l, size_t max_width)
{
  std::cout << "    OBJECTS are:\n";

  if (l.length() <= 0)
    return;

  rdb::RDBAttributeList_var alist;
  rdb::RDBRelationshipList_var rlist;

  if (show_data_old_style)
    {
      try
        {
          if (is_writer)
            {
              writer->get_attributes(session, l[0].classid, alist);
              writer->get_relationships(session, l[0].classid, rlist);
            }
          else
            {
              cursor->get_attributes(l[0].classid, alist);
              cursor->get_relationships(l[0].classid, rlist);
            }
        }
      catch (CORBA::SystemException& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attributes/get_relationships", static_cast<const char*>(l[0].classid), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
          return;
        }
      catch (const rdb::NotFound& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attributes/get_relationships", static_cast<const char*>(l[0].classid), "", *daq::idl2ers_issue(ex.issue)));
          return;
        }
      catch (rdb::SessionNotFound& ex)
        {
          throw(rdb_dump::RequestFailed(ERS_HERE, "get_attributes/get_relationships", "", session2str(ex.session_id)));
        }
    }

  for (unsigned long idx = 0; idx < l.length(); ++idx)
    {
      std::cout << "      " << l[idx].name;

      if (!show_data && !show_data_old_style)
        {
          std::cout << std::endl;
          continue;
        }

      std::cout << " {\n";

      if (show_data_old_style)
        {
          try
            {
              for (unsigned long j = 0; j < alist->length(); ++j)
                {
                  if (alist[j].isMultiValue == false)
                    {
                      rdb::DataUnion_var data;

                      if (is_writer)
                        writer->get_attribute_value(session, l[idx], alist[j].name, data);
                      else
                        cursor->get_attribute_value(l[idx], alist[j].name, data);

                      print_name("        ", max_width, alist[j].name);
                      print_value(static_cast<const rdb::DataUnion&>(data));
                    }
                }

              for (unsigned long j = 0; j < alist->length(); ++j)
                {
                  if (alist[j].isMultiValue == true)
                    {
                      rdb::DataListUnion_var dlist;

                      if (is_writer)
                        writer->get_attribute_values(session, l[idx], alist[j].name, dlist);
                      else
                        cursor->get_attribute_values(l[idx], alist[j].name, dlist);

                      print_name("        ", max_width, alist[j].name);
                      print_data(static_cast<const rdb::DataListUnion&>(dlist));
                    }
                }

              for (unsigned long j = 0; j < rlist->length(); ++j)
                {
                  rdb::RDBObjectList_var olist;

                  if (is_writer)
                    writer->get_objects_of_relationship(session, l[idx], rlist[j].name, olist);
                  else
                    cursor->get_objects_of_relationship(l[idx], rlist[j].name, olist);

                  print_rel_value(static_cast<const rdb::RDBObjectList&>(olist), rlist[j].name, rlist[j].isMultiValue, max_width);
                }
            }

          catch (CORBA::SystemException& ex)
            {
              ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attribute_value(s)/get_objects_of_relationship", obj2str(l[idx]), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
              return;
            }
          catch (const rdb::NotFound& ex)
            {
              ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attribute_value(s)/get_objects_of_relationship", obj2str(l[idx]), "", *daq::idl2ers_issue(ex.issue)));
              return;
            }
          catch (const rdb::CannotProceed& ex)
            {
              ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attribute_value(s)/get_objects_of_relationship", obj2str(l[idx]), "", *daq::idl2ers_issue(ex.issue)));
              return;
            }
          catch (rdb::SessionNotFound& ex)
            {
              ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attribute_value(s)/get_objects_of_relationship", "", session2str(ex.session_id)));
              return;
            }
        }
      else
        {
          print_object(l[idx], max_width);
        }

      std::cout << "      }\n";
    }

  std::cout << std::endl;
}

    /// Print class.
    /**
     *  The function prints class description with different level of details
     *  depending on user options defined via command line.
     */

static void
print_class(const rdb::RDBClass& c)
{
    // need to know max width of column with names of attributes and relationships
    // which is used for right alignment

  size_t value_name_column_width = 0;


  std::cout << "  CLASS                        : \"" << c.name << "\"\n"
               "    is abstract                : "   << (c.isAbstract ? "\"Yes\"" : "\"No\"" ) << "\n"
               "    object number              : "   << c.objectNumber << "\n"
               "    relationship number        : "   << c.relationshipNumber << "\n"
	       "    direct relationship number : "   << c.directRelationshipNumber << "\n"
	       "    attribute number           : "   << c.attributeNumber << "\n"
	       "    direct attribute number    : "   << c.directAttributeNumber << "\n"
	       "    description                : \"" << c.description << "\"\n";

  if (show_attributes || show_data || show_data_old_style)
    {
      rdb::RDBAttributeList_var alist;

      try
        {
          if (is_writer)
            writer->get_attributes(session, c.name, alist);
          else
            cursor->get_attributes(c.name, alist);
        }
      catch (CORBA::SystemException& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attributes", static_cast<const char*>(c.name), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
          return;
        }
      catch (const rdb::NotFound& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_attributes", static_cast<const char*>(c.name), "", *daq::idl2ers_issue(ex.issue)));
          return;
        }
      catch (rdb::SessionNotFound& ex)
        {
          throw(rdb_dump::RequestFailed(ERS_HERE, "get_attributes", "", session2str(ex.session_id)));
        }

      if (show_attributes)
        print_attributes(alist);

      if (show_data || show_data_old_style)
        {
          for (unsigned long idx = 0; idx < alist->length(); ++idx)
            {
              size_t len = strlen(alist[idx].name);
              if (len > value_name_column_width)
                value_name_column_width = len;
            }
        }
    }

  if (show_relationships || show_data || show_data_old_style)
    {
      rdb::RDBRelationshipList_var rlist;

      try
        {
          if (is_writer)
            writer->get_relationships(session, c.name, rlist);
          else
            cursor->get_relationships(c.name, rlist);
        }
      catch (CORBA::SystemException& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_relationships", static_cast<const char*>(c.name), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
          return;
        }
      catch (const rdb::NotFound& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_relationships", static_cast<const char*>(c.name), "", *daq::idl2ers_issue(ex.issue)));
          return;
        }
      catch (rdb::SessionNotFound& ex)
        {
          throw(rdb_dump::RequestFailed(ERS_HERE, "get_relationships", "", session2str(ex.session_id)));
        }

      if (show_relationships)
        print_relationships(rlist);

      if (show_data || show_data_old_style)
        {
          for (unsigned int idx = 0; idx < rlist->length(); ++idx)
            {
              size_t len = strlen(rlist[idx].name);
              if (len > value_name_column_width)
                value_name_column_width = len;
            }
        }
    }

  if (show_objects || show_data || show_data_old_style)
    {
      rdb::RDBObjectList_var olist;

      try
        {
          if (is_writer)
            writer->get_all_objects(session, c.name, false, olist);
          else
            cursor->get_all_objects(c.name, false, olist);
        }
      catch (CORBA::SystemException& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_all_objects", static_cast<const char*>(c.name), daq::ipc::CorbaSystemException(ERS_HERE, &ex)));
          return;
        }
      catch (const rdb::NotFound& ex)
        {
          ers::error(rdb_dump::RequestFailed(ERS_HERE, "get_all_objects", static_cast<const char*>(c.name), *daq::idl2ers_issue(ex.issue)));
          return;
        }
      catch (rdb::SessionNotFound& ex)
        {
          throw(rdb_dump::RequestFailed(ERS_HERE, "get_all_objects", "", session2str(ex.session_id)));
        }

      print_objects(olist, value_name_column_width);
    }
}

static void
print_classes(const rdb::RDBClassList &l)
{
  std::cout << "Classes are:\n";

  for (unsigned int idx = 0; idx < l.length(); idx++)
    print_class(l[idx]);

  std::cout << std::endl;
}


int
main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }

  std::string partition_name, database_name, class_name, object_name, path_query;
  int number=1;

  try
    {
      boost::program_options::variables_map vm;
      boost::program_options::options_description desc("Display the contents of read-only or read-write RDB server at different level of details.");

      desc.add_options()
        ( "partition,p", boost::program_options::value<std::string>(&partition_name)->default_value(ipc::partition::default_name), "partition to work in" )
        ( "database,d", boost::program_options::value<std::string>(&database_name)->required(), "name of database server to work with" )
        ( "class,c", boost::program_options::value<std::string>(&class_name), "print information about given class." )
        ( "object,o", boost::program_options::value<std::string>(&object_name), "print information about given object (the class must be defined)." )
        ( "number,n", boost::program_options::value<int>(&number)->default_value(number), "repeat dump number of times" )
        ( "path-query,y", boost::program_options::value<std::string>(&path_query), "defines pattern for path query" )
        ( "attributes,A", "show classes attributes." )
        ( "relationships,R", "show classes relationships." )
        ( "objects,O", "show objects of the class." )
        ( "data,D", "show attributes and relationships values." )
        ( "data-old-access,T", "show data using old access (read each attribute and relationship value in separate call)." )
        ( "help,h", "Print help message");

      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.find("help") != vm.end())
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      show_attributes = (vm.find("attributes") != vm.end());
      show_relationships = (vm.find("relationships") != vm.end());
      show_objects = (vm.find("objects") != vm.end());
      show_data = (vm.find("data") != vm.end());
      show_data_old_style = (vm.find("data-old-access") != vm.end());

      boost::program_options::notify(vm);
    }
  catch (const std::exception& ex)
    {
      ers::fatal(rdb_dump::BadCommandLine( ERS_HERE, "command line parse failed", ex));
      return EXIT_FAILURE;
    }

  try
    {
      RDBWriterPingCB * ping_cb(nullptr);
      rdb::ProcessInfo proc_info;

      IPCPartition p(partition_name);

      // detect rdb read-only or rdb read-write server:
      // if rdb::cursor is not valid, assume it is rdb read-write one
      is_writer = !(p.isObjectValid<rdb::cursor>(database_name) );

      try
        {
          if (is_writer)
            {
              writer = p.lookup<rdb::writer>(database_name);

              rdb::ProcessInfoHelper::set(proc_info);
              ping_cb = new RDBWriterPingCB();
              session = writer->open_session(proc_info, ping_cb->_this());
              ping_cb->set_session_id(session);
            }
          else
            {
              cursor = p.lookup<rdb::cursor>(database_name);
            }
        }
      catch (CORBA::SystemException& ex)
        {
          throw rdb_dump::RequestFailed(ERS_HERE, "open_session", "", daq::ipc::CorbaSystemException(ERS_HERE, &ex));
        }
      catch (ers::Issue& ex)
        {
          throw daq::rdb::LookupFailed(ERS_HERE, database_name, partition_name, ex);
        }

      std::cout.setf(std::ios::left, std::ios::adjustfield);

      for (int i = 0; i < number; ++i)
        {
          if (!path_query.empty())
            {
              if (class_name.empty() || object_name.empty())
                throw rdb_dump::BadCommandLine(ERS_HERE, "use class-name and object-id to define the from object used in the path query");

              rdb::RDBObject o;
              o.classid = class_name.c_str();
              o.name = object_name.c_str();
              rdb::RDBObjectList_var olist;

              try
                {
                  if (is_writer)
                    writer->get_objects_by_path(session, o, path_query.c_str(), olist);
                  else
                    cursor->get_objects_by_path(o, path_query.c_str(), olist);

                  std::cout << "Found " << olist->length() << " objects in the path:\n";
                  for (unsigned int idx = 0; idx < olist->length(); ++idx)
                    {
                      std::cout << '[' << (idx + 1) << "] Object \"" << olist[idx].name << '@' << olist[idx].classid << "\":\n";
                      print_object(olist[idx], 0, "  ");
                    }
                }
              catch (CORBA::SystemException& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_objects_by_path", path_query, daq::ipc::CorbaSystemException(ERS_HERE, &ex));
                }
              catch (const rdb::NotFound& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_objects_by_path", path_query, *daq::idl2ers_issue(ex.issue));
                }
              catch (const rdb::CannotProceed& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_objects_by_path", path_query, *daq::idl2ers_issue(ex.issue));
                }
              catch (rdb::SessionNotFound& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_objects_by_path", "", session2str(ex.session_id));
                }
            }
          else if (!class_name.empty())
            {
              const char* cname = class_name.c_str();

              if (!object_name.empty())
                {
                  const char* oname = object_name.c_str();
                  std::cout << "Print description of object \"" << object_name << '@' << class_name << "\":\n";

                  rdb::RDBObject o;
                  o.classid = cname;
                  o.name = oname;
                  print_object(o, 0, "  ");
                }
              else
                {
                  std::cout << "Print description of class \"" << class_name << "\":\n";
                  rdb::RDBClass_var c;

                  try
                    {
                      if (is_writer)
                        writer->get_class(session, cname, c);
                      else
                        cursor->get_class(cname, c);
                    }
                  catch (CORBA::SystemException& ex)
                    {
                      throw rdb_dump::RequestFailed(ERS_HERE, "get_class", cname, daq::ipc::CorbaSystemException(ERS_HERE, &ex));
                    }
                  catch (rdb::SessionNotFound& ex)
                    {
                      throw rdb_dump::RequestFailed(ERS_HERE, "get_class", "", session2str(ex.session_id));
                    }
                  catch (const rdb::NotFound& ex)
                    {
                      throw rdb_dump::RequestFailed(ERS_HERE, "get_class", cname, *daq::idl2ers_issue(ex.issue));
                    }

                  print_class(c);
                }
            }
          else
            {
              rdb::RDBClassList_var l;

              try
                {
                  if (is_writer)
                    {
                      writer->get_all_classes(session, l);
                    }
                  else
                    {
                      cursor->get_all_classes(l);
                    }
                }
              catch (rdb::SessionNotFound& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_all_classes", "", session2str(ex.session_id));
                }
              catch (CORBA::SystemException& ex)
                {
                  throw rdb_dump::RequestFailed(ERS_HERE, "get_all_classes", "", daq::ipc::CorbaSystemException(ERS_HERE, &ex));
                }

              print_classes(l);
            }
        }

      // if is writer, close session

      if (is_writer)
        {
          try
            {
              writer->close_session(session, proc_info);
            }
          catch (rdb::SessionNotFound& ex)
            {
              throw rdb_dump::RequestFailed(ERS_HERE, "close_session", "", session2str(ex.session_id));
            }
          catch (CORBA::SystemException& ex)
            {
              throw rdb_dump::RequestFailed(ERS_HERE, "close_session", "", daq::ipc::CorbaSystemException(ERS_HERE, &ex));
            }
        }
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
