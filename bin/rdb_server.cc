#include <algorithm>
#include <string>
#include <fstream>

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <cmdl/cmdargs.h>
#include <pmg/pmg_initSync.h>

#include <ipc/core.h>
#include <ipc/signal.h>
#include <ers2idl/ers2idl.h>

#include <oks/kernel.h>

#include "rdb_impl.h"
#include "rdb/errors.h"
#include "server_utils.h"

ERS_DECLARE_ISSUE(
  rdb_server,
  CommandLineProblem,
  "Command line problem: \"" << what << '\"',
  ((const char*)what)
)

ERS_DECLARE_ISSUE(
  rdb_server,
  ReadFileInfoProblem,
  "lstat(\"" << path << "\") failed with code " << code << ": " << error,
  ((const char*)path)
  ((int)code)
  ((const char*)error)
)

ERS_DECLARE_ISSUE(
  rdb_server,
  PoolServerOperationFailed,
  "cannot " << op << " pool server \"" << name << '\"',
  ((const char*)op)
  ((const char*)name)
)


  /**
   *  The functions cvt_symbol() and alnum_name() are used
   *  to replace all symbols allowed in names of classes, attributes
   *  and relationships, but misinterpreted by c++ or java.
   */

char
cvt_symbol(char c)
{
  return ((isalnum(c) || c == '.') ? c : '_');
}

std::string
alnum_name(const std::string& in)
{
  std::string s(in);
  std::transform(s.begin(), s.end(), s.begin(), cvt_symbol);
  return s;
}

  // replace colons by underscore

static char
cvt_colon(char c)
{
  return (c != ':' ? c : '_');
}


  /** Return true, if the path is a directory */

static bool is_directory(const char * path)
{
  struct stat buf;
  buf.st_mode = 0;

  if(int error = stat(path, &buf)) {
    if(errno == ENOENT) return false;  // the path does not exist
    throw rdb_server::ReadFileInfoProblem(ERS_HERE, path, error, strerror(errno));
  }

  return S_ISDIR(buf.st_mode);
}


int main(int argc, char ** argv)
{
  // initialise IPC and set default CORBA parameters

  try
    {
      long thread_pool_size = sysconf(_SC_NPROCESSORS_ONLN);

      if (thread_pool_size < 16)
        thread_pool_size = 16;

      ERS_DEBUG(0, "default maxServerThreadPoolSize = " << thread_pool_size);

      auto ipc_opts = IPCCore::extractOptions(argc, argv);

      ipc_opts.emplace_front("maxServerThreadPoolSize", std::to_string(thread_pool_size));
      ipc_opts.emplace_front("threadPerConnectionPolicy", "0");
      ipc_opts.emplace_front("threadPoolWatchConnection", "0");

      IPCCore::init(ipc_opts);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }


    // parse command line

  CmdArgBool    no_cache ('c', "no-cache", "turn off \'use-cache\' mode");
  CmdArgStr     partition_name('p', "partition", "partition-name", "partition to work in");
  CmdArgStr     database_name('d', "database", "database-name", "name of database server to start");
  CmdArgStr     db_var_name('a', "db-variable-name", "variable-name", "read database server name from process environment with given name instead of -d option");
  CmdArgBool    sync_file('s', "create-pmg-sync-file", "create pmg synchronization file at startup");
  CmdArgStrList	scheme_files('S', "schema", "schema-files", "files to load database schema from", CmdArg::isLIST);
  CmdArgStrList	data_files('D', "data", "data-files", "files to load database data from", CmdArg::isLIST);
  CmdArgStr	master_server_name_param('m', "master-rdb-server", "server-name", "name of database server to read data from");
  CmdArgStr	master_server_name_param_env('e', "master-variable-name", "variable-name", "read name of master server name from process environment with given name instead of -m option");
  CmdArgStr	master_server_from_pool_name('o', "read-master-name-from-pool", "server-name", "read name of master server from pool server");
  CmdArgStr     pool_server_name_param ('g', "register-in-pool", "pool-server-name", "register this server in pool");
  CmdArgStr     restore_from('r', "restore", "restore-from-file", "backup file to restore state from (can be directory if -a option is used)");
  CmdArgStr     backup_to('b', "backup", "backup-to-file", "file to backup state to (can be directory if -a option is used)");
  CmdArgInt     backup_interval('k', "backup-interval", "time", "backup interval in seconds (default 10\")");       
  CmdArgBool    push_mp ('M', "push-mirrored-partition", "turn on \'push\' mode for mirrored partition");
  CmdArgInt     retry_mp_update_interval('u',"retry-push-mirror-partition-interval","time","retry interval to push mirror partition data in seconds (min 30\", default 60\")");       

  CmdLine       cmd(*argv, &partition_name, &database_name, &db_var_name, &no_cache, &scheme_files, &data_files, &master_server_name_param, &master_server_name_param_env, &master_server_from_pool_name, &pool_server_name_param, &push_mp, &retry_mp_update_interval, &backup_to, &restore_from, &backup_interval, &sync_file, NULL);
  CmdArgvIter	arg_iter(--argc, ++argv);

  cmd.description("Server to provide CORBA interface to OKS database. For verbose output set TDAQ_ERS_DEBUG_LEVEL variable.");

  backup_interval = 10;
  retry_mp_update_interval = 60;
  sync_file = 0;
  push_mp = 0;

  database_name="";
  db_var_name="";
  master_server_name_param="";
  master_server_name_param_env="";
  master_server_from_pool_name="";
  pool_server_name_param="";
  restore_from="";
  backup_to="";

  cmd.parse(arg_iter);

  if((int)retry_mp_update_interval < 30) {
    ers::warning(rdb_server::CommandLineProblem(ERS_HERE, "retry interval to update mirror partition is too small; set it to 30 seconds"));
    retry_mp_update_interval = 30;
  }

  if(!(database_name.flags() & CmdArg::GIVEN)) {
    if(!(db_var_name.flags() & CmdArg::GIVEN)) {
      ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, "Server name is not provided (use -d or -a option)"));
      return EXIT_FAILURE;
    }
    else {
      const char * v = getenv(static_cast<const char *>(db_var_name));
      if(v && *v) {
        database_name = v;
      }
      else {
        std::string text("Server name is not provided since the environment variable ");
        text += static_cast<const char *>(db_var_name);
        text += " specified by command line is not set or empty.";
        ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, text.c_str()));
        return EXIT_FAILURE;
      }
    }
  }

  std::string master_server_name;

  if( (master_server_name_param.flags() & CmdArg::GIVEN) || (master_server_name_param_env.flags() & CmdArg::GIVEN) ) {
    if((scheme_files.flags() & CmdArg::GIVEN) || (data_files.flags() & CmdArg::GIVEN)) {
      ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, "When server gets data from master rdb server, it cannot read any extra OKS files"));
      return EXIT_FAILURE;
    }
    
    if((master_server_from_pool_name.flags() & CmdArg::GIVEN)) {
      ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, "Either provide master rdb server, or read it from pool; cannot provide both at the same time"));
      return EXIT_FAILURE;
    }

    if(master_server_name_param.flags() & CmdArg::GIVEN) {
      master_server_name = static_cast<const char *>(master_server_name_param);
    }
    else {
      const char * v = getenv(static_cast<const char *>(master_server_name_param_env));
      if(v && *v) {
        master_server_name = v;
      }
      else {
        std::string text("Master server name is not provided since the environment variable ");
        text += static_cast<const char *>(master_server_name_param_env);
        text += " specified by command line is not set or empty.";
        ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, text.c_str()));
        return EXIT_FAILURE;
      }
      
    }
  }

  IPCPartition p(partition_name);


    // if defined, read name of master server from pool server

  if( (master_server_from_pool_name.flags() & CmdArg::GIVEN) ) {
    if((scheme_files.flags() & CmdArg::GIVEN) || (data_files.flags() & CmdArg::GIVEN)) {
      ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, "When server gets data from master rdb server, it cannot read any extra OKS files"));
      return EXIT_FAILURE;
    }

    try {
      CORBA::String_var parent;
      rdb::cursor_var c = p.lookup<rdb::cursor>(static_cast<const char *>(master_server_from_pool_name));
      c->request_parent(static_cast<const char *>(database_name), OksKernel::get_host_name().c_str(), parent);
      master_server_name = static_cast<const char *>(parent);
      ERS_LOG("assigned pool master server \'" << master_server_name << "\'" );
    }
    catch( daq::ipc::Exception & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "read data from", static_cast<const char*>(master_server_from_pool_name),
        daq::rdb::LookupFailed(ERS_HERE, static_cast<const char*>(master_server_from_pool_name), static_cast<const char*>(partition_name), ex)));
    }
    catch ( const rdb::CannotProceed & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "read data from", static_cast<const char*>(master_server_from_pool_name), *daq::idl2ers_issue(ex.issue)));
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "read data from", static_cast<const char*>(master_server_from_pool_name), daq::ipc::CorbaSystemException( ERS_HERE, &ex)));
    }
    catch ( ers::Issue & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "read data from", static_cast<const char*>(master_server_from_pool_name), ex));
    }

    if(master_server_name.empty()) return EXIT_FAILURE;
  }


    // create names for backup/restore files (can be just a directory name for template applications!)

  std::string backup_to_file;
  std::string restore_from_file;

  try {
    if(backup_to.flags() & CmdArg::GIVEN) {
      if(is_directory(static_cast<const char *>(backup_to))) {
        if(db_var_name.flags() & CmdArg::GIVEN) {
	  backup_to_file = static_cast<const char *>(backup_to);
	  backup_to_file += '/';
	  backup_to_file += alnum_name(static_cast<const char *>(database_name));
	}
	else {
          std::string text("The path passed with -b option \'");
	  text += static_cast<const char *>(backup_to);
	  text += "\' is a directory. It cannot be used without -a option.";
          ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, text.c_str()));
          return EXIT_FAILURE;
	}
      }
      else {
        backup_to_file = static_cast<const char *>(backup_to);
      }

        // replace any colons in backup file name

      std::transform(backup_to_file.begin(), backup_to_file.end(), backup_to_file.begin(), cvt_colon);

      ERS_DEBUG( 1 , "backup-to = \'" << backup_to_file << "\'" );


        // create backup directory, if needed

      if(!rdb::create_dir_for_backup_file(backup_to_file)) return EXIT_FAILURE;
    }

    if(restore_from.flags() & CmdArg::GIVEN) {
      if(is_directory(static_cast<const char *>(restore_from))) {
        if(db_var_name.flags() & CmdArg::GIVEN) {
	  restore_from_file = static_cast<const char *>(restore_from);
	  restore_from_file += '/';
	  restore_from_file += alnum_name(static_cast<const char *>(database_name));
	}
	else {
          std::string text("The path passed with -r option \'");
	  text += static_cast<const char *>(restore_from);
	  text += "\' is a directory. It cannot be used without -a option.";
          ers::fatal(rdb_server::CommandLineProblem(ERS_HERE, text.c_str()));
          return EXIT_FAILURE;
	}
      }
      else {
        restore_from_file = static_cast<const char *>(restore_from);
      }

        // replace any colons in backup file name

      std::transform(restore_from_file.begin(), restore_from_file.end(), restore_from_file.begin(), cvt_colon);

      ERS_DEBUG( 1 , "restore-from = \'" << restore_from_file << "\'" );
    }
  }
  catch(ers::Issue& ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }


  ERS_DEBUG( 1 , "run rdb-server with name \'" << static_cast<const char *>(database_name) << "\'" );

  try {
    if (
      p.isObjectValid<rdb::cursor>( static_cast<const char *>(database_name) ) ||
      p.isObjectValid<rdb::writer>( static_cast<const char *>(database_name) ) 
    ) {
      ers::fatal(
        daq::rdb::AlreadyRunning(
          ERS_HERE,
          static_cast<const char *>(database_name),
          static_cast<const char *>(partition_name)
        )
      );
      return EXIT_FAILURE;
    }
  }
  catch(daq::ipc::InvalidPartition & ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }

  RDBServer * s_rdb_server;

  try {
    s_rdb_server = new RDBServer(database_name, p, (no_cache ? false : true), master_server_name.empty());
  }
  catch (ers::Issue & ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }

  if(push_mp) s_rdb_server->set_push_mirror_partition((int)retry_mp_update_interval);

  ERS_DEBUG( 1 , "run rdb-server in " << (no_cache ? "do-not-" : "") << "use-cache mode\n");

  {
    bool read_backup = false;

    if(!restore_from_file.empty()) {
      try {
        s_rdb_server->load_backup(restore_from_file.c_str());
        read_backup = true;
      }
      catch ( daq::rdb::BadBackupFile& e ) {
        ers::error(
          daq::rdb::BadBackupData(
            ERS_HERE, restore_from_file.c_str(), "caught exception", e
          )
        );
      }
    }

    if(read_backup == false) {
      if(!master_server_name.empty()) {
	try {
	  s_rdb_server->read_complete_db(master_server_name.c_str(), false);
	}
	catch(ers::Issue& ex) {
          s_rdb_server->_destroy(true);
          ers::fatal(ex);
          return EXIT_FAILURE;
	}
      }
      else {
        unsigned int i;
        for( i = 0; i < scheme_files.count(); i++ ) {
          try {
            s_rdb_server->add_file(scheme_files[i]);
          }
          catch(daq::rdb::FailedLoadDatabaseFile& ex) {
            s_rdb_server->_destroy(true);
            ers::fatal(ex);
            return EXIT_FAILURE;
          }
        }

        for( i = 0; i < data_files.count(); i++ ) {
          try {
            s_rdb_server->add_file(data_files[i]);
          }
          catch(daq::rdb::FailedLoadDatabaseFile& ex) {
            s_rdb_server->_destroy(true);
            ers::fatal(ex);
            return EXIT_FAILURE;
          }
        }
      }
    }
  }

  if(!backup_to_file.empty()) {
    s_rdb_server->set_backup_file(backup_to_file);
  }
  else if(!restore_from_file.empty()) {
    s_rdb_server->set_backup_file(restore_from_file);
  }

  if(s_rdb_server->get_push_mirror_partition()) {
    s_rdb_server->start_push_mirror_data_thread();
  }

  s_rdb_server->start_backup_thread(backup_interval);

  if (sync_file)
    pmg_initSync();


  ERS_LOG( "Server \"" << s_rdb_server->name() << "\" has been started in the partition \"" << s_rdb_server->partition().name() << '\"' );

  s_rdb_server->request_backup();

  if( (pool_server_name_param.flags() & CmdArg::GIVEN) ) {
    const char * pool_server_name(static_cast<const char *>(pool_server_name_param));

    try {
      CORBA::String_var parent;
      rdb::cursor_var c = p.lookup<rdb::cursor>(pool_server_name);
      c->register_parent(static_cast<const char *>(database_name), OksKernel::get_host_name().c_str());
      ERS_LOG("register server on pool server \'" << pool_server_name << "\'" );
    }
    catch( daq::ipc::Exception & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "register on", pool_server_name, daq::rdb::LookupFailed(ERS_HERE, pool_server_name, static_cast<const char*>(partition_name), ex)));
      return EXIT_FAILURE;
    }
    catch ( const rdb::CannotProceed & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "register on", pool_server_name, *daq::idl2ers_issue(ex.issue)));
      return EXIT_FAILURE;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "register on", pool_server_name, daq::ipc::CorbaSystemException( ERS_HERE, &ex)));
      return EXIT_FAILURE;
    }
    catch ( ers::Issue & ex ) {
      ers::fatal(rdb_server::PoolServerOperationFailed(ERS_HERE, "register on", pool_server_name, ex));
      return EXIT_FAILURE;
    }

  }

  auto sig_received = daq::ipc::signal::wait_for();

  ERS_LOG( "Got signal " << sig_received );

  if( (pool_server_name_param.flags() & CmdArg::GIVEN) ) {
    const char * pool_server_name(static_cast<const char *>(pool_server_name_param));

    try {
      CORBA::String_var parent;
      rdb::cursor_var c = p.lookup<rdb::cursor>(pool_server_name);
      c->register_parent(static_cast<const char *>(database_name), "");
      ERS_LOG("unregister server on pool server \'" << pool_server_name << "\'" );
    }
    catch( const daq::ipc::Exception & ex ) {
      ers::warning( rdb_server::PoolServerOperationFailed(ERS_HERE, "unregister on", pool_server_name, daq::rdb::LookupFailed(ERS_HERE, pool_server_name, static_cast<const char*>(partition_name), ex)));
    }
    catch ( const rdb::CannotProceed & ex ) {
      ers::warning(rdb_server::PoolServerOperationFailed(ERS_HERE, "unregister on", pool_server_name, *daq::idl2ers_issue(ex.issue)));
    }
    catch ( CORBA::SystemException & ex ) {
      ers::warning(rdb_server::PoolServerOperationFailed(ERS_HERE, "unregister on", pool_server_name, daq::ipc::CorbaSystemException( ERS_HERE, &ex)));
    }
    catch ( const ers::Issue & ex ) {
      ers::warning(rdb_server::PoolServerOperationFailed(ERS_HERE, "unregister on", pool_server_name, ex));
    }

  }

  RDBServer::stop_backup_thread();

  s_rdb_server->_destroy(true);

  return EXIT_SUCCESS;
}
