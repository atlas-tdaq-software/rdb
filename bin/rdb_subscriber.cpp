#include <cstdlib>
#include <iostream>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ipc/signal.h>

#include "rdb/rdb_info.h"
#include "rdb/errors.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

  /** Failed to subscribe */

ERS_DECLARE_ISSUE(
  rdb_subscriber,
  SubscriptionFailed,
  "Failed to subscribe on changes",
)

ERS_DECLARE_ISSUE(
  rdb_subscriber,
  UnsubscriptionFailed,
  "Failed to unsubscribe",
)

ERS_DECLARE_ISSUE(
  rdb_subscriber,
  BadCommandLine,
  "bad command line",
)
////////////////////////////////////////////////////////////////////////////////////////////////////


static void
callback(RDBCallbackInfo *isc)
{
  ERS_LOG(isc->get_changes()->length() << " classe(s) were changed");

  for (unsigned int idx = 0; idx < isc->get_changes()->length(); idx++)
    {
      const rdb::RDBClassChanges& class_changes = (*isc->get_changes())[idx];
      std::cout << '(' << (idx + 1) << ") class " << class_changes.name << std::endl;

      std::cout << "   - there are " << class_changes.modified.length() << " modified objects\n";
      for (unsigned int i = 0; i < class_changes.modified.length(); i++)
        std::cout << "      * " << class_changes.modified[i] << std::endl;

      std::cout << "   - there are " << class_changes.created.length() << " created objects\n";
      for (unsigned int i = 0; i < class_changes.created.length(); i++)
        std::cout << "      * " << class_changes.created[i] << std::endl;

      std::cout << "   - there are " << class_changes.removed.length() << " removed objects\n";
      for (unsigned int i = 0; i < class_changes.removed.length(); i++)
        std::cout << "      * " << class_changes.removed[i] << std::endl;
    }

  std::cout << "#############################################" << std::endl;
}

int
main(int argc, char **argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (const daq::ipc::Exception& ex)
    {
      ers::fatal(ex);
      return 1;
    }

  std::string partition_name(ipc::partition::default_name);
  std::string database_name;
  std::vector<std::string> class_names;
  bool look_in_sclasses;
  std::vector<std::string> obj_names;

  try
    {
      boost::program_options::variables_map vm;
      boost::program_options::options_description desc("RDB subscriber to demonstrate callback functionality.");

      desc.add_options()
        ( "partition,p", boost::program_options::value<std::string>(&partition_name)->default_value(partition_name), "partition to work in" )
        ( "database,d", boost::program_options::value<std::string>(&database_name)->required(), "name of database server to work with" )
        ( "class-names,c", boost::program_options::value<std::vector<std::string> >(&class_names)->multitoken(), "list of classes for subscription" )
        ( "look-in-subclasses,s", "subscribe on changes in subclasses" )
        ( "objects,o", boost::program_options::value<std::vector<std::string> >(&obj_names)->multitoken(), "list of space-separated \"class-name\" \"object-id\" pairs for subscription" )
        ( "help,h", "Print help message");

      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.find("help") != vm.end())
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      boost::program_options::notify(vm);

      look_in_sclasses = (vm.find("look-in-subclasses") != vm.end());
    }
  catch (const std::exception& ex)
    {
      ers::fatal(rdb_subscriber::BadCommandLine( ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  IPCPartition p(partition_name);

  RDBInfoReceiver* info_receiver = new RDBInfoReceiver(p, database_name.c_str());

  std::set<std::string> classes(class_names.begin(), class_names.end());
  std::map<std::string, std::set<std::string> > objects;

  for (auto x = obj_names.begin(); x != obj_names.end(); x++)
    {
      const std::string class_name = *x;
      if (++x == obj_names.end())
        {
          ers::fatal(rdb_subscriber::BadCommandLine( ERS_HERE, std::string("no object id for class ") + class_name));
          return EXIT_FAILURE;
        }
      objects[class_name].insert(*x);
    }

  try
    {
      info_receiver->subscribe(classes, (look_in_sclasses ? true : false), objects, callback);
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(rdb_subscriber::SubscriptionFailed(ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  auto sig_received = daq::ipc::signal::wait_for();

  std::cout << "Got signal " << sig_received << ", exiting ..." << std::endl;

  try
    {
      info_receiver->unsubscribe();
    }
  catch (const ers::Issue& ex)
    {
      ers::error(rdb_subscriber::UnsubscriptionFailed(ERS_HERE, ex));
    }

  return EXIT_SUCCESS;
}
