#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <thread>

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <ipc/core.h>
#include <ipc/signal.h>
#include <cmdl/cmdargs.h>
#include <pmg/pmg_initSync.h>
#include <oks/kernel.h>

#include "rdb_writer_impl.h"
#include "rdb/errors.h"
#include "server_utils.h"

ERS_DECLARE_ISSUE(
  rdb_writer,
  CommandLineProblem,
  "Command line problem: \"" << what << '\"',
  ((const char*)what)
)

ERS_DECLARE_ISSUE(
  rdb_writer,
  ReadFileInfoProblem,
  "stat(\"" << path << "\") failed with code " << code << ": " << error,
  ((const char*)path)
  ((int)code)
  ((const char*)error)
)

RDBWriter * s_rdb_server;


  /**
   *  The functions cvt_symbol() and alnum_name() are used
   *  to replace all symbols allowed in names of classes, attributes
   *  and relationships, but misinterpreted by c++ or java.
   */

char
cvt_symbol(char c)
{
  return ((isalnum(c) || c == '.') ? c : '_');
}

std::string
alnum_name(const std::string& in)
{
  std::string s(in);
  std::transform(s.begin(), s.end(), s.begin(), cvt_symbol);
  return s;
}

  // replace colons by underscore

static char
cvt_colon(char c)
{
  return (c != ':' ? c : '_');
}


  /** Return true, if the path is a directory */

static bool is_directory(const char * path)
{
  struct stat buf;
  buf.st_mode = 0;

  if(int error = stat(path, &buf)) {
    if(errno == ENOENT) return false;  // the path does not exist
    throw rdb_writer::ReadFileInfoProblem(ERS_HERE, path, error, strerror(errno));
  }

  return S_ISDIR(buf.st_mode);
}


////////////////////

struct PingSessions {
  RDBWriter * m_server;
  static bool m_stop;
  unsigned long m_count;

  PingSessions(RDBWriter * p) :  m_server (p), m_count(0) { ; }

  static void stop() { m_stop = true; }

  void operator()() {
    while (!m_stop) {
      sleep(1);
      m_count++;
      if(m_count%120 == 0) {
        m_server->ping_sessions();
      }
    }
  }

};

bool PingSessions::m_stop(false);

///////////////////


int main(int argc, char ** argv)
{          
    // initialise IPC and set default CORBA parameters

  try
    {
      auto ipc_opts = IPCCore::extractOptions(argc, argv);

      ipc_opts.emplace_front("maxServerThreadPoolSize", "8");
      ipc_opts.emplace_front("threadPerConnectionPolicy", "0");
      ipc_opts.emplace_front("threadPoolWatchConnection", "0");

      IPCCore::init(ipc_opts);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }


    // parse command line

  CmdArgStr     partition_name('p', "partition", "partition-name", "partition to work in");
  CmdArgStr     database_name('d', "database", "database-name", "name of database server to start");
  CmdArgStr     db_var_name('a', "db-variable-name", "variable-name", "read database server name from process environment with given name instead of -d option");
  CmdArgBool    sync_file('s', "create-pmg-sync-file", "create pmg synchronization file at startup");
  CmdArgStrList	scheme_files('S', "schema", "schema-files", "files to load database schema from", CmdArg::isLIST);
  CmdArgStrList	data_files('D', "data", "data-files", "files to load database data from", CmdArg::isLIST);
  CmdArgStr     restore_from('r', "restore", "restore-from-file", "backup file to restore state from (can be directory if -a option is used)");
  CmdArgStr     backup_to('b', "backup", "backup-to-file", "file to backup state to (can be directory if -a option is used)");
  CmdArgInt     backup_interval('k', "backup-interval", "time", "backup interval in seconds (default 10\")");       

  CmdLine       cmd(*argv, &partition_name, &database_name, &db_var_name, &scheme_files, &data_files, &backup_to, &backup_interval, &restore_from, &sync_file, NULL);
  CmdArgvIter	arg_iter(--argc, ++argv);

  cmd.description("Server to provide read/write CORBA interface to OKS database. For verbose output set TDAQ_ERS_DEBUG_LEVEL variable.");

  sync_file = 0;

  backup_interval = 10;
  database_name="";
  db_var_name="";
  restore_from="";
  backup_to="";

  cmd.parse(arg_iter);

  if(!(database_name.flags() & CmdArg::GIVEN)) {
    if(!(db_var_name.flags() & CmdArg::GIVEN)) {
      ers::fatal(rdb_writer::CommandLineProblem(ERS_HERE, "Server name is not provided (use -d or -a option)"));
      return EXIT_FAILURE;
    }
    else {
      const char * v = getenv(static_cast<const char *>(db_var_name));
      if(v && *v) {
        database_name = v;
      }
      else {
        std::string text("Server name is not provided since the environment variable ");
        text += static_cast<const char *>(db_var_name);
        text += " mentioned by command line is not set or empty.";
        ers::fatal(rdb_writer::CommandLineProblem(ERS_HERE, text.c_str()));
        return EXIT_FAILURE;
      }
    }
  }


    // create names for backup/restore files (can be just a directory name for template applications!)

  std::string backup_to_file;
  std::string restore_from_file;

  try {
    if(backup_to.flags() & CmdArg::GIVEN) {
      if(is_directory(static_cast<const char *>(backup_to))) {
        if(db_var_name.flags() & CmdArg::GIVEN) {
	  backup_to_file = static_cast<const char *>(backup_to);
	  backup_to_file += '/';
	  backup_to_file += alnum_name(static_cast<const char *>(database_name));
	}
	else {
          std::string text("The path passed with -b option \'");
	  text += static_cast<const char *>(backup_to);
	  text += "\' is a directory. It cannot be used without -a option.";
          ers::fatal(rdb_writer::CommandLineProblem(ERS_HERE, text.c_str()));
          return EXIT_FAILURE;
	}
      }
      else {
        backup_to_file = static_cast<const char *>(backup_to);
      }

        // replace any colons in backup file name

      std::transform(backup_to_file.begin(), backup_to_file.end(), backup_to_file.begin(), cvt_colon);

      ERS_DEBUG( 1 , "backup-to = \'" << backup_to_file << "\'" );

        // create backup directory, if needed

      if(!rdb::create_dir_for_backup_file(backup_to_file)) return EXIT_FAILURE;
    }

    if(restore_from.flags() & CmdArg::GIVEN) {
      if(is_directory(static_cast<const char *>(restore_from))) {
        if(db_var_name.flags() & CmdArg::GIVEN) {
	  restore_from_file = static_cast<const char *>(restore_from);
	  restore_from_file += '/';
	  restore_from_file += alnum_name(static_cast<const char *>(database_name));
	}
	else {
          std::string text("The path passed with -r option \'");
	  text += static_cast<const char *>(restore_from);
	  text += "\' is a directory. It cannot be used without -a option.";
          ers::fatal(rdb_writer::CommandLineProblem(ERS_HERE, text.c_str()));
          return EXIT_FAILURE;
	}
      }
      else {
        restore_from_file = static_cast<const char *>(restore_from);
      }

        // replace any colons in backup file name

      std::transform(restore_from_file.begin(), restore_from_file.end(), restore_from_file.begin(), cvt_colon);

      ERS_DEBUG( 1 , "restore-from = \'" << restore_from_file << "\'" );
    }
  }
  catch(ers::Issue& ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }


  ERS_DEBUG( 1 , "run rdb-server with name \'" << static_cast<const char *>(database_name) << "\'" );

  IPCPartition p(partition_name);

  try {
    if (
      p.isObjectValid<rdb::cursor>( static_cast<const char *>(database_name) ) ||
      p.isObjectValid<rdb::writer>( static_cast<const char *>(database_name) ) 
    ) {
      ers::fatal(
        daq::rdb::AlreadyRunning(
          ERS_HERE,
          static_cast<const char *>(database_name),
          static_cast<const char *>(partition_name)
        )
      );
      return EXIT_FAILURE;
    }
  }
  catch(daq::ipc::InvalidPartition & ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }

  s_rdb_server = new RDBWriter(database_name, p);

  {
    bool read_backup = false;

    if(!restore_from_file.empty()) {
      try {
        s_rdb_server->load_backup(restore_from_file.c_str());
        read_backup = true;
      }
      catch ( daq::rdb::BadBackupFile& e ) {
        ers::error(
          daq::rdb::BadBackupData(
            ERS_HERE, restore_from_file.c_str(), "caught exception", e
          )
        );
      }
    }

    if(read_backup == false) {
      unsigned int i;
      for( i = 0; i < scheme_files.count(); i++ ) {
        try {
	  s_rdb_server->get_kernel().load_schema(static_cast<const char *>(scheme_files[i]));
        }
        catch (oks::exception & ex) {
          s_rdb_server->_destroy(true);
          ers::fatal(daq::rdb::FailedLoadDatabaseFile( ERS_HERE, ex.what()));
          return EXIT_FAILURE;
        }
      }

      for( i = 0; i < data_files.count(); i++ ) {
        try {
	  s_rdb_server->get_kernel().load_data(static_cast<const char *>(data_files[i]));
        }
        catch (oks::exception & ex) {
          s_rdb_server->_destroy(true);
          ers::fatal(daq::rdb::FailedLoadDatabaseFile( ERS_HERE, ex.what()));
          return EXIT_FAILURE;
        }
      }
    }
  }

  if(!backup_to_file.empty()) {
    s_rdb_server->set_backup_file(backup_to_file);
  }
  else if(!restore_from_file.empty()) {
    s_rdb_server->set_backup_file(restore_from_file);
  }

  s_rdb_server->start_backup_thread(backup_interval);

  if(sync_file) {
    pmg_initSync();
  }

  ERS_LOG( "RDB-writer server \"" << s_rdb_server->name() << "\" has been started in the partition \"" << s_rdb_server->partition().name() << '\"' );

  s_rdb_server->request_backup();

  PingSessions ping_thread_cmd(s_rdb_server);
  std::thread ping_thread(ping_thread_cmd);

  auto sig_received = daq::ipc::signal::wait_for();

  ERS_LOG( "Got signal " << sig_received );

  PingSessions::stop();
  ping_thread.join();
  
  RDBWriter::stop_backup_thread();

  s_rdb_server->_destroy(true);

  ERS_LOG( "Exiting..." );

  return EXIT_SUCCESS;
}
