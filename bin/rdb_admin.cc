#include <ctime>
#include <string.h>
#include <stdlib.h>
#include <iostream>

#include <boost/program_options.hpp>

#include <daq_tokens/acquire.h>
#include <daq_tokens/common.h>

#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/core.h>

#include <ers/ers.h>
#include <ers2idl/ers2idl.h>

#include "rdb/rdb.hh"
#include "rdb/errors.h"
#include "rdb/rdb_info.h"

ERS_DECLARE_ISSUE(
  rdb_admin,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_admin,
  BadServerType,
  "the " << (is_writer ? "read-write" : "read-only") << " server cannot be used for " << request << " request",
  ((bool)is_writer)
  ((const char*)request)
)

ERS_DECLARE_ISSUE(
  rdb_admin,
  RequestFailed,
  "remote method \"" << method << "\" failed",
  ((const char*)method)
)

ERS_DECLARE_ISSUE(
  rdb_admin,
  CorbaRequestFailed,
  "method " << method << "() raised CORBA exception " << reason,
  ((const char*)method)
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_admin,
  TokenAcquireFailed,
  "cannot acquire daq token",
  ERS_EMPTY
)

namespace po = boost::program_options;

static const char * session2str(rdb::RDBSessionId session)
{
  static std::string buf;

  if(buf.empty()) {
    std::ostringstream s;
    s << "session " << session << " is not found";
    buf = s.str();
  }

  return buf.c_str();
}


struct WriterSession {
  rdb::writer_var cursor;
  rdb::RDBSessionId session;
  RDBWriterPingCB * ping_cb;

  WriterSession() : session(0), ping_cb(0) {;}

  ~WriterSession();
};


WriterSession::~WriterSession()
{
  if(session) {
    try {
      cursor->close_session( session, rdb::ProcessInfoHelper::get() );
    }
    catch(CORBA::SystemException & ex) {
      ers::error (rdb_admin::CorbaRequestFailed(ERS_HERE, "close_session", ex._name()));
    }
    catch(rdb::SessionNotFound& ex) {
      ers::error (rdb_admin::RequestFailed(ERS_HERE, "close_session", session2str(ex.session_id)));
    }
  }
}

static void
print(rdb::RDBRepositoryVersionList& versions)
{
  const auto len = versions.length();
  for (unsigned int x = 0; x < len; x++)
    {
      char buf[50];
      std::strftime(buf, 50, "%F %T %Z", std::localtime(reinterpret_cast<std::time_t*>(&versions[x].timestamp)));
      std::cout << " * version [" << x + 1 << '/' << len << "]\n"
          "    id: " << versions[x].id << "\n"
          "    user: " << versions[x].user << "\n"
          "    date: " << buf << "\n"
          "    comment: " << versions[x].comment << "\n"
          "    files:\n";

      const auto num = versions[x].files.length();
      for (unsigned int i = 0; i < num; i++)
        std::cout << "     - \"" << versions[x].files[i] << "\"\n";
    }
}


int
main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return 1;
    }

  po::variables_map vm;

  std::string partition_name(ipc::partition::default_name);
  std::string database_name;
  std::vector<std::string> reload;
  std::vector<std::string> add_files;
  std::vector<std::string> close_files;
  std::vector<std::string> merge_command;
  std::vector<std::string> versions_param;
  rdb::RDBSessionId session_id = 0;
  std::string update_status;

  if (const char * env = getenv("TDAQ_PARTITION"))
    partition_name = env;

  try
    {
      po::options_description desc("The utility to administrate rdb_server.");

      desc.add_options()
        ( "partition,p", po::value<std::string>(&partition_name)->default_value(partition_name), "partition to work in" )
        ( "database,d", po::value<std::string>(&database_name)->required(), "name of database server to work with" )
        ( "shutdown,s", "shutdown database server" )
        ( "reload,r", po::value<std::vector<std::string> >(&reload)->multitoken()->zero_tokens(), "reload explicitly provided or all updated non-repository database files (if empty), or reload repository by hash" )
        ( "get-files,f", "print list of loaded schema and data files (-S can be used for read-write server)" )
        ( "get-modified-files,m", "print list of modified data files (-S can be used for read-write server)" )
        ( "get-changes,n", "print details of new repository versions or modified files" )
        ( "get-versions,v", po::value<std::vector<std::string> >(&versions_param)->multitoken(), "print details of versions providing 4 parameters all|skip date|id|tag since until" )
        ( "load,l", po::value<std::vector<std::string> >(&add_files)->multitoken(), "list of database files to be loaded by read-only server" )
        ( "close,c", po::value<std::vector<std::string> >(&close_files)->multitoken(), "list of loaded database files to be closed by read-only server" )
        ( "merge,e", po::value<std::vector<std::string> >(&merge_command)->multitoken(), "merge current schema and data files by read-only server; parameters are: schema-file-name data-file-name query-class query reference-level" )
        ( "list-sessions,L", "print list of sessions of read-write server" )
        ( "commit-modified-files,O", "commit files modified by read-write server (-S is required)" )
        ( "session,S", po::value<rdb::RDBSessionId>(&session_id), "provide id for read-write server session" )
        ( "update-db-status,T", po::value<std::string>(&update_status), "update timestamp of loaded file (-S is required for read-write server)" )
        ( "help,h", "Print help message");

      po::store(po::parse_command_line(argc, argv, desc), vm);

      if (vm.find("help") != vm.end())
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

    }
  catch(const std::exception& ex)
    {
      ers::fatal( rdb_admin::BadCommandLine( ERS_HERE, ex.what() ) );
      return 1;
    }

  if (!versions_param.empty())
    {
      if(versions_param.size() != 4)
        {
          ers::fatal( rdb_admin::BadCommandLine( ERS_HERE, "versions parameters are: \"all|skip\" \"date|id|tag\" \"since-value\" \"until-value\"" ) );
          return 1;
        }
    }

  if (vm.count("session") && session_id <= 0)
    {
      ers::fatal(rdb_admin::BadCommandLine(ERS_HERE, "session id must be greater 0"));
      return 1;
    }


  IPCPartition p(partition_name);


    // Choose RDB read-only or read-write server

  rdb::cursor_var cursor;        /// the 'cursor' is used for communication with read-only server
  WriterSession writer;          /// the 'writer' contains several objects used for communication with read-write server
  bool is_writer;

  try {
    is_writer = !(p.isObjectValid<rdb::cursor>( database_name ) );
  }
  catch(daq::ipc::InvalidPartition & ex) {
    ers::fatal(ex);
    return 1;
  }

  try {
    if(is_writer) {
      writer.cursor = p.lookup<rdb::writer>(database_name);
      writer.ping_cb = new RDBWriterPingCB();
      writer.session = writer.cursor->open_session( rdb::ProcessInfoHelper::get(), writer.ping_cb->_this() );
      writer.ping_cb->set_session_id(writer.session);
    }
    else {
      cursor = p.lookup<rdb::cursor>(database_name);
    }
  }
  catch ( CORBA::SystemException & ex ) {
    ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "open_session", ex._name()));
  }
  catch( ers::Issue & ex) {
    ers::fatal(daq::rdb::LookupFailed(ERS_HERE, database_name.c_str(), partition_name.c_str(), ex));
    return 1;
  }


    // load files if any

  if(const auto len = add_files.size()) {
    if(is_writer) {
      ers::fatal(rdb_admin::BadServerType(ERS_HERE, is_writer, "open_database"));
      return 2;
    }

    rdb::RDBNameList files;
    files.length(len);
    for (size_t i=0; i < len; ++i) {
      files[i] = CORBA::string_dup(add_files[i].c_str());
    }
    try {
      cursor->open_database(rdb::ProcessInfoHelper::get(), files);
      std::cout << "The files were loaded\n";
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "open_database", ex._name()));
      return 1;
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "open_database", *daq::idl2ers_issue(ex.issue)));
      return 1;
    }
    catch ( const ers::Issue & ex ) {
      ers::fatal(ex);
      return 1;
    }
  }


    // close files if any

  if(const auto len = close_files.size()) {
    if(is_writer) {
      ers::fatal(rdb_admin::BadServerType(ERS_HERE, is_writer, "close_database"));
      return 2;
    }

    rdb::RDBNameList files;
    files.length(len);
    for (size_t i=0; i < len; ++i) {
      files[i] = CORBA::string_dup(close_files[i].c_str());
    }
    try {
      cursor->close_database(rdb::ProcessInfoHelper::get(), files);
      std::cout << "The files were closed\n";
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "close_database", ex._name()));
      return 1;
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "close_database", *daq::idl2ers_issue(ex.issue)));
      return 1;
    }
    catch ( const ers::Issue & ex ) {
      ers::fatal(ex);
      return 1;
    }
  }


  if (vm.find("reload") != vm.end())
    try
      {
        rdb::ProcessInfo info = rdb::ProcessInfoHelper::get();
        rdb::RDBNameList files;

        const auto len = reload.size();

        files.length(len);

        for (size_t i = 0; i < len; ++i)
          files[i] = CORBA::string_dup(reload[i].c_str());

        if (is_writer)
          writer.cursor->reload_database(info, files);
        else
          cursor->reload_database(info, files);

        std::cout << "The database was reloaded\n";
      }
    catch (CORBA::SystemException & ex)
      {
        ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "reload_database", ex._name()));
        return 1;
      }
    catch ( const rdb::CannotProceed & ex )
      {
        ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "reload_database", *daq::idl2ers_issue(ex.issue)));
        return 1;
      }
    catch ( const ers::Issue & ex )
      {
        ers::fatal(ex);
        return 1;
      }

  
  if (vm.count("get-files")) {
    rdb::RDBNameList_var schema_files;
    rdb::RDBNameList_var data_files;

    try {
      if(is_writer) {
        if(session_id) {
	  std::cout << "Scope: session " << session_id << std::endl;
          writer.cursor->get_databases(session_id, schema_files, data_files);
	}
	else {
	  std::cout << "Scope: default"<< std::endl;
          writer.cursor->get_databases(writer.session, schema_files, data_files);
	}
      }
      else {
        cursor->get_databases(schema_files, data_files);
      }

      std::cout << "Loaded files:\n";
      std::cout << "  " << schema_files.inout().length() << " schema files:\n";
      for(unsigned int idx1 = 0; idx1 < schema_files.inout().length(); idx1++) {
        std::cout << "   \'" << schema_files[idx1] << "\'\n";
      }

      std::cout << "  " << data_files.inout().length() << " data files:\n";
      for(unsigned int idx1 = 0; idx1 < data_files.inout().length(); idx1++) {
        std::cout << "   \'" << data_files[idx1] << "\'\n";
      }
    }
    catch ( rdb::SessionNotFound & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "get_databases", session2str(ex.session_id)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "get_databases", ex._name()));
      return 1;
    }
  }

  if (vm.count("commit-modified-files")) {
    if(!is_writer) {
      ers::fatal(rdb_admin::BadServerType(ERS_HERE, is_writer, "commit_changes"));
      return 2;
    }

    if(!session_id) {
      ers::fatal(rdb_admin::BadCommandLine(ERS_HERE, "session id is required to commit files"));
      return 1;
    }

    std::string token;

    if (daq::tokens::enabled())
      try
        {
          token = daq::tokens::acquire();
        }
      catch(const ers::Issue& ex)
        {
          ers::fatal(rdb_admin::TokenAcquireFailed(ERS_HERE, ex));
          return 1;
        }

    try {
      writer.cursor->commit_changes(session_id, token.c_str(), "rdb_admin utility");
    }
    catch ( rdb::SessionNotFound & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "commit_changes", session2str(ex.session_id)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "commit_changes", ex._name()));
      return 1;
    }
  }

  if (vm.count("get-modified-files")) {
    rdb::RDBNameList_var externally_updated_datafiles;
    rdb::RDBNameList_var externally_removed_datafiles;
    
    try {
      if(is_writer) {
        rdb::RDBNameList_var updated_datafiles;

        if(session_id) {
	  std::cout << "Scope: session " << session_id << std::endl;
          writer.cursor->get_modified_databases(session_id, updated_datafiles, externally_updated_datafiles, externally_removed_datafiles);
	}
	else {
	  std::cout << "Scope: default"<< std::endl;
          writer.cursor->get_modified_databases(writer.session, updated_datafiles, externally_updated_datafiles, externally_removed_datafiles);
	}

        if(unsigned int len = updated_datafiles.inout().length()) {
          std::cout << "There are " << len << " uncommitted files updated by RDB server:\n";
          for(unsigned int idx = 0; idx < len; idx++) {
            std::cout << " - \'" << updated_datafiles[idx] << "\'\n";
	  }
        }
        else {
          std::cout << "There are no uncommitted files updated by RDB server\n";
        }
      }
      else {
        cursor->get_modified_databases(externally_updated_datafiles, externally_removed_datafiles);
      }

      if(unsigned int len = externally_updated_datafiles.inout().length()) {
        std::cout << "There are " << len << " externally modified data files:\n";
        for(unsigned int idx = 0; idx < len; idx++) {
          std::cout << " - \'" << externally_updated_datafiles[idx] << "\'\n";
	}
      }
      else {
        std::cout << "There are no externally modified data files\n";
      }

      if(unsigned int len = externally_removed_datafiles.inout().length()) {
        std::cout << "There are " << len << " externally removed data files:\n";
        for(unsigned int idx = 0; idx < len; idx++) {
          std::cout << " - \'" << externally_removed_datafiles[idx] << "\'\n";
        }
      }
      else {
        std::cout << "There are no externally removed data files\n";
      }

    }
    catch ( rdb::SessionNotFound & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "get_modified_databases", session2str(ex.session_id)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "get_modified_databases", ex._name()));
      return 1;
    }
  }

  if (vm.count("get-changes"))
    {
      rdb::RDBRepositoryVersionList_var versions;

      try
        {
          if (is_writer)
            versions = writer.cursor->get_changes();
          else
            versions = cursor->get_changes();
        }
      catch (rdb::CannotProceed & ex)
        {
          ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "get_repository_versions", *daq::idl2ers_issue(ex.issue)));
          return 1;
        }
      catch ( CORBA::SystemException & ex )
        {
          ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "get_repository_versions", ex._name()));
          return 1;
        }

      if (auto len = versions.inout().length())
        {
          if (len == 1 && versions[0].timestamp == 0)
            {
              const auto num = versions[0].files.length();
              std::cout << "There are " << num << " modified non-repository files:\n";
              for (unsigned int i = 0; i < num; i++)
                std::cout << " - \"" << versions[0].files[i] << "\"\n";
            }
          else
            {
              std::cout << "New repository versions:\n";
              print(versions.inout());
            }
        }
      else
        {
          std::cout << "There are no new repository versions" << std::endl;
        }
    }

  if (!versions_param.empty())
    {
      bool skip_irrelevant;

      if(versions_param[0] == "all")
        skip_irrelevant = false;
      else if(versions_param[0] == "skip")
        skip_irrelevant = true;
      else
        {
          ers::fatal( rdb_admin::BadCommandLine( ERS_HERE, "first versions parameter must be \"all\" or \"skip\"" ) );
          return 1;
        }

      rdb::VersionsQueryType query_type;

      if(versions_param[1] == "date")
        query_type = rdb::query_by_date;
      else if(versions_param[1] == "id")
        query_type = rdb::query_by_id;
      else if(versions_param[1] == "tag")
        query_type = rdb::query_by_tag;
      else
        {
          ers::fatal( rdb_admin::BadCommandLine( ERS_HERE, "second versions parameter must be \"date\", \"id\" or \"tag\"" ) );
          return 1;
        }

      rdb::RDBRepositoryVersionList_var versions;

      try
        {
          if (is_writer)
            versions = writer.cursor->get_versions(skip_irrelevant, query_type, versions_param[2].c_str(), versions_param[3].c_str());
          else
            versions = cursor->get_versions(skip_irrelevant, query_type, versions_param[2].c_str(), versions_param[3].c_str());
        }
      catch (rdb::CannotProceed & ex)
        {
          ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "get_repository_versions", *daq::idl2ers_issue(ex.issue)));
          return 1;
        }
      catch ( CORBA::SystemException & ex )
        {
          ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "get_repository_versions", ex._name()));
          return 1;
        }

      if (versions.inout().length())
        {
          std::cout << "Repository versions:\n";
          print(versions.inout());
        }
      else
        {
          std::cout << "No repository versions found" << std::endl;
        }

    }

  if(!update_status.empty()) {
    
    try {
      rdb::ProcessInfo info = rdb::ProcessInfoHelper::get();

      if(is_writer) {
        if(session_id) {
	  std::cout << "Scope: session " << session_id << std::endl;
          writer.cursor->update_database_status(info, session_id, update_status.c_str());
	}
	else {
	  std::cout << "Scope: default"<< std::endl;
          writer.cursor->update_database_status(info, 0, update_status.c_str());
	}
      }
      else {
        cursor->update_database_status(info, update_status.c_str());
      }
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "update_database_status", *daq::idl2ers_issue(ex.issue)));
     return 1;
    }
    catch ( rdb::SessionNotFound & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "update_database_status", session2str(ex.session_id)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "update_database_status", ex._name()));
      return 1;
    }
    catch ( const ers::Issue & ex ) {
      ers::fatal(ex);
      return 1;
    }
  }

  if(const auto len = merge_command.size()) {
    if(is_writer) {
      ers::fatal(rdb_admin::BadServerType(ERS_HERE, is_writer, "merge_database"));
      return 2;
    }

    static const char * reload_usage =
      "valid 5 parameters for \'--merge\' are: <schema-file-name> <data-file-name> <class-name> <query> <reference-level>.";

    if(len != 5) {
      ers::fatal(rdb_admin::BadCommandLine(ERS_HERE, reload_usage));
      return 1;
    }

    const char * merge_schema = merge_command[0].c_str();
    const char * merge_data = merge_command[1].c_str();
    const char * class_name = merge_command[2].c_str();
    const char * query = merge_command[3].c_str();
    const char * ref_str = merge_command[4].c_str();

    CORBA::Long ref_level = atoi(ref_str);

    try {
      cursor->merge_database(merge_data, merge_schema, class_name, query, ref_level, false);
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "merge_database", *daq::idl2ers_issue(ex.issue)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "merge_database", ex._name()));
      return 1;
    }
  }

  if (vm.count("list-sessions")) {
    if(!is_writer) {
      ers::fatal(rdb_admin::BadServerType(ERS_HERE, is_writer, "list_sessions"));
      return 2;
    }

    try {
      rdb::SessionInfoList_var l;
      writer.cursor->list_sessions(rdb::ProcessInfoHelper::get(), l);

      std::cout << "Number of sessions: " << l->length() << std::endl;

      for(unsigned int idx = 0; idx < l->length(); idx++) {
        std::cout << idx << '\t' << l[idx].id << " \t" << l[idx].creation_ts << " \t" << l[idx].host << " \t" << l[idx].process_id << " \t" << l[idx].user_id << " \t" << l[idx].app_name << std::endl;
      }
    }
    catch ( rdb::CannotProceed & ex ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "list_sessions", *daq::idl2ers_issue(ex.issue)));
      return 1;
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "list_sessions", ex._name()));
      return 1;
    }
  }

  if (vm.count("shutdown")) {
    try {
      if(is_writer) {
        writer.cursor->shutdown();
      }
      else {
        cursor->shutdown();
      }
      std::cout << "The server was shutdown\n";
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_admin::CorbaRequestFailed(ERS_HERE, "shutdown", ex._name()));
      return 1;
    }
    catch( ers::Issue & ex) {
      ers::fatal(daq::rdb::LookupFailed(ERS_HERE, database_name, partition_name, ex));
      return 1;
    }
    catch ( ... ) {
      ers::fatal(rdb_admin::RequestFailed(ERS_HERE, "shutdown", "unknown reason"));
      return 1;
    }
  }

  return 0; 
}
