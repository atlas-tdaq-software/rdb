#!/bin/sh

#############################################################################

startup_timeout='10'

#############################################################################

binary="rdb_${2}"
test="test_${binary}"
binary_path="${3}/${binary}"

echo "binary: ${binary}"

#############################################################################

echo "START UP $binary ..."

  # run the server

db_name=${1}
shift
shift
shift
cmd="${binary_path} -d ${db_name} $@"

echo " * execute \"${cmd}\""

${cmd} > /dev/null &

count=1
result=1
while [ $count -le ${startup_timeout} ] ; do
  sleep 1
  ${test} -n ${db_name}
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo " * waiting \"${db_name}\" ${binary} startup (${count} of ${startup_timeout}) ..."
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run ${binary} ${db_name} (timeout after $startup_timeout seconds)"
  exit 1
fi

echo " * the ${binary} is running"
