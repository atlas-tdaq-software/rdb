#!/bin/sh

#################################################################
#
#	New ctest the RDB package
#
#################################################################

dir=`mktemp -d` || exit 1

#################################################################

ipc_file="${dir}/ipc_root.ref"
TDAQ_IPC_INIT_REF="file:${ipc_file}"
export TDAQ_IPC_INIT_REF

cleanup='ipc_rm -i ".*" -n ".*" -f -t'

schema_file="${2}/test/test.schema.xml"
data_file="${dir}/test.data.$$.xml"
master_server_name='RDB'
pool_server_name='RDB_POOL'
read_write_server_name='RDB_RW'
exit_status=0

#################################################################

rdb_master_pid=0
rdb_pool_pid=0
rdb_writer_pid=0
ipc_server_pid=0

Cleanup () {
  if [ $rdb_writer_pid -ne 0 ]
  then
    echo "kill rdb_writer process $rdb_writer_pid"
    kill $rdb_writer_pid
  fi

  if [ $rdb_master_pid -ne 0 ]
  then
    echo "kill rdb_server process $rdb_master_pid"
    kill $rdb_master_pid
  fi

  if [ $rdb_pool_pid -ne 0 ]
  then
    echo "kill rdb_server process $rdb_pool_pid"
    kill $rdb_pool_pid
  fi

  sleep 0.25

  if [ $ipc_server_pid -ne 0 ]
  then
    echo "kill ipc_server process $ipc_server_pid"
    kill $ipc_server_pid
  fi
  
  echo "rm -rf ${dir}"
  rm -rf ${dir}
}

#################################################################

# destroy any running server

trap 'echo "caught signal: destroy servers ..."; Cleanup; exit 1' 1 2 15

#################################################################

startup_timeout='10'

# run ipc_server
echo "(1/8) run: \"ipc_server > ${dir}/ipc-server.out 2>&1 &\""
ipc_server > "${dir}/ipc-server.out" 2>&1 &
ipc_server_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  test_ipc_server
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo ' * waiting general ipc_server startup ...'
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run general ipc_server (timeout after $startup_timeout seconds)"
  exit 1
fi

#################################################################

# run rdb_writer
echo "(2/8) run: \"${1}/rdb_writer -d ${read_write_server_name} > ${dir}/${read_write_server_name}.out 2>&1 &\""
${1}/rdb_writer -d ${read_write_server_name} > "${dir}/${read_write_server_name}.out" 2>&1 &
rdb_writer_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  #test_rdb_writer -n ${read_write_server_name}
  test_corba_server -n ${read_write_server_name} -c 'rdb/writer'
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo " * waiting rdb_writer ${read_write_server_name} startup ..."
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run rdb_writer (timeout after $startup_timeout seconds)"
  exit 1
fi

#################################################################

# run rdb_server
echo "(3/8) run: \"${1}/rdb_server -d ${master_server_name} > ${dir}/${master_server_name}.out 2>&1 &\""
${1}/rdb_server -d ${master_server_name} > "${dir}/${master_server_name}.out" 2>&1 &
rdb_master_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  #test_rdb_writer -n ${master_server_name}
  test_corba_server -n ${master_server_name} -c 'rdb/cursor'
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo " * waiting rdb_server ${master_server_name} startup ..."
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run rdb_writer (timeout after $startup_timeout seconds)"
  exit 1
fi

#################################################################

${1}/rdb_test -d ${master_server_name} -w ${read_write_server_name} -S ${schema_file} -D ${data_file}
exit_status=$(( $exit_status | $? ))

#################################################################

${1}/rdb_dump -d ${master_server_name} -ODT

#################################################################

cmd="${1}/rdb_admin -d ${master_server_name} -c ${data_file}"
echo " * execute \"${cmd}\""
${cmd} > /dev/null
exit_status=$(( $exit_status | $? ))

cmd="${1}/rdb_admin -d ${master_server_name} -l daq/segments/setup.data.xml"
echo " * execute \"${cmd}\""
${cmd} > /dev/null
exit_status=$(( $exit_status | $? ))

echo "(4/8) run: \"${1}/rdb_server -d ${pool_server_name} -m ${master_server_name} > ${dir}/${pool_server_name}.out 2>&1 &\""
${1}/rdb_server -d ${pool_server_name} -m ${master_server_name} > "${dir}/${pool_server_name}.out" 2>&1 &
rdb_pool_pid=$!

count=0
result=1
while [ $count -lt ${startup_timeout} ] ; do
  sleep 1
  test_rdb_server -n ${pool_server_name}
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo " * waiting rdb_server ${pool_server_name} startup ..."
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR: failed to run rdb_server (timeout after $startup_timeout seconds)"
  exit 1
fi

echo '(5/8) test replication'

cmd="${1}/rdb_admin -d ${master_server_name} -e ${dir}/${master_server_name}.schema.xml ${dir}/${master_server_name}.data.xml"
echo " * execute \"${cmd}\" '' '' 1000"
${cmd} '' '' 1000 > /dev/null
exit_status=$(( $exit_status | $? ))

cmd="${1}/rdb_admin -d ${pool_server_name} -e ${dir}/${pool_server_name}.schema.xml ${dir}/${pool_server_name}.data.xml"
echo " * execute \"${cmd}\" '' '' 1000"
${cmd} '' '' 1000 > /dev/null
exit_status=$(( $exit_status | $? ))

sed -i '/<file path/d;/<info name/d;/<method/d;/<\/method/d' ${dir}/${master_server_name}.schema.xml
sed -i '/<file path/d;/<info name/d' ${dir}/${pool_server_name}.schema.xml
cmd="diff ${dir}/${master_server_name}.schema.xml ${dir}/${pool_server_name}.schema.xml"
echo " * execute \"${cmd}\""
${cmd}
exit_status=$(( $exit_status | $? ))
sed -i '/\<file path/d;/\<info name/d' ${dir}/${master_server_name}.data.xml
sed -i '/\<file path/d;/\<info name/d' ${dir}/${pool_server_name}.data.xml
cmd="diff ${dir}/${master_server_name}.data.xml ${dir}/${pool_server_name}.data.xml"
echo " * execute \"${cmd}\""
${cmd}
exit_status=$(( $exit_status | $? ))

echo '(6/8) test data access methods'

fast_dump="${dir}/${master_server_name}_fast_dump.txt"
old_dump="${dir}/${master_server_name}_old_dump.txt"

cmd="${1}/rdb_dump -d ${master_server_name} -D"
echo " * execute \"${cmd}\" > ${fast_dump}"
${cmd} > ${fast_dump}
exit_status=$(( $exit_status | $? ))

cmd="${1}/rdb_dump -d ${master_server_name} -T"
echo " * execute \"${cmd}\" > ${old_dump}"
${cmd} > ${old_dump}
exit_status=$(( $exit_status | $? ))

cmd="diff ${fast_dump} ${old_dump}"
echo " * execute \"${cmd}\""
${cmd}
exit_status=$(( $exit_status | $? ))

#################################################################

echo '(7/8) stop RDB servers'

cmd="${1}/rdb_admin -d ${master_server_name} --shutdown"
echo " * execute \"${cmd}\""
${cmd} > /dev/null
exit_status=$(( $exit_status | $? ))

cmd="${1}/rdb_admin -d ${pool_server_name} --shutdown"
echo " * execute \"${cmd}\""
${cmd} > /dev/null
exit_status=$(( $exit_status | $? ))

cmd="${1}/rdb_admin -d ${read_write_server_name} --shutdown"
echo " * execute \"${cmd}\""
${cmd} > /dev/null
exit_status=$(( $exit_status | $? ))

sleep 0.5

echo 'done'
echo ''

#################################################################

# clean processes

echo 'DESTROY RUNNING SERVERS ...'
echo '(8/8) final cleanup'

Cleanup
echo ' * done'
echo ''

#################################################################

# exit

echo ''
echo '********************************'
echo "*** DONE, exit with status $exit_status ***"
echo '********************************'
echo ''

exit $exit_status

#################################################################
