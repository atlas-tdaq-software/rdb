#include <iostream>
#include <cstdlib>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/exceptions.h>

#include <ers2idl/ers2idl.h>

#include "rdb/rdb.hh"
#include "rdb/rdb_info.h"
#include "rdb/errors.h"

ERS_DECLARE_ISSUE(
  rdb_test,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_test,
  RequestFailed,
  "method \"" << method << "\" raised exception:\n" << reason,
  ((const char*)method)
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_test,
  CorbaRequestFailed,
  "a method raised CORBA exception " << reason,
  ((const char*)reason)
)

static rdb::cursor_var cursor;        /// the 'cursor' is used for communication with RDB read-only server
static rdb::writer_var writer;        /// the 'cursor' is used for communication with RDB writer server
static rdb::RDBSessionId session;     /// the 'session' is identifier for all communications with writer server

int okcheck = EXIT_SUCCESS;


static void
test_passed()
{
  std::cout << "PASSED" << std::endl;
  std::cout.flush();
}

static void
test_failed()
{
  okcheck = EXIT_FAILURE;
  std::cout << "FAILED" << std::endl;
  std::cout.flush();
}

static void
create_object(rdb::RDBObject& o, const std::string& where)
{
  std::cout << "TEST writer create_object(\"" << o.name << '@' <<  o.classid << "\") ";
  writer->create_object(session, o.classid, o.name, where.c_str());
  test_passed();
}

static void
set_attribute(const char * aname, const rdb::RDBObject& o, const rdb::DataUnion& d)
{
  std::cout << "TEST writer set_attribute_value(\"" << o.name << '@' <<  o.classid << "\", \"" << aname << "\") ";
  writer->set_attribute_value(session, o, aname, d);
  test_passed();
}

static void
set_attribute(const char * aname, const rdb::RDBObject& o, const rdb::DataListUnion& d)
{
  std::cout << "TEST writer set_attribute_values(\"" << o.name << '@' <<  o.classid << "\", \"" << aname << "\") ";
  writer->set_attribute_values(session, o, aname, d);
  test_passed();
}

static void
set_relationship(const char * rname, const rdb::RDBObject& o, const rdb::RDBObjectList& d)
{
  std::cout << "TEST writer set_objects_of_relationship(\"" << o.name << '@' <<  o.classid << "\", \"" << rname << "\") ";
  writer->set_objects_of_relationship(session, o, rname, d, false);
  test_passed();
}

static void
get_attribute(bool reader, const char * aname, const rdb::RDBObject& o, rdb::DataUnion_var& d)
{
  std::cout << "TEST read_attribute_value(\"" << o.name << '@' <<  o.classid << "\", \"" << aname << "\") on " << (reader ? "reader" : "writer") << " server ";
  if (reader)
    cursor->get_attribute_value(o, aname, d);
  else
    writer->get_attribute_value(session, o, aname, d);
  test_passed();
}

static void
get_attribute(bool reader, const char * aname, const rdb::RDBObject& o, rdb::DataListUnion_var& d)
{
  std::cout << "TEST read_attribute_values(\"" << o.name << '@' <<  o.classid << "\", \"" << aname << "\") on " << (reader ? "reader" : "writer") << " server ";
  if (reader)
    cursor->get_attribute_values(o, aname, d);
  else
    writer->get_attribute_values(session, o, aname, d);
  test_passed();
}

static void
get_relationship(bool reader, std::size_t min_len, std::size_t max_len, const char * rname, const rdb::RDBObject& o, rdb::RDBObjectList_var& d)
{
  std::cout << "TEST get_objects_of_relationship(\"" << o.name << '@' <<  o.classid << "\", \"" << rname << "\") on " << (reader ? "reader" : "writer") << " server ";

  if (reader)
    cursor->get_objects_of_relationship(o, rname, d);
  else
    writer->get_objects_of_relationship(session, o, rname, d);

  if (d->length() < min_len || d->length() > max_len)
    {
      std::stringstream text;
      text << "wrong cardinality: got " << d->length() << " objects instead of [" << min_len << '-' << max_len << "] expected";
      throw std::runtime_error(text.str().c_str());
    }

  test_passed();
}

template <typename T>
static void
compare_attribute(const char * aname, const std::string id, const char * class_name, const T& x1, const T& x2)
{
  if (x1 != x2)
    {
      std::ostringstream text;
      text << "value of attribute \"" << aname << "\" of object \"" << id << '@' <<  class_name << "\" = \"" << x1 << "\" is different from original \"" << x2 << "\"";
      throw std::runtime_error(text.str().c_str());
    }
}

template <int N, typename T>
static void
compare_attributes(const char * aname, const std::string id, const char * class_name, const T (&x1)[N], const T (&x2)[N])
{
  for(int i = 0; i < N; ++i)
    if (x1[i] != x2[i])
      {
        std::ostringstream text;
        text << "array value of attribute \"" << aname << "\" of object \"" << id << '@' <<  class_name << "\" = \"" << x1[i] << "\" is different from \"" << x2[i] << "\" for index " << i;
        throw std::runtime_error(text.str().c_str());
      }
}

template<typename T>
  static void
  compare_relationship(const char *rname, const std::string id, const char *class_name, const T& x1, const T& x2)
  {
    if (x1 == nullptr && x2 == nullptr)
      return;

    if (x1 == nullptr || x2 == nullptr || x1->m_id != x2->m_id)
      {
        std::ostringstream text;
        text << "value of relationship \"" << rname << "\" of object \"" << id << '@' << class_name << "\" = \"" << x1 << "\" is different from original \"" << x2 << "\"";
        throw std::runtime_error(text.str().c_str());
      }
  }

template<int N, typename T>
  static void
  compare_relationship(const char *rname, const std::string id, const char *class_name, const T (&x1)[N], const T (&x2)[N])
  {
    for(int i = 0; i < N; ++i)
      if (x1[i]->m_id != x2[i]->m_id)
        {
          std::ostringstream text;
          text << "array value of relationship \"" << rname << "\" of object \"" << id << '@' <<  class_name << "\" = \"" << x1[i]->m_id << "\" is different from \"" << x2[i]->m_id << "\" for index " << i;
          throw std::runtime_error(text.str().c_str());
        }
  }

////

struct ItemBase {
  std::string m_id;
};

template <int N>
struct Item : public ItemBase
{
  bool m_bool;
  int8_t m_s8;
  uint8_t m_u8;
  int16_t m_s16;
  uint16_t m_u16;
  int32_t m_s32;
  uint32_t m_u32;
  int64_t m_s64;
  uint64_t m_u64;
  float m_float;
  double m_double;
  std::string m_date;
  std::string m_time;
  std::string m_string;
  std::string m_enum;
  std::string m_class;
  bool m_array_bool[N];
  int8_t m_array_s8[N];
  uint8_t m_array_u8[N];
  int16_t m_array_s16[N];
  uint16_t m_array_u16[N];
  int32_t m_array_s32[N];
  uint32_t m_array_u32[N];
  int64_t m_array_s64[N];
  uint64_t m_array_u64[N];
  float m_array_float[N];
  double m_array_double[N];
  std::string m_array_date[N];
  std::string m_array_time[N];
  std::string m_array_string[N];
  std::string m_array_enum[N];
  std::string m_array_class[N];

  rdb::RDBObject create(const std::string& where)
  {
    rdb::RDBObject o;

    o.classid = "Item";
    o.name = m_id.c_str();

    create_object(o, where);

    rdb::DataUnion d;

    d.du_bool(m_bool); set_attribute("bool", o, d);
    d.du_s8_int(m_s8); set_attribute("s8", o, d);
    d.du_u8_int(m_u8); set_attribute("u8", o, d);
    d.du_s16_int(m_s16); set_attribute("s16", o, d);
    d.du_u16_int(m_u16); set_attribute("u16", o, d);
    d.du_s32_int(m_s32); set_attribute("s32", o, d);
    d.du_u32_int(m_u32); set_attribute("u32", o, d);
    d.du_s64_int(m_s64); set_attribute("s64", o, d);
    d.du_u64_int(m_u64); set_attribute("u64", o, d);
    d.du_float(m_float); set_attribute("float", o, d);
    d.du_double(m_double); set_attribute("double", o, d);
    d.du_string(m_date.c_str()); set_attribute("date", o, d);
    d.du_string(m_time.c_str()); set_attribute("time", o, d);
    d.du_string(m_string.c_str()); set_attribute("string", o, d);
    d.du_string(m_enum.c_str()); set_attribute("enum", o, d);
    d.du_string(m_class.c_str()); set_attribute("class", o, d);

    rdb::DataListUnion dl;

    { rdb::DataListUnion::_du_bools_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_bool[i]; dl.du_bools(seq); set_attribute("array<bool>", o, dl); }
    { rdb::DataListUnion::_du_s8_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_s8[i]; dl.du_s8_ints(seq); set_attribute("array<s8>", o, dl); }
    { rdb::DataListUnion::_du_u8_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_u8[i]; dl.du_u8_ints(seq); set_attribute("array<u8>", o, dl); }
    { rdb::DataListUnion::_du_s16_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_s16[i]; dl.du_s16_ints(seq); set_attribute("array<s16>", o, dl); }
    { rdb::DataListUnion::_du_u16_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_u16[i]; dl.du_u16_ints(seq); set_attribute("array<u16>", o, dl); }
    { rdb::DataListUnion::_du_s32_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_s32[i]; dl.du_s32_ints(seq); set_attribute("array<s32>", o, dl); }
    { rdb::DataListUnion::_du_u32_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_u32[i]; dl.du_u32_ints(seq); set_attribute("array<u32>", o, dl); }
    { rdb::DataListUnion::_du_s64_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_s64[i]; dl.du_s64_ints(seq); set_attribute("array<s64>", o, dl); }
    { rdb::DataListUnion::_du_u64_ints_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_u64[i]; dl.du_u64_ints(seq); set_attribute("array<u64>", o, dl); }
    { rdb::DataListUnion::_du_floats_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_float[i]; dl.du_floats(seq); set_attribute("array<float>", o, dl); }
    { rdb::DataListUnion::_du_doubles_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_double[i]; dl.du_doubles(seq); set_attribute("array<double>", o, dl); }
    { rdb::DataListUnion::_du_strings_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_date[i].c_str(); dl.du_strings(seq); set_attribute("array<date>", o, dl); }
    { rdb::DataListUnion::_du_strings_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_time[i].c_str(); dl.du_strings(seq); set_attribute("array<time>", o, dl); }
    { rdb::DataListUnion::_du_strings_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_string[i].c_str(); dl.du_strings(seq); set_attribute("array<string>", o, dl); }
    { rdb::DataListUnion::_du_strings_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_enum[i].c_str(); dl.du_strings(seq); set_attribute("array<enum>", o, dl); }
    { rdb::DataListUnion::_du_strings_seq seq; seq.length(N); for(int i = 0; i < N; ++i) seq[i] = m_array_class[i].c_str(); dl.du_strings(seq); set_attribute("array<class>", o, dl); }

    return o;
  }

  void
  read(bool reader, const std::string& id)
  {
    rdb::RDBObject o;

    o.classid = "Item";
    o.name = id.c_str();

    rdb::DataUnion_var d;

    get_attribute(reader, "bool", o, d); m_bool = d->du_bool();
    get_attribute(reader, "s8", o, d); m_s8 = d->du_s8_int();
    get_attribute(reader, "u8", o, d); m_u8 = d->du_u8_int();
    get_attribute(reader, "s16", o, d); m_s16 = d->du_s16_int();
    get_attribute(reader, "u16", o, d); m_u16 = d->du_u16_int();
    get_attribute(reader, "s32", o, d); m_s32 = d->du_s32_int();
    get_attribute(reader, "u32", o, d); m_u32 = d->du_u32_int();
    get_attribute(reader, "s64", o, d); m_s64 = d->du_s64_int();
    get_attribute(reader, "u64", o, d); m_u64 = d->du_u64_int();
    get_attribute(reader, "float", o, d); m_float = d->du_float();
    get_attribute(reader, "double", o, d); m_double = d->du_double();
    get_attribute(reader, "date", o, d); m_date = d->du_string();
    get_attribute(reader, "time", o, d); m_time = d->du_string();
    get_attribute(reader, "string", o, d); m_string = d->du_string();
    get_attribute(reader, "enum", o, d); m_enum = d->du_string();
    get_attribute(reader, "class", o, d); m_class = d->du_string();

    rdb::DataListUnion_var dl;

    { get_attribute(reader, "array<bool>", o, dl); for (int i = 0; i < N; ++i) m_array_bool[i] = dl->du_bools()[i]; }
    { get_attribute(reader, "array<s8>", o, dl); for (int i = 0; i < N; ++i) m_array_s8[i] = dl->du_s8_ints()[i]; }
    { get_attribute(reader, "array<u8>", o, dl); for(int i = 0; i < N; ++i) m_array_u8[i] = dl->du_u8_ints()[i]; }
    { get_attribute(reader, "array<s16>", o, dl); for(int i = 0; i < N; ++i) m_array_s16[i] = dl->du_s16_ints()[i]; }
    { get_attribute(reader, "array<u16>", o, dl); for(int i = 0; i < N; ++i) m_array_u16[i] = dl->du_u16_ints()[i]; }
    { get_attribute(reader, "array<s32>", o, dl); for(int i = 0; i < N; ++i) m_array_s32[i] = dl->du_s32_ints()[i]; }
    { get_attribute(reader, "array<u32>", o, dl); for(int i = 0; i < N; ++i) m_array_u32[i] = dl->du_u32_ints()[i]; }
    { get_attribute(reader, "array<s64>", o, dl); for(int i = 0; i < N; ++i) m_array_s64[i] = dl->du_s64_ints()[i]; }
    { get_attribute(reader, "array<u64>", o, dl); for(int i = 0; i < N; ++i) m_array_u64[i] = dl->du_u64_ints()[i]; }
    { get_attribute(reader, "array<float>", o, dl); for(int i = 0; i < N; ++i) m_array_float[i] = dl->du_floats()[i]; }
    { get_attribute(reader, "array<double>", o, dl); for(int i = 0; i < N; ++i) m_array_double[i] = dl->du_doubles()[i]; }
    { get_attribute(reader, "array<date>", o, dl); for(int i = 0; i < N; ++i) m_array_date[i] = dl->du_strings()[i]; }
    { get_attribute(reader, "array<time>", o, dl); for(int i = 0; i < N; ++i) m_array_time[i] = dl->du_strings()[i]; }
    { get_attribute(reader, "array<string>", o, dl); for(int i = 0; i < N; ++i) m_array_string[i] = dl->du_strings()[i]; }
    { get_attribute(reader, "array<enum>", o, dl); for(int i = 0; i < N; ++i) m_array_enum[i] = dl->du_strings()[i]; }
    { get_attribute(reader, "array<class>", o, dl); for(int i = 0; i < N; ++i) m_array_class[i] = dl->du_strings()[i]; }
  }

  void
  do_compare_test(const Item<N>& other, const std::string& server)
  {
    std::cout << "TEST compare object \"" << m_id << '@' <<  "Item" << "\" read from \"" << server << "\" with reference one ";

    compare_attribute("bool", m_id, "Item", m_bool, other.m_bool);
    compare_attribute("s8", m_id, "Item", m_s8, other.m_s8);
    compare_attribute("u8", m_id, "Item", m_u8, other.m_u8);
    compare_attribute("s16", m_id, "Item", m_s16, other.m_s16);
    compare_attribute("u16", m_id, "Item", m_u16, other.m_u16);
    compare_attribute("s32", m_id, "Item", m_s32, other.m_s32);
    compare_attribute("u32", m_id, "Item", m_u32, other.m_u32);
    compare_attribute("s64", m_id, "Item", m_s64, other.m_s64);
    compare_attribute("u64", m_id, "Item", m_u64, other.m_u64);
    compare_attribute("float", m_id, "Item", m_float, other.m_float);
    compare_attribute("double", m_id, "Item", m_double, other.m_double);
    compare_attribute("date", m_id, "Item", m_date, other.m_date);
    compare_attribute("time", m_id, "Item", m_time, other.m_time);
    compare_attribute("string", m_id, "Item", m_string, other.m_string);
    compare_attribute("enum", m_id, "Item", m_enum, other.m_enum);
    compare_attribute("class", m_id, "Item", m_class, other.m_class);

    compare_attributes<N, bool>("array<bool>", m_id, "Item", m_array_bool, other.m_array_bool);
    compare_attributes<N, int8_t>("array<s8>", m_id, "Item", m_array_s8, other.m_array_s8);
    compare_attributes<N, uint8_t>("array<u8>", m_id, "Item", m_array_u8, other.m_array_u8);
    compare_attributes<N, int16_t>("array<s16>", m_id, "Item", m_array_s16, other.m_array_s16);
    compare_attributes<N, uint16_t>("array<u16>", m_id, "Item", m_array_u16, other.m_array_u16);
    compare_attributes<N, int32_t>("array<s32>", m_id, "Item", m_array_s32, other.m_array_s32);
    compare_attributes<N, uint32_t>("array<u32>", m_id, "Item", m_array_u32, other.m_array_u32);
    compare_attributes<N, int64_t>("array<s64>", m_id, "Item", m_array_s64, other.m_array_s64);
    compare_attributes<N, uint64_t>("array<u64>", m_id, "Item", m_array_u64, other.m_array_u64);
    compare_attributes<N, float>("array<float>", m_id, "Item", m_array_float, other.m_array_float);
    compare_attributes<N, double>("array<double>", m_id, "Item", m_array_double, other.m_array_double);
    compare_attributes<N, std::string>("array<date>", m_id, "Item", m_array_date, other.m_array_date);
    compare_attributes<N, std::string>("array<time>", m_id, "Item", m_array_time, other.m_array_time);
    compare_attributes<N, std::string>("array<string>", m_id, "Item", m_array_string, other.m_array_string);
    compare_attributes<N, std::string>("array<enum>", m_id, "Item", m_array_enum, other.m_array_enum);
    compare_attributes<N, std::string>("array<class>", m_id, "Item", m_array_class, other.m_array_class);

    test_passed();
  }

};

struct ContainerBase {
  std::string m_id;
};

template<int A, int B, int C>
  struct Container : public ContainerBase
  {
    ItemBase* m_item;
    ItemBase* m_items[A];
    ItemBase* m_one_item;
    ItemBase* m_one_or_many_items[B];
    ContainerBase* m_container;
    ContainerBase* m_containers[C];

    template<typename T>
    void
    fill(T * item, const char *classid, rdb::RDBObjectList &olist)
    {
      if (item)
        {
          olist.length(1);
          olist[0].classid = classid;
          olist[0].name = item->m_id.c_str();
        }
      else
        olist.length(0);
    }

    template<int N, typename T>
    void
    fills(const T (&items)[N], const char *classid, rdb::RDBObjectList &olist)
      {
        olist.length(N);

        for (int i = 0; i < N; ++i)
          {
            olist[i].classid = classid;
            olist[i].name = items[i]->m_id.c_str();
          }
      }


  rdb::RDBObject create(const std::string& where)
  {
    rdb::RDBObject o;

    o.classid = "Container";
    o.name = m_id.c_str();

    create_object(o, where);

    rdb::RDBObjectList olist;

    fill(m_item, "Item", olist); set_relationship("item", o, olist);
    fills(m_items, "Item", olist); set_relationship("items", o, olist);
    fill(m_one_item, "Item", olist); set_relationship("one-item", o, olist);
    fills(m_one_or_many_items, "Item", olist); set_relationship("one-or-many-items", o, olist);
    fill(m_container, "Container", olist); set_relationship("container", o, olist);
    fills(m_containers, "Container", olist); set_relationship("containers", o, olist);

    return o;
  }

  ItemBase *
  make_item(const rdb::RDBObject& o)
  {
    static std::string classid("Item");

    if (classid == (const char *)o.classid)
      {
        auto obj = new ItemBase();
        obj->m_id = o.name;
        return obj;
      }
    else
      {
        std::ostringstream text;
        text << "wrong classid = \"" << o.classid << "\" instead of \"" << classid << "\" expected";
        throw std::runtime_error(text.str().c_str());
      }
  }

  ContainerBase *
  make_container(const rdb::RDBObject& o)
  {
    static std::string classid("Container");

    if (classid == (const char *)o.classid)
      {
        auto obj = new ContainerBase();
        obj->m_id = o.name;
        return obj;
      }
    else
      {
        std::ostringstream text;
        text << "wrong classid = \"" << o.classid << "\" instead of \"" << classid << "\" expected";
        throw std::runtime_error(text.str().c_str());
      }
  }

  void
  read(bool reader, const std::string& id)
  {
    rdb::RDBObject o;

    o.classid = "Container";
    o.name = id.c_str();

    rdb::RDBObjectList_var d;

    get_relationship(reader, 0, 1, "item", o, d); m_item = (d->length() ? make_item(d[0]) : nullptr);
    get_relationship(reader, A, A, "items", o, d); for (int i = 0; i < A; ++i) m_items[i] = make_item(d[i]);
    get_relationship(reader, 1, 1, "one-item", o, d); m_one_item = make_item(d[0]);
    get_relationship(reader, B, B, "one-or-many-items", o, d); for (int i = 0; i < B; ++i) m_one_or_many_items[i] = make_item(d[i]);
    get_relationship(reader, 0, 1, "container", o, d); m_container = (d->length() ? make_container(d[0]) : nullptr);
    get_relationship(reader, C, C, "containers", o, d); for (int i = 0; i < C; ++i) m_containers[i] = make_container(d[i]);
  }

  void
    do_compare_test(const Container &other, const std::string &server)
    {
      std::cout << "TEST compare object \"" << m_id << '@' << "Container" << "\" read from \"" << server << "\" with reference one ";

      compare_relationship("item", m_id, "Container", m_item, other.m_item);
      compare_relationship("items", m_id, "Container", m_items, other.m_items);
      compare_relationship("one-item", m_id, "Container", m_one_item, other.m_one_item);
      compare_relationship("one-or-many-items", m_id, "Container", m_one_or_many_items, other.m_one_or_many_items);
      compare_relationship("container", m_id, "Container", m_container, other.m_container);
      compare_relationship("containers", m_id, "Container", m_containers, other.m_containers);
      test_passed();
    }
  };

Item<1> item1 = {"item1", false, -5, 7, -234, 222, 0, 0xffffffff, -12345678901234, 9999888877776666, -3.14, 2.71, "2000-Jan-01", "2000-Jan-01 00:00:00", "hello, world", "A", "Container",
    {false}, {-5}, {7}, {-234}, {222}, {0}, {0xffffffff}, {-12345678901234}, {9999888877776666}, {-3.14}, {2.71}, {"2000-Jan-01"}, {"2000-Jan-01 00:00:00"}, {"hello, world"}, {"A"}, {"Container"} };

Item<2> item2 = {"item2", true, -22, 19, -1, 244, -1000000, 1000000, -1, 888877776666555, -3.1415, 2.719, "1970-Jan-01", "1970-Jan-01 00:00:00", "hi there", "B", "Item",
    {false, true}, {-5, -22}, {7, 19}, {-234, -1}, {222, 244}, {0, -1000000}, {0xffffffff, 1000000}, {-12345678901234, -1}, {9999888877776666, 888877776666555}, {-3.14, -3.1415}, {2.71, 2.719},
    {"2000-Jan-01", "1970-Jan-01"}, {"2000-Jan-01 00:00:00", "1970-Jan-01 00:00:00"}, {"hello, world", "hi there"}, {"A", "B"}, {"Container", "Item"} };

Container<1,1,1> container1 = {"container1", nullptr, {&item1}, &item1, {&item1}, nullptr, {&container1} };
Container<2,2,2> container2 = {"container2", &item1, {&item2, &item1}, &item2, {&item2, &item1}, &container1, {&container2, &container1} };



int main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return 1;
    }

  std::string partition_name, server_name, writer_name, schema_name, data_name;

  try
    {
      boost::program_options::options_description desc("CTest binary for RDB package");

      desc.add_options()
        ( "partition,p", boost::program_options::value<std::string>(&partition_name)->default_value("initial"), "IPC partition name" )
        ( "database,d", boost::program_options::value<std::string>(&server_name)->required(), "RDB read-only server name" )
        ( "writer,w", boost::program_options::value<std::string>(&writer_name)->required(), "RDB writer server name" )
        ( "schema,S", boost::program_options::value<std::string>(&schema_name)->required(), "OKS schema file" )
        ( "data,D", boost::program_options::value<std::string>(&data_name)->required(), "OKS data file" )
        ( "help,h", "print help message" );

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      boost::program_options::notify(vm);
    }
  catch (std::exception &ex)
    {
      ers::fatal(rdb_test::BadCommandLine(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  IPCPartition p(partition_name);


  try
    {
      cursor = p.lookup<rdb::cursor>(server_name);
    }
  catch (ers::Issue& ex)
    {
      ers::fatal(daq::rdb::LookupFailed(ERS_HERE, server_name, partition_name, ex));
      return EXIT_FAILURE;
    }

  try
    {
      writer = p.lookup<rdb::writer>(writer_name);
    }
  catch (ers::Issue& ex)
    {
      ers::fatal(daq::rdb::LookupFailed(ERS_HERE, writer_name, partition_name, ex));
      return EXIT_FAILURE;
    }

  rdb::ProcessInfo proc_info;
  rdb::ProcessInfoHelper::set(proc_info);

  RDBWriterPingCB * ping_cb = new RDBWriterPingCB();


    // catch any CORBA communication exception

  try
    {
      // open new session
      std::cout << "TEST writer open_session() ";
      session = writer->open_session(proc_info, ping_cb->_this());
      ping_cb->set_session_id(session);
      test_passed();

      // create new database file
      std::cout << "TEST writer create_database(\"" << data_name << "\") ";
      writer->create_database(session, data_name.c_str());
      test_passed();

      // add schema file and load it
      std::cout << "TEST writer database_add_file(\"" << data_name << "\", \"" << schema_name << "\") ";
      writer->database_add_file(session, data_name.c_str(), schema_name.c_str());
      test_passed();

      // create items
      item1.create(data_name);
      item2.create(data_name);

      container1.create(data_name);
      container2.create(data_name);

      // commit
      std::cout << "TEST commit commit_changes() ";
      writer->commit_changes(session, "", "test commit");
      test_passed();

      Item<1> w1; w1.read(false, "item1");
      Item<2> w2; w2.read(false, "item2");
      Container<1,1,1> wc1 ; wc1.read(false, "container1");
      Container<2,2,2> wc2 ; wc2.read(false, "container2");

      item1.do_compare_test(w1, writer_name);
      item2.do_compare_test(w2, writer_name);
      container1.do_compare_test(wc1, writer_name);
      container2.do_compare_test(wc2, writer_name);

      // open database of read-only server
      std::cout << "TEST reader open_database(\"" << data_name << "\") ";
      rdb::RDBNameList files;
      files.length(1);
      files[0] = CORBA::string_dup(data_name.c_str());
      rdb::ProcessInfo info;
      rdb::ProcessInfoHelper::set(info);
      cursor->open_database(info, files);
      test_passed();

      Item<1> r1; r1.read(true, "item1");
      Item<2> r2; r2.read(true, "item2");
      Container<1,1,1> rc1 ; rc1.read(true, "container1");
      Container<2,2,2> rc2 ; rc2.read(true, "container2");

      item1.do_compare_test(r1, server_name);
      item2.do_compare_test(r2, server_name);
      container1.do_compare_test(rc1, server_name);
      container2.do_compare_test(rc2, server_name);

      // close session
      writer->close_session(session, proc_info);
    }
  catch (const rdb::SessionNotFound& ex)
    {
      test_failed();
      std::cerr << "FATAL: session " << ex.session_id << " is not found" << std::endl;
    }
  catch (const rdb::NotFound& ex)
    {
      test_failed();
      ers::fatal(*daq::idl2ers_issue(ex.issue));
    }
  catch (const rdb::CannotProceed& ex)
    {
      test_failed();
      ers::fatal(*daq::idl2ers_issue(ex.issue));
    }
  catch (const rdb_test::RequestFailed& ex)
    {
      test_failed();
      ers::fatal(ex);
    }
  catch (CORBA::SystemException& ex)
    {
      test_failed();
      ers::fatal(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
  catch (const std::exception& ex)
    {
      test_failed();
      std::cerr << "ERROR: " << ex.what() << std::endl;
    }
  return okcheck;
}
