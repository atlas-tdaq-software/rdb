#include <stdlib.h>
#include <iostream>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <ers2idl/ers2idl.h>

#include "rdb/rdb.hh"
#include "rdb/rdb_info.h"
#include "rdb/errors.h"

  /// Global variables

static rdb::writer_var cursor;        /// the 'cursor' is used for communication with server
static rdb::RDBSessionId session;     /// the 'session' is identifier for all communications with writer server


  /// Create text to report 'session not found' error

static const char * get_session_id_str()
{
  static std::string buf;

  if(buf.empty()) {
    std::ostringstream s;
    s << "session " << session << " is not found";
    buf = s.str();
  }

  return buf.c_str();
}


  /// Declare possible issues

ERS_DECLARE_ISSUE(
  rdb_tutorial,
  RequestFailed,
  "method \"" << method << "\" raised exception:\n" << reason,
  ((const char*)method)
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_tutorial,
  CorbaRequestFailed,
  "a method raised CORBA exception " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  rdb_tutorial,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

static void
set_attribute(const char * aname, const rdb::RDBObject& o, const rdb::DataUnion& d)
{
  try {
    cursor->set_attribute_value(session, o, aname, d);
  }
  catch ( rdb::SessionNotFound & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "set_attribute_value", get_session_id_str());
  }
  catch ( const rdb::NotFound & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "set_attribute_value", "", *daq::idl2ers_issue(ex.issue));
  }
  catch ( const rdb::CannotProceed & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "set_attribute_value", "", *daq::idl2ers_issue(ex.issue));
  }
}

static void
create_object(rdb::RDBObject& o, const char * where)
{
  try {
    cursor->create_object(session, o.classid, o.name, where);
  }
  catch ( rdb::SessionNotFound & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "create_object", get_session_id_str());
  }
  catch ( const rdb::CannotProceed & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "create_object", "", *daq::idl2ers_issue(ex.issue));
  }
}

static void
rename_object(rdb::RDBObject& o, const char * new_name)
{
  try {
    cursor->rename_object(session, o, new_name);
  }
  catch ( rdb::SessionNotFound & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "rename_object", get_session_id_str());
  }
  catch ( const rdb::CannotProceed & ex ) {
    throw rdb_tutorial::RequestFailed(ERS_HERE, "rename_object", "", *daq::idl2ers_issue(ex.issue));
  }
}

static rdb::RDBObject
create_department(const char * where, const char * id, const char * name)
{
    // make rdb object reference

  rdb::RDBObject o;
  o.classid = "Department";
  o.name    = id;


    // create object

  create_object(o, where);


    // set attribute 'Name'

  rdb::DataUnion d;
  d.du_string(name);
  set_attribute("Name", o, d);

  return o;
}

static rdb::RDBObject
create_employee(const char * where, const char * id, const char * name,
                const char * date_of_birth, const char * family_situation,
		unsigned long  salary)
{

    // make rdb object reference

  rdb::RDBObject o;
  o.classid = "Employee";
  o.name    = id;


    // create object

  create_object(o, where);


    // describe data (single attribute value)

  rdb::DataUnion d;


    // set attribute 'Name'

  d.du_string(name);
  set_attribute("Name", o, d);


    // set attribute 'Birthday'

  d.du_string(date_of_birth);
  set_attribute("Birthday", o, d);


    // set attribute 'Family Situation'

  d.du_string(family_situation);
  set_attribute("Family Situation", o, d);


    // set attribute 'Salary'

  d.du_u32_int(salary);
  set_attribute("Salary", o, d);

  return o;
}

void
hire(rdb::RDBObject & department, rdb::RDBObject & employee)
{
    // set staff

  {

      // get existing staff

    rdb::RDBObjectList_var olist;

    try {
      cursor->get_objects_of_relationship(session, department, "Staff", olist);
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "get_objects_of_relationship", get_session_id_str());
    }
    catch ( const rdb::NotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "get_objects_of_relationship", "", *daq::idl2ers_issue(ex.issue));
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "get_objects_of_relationship", "", *daq::idl2ers_issue(ex.issue));
    }


      // create new object in the staff list

    unsigned long idx = olist->length();
    olist->length(idx + 1);
    olist[idx].classid = CORBA::string_dup(employee.classid);
    olist[idx].name    = CORBA::string_dup(employee.name);


      // add employee to staff

    try {
      cursor->set_objects_of_relationship(session, department, "Staff", olist, false);
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship", get_session_id_str());
    }
    catch ( const rdb::NotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship","", *daq::idl2ers_issue(ex.issue));
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship", "", *daq::idl2ers_issue(ex.issue));
    }
  }
  
  
    // set employee's department

  {
    rdb::RDBObjectList olist;
    olist.length(1);
    olist[0].classid = CORBA::string_dup(department.classid);
    olist[0].name    = CORBA::string_dup(department.name);


      // set place of work

    try {
      cursor->set_objects_of_relationship(session, employee, "Works at", olist, false);
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship", get_session_id_str());
    }
    catch ( const rdb::NotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship", "", *daq::idl2ers_issue(ex.issue));
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "set_objects_of_relationship", "", *daq::idl2ers_issue(ex.issue));
    }
  }
}

int main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (const daq::ipc::Exception &ex)
    {
      ers::fatal(ex);
      return 1;
    }

  std::string partition_name(ipc::partition::default_name);
  std::string database_name;
  std::string schema_name;
  std::string data_name;

  try
    {
      boost::program_options::options_description desc("This program is an example using RDB server to modify database. It uses schema produced by the oks_tutorial application.");
      boost::program_options::variables_map vm;

      desc.add_options()
        ( "partition,p", boost::program_options::value<std::string>(&partition_name)->default_value(partition_name), "partition to work in" )
        ( "database,d", boost::program_options::value<std::string>(&database_name)->required(), "name of database server to work with" )
        ( "schema-file,S", boost::program_options::value<std::string>(&schema_name)->required(), "name of schema file on rdb-server" )
        ( "data-file,D", boost::program_options::value<std::string>(&data_name)->required(), "name of data file on rdb-server" )
        ( "help,h", "Print help message");

      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.find("help") != vm.end())
        {
          std::cout << desc << std::endl;
          return 0;
        }

      boost::program_options::notify(vm);
    }
  catch(const std::exception& ex)
    {
      ers::fatal( rdb_tutorial::BadCommandLine( ERS_HERE, ex.what() ) );
      return 1;
    }

  const char * df_name = data_name.c_str();


  IPCPartition p(partition_name);

  try {
    cursor = p.lookup<rdb::writer>(database_name);
  }
  catch( ers::Issue & ex) {
    ers::fatal(daq::rdb::LookupFailed(ERS_HERE, database_name, partition_name));
    return 1;
  }

  rdb::ProcessInfo proc_info;
  rdb::ProcessInfoHelper::set(proc_info);

  RDBWriterPingCB * ping_cb = new RDBWriterPingCB();


    // catch any CORBA communication exception

  try {

      // open new session

    try {
      session = cursor->open_session( proc_info, ping_cb->_this() );
      ping_cb->set_session_id(session);
    }
    catch ( CORBA::SystemException & ex ) {
      ers::fatal(rdb_tutorial::CorbaRequestFailed(ERS_HERE, ex._name()));
      return 1;
    }


      // create new database file

    try {
      cursor->create_database(session, df_name);
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "create_database", get_session_id_str());
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "create_database", "", *daq::idl2ers_issue(ex.issue));
    }


      // add schema file and load it

    try {
      cursor->database_add_file(session, df_name, schema_name.c_str());
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "database_add_file", get_session_id_str());
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "database_add_file", "", *daq::idl2ers_issue(ex.issue));
    }


      // create new 'Department' objects

    rdb::RDBObject ms_obj = create_department(df_name, "MS", "Microsoft");
    rdb::RDBObject tr_obj = create_department(df_name, "TR", "Transmeta Corp");


      // create new 'Employee' objects

    rdb::RDBObject bg_obj = create_employee(df_name, "BG", "Bill Gates",     "1955/10/28",  "Married", 0xFFFF0000);
    rdb::RDBObject pa_obj = create_employee(df_name, "PA", "Paul Allen",     "1955-Nov-28", "Married", 0xFFFE0000);
    rdb::RDBObject lt_obj = create_employee(df_name, "Lt", "Linus Torvalds", "1970-12-28",  "Married", 0x0000FFFF);


      // set links between departments and employees

    hire(ms_obj, bg_obj);
    hire(ms_obj, pa_obj);
    hire(tr_obj, lt_obj);


      // rename object

    rename_object(lt_obj, "LT");


      // commit changes

    try {
      cursor->commit_changes(session, "", "RDB tutorial");
    }
    catch ( rdb::SessionNotFound & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "commit_changes", get_session_id_str());
    }
    catch ( const rdb::CannotProceed & ex ) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "commit_changes", "", *daq::idl2ers_issue(ex.issue));
    }

      // close session

    try {
      cursor->close_session( session, proc_info );
    }
    catch(rdb::SessionNotFound& ex) {
      throw rdb_tutorial::RequestFailed(ERS_HERE, "close_session", get_session_id_str());
    }

  }


    // report any found problem and exit with bad status

  catch (rdb_tutorial::RequestFailed & ex) {
    ers::fatal(ex);
    return 1;
  }


    // report CORBA exception if any and exit with bad status

  catch (CORBA::SystemException & ex) {
    ers::fatal(rdb_tutorial::CorbaRequestFailed(ERS_HERE, ex._name()));
    return 2;
  }

  return 0;
}
