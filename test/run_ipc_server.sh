#!/bin/sh

#############################################################################

startup_timeout='10'

#############################################################################

  # run general ipc_server

echo 'START UP GENERAL IPC SERVER ...'
echo ' * execute "ipc_server"'

ipc_server > /dev/null &

count=1
result=1
while [ $count -le ${startup_timeout} ] ; do
  sleep 1
  test_ipc_server
  result=$?
  if [ $result -eq 0 ] ; then break ; fi
  echo " * waiting general ipc_server startup (${count} of ${startup_timeout}) ..."
  count=`expr $count + 1`
done

if [ ! $result -eq 0 ] ; then
  echo "ERROR [run_ipc_server.sh]: failed to run general ipc_server (timeout after $startup_timeout seconds)"
  exit 1
fi

echo ' * the server is running'
