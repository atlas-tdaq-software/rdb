#!/bin/sh

#################################################################
#
#	Check target script for the RDB package
#	Created by Igor Soloviev 23.02.2004
#
#################################################################

Cleanup () {
  echo 'destroy running ipc programs ...'
  echo "ipc_rm -i '.*' -n '.*' -f -t"
  ipc_rm -i '.*' -n '.*' -f -t || exit 1
}

#################################################################

ipc_file="/tmp/ipc_root.$$.ref"
TDAQ_IPC_INIT_REF="file:${ipc_file}"
export TDAQ_IPC_INIT_REF

cleanup='ipc_rm -i ".*" -n ".*" -f -t'

test_schema_file="/tmp/test.schema.$$.xml"
test_data_file="/tmp/test.data.$$.xml"
test_data_lock_file="/tmp/.oks_lock_test.data.$$.xml"
partition_db_name='daq/schema/core.schema.xml'
read_only_server_name='CheckRead'
read_write_server_name='CheckUpdate'
exit_status=0

#################################################################

# destroy any running ipc programs

trap " \
echo 'caught signal';\
echo 'remove generated files ...';\
rm -f ${test_schema_file} ${test_data_file} ${test_data_lock_file};\
echo 'destroy running ipc programs ...';\
ipc_rm -i '.*' -n '.*' -f;\
exit 1\
" 1 2 15

#################################################################

# run ipc_server

if ${2}/test/run_ipc_server.sh
then
  echo ''
else
  echo 'ERROR: can not start general ipc_server, exiting...'
  exit 1
fi

#################################################################

# run read-only rdb_server using online database repository

if ${2}/test/run_rdb_server.sh ${read_only_server_name} 'server' "${1}" -D ${partition_db_name}
then
  echo ''
else
  echo 'ERROR: can not start read-only rdb server, exiting...'
  exit 1
fi

#################################################################

# run c++ read check

cmd="${1}/rdb_dump -d ${read_only_server_name}"

echo 'CHECK C++ READ ACCESS ...'
echo " * execute \"${cmd}\""

if ${cmd}
then
  echo ''
  echo '*********************************'
  echo '*** RDB c++ read check passed ***'
  echo '*********************************'
  echo ''
else
  echo 'ERROR: RDB c++ read check failed'
  exit_status=100
fi

#################################################################

# generate oks files which are used by the rdb rw server

rm -f ${test_schema_file} ${test_data_file}
cmd="oks_tutorial ${test_schema_file} ${test_data_file}"

echo 'GENERATE FILES FOR READ-WRITE TEST ...'
echo " * execute \"${cmd}\""

${cmd} > /dev/null
if [ ! -f ${test_schema_file} ] ; then
  echo 'ERROR: oks_tutorial failed, can not produce schema file for test'
fi

rm -f ${test_data_file} || exit 1

echo ' * done'
echo ''

#################################################################

# run read-write rdb_server using oks_tutorial schema

if ${2}/test/run_rdb_server.sh ${read_write_server_name} 'writer' "${1}" -S ${test_schema_file}
then
  echo ''
else
  echo 'ERROR: can not start read-write rdb server, exiting...'
  exit 1
fi

#################################################################

# run c++ read-write check

cmd="${1}/rdb_tutorial -d ${read_write_server_name} -S ${test_schema_file} -D ${test_data_file}"

echo 'CHECK C++ READ-WRITE ACCESS ...'
echo " * execute \"${cmd}\""

if ${cmd}
then
  echo ' * generated file is:'
  cat ${test_data_file}

  echo ''
  echo '***************************************'
  echo '*** RDB c++ read-write check passed ***'
  echo '***************************************'
  echo ''
else
  echo 'ERROR: RDB c++ read-write check failed'
  exit_status=101
fi

#################################################################

ref_param=''

if test $TDAQ_IPC_INIT_REF
then
  ref_param="-Dtdaq.ipc.init.ref=$TDAQ_IPC_INIT_REF"
fi

#################################################################

TDAQ_ERS_NO_SIGNAL_HANDLERS=1
export TDAQ_ERS_NO_SIGNAL_HANDLERS

# run java check (1)

echo 'CHECK JAVA READ ACCESS (rdb_server) ...'

echo ""
if $TDAQ_JAVA_HOME/bin/java -classpath $1/test_rdb.jar:$1/rdb.jar:$CLASSPATH $ref_param test_rdb.Test ${read_only_server_name}
then
  echo ''
  echo '***********************************************'
  echo '*** RDB java read check passed (rdb_server) ***'
  echo '***********************************************'
  echo ''
else
  echo 'ERROR: RDB java read check failed (rdb_server)'
  exit_status=102
fi

#################################################################

# run java check (2)

echo 'CHECK JAVA READ ACCESS (rdb_writer) ...'

echo ""
if $TDAQ_JAVA_HOME/bin/java -classpath $1/test_rdb.jar:$1/rdb.jar:$CLASSPATH $ref_param test_rdb.Test ${read_write_server_name}
then
  echo ''
  echo '***********************************************'
  echo '*** RDB java read check passed (rdb_writer) ***'
  echo '***********************************************'
  echo ''
else
  echo 'ERROR: RDB java read check failed (rdb_writer)'
  exit_status=102
fi

#################################################################

echo 'STOP RUNNING RDB SERVERS ...'

cmd="${1}/rdb_admin -d ${read_only_server_name} --shutdown"
echo " * execute \"${cmd}\""
${cmd} > /dev/null

cmd="${1}/rdb_admin -d ${read_write_server_name} --shutdown"
echo " * execute \"${cmd}\""
${cmd} > /dev/null

sleep 2

echo 'done'
echo ''

#################################################################

# clean ipc

echo 'DESTROY RUNNING IPC PROGRAMS (IF ANY) ...'

Cleanup
echo ' * done'
echo ''

rm -f ${test_schema_file} ${test_data_file} ${test_data_lock_file} ${ipc_file}

#################################################################

# exit

echo ''
echo '********************************'
echo "*** DONE, exit with status $exit_status ***"
echo '********************************'
echo ''

exit $exit_status

#################################################################
