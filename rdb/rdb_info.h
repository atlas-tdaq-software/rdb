#ifndef RDB_INFO_H
#define RDB_INFO_H

#include <set>
#include <string>

#include <ipc/object.h>

#include "rdb/errors.h"
#include "rdb/rdb.hh"

namespace rdb {

  class ProcessInfoHelper {

    public:
      static const std::string& get_user_id() noexcept  { init(); return m_user_id;    }
      static const std::string& get_host() noexcept     { init(); return m_host;       }
      static long get_process_id() noexcept             { init(); return m_process_id; }
      static const std::string& get_app_name() noexcept { init(); return m_app_name;   }
      static void set_app_name(const char * v)          { if(v) m_app_name = v; }

      static void set(rdb::ProcessInfo& info);
      static void refresh(rdb::ProcessInfo& info);
      static rdb::ProcessInfo get();

    private:
      static void init() noexcept;

      static std::string m_user_id;
      static std::string m_host;
      static long m_process_id;
      static std::string m_app_name;

  };
}

std::ostream& operator<<(std::ostream& s, const rdb::ProcessInfo& info);


class RDBCallbackInfo : public ::IPCObject<POA_rdb::callback>
{
  friend class RDBInfoReceiver;

  public:

    typedef void (*Callback) ( RDBCallbackInfo * );

    const rdb::RDBClassChangesList * get_changes() const {return m_changes;}
    int64_t get_parameter() const {return m_parameter;}

  private:

    RDBCallbackInfo( const RDBCallbackInfo & );
    RDBCallbackInfo& operator= ( const RDBCallbackInfo & );

    RDBCallbackInfo( Callback , int64_t );

    CORBA::Long inform (const rdb::RDBClassChangesList&, CORBA::LongLong);
    void server_exits(CORBA::LongLong);

    const rdb::RDBClassChangesList * m_changes;
    Callback m_callback;
    int64_t m_parameter;

};


  /**
   *  \brief This class provides a methods to subscribe on notification about changes on RDB server.
   *
   *  To subscribe on changes the user has to create an object of the RDBInfoReceiver class
   *  and to provide subscription criteria via subscribe() method.
   */

class RDBInfoReceiver
{
  friend class RDBCallbackInfo;

  public:

      /**
       *  \brief Create new Info Receiver object to get notification on changes from RDB server.
       *
       *  In constructor has the following parameters:
       *    \param p               IPC partition in which the RDB server is running
       *    \param server_name     name of the RDB server
       *    \param session_id      optional parameter used by RDB writer
       *    \param app_name        optional parameter to report name of client application
       *
       *  \throw Never throws exceptions.
       */

    RDBInfoReceiver(const IPCPartition & p, const char * server_name, rdb::RDBSessionId session_id = 0, const char * app_name = 0) noexcept;


      /**
       *  \brief Destroys Info Receiver object.
       *
       *  \throw Never throws exceptions.
       */

    ~RDBInfoReceiver() noexcept { ; }


      /**
       *  \brief Subscribe on database changes.
       *
       *  The method subscribes on changes. When a class or an object matching subscription criteria is changed,
       *  the method calls users callback function, passes to it the users parameter and details of changes.
       *
       *  In case of problems the method throws \b rdb::Exception or \b daq::ipc::CorbaSystemException exception.
       *
       *  In method has the following parameters:
       *    \param class_names          names of classes for subscription criteria
       *    \param look_in_subclasses   if true, add to the criteria subclasses from above set
       *    \param objects              identities of objects for subscription criteria
       *    \param cb                   user-defined callback to be called for notification about changes
       *    \param param                optional parameter for above callback function
       */

    void subscribe(const std::set<std::string>& class_names, bool look_in_subclasses, const std::map<std::string, std::set<std::string>>& objects, RDBCallbackInfo::Callback cb, int64_t param = 0);


      /**
       *  \brief Remove subscription.
       *
       *  The method unsubscribes on changes done using subscribe() method.
       *
       *  In case of problems the method throws \b rdb::Exception or daq::ipc::CorbaSystemException exception.
       */

    void unsubscribe();


      /**
       *  \brief Return subscription ID.
       *
       *  The method returns subscription ID, used for registration on RDB server.
       *
       *  \return the RDB server subscription ID
       *  \throw Never throws exceptions.
       */

    long get_id() const noexcept {return m_id;}


  private:

    RDBInfoReceiver( const RDBInfoReceiver & );
    RDBInfoReceiver& operator= ( const RDBInfoReceiver & );

    void init(); /// \throw daq::rdb::LookupFailed if failed
    void clear_cb_info() noexcept { m_cb_info->_destroy(true); m_cb_info = 0; }

    IPCPartition m_partition;
    std::string m_server_name;
    rdb::cursor_var m_read_cursor;
    rdb::writer_var m_write_cursor;
    RDBCallbackInfo * m_cb_info;
    long m_id;
    rdb::RDBSessionId m_session_id;

};

inline RDBCallbackInfo::RDBCallbackInfo(RDBCallbackInfo::Callback cb, int64_t p ) :
  m_changes    (0),
  m_callback   (cb),
  m_parameter  (p)
{
}

class RDBWriterPingCB : public IPCObject<POA_rdb::ping>
{

  public:

    RDBWriterPingCB() : m_id(0) { ; }
    void set_session_id(rdb::RDBSessionId id) { m_id = id; }

    rdb::RDBSessionId get_session_id() { return m_id; }


  private:

    RDBWriterPingCB( const RDBWriterPingCB & );
    RDBWriterPingCB& operator= ( const RDBWriterPingCB & );

    rdb::RDBSessionId m_id;

};

#endif
