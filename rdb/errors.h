#ifndef RDB_ERRORS_H
#define RDB_ERRORS_H

#include <stdint.h>
#include <ers/ers.h>

namespace daq {

    /**
     *  \class rdb::Exception
     *  This is a base class for all daq::rdb exceptions.
     */

  ERS_DECLARE_ISSUE( rdb, Exception, , )



    /**
     *  \class rdb::SessionNotFound
     *  Cannot find writer session by ID
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    SessionNotFound,
    rdb::Exception,
    "session " << id << " was not found",
    ,
    ((int64_t)id)
  )


  /**
   *  \class rdb::ClassNotFound
   *  Cannot find writer session by ID
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ClassNotFound,
    rdb::Exception,
    "no class \'" << name << '\'',
    ,
    ((const char *)name)
  )

  /**
   *  \class rdb::AttributeNotFound
   *  Cannot find writer session by ID
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    AttributeNotFound,
    rdb::Exception,
    "no attribute \'" << name << "\' in class \'" << class_name << '\'',
    ,
    ((const char *)name)
    ((const char *)class_name)
  )

  /**
   *  \class rdb::RelationshipNotFound
   *  Cannot find writer session by ID
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    RelationshipNotFound,
    rdb::Exception,
    "no relationship \'" << name << "\' in class \'" << class_name << '\'',
    ,
    ((const char *)name)
    ((const char *)class_name)
  )

  /**
   *  \class rdb::ObjectNotFound
   *  Cannot find writer session by ID
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ObjectNotFound,
    rdb::Exception,
    "no object \'" << name << "\' in class \'" << class_name << '\'' << (look_in_subclasses ? " and its subclasses" : ""),
    ,
    ((const char *)name)
    ((const char *)class_name)
    ((bool)look_in_subclasses)
  )

    /**
     *  \class rdb::NotFoundSubscriptionException
     *  Cannot unsibscribe client: id is not found.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    NotFoundSubscriptionException,
    rdb::Exception,
    "subscription " << id << " was not found",
    ,
    ((long)id)
  )

    /**
     *  \class rdb::LookupFailed
     *  Cannot find RDB server exception.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    LookupFailed,
    rdb::Exception,
    "lookup failed for RDB server \'" << server_name << '@' << partition_name << '\'',
    ,
    ((std::string)server_name)
    ((std::string)partition_name)
  )


    /**
     *  \class rdb::NotificationFailed
     *  Cannot notify RDB server subscriber.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    NotificationFailed,
    rdb::Exception,
    "failed to notify subscriber " << id << ": \' application \'" << name << "\' subscribed on \'" << creation_ts
      << "\' started by \'" << user << "\' on \'" << host << "\' with pid " << pid << " is not responding",
    ,
    ((long)id)
    ((std::string)name)
    ((std::string)creation_ts)
    ((std::string)user)
    ((std::string)host)
    ((long)pid)
  )

    /**
     *  \class rdb::SessionNotificationFailed
     *  Cannot notify RDB writer session subscriber.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    SessionNotificationFailed,
    rdb::Exception,
    "failed to notify subscriber of session " << id << ": \' application \'" << name << "\' subscribed on \'" << creation_ts
      << "\' started by \'" << user << "\' on \'" << host << "\' with pid " << pid << " is not responding",
    ,
    ((int64_t)id)
    ((std::string)name)
    ((std::string)creation_ts)
    ((std::string)user)
    ((std::string)host)
    ((long)pid)
  )


    /**
     *  \class rdb::ServerNotificationFailed
     *  Cannot notify RDB server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ServerNotificationFailed,
    rdb::Exception,
    "failed to notify server \'" << name << "\': " << reason,
    ,
    ((const char *)name)
    ((const char *)reason)
  )


    /**
     *  \class rdb::SessionPingFailed
     *  Cannot ping session.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    SessionPingFailed,
    rdb::Exception,
    "failed to ping session " << session_id << " (application: \'" << app_name << "\' run by \'" << user_id << "\' on \'" << host << "\' with pid " << pid << "), " << action,
    ,
    ((int64_t)session_id)
    ((std::string)app_name)
    ((std::string)user_id)
    ((std::string)host)
    ((long)pid)
    ((const char *)action)
  )

    /**
     *  \class rdb::AlreadyRunning
     *  Failed to run rdb server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    AlreadyRunning,
    rdb::Exception,
    "rdb server \'" << server_name << '@' << (partition_name ? partition_name : "initial") << "\' is already running",
    ,
    ((const char*)server_name)
    ((const char*)partition_name)
  )


    /**
     *  \class rdb::CannotPublish
     *  Failed to publish rdb server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotPublish,
    rdb::Exception,
    "cannot publish rdb server object \'" << server_name << '@' << (partition_name ? partition_name : "initial") << '\'',
    ,
    ((const char*)server_name)
    ((const char*)partition_name)
  )


    /**
     *  \class rdb::CannotUnpublish
     *  Failed to publish rdb server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotUnpublish,
    rdb::Exception,
    "cannot unpublish rdb server object \'" << server_name << '@' << partition_name << '\'',
    ,
    ((std::string)server_name)
    ((std::string)partition_name)
  )

    /**
     *  \class rdb::CannotSubscribe
     *  User subscription failed.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotSubscribe,
    rdb::Exception,
    "subscribe failed",
    ,
  )

    /**
     *  \class rdb::CannotUnsubscribe
     *  Failed to unsubscribe client.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotUnsubscribe,
    rdb::Exception,
    "unsubscribe failed",
    ,
  )


    /**
     *  \class rdb::CannotUnsubscribeServer
     *  Failed to unsubscribe server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotUnsubscribeServer,
    rdb::Exception,
    "cannot remove \'" << this_name << "\' server subscription from master server \'" << server_name << "\' in partition \'" << partition_name << '\'',
    ,
    ((std::string)this_name)
    ((std::string)server_name)
    ((std::string)partition_name)
  )

  /**
   *  \class rdb::UserRequestFailed
   *  User request on RDB server has failed.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    UserRequestFailed,
    rdb::Exception,
    "request on rdb server \'" << server_name << '@' << partition_name << "\' has failed",
    ,
    ((std::string)server_name)
    ((std::string)partition_name)
  )


  /**
   *  \class rdb::NoAccessManagerPermission
   *  No Access Manager permission granted.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    NoAccessManagerPermission,
    rdb::Exception,
    "no permission granted by Access Manager to perform \"" << action << "\" action by user " << user,
    ,
    ((std::string)user)
    ((std::string)action)
  )


  /**
   *  \class rdb::DatabaseOpenFailed
   *  Failed to open database.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    DatabaseOpenFailed,
    rdb::Exception,
    "cannot open database",
    ,
  )


  /**
   *  \class rdb::DatabaseCloseFailed
   *  Failed to close database.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    DatabaseCloseFailed,
    rdb::Exception,
    "cannot close database" << reason,
    ,
    ((std::string)reason)
  )


  /**
   *  \class rdb::DatabaseRealoadFailed
   *  Failed to reload database.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    DatabaseReloadFailed,
    rdb::Exception,
    "cannot reload database" << reason,
    ,
    ((std::string)reason)
  )


  /**
   *  \class rdb::SchemaReloadNotAllowed
   *  Schema file reload is not allowed.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    SchemaReloadNotAllowed,
    rdb::Exception,
    "reload of a schema file (\'" << file << "\') in not allowed.\nPartition infrastructure needs to be restarted.",
    ,
    ((std::string)file)
  )


    /**
     *  \class rdb::ReloadCommandFailed
     *  Failed to run reload rdb server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ReloadCommandFailed,
    rdb::Exception,
    "cannot reload rdb server \'" << server_name << '@' << partition_name << "\': " << reason,
    ,
    ((std::string)server_name)
    ((std::string)partition_name)
    ((std::string)reason)
  )


    /**
     *  \class rdb::PushCommandFailed
     *  Failed to push rdb server data.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    PushCommandFailed,
    rdb::Exception,
    "cannot push data on rdb server \'" << server_name << '@' << partition_name << '\'',
    ,
    ((const char*)server_name)
    ((const char*)partition_name)
  )


    /**
     *  \class rdb::GetAllClassesCommandFailed
     *  Failed to run reload rdb server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    GetAllClassesCommandFailed,
    rdb::Exception,
    "cannot get description of classes from rdb server \'" << server_name << '@' << partition_name << '\'',
    ,
    ((const char*)server_name)
    ((const char*)partition_name)
  )

  /**
   *  \class rdb::FailedRemoveFile
   *  Failed to remove temporal oks database file.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    NoDataFile,
    rdb::Exception,
    "data file \"" << file << "\" is not found",
    ,
    ((std::string)file)
  )

  /**
   *  \class rdb::FailedRemoveFile
   *  Failed to remove temporal oks database file.
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedLockDataFile,
    rdb::Exception,
    "failed to lock data file \"" << file << "\": " << why,
    ,
    ((std::string)file)
    ((std::string)why)
 )

  /**
   *  \class rdb::FailedAddIncludeFile
   *  Failed to add include file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedAddIncludeFile,
    rdb::Exception,
    "failed add include \"" << include << "\" to file \"" << file << "\" in session " << id,
    ,
    ((std::string)include)
    ((std::string)file)
    ((int64_t)id)
  )


  /**
   *  \class rdb::FailedRemoveIncludeFile
   *  Failed to remove include file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedRemoveIncludeFile,
    rdb::Exception,
    "failed remove include \"" << include << "\" to file \"" << file << "\" in session " << id,
    ,
    ((std::string)include)
    ((std::string)file)
    ((int64_t)id)
  )

  struct PrintNonZeroSession
  {
    int64_t m_id;

    PrintNonZeroSession(int64_t id) :
        m_id(id)
    {
      ;
    }
  };

  inline std::ostream&
  operator<<(std::ostream& s, PrintNonZeroSession x)
  {
    if (x.m_id > 0)
      s << " in session " << x.m_id;

    return s;
  }

  /**
   *  \class rdb::FailedCheckFileStatus
   *  Failed to check status of file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedCheckFileStatus,
    rdb::Exception,
    "failed to check access status of file \"" << file << '\"' << PrintNonZeroSession(id),
    ,
    ((std::string)file)
    ((int64_t)id)
  )


  /**
   *  \class rdb::FailedUpdateFileStatus
   *  Failed to update file status
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedUpdateFileStatus,
    rdb::Exception,
    "failed to update status of file \"" << file << '\"' << PrintNonZeroSession(id),
    ,
    ((std::string)file)
    ((int64_t)id)
  )

  /**
   *  \class rdb::FailedGetFileIncludes
   *  Failed to get includes
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedGetFileIncludes,
    rdb::Exception,
    "failed to get includes of file \"" << file << '\"' << PrintNonZeroSession(id),
    ,
    ((std::string)file)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotCreateObject
   *  Failed to remove include file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotCreateObject,
    rdb::Exception,
    "cannot create object \"" << oid << '@' << cid << "\" on file \"" << file << "\" in session " << id,
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((std::string)file)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotDeleteObject
   *  Failed to remove include file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotDeleteObject,
    rdb::Exception,
    "cannot delete object \"" << oid << '@' << cid << "\" in session " << id,
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotReadAttribute
   *  Failed to read attribute value
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotReadAttribute,
    rdb::Exception,
    "cannot read attribute \"" << attribute << "\" of object \"" << oid << '@' << cid << '\"' << PrintNonZeroSession(id),
    ,
    ((const char *)attribute)
    ((const char *)oid)
    ((const char *)cid)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotMoveObject
   *  Failed to remove include file
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotMoveObject,
    rdb::Exception,
    "cannot move object \"" << oid << '@' << cid << "\" on file \"" << file << "\" in session " << id,
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((std::string)file)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotRenameObject
   *  Failed to rename object
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotRenameObject,
    rdb::Exception,
    "cannot set object \"" << oid << '@' << cid << "\" id = \"" << new_id << "\" in session " << id,
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((std::string)new_id)
    ((int64_t)id)
  )

  /**
   *  \class rdb::CannotUpdateObject
   *  Failed to rename object
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotUpdateObject,
    rdb::Exception,
    "cannot update object \"" << oid << '@' << cid << "\" in session " << id,
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((int64_t)id)
  )

  /**
   *  \class rdb::ObjectAlreadyExist
   *  Cannot create an object since such object already exists
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ObjectAlreadyExist,
    rdb::Exception,
    "object \"" << oid << '@' << cid << "\" already exist in file \"" << file << '\"',
    ,
    ((const char *)oid)
    ((const char *)cid)
    ((std::string)file)
  )

  /**
   *  \class rdb::ClassIsAbstract
   *  Class is abstract
   */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    ClassIsAbstract,
    rdb::Exception,
    "class \"" << cid << "\" is abstract",
    ,
    ((const char *)cid)
  )
    /**
     *  \class rdb::FailedCreateData
     *  Failed to load oks database file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedCreateData,
    rdb::Exception,
    "internal error during creation of OKS data:\n" << why,
    ,
    ((const char*)why)
  )

    /**
     *  \class rdb::FailedLoadDatabaseFile
     *  Failed to load oks database file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedLoadDatabaseFile,
    rdb::Exception,
    "failed to load file:\n" << why,
    ,
    ((const char*)why)
  )


    /**
     *  \class rdb::FailedRemoveFile
     *  Failed to remove temporal oks database file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedRemoveFile,
    rdb::Exception,
    "failed to remove temporal file: " << why,
    ,
    ((const char*)why)
  )


    /**
     *  \class rdb::FailedReadDB
     *  Failed to read database from master RDB server.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedReadDB,
    rdb::Exception,
    "failed to read database from \'" << server_name << '@' << (partition_name ? partition_name : "initial") << "\' " << reason,
    ,
    ((const char*)server_name)
    ((const char*)partition_name)
    ((const char*)reason)
  )


    /**
     *  \class rdb::BadBackupFile
     *  Failed to load RDB server backup file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    BadBackupFile,
    rdb::Exception,
    "failed lo load backup file \'" << file_name << "\': " << reason,
    ,
    ((const char*)file_name)
    ((const char*)reason)
  )


    /**
     *  \class rdb::FailedCreateBackupFile
     *  Failed to create RDB server backup file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    FailedCreateBackupFile,
    rdb::Exception,
    "failed lo create backup file \'" << file_name << '\'',
    ,
    ((const char*)file_name)
  )


    /**
     *  \class rdb::CannotCreateDirectory
     *  Failed to create directory for RDB server backup file.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    CannotCreateDirectory,
    rdb::Exception,
    "Backup directory '" << directory << "' can not be created",
    ,
    ((std::string)directory)
  )


    /**
     *  \class rdb::BadBackupFile
     *  Failed to apply RDB server backup data.
     */

  ERS_DECLARE_ISSUE_BASE(
    rdb,
    BadBackupData,
    rdb::Exception,
    "restore from backup file \'" << file_name << "\' failed: " << reason,
    ,
    ((const char*)file_name)
    ((const char*)reason)
  )


}  // close namespace daq

#endif
