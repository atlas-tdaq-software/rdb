////////////////////////////////////////////////////////////////////////////
//
//      rdb/proxy/proxy.cpp
//
//      Implementation of the RDB proxy library
//
//      Igor Soloviev 2012
//
//      description:
//              This file contains inplementation of the RDB service proxy
//              caching results of the following RDB server calls:
//              - get_inheritance_hierarchy()
//              - xget_object()
//
////////////////////////////////////////////////////////////////////////////

#include <tbb/spin_rw_mutex.h>

#include <rdb/rdb.hh>
#include <ipc/proxy/factory.h>

#include "src/common_utils.h"


template <class T>
class RDBProxy : public POA_rdb::cursor_tie<T>
{
  public:

    RDBProxy( T* t ) :
      POA_rdb::cursor_tie<T>             ( t ),
      m_validity_key                     ( 0 ),
      m_cache_inheritance_hierarchy_list ( 0 ),
      m_cache_object_id_map              ( 0 ),
      m_cache_query_map                  ( 0 )
    {
      ERS_LOG( "create RDB proxy object " << (void *)this );
    }

    ~RDBProxy()
    {
      ERS_LOG( "destroy RDB proxy object " << (void *)this );
      clean_cache();
    }

    void get_inheritance_hierarchy_from_cache (rdb::RDBInheritanceHierarchyList_out& l)
    {
      l = new rdb::RDBInheritanceHierarchyList( *m_cache_inheritance_hierarchy_list );
    }

    ::CORBA::Long get_inheritance_hierarchy(rdb::RDBInheritanceHierarchyList_out l)
    {

      CORBA::Long key(-1); // set invalid key

        // try to get inheritance hierarchy from cache, if the key is valid
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, false);

        if(m_cache_inheritance_hierarchy_list) {
	  key = POA_rdb::cursor_tie<T>::get_validity_key();
          if(key == m_validity_key) {
            get_inheritance_hierarchy_from_cache (l);
            return key;
	  }
        }
      }

        // clean cache if key is invalid
        // put inheritance hierarchy to cache
        // get inheritance hierarchy from cache
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, true);

          // check if cache expired

        if(key != -1 && key != m_validity_key) {
	  clean_cache();
	}

        if(!m_cache_inheritance_hierarchy_list) {
	  rdb::RDBInheritanceHierarchyList_var lv(0);
	  m_validity_key = POA_rdb::cursor_tie<T>::get_inheritance_hierarchy(lv);
	  m_cache_inheritance_hierarchy_list = lv._retn();
	  ERS_LOG( "read inheritance hierarchy from server (proxy: " << (void *)this << ", key: " << m_validity_key << ')' );
	}

        get_inheritance_hierarchy_from_cache (l);
        return m_validity_key;
      }
    }


    rdb::RDBObject * get_object_id_from_cache (
      rdb::ObjectIDCache::iterator& i,
      const char * object_id,
      rdb::RDBClassValueList_out values)
    {
      values = new rdb::RDBClassValueList( *i->second.second );
      return rdb::make_rdb_object(i->second.first, object_id);
    }


    ::CORBA::Long xget_object(
      const char* cid,
      ::CORBA::Boolean lis,
      const char* oid,
      rdb::RDBObject_out o,
      ::CORBA::Long recursion_depth,
      const rdb::RDBNameList& rcls,
      rdb::RDBClassValueList_out values)
    {

      CORBA::Long key(-1); // set invalid key

      std::string qkey = rdb::object_id_params_to_string(cid, lis, oid, recursion_depth, rcls);

        // try to get object from cache, if the key is valid
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, false);

        if(m_cache_object_id_map) {
          rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(qkey);
	  if(i != m_cache_object_id_map->end()) {
	    key = POA_rdb::cursor_tie<T>::get_validity_key();
            if(key == m_validity_key) {
              o = get_object_id_from_cache (i, oid, values);
              return m_validity_key;
	    }
	  }
	}
      }

        // clean cache if key is invalid
        // put x-object to cache ; get x-object from cache
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, true);

          // check if cache expired

        if(key != -1 && key != m_validity_key) {
	  clean_cache();
	}

	if(!m_cache_object_id_map) m_cache_object_id_map = new rdb::ObjectIDCache();

	rdb::ObjectIDCache::iterator i = m_cache_object_id_map->find(qkey);
	if(i == m_cache_object_id_map->end()) {
	  rdb::RDBObject_var obj(0);
          rdb::RDBClassValueList_var v(0);
          m_validity_key = POA_rdb::cursor_tie<T>::xget_object(cid, lis, oid, obj, recursion_depth, rcls, v);
	  i = m_cache_object_id_map->insert( std::make_pair(qkey, std::make_pair(static_cast<const char *>(obj->classid), v._retn()) ) ).first;
	  ERS_LOG( "read object " << qkey << " from server (proxy: " << (void *)this << ", key: " << m_validity_key << ')' );
	}

        o = get_object_id_from_cache (i, oid, values);
        return m_validity_key;
      }

    }

    void get_query_from_cache (
      rdb::QueryCache::iterator& i,
      rdb::RDBObjectList_out rlist,
      rdb::RDBClassValueList_out values)
    {
      rlist = new rdb::RDBObjectList(*i->second.first);
      values = new rdb::RDBClassValueList(*i->second.second);
    }

    ::CORBA::Long xget_all_objects(
      const char* cid,
      ::CORBA::Boolean lis,
      rdb::RDBObjectList_out l,
      ::CORBA::Long recursion_depth,
      const rdb::RDBNameList& rcls,
      rdb::RDBClassValueList_out values)
    {

      CORBA::Long key(-1); // set invalid key

      const char * query = (lis ? "*" : "T");
      std::string qkey = rdb::query_params_to_string(cid, query, recursion_depth, rcls);

        // try to get query result from cache, if the key is valid
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, false);

        if(m_cache_query_map) {
          rdb::QueryCache::iterator i = m_cache_query_map->find(qkey);
	  if(i != m_cache_query_map->end()) {
	    key = POA_rdb::cursor_tie<T>::get_validity_key();
            if(key == m_validity_key) {
              get_query_from_cache (i, l, values);
              return m_validity_key;
	    }
	  }
	}
      }


        // clean cache if key is invalid
        // put query result to cache and get it back
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, true);

          // check if cache expired

        if(key != -1 && key != m_validity_key) {
	  clean_cache();
	}

	if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

	rdb::QueryCache::iterator i = m_cache_query_map->find(qkey);
	if(i == m_cache_query_map->end()) {
	  rdb::RDBObjectList_var o(0);
          rdb::RDBClassValueList_var v(0);
          m_validity_key = POA_rdb::cursor_tie<T>::xget_all_objects(cid, lis, o, recursion_depth, rcls, v);
	  i = m_cache_query_map->insert( std::make_pair(qkey, std::make_pair(o._retn(),v._retn()) ) ).first;
	  ERS_LOG( "read all objects " << qkey << " from server (proxy: " << (void *)this << ", key: " << m_validity_key << ')' );
	}

        get_query_from_cache (i, l, values);
        return m_validity_key;
      }

    }

    ::CORBA::Long xget_objects_by_query(const char* cid, const char* query, rdb::RDBObjectList_out l, ::CORBA::Long recursion_depth, const rdb::RDBNameList& rcls, rdb::RDBClassValueList_out values) {

      CORBA::Long key(-1); // set invalid key

      std::string qkey = rdb::query_params_to_string(cid, query, recursion_depth, rcls);

        // try to get query result from cache, if the key is valid
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, false);

        if(m_cache_query_map) {
          rdb::QueryCache::iterator i = m_cache_query_map->find(qkey);
	  if(i != m_cache_query_map->end()) {
	    key = POA_rdb::cursor_tie<T>::get_validity_key();
            if(key == m_validity_key) {
              get_query_from_cache (i, l, values);
              return m_validity_key;
	    }
	  }
	}
      }


        // clean cache if key is invalid
        // put query result to cache and get it back
      {
        tbb::spin_rw_mutex::scoped_lock lock(m_cache_mutex, true);

          // check if cache expired

        if(key != -1 && key != m_validity_key) {
	  clean_cache();
	}

	if(!m_cache_query_map) m_cache_query_map = new rdb::QueryCache();

	rdb::QueryCache::iterator i = m_cache_query_map->find(qkey);
	if(i == m_cache_query_map->end()) {
	  rdb::RDBObjectList_var o(0);
          rdb::RDBClassValueList_var v(0);
          m_validity_key = POA_rdb::cursor_tie<T>::xget_objects_by_query(cid, query, o, recursion_depth, rcls, v);
	  i = m_cache_query_map->insert( std::make_pair(qkey, std::make_pair(o._retn(),v._retn()) ) ).first;
	  ERS_LOG( "read query " << qkey << " from server (proxy: " << (void *)this << ", key: " << m_validity_key << ')' );
	}

        get_query_from_cache (i, l, values);
        return m_validity_key;
      }
    }


  private:

    void clean_cache() {
      if(m_cache_inheritance_hierarchy_list) {
        delete m_cache_inheritance_hierarchy_list;
        m_cache_inheritance_hierarchy_list = 0;
      }

      if(m_cache_object_id_map) {
        rdb::destroy(m_cache_object_id_map);
      }

      if(m_cache_query_map) {
        rdb::destroy(m_cache_query_map);
      }
    }


  private:

    CORBA::Long m_validity_key;

    tbb::spin_rw_mutex m_cache_mutex;
    rdb::RDBInheritanceHierarchyList * m_cache_inheritance_hierarchy_list;
    rdb::ObjectIDCache * m_cache_object_id_map;
    rdb::QueryCache * m_cache_query_map;

};


namespace
{
  IPCProxyFactory<rdb::cursor, POA_rdb::cursor, RDBProxy> __f1__;
}
