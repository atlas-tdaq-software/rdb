package test_rdb;

import ipc.*;
import rdb.*;
 
public class Test {

  public static void main(String args[]) {

    ipc.Partition p = new ipc.Partition();

    rdb.cursor read_cursor = null;

    try {
      read_cursor = (rdb.cursor)p.lookup( rdb.cursor.class, args[0] );
    }
    catch(final ers.Issue ex) {
      ;
    }

    rdb.writer write_cursor = null;
    long write_session_id = 0;
    rdb.WriterPing cb = null;
    rdb.ProcessInfo proc_info = null;

    if ( read_cursor == null ) {
      try {
        write_cursor = (rdb.writer)p.lookup( rdb.writer.class, args[0] );
      }
      catch(final ers.Issue ex) {
        ;
      }

      if(write_cursor == null) {
        ers.Logger.fatal(new ers.Issue("ERROR: can not find db server"));
        System.exit(1);
      }
      else {
        try {
          cb = new rdb.WriterPing();
          proc_info = rdb.CreateProcessInfo.get(null);
          write_session_id = write_cursor.open_session(proc_info, cb._this( ipc.Core.getORB() ));
          cb.set_session_id(write_session_id);
        }
        catch(final org.omg.CORBA.SystemException ex) {
          ers.Logger.fatal(new rdb.CorbaSystemException(ex));
          System.exit(1);
        }
        catch(final rdb.CannotProceed ex) {
          ers.Logger.fatal(new rdb.CannotProceedException(ex));
          System.exit(1);
        }
        catch(final daq.tokens.AcquireTokenException ex) {
          ers.Logger.fatal(ex);
          System.exit(1);
        }
      }
    }

    rdb.RDBClassListHolder list = new rdb.RDBClassListHolder();

    try {
      if(read_cursor != null) {
        read_cursor.get_all_classes( list );
      }
      else {
        write_cursor.get_all_classes( write_session_id, list );
      }
    }
    catch(final rdb.SessionNotFound ex) {
      ers.Logger.fatal(new rdb.SessionNotFoundException(ex));
      System.exit(1);
    }
    catch(final org.omg.CORBA.SystemException ex) {
      ers.Logger.fatal(new rdb.CorbaSystemException(ex));
      System.exit(1);
    }

    System.out.println( "\"" + args[0] + "\" database schema :");

    for ( int i = 0; i < list.value.length; i++ ) {
      String class_name = list.value[i].name;
      System.out.println( "  class " + class_name );
      
      try {
        rdb.RDBAttributeListHolder attrs = new rdb.RDBAttributeListHolder();
        rdb.RDBRelationshipListHolder rels = new rdb.RDBRelationshipListHolder();

        if(read_cursor != null) {
          read_cursor.get_attributes( class_name, attrs );
          read_cursor.get_relationships( class_name, rels );
       }
        else {
          write_cursor.get_attributes( write_session_id, class_name, attrs );
          write_cursor.get_relationships( write_session_id, class_name, rels );
        }

        for ( int j = 0; j < attrs.value.length; j++ )
          System.out.println( "    - " + attrs.value[j].name );

        for ( int j = 0; j < rels.value.length; j++ )
          System.out.println( "    + " + rels.value[j].name );
      }
      catch(final rdb.NotFound ex) {
        ers.Logger.fatal(new rdb.NotFoundException(ex));
        System.exit(1);
      }
      catch(final rdb.SessionNotFound ex) {
        ers.Logger.fatal(new rdb.SessionNotFoundException(ex));
        System.exit(1);
      }
      catch(final org.omg.CORBA.SystemException ex) {
        ers.Logger.fatal(new rdb.CorbaSystemException(ex));
        System.exit(1);
      }

      
    }

    if(write_cursor != null) {
      try {
        write_cursor.close_session( write_session_id, proc_info );
      }
      catch(final rdb.SessionNotFound ex) {
        ers.Logger.fatal(new rdb.SessionNotFoundException(ex));
        System.exit(1);
      }
      catch(final org.omg.CORBA.SystemException ex) {
        ers.Logger.fatal(new rdb.CorbaSystemException(ex));
        System.exit(1);
      }
    }

  }
}

