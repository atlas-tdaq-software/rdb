package rdb;

public class SessionNotFoundException extends ers.Issue
  {
    public String reason;
    public int session_id;

    public SessionNotFoundException(rdb.SessionNotFound ex, ers.Issue cause)
      {
        super("session %d is not found", ex.session_id, cause);
      }

    public SessionNotFoundException(rdb.SessionNotFound ex)
      {
        super("session %d is not found", ex.session_id);
      }
  };
