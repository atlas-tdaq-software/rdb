package rdb;

public class CannotProceedException extends ers.Issue
  {
    public CannotProceedException(rdb.CannotProceed ex)
      {
        super("remote method failed with " + rdb.CannotProceedHelper.id() + " exception", ers2idl.Converter.idl2ersIssue(ex.issue));
      }
  };