package rdb;

import rdb.callbackPOA;

public final class WriterPing extends rdb.pingPOA {

  long m_session_id;

  public WriterPing() {
    m_session_id = 0;
  }

  public final void set_session_id(long id) {
    m_session_id = id;
  }

  public final long get_session_id() {
    return m_session_id;
  }

}

