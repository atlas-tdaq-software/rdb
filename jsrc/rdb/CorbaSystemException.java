package rdb;

public class CorbaSystemException extends ers.Issue
  {
    public String reason;

    public CorbaSystemException(org.omg.CORBA.SystemException ex, ers.Issue cause)
      {
        super("CORBA System Exception \"%s\"", ex.toString(), cause);
      }

    public CorbaSystemException(org.omg.CORBA.SystemException ex)
      {
        super("CORBA System Exception \"%s\"", ex.toString());
      }
  };