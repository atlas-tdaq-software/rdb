package rdb;

import java.net.InetAddress;
import java.io.File;
import java.io.IOException;


public final class CreateProcessInfo
{
  static private int getProcessId()
  {
    int process_id = 0;

    try {
      process_id = Integer.parseInt( ( new File("/proc/self")).getCanonicalFile().getName() );
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    
    return process_id;
  }

  static private String getHostName()
  {
    String hostname = null;
    try {
      hostname = java.net.InetAddress.getLocalHost().getCanonicalHostName();
    }
    catch(java.net.UnknownHostException e) {
      hostname = new String("(unknown:" + e.getMessage() + ")");
    }
    return hostname;
  }
  
  static private String getApplicationName()
  {
    String app_name = null;

    try
      {
        app_name = System.getenv("TDAQ_APPLICATION_NAME");
      }
    catch (SecurityException e)
      {
        e.printStackTrace();
      }

    if (app_name == null)
      app_name = "(unknown Java)";
    
    return app_name;
  }
  
  private static final boolean s_tokens_enabled = daq.tokens.JWToken.enabled();

  private static final String s_user_id = System.getProperty("user.name");
  private static final int s_process_id = getProcessId();
  private static final String s_hostname = getHostName();
  private static final String s_application_name = getApplicationName();

  static public ProcessInfo get(String app_name) throws daq.tokens.AcquireTokenException
  {
    return new ProcessInfo(s_tokens_enabled ? daq.tokens.JWToken.acquire(daq.tokens.JWToken.MODE.REUSE) : s_user_id, s_hostname, s_process_id, app_name == null ? s_application_name : app_name);
  }
}

