package rdb;

public class NotFoundException extends ers.Issue
  {
    public NotFoundException(rdb.NotFound ex)
      {
        super("remote method failed with " + rdb.NotFoundHelper.id() + " exception", ers2idl.Converter.idl2ersIssue(ex.issue));
      }
  };