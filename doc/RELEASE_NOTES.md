# RDB

## tdaq-11-02-00

### Introdice RDB Access Manager policy

Introduce Access Manager policy to send commands changing state of RDB server:
* OPEN
* CLOSE
* UPDATE
* RELOAD
* OPEN_SESSION
* LIST_SESSIONS

An appropriate permission has to be granted using AM DAQ:rdb rules. The user running RDB server has permission to send any command.

### Pass ERS exceptions using ers2idl package

Use `ers2idl::Issue` to pass ERS exceptions from server to client over CORBA and modify exception declarations:

```
  exception NotFound {
    ers2idl::Issue issue;
  };

  exception CannotProceed {
    ers2idl::Issue issue;
  };
```

### Use DAQ tokens for user authentication

Use DAQ tokens to pass information about user performing RDB actions and log important about user actions into RDB logs.

## tdaq-09-03-00

Use daq tokens to pass committer name to authenticate user on the oks git server in a secure way.
The commit_changes() IDL method requires a daq token, if the daq tokens are enabled.

## tdaq-09-01-00

Jira: [ADTCC-227](https://its.cern.ch/jira/browse/ADTCC-227)
Changes caused by replacement of OKS cvs server by git technology.

### IDL Changes

Add methods to access oks git repository revisions:
```
RDBRepositoryVersionList get_changes() raises (CannotProceed);
RDBRepositoryVersionList get_versions(in boolean skip_irrelevant, in VersionsQuerytType query_type, in string since, in string until) raises (CannotProceed);
```

Modify reload method:
* remove _ReloadParam_ parameter because possibilities to reload partition and all rdb servers of IPC domain were never used in practice; the reload command reloads this server only; it this server is master RDB server, it pushes changes to mirror, pool and slave servers
* change meaning of the RDBNameList names:
  * in case of oks repository used its vector must contain one and only one config version string;
  * otherwise, in case of filesystem repository used, as before, it contains lists of files to be reloaded.

### Utility Changes

Following above changes of IDL, the rdb_admin modified command line option to reload databases and added options to display new and archived configuration versions.

## tdaq-04-00-00

### Bug Fixes and Improvements  

* Fix bug [78326](https://savannah.cern.ch/bugs/?78326):
  * RDB writer updates backup information, when session is detected as bad and deleted.
  * RDB writer does not issue ERS error, if client survived it's restart tries to close a session; the error was downgraded to ERS log.
  * RDB server should not save backup file after each subscription on changes in case of massive subscribers; instead a separate thread periodically checks [by default each 10 seconds] if there are changes for backup and makes backup if needed.
  * RDB writer does not crash, when several clients actively update database (it was caused by fast boost allocator with null mutex in OKS library).
* Fix bug [79590](https://savannah.cern.ch/bugs/?79590): fix wrong calculation of parameters for _RDBServer::get_referenced_by()_ method used by rdbconfig plugin.
* Fix bug [79595](https://savannah.cern.ch/bugs/?79595): RDB writer continues notify clients on changes after client performs a commit (original problem was with the online expert system not reloading the database correctly.
* Implement feature request [81769](https://savannah.cern.ch/bugs/?81769): in notification exceptions add explicit fields for application name (and also user, host, pid, etc.) as requested by Run Control.
* Implement feature request [81655](https://savannah.cern.ch/bugs/?81655): if the RDB servers cannot contact subscriber or ping open session, the ERS error was downgraded to LOG message in case the origin of error was TRANSIENT CORBA exception (i.e. a client exit without informing RDB server); the errors are still reported in other cases, e.g. on timeouts when a client is alive but not responding.  
* Fix bug [86993](https://savannah.cern.ch/bugs/?86993): after commissioning of new HLT racks on P1, the number of clients reading data from main RDB server results too big access times: the timeout was increased from 30" to 40"; the bottleneck is the control network throughput):  
  * to reduce size of data structures passed between master and segment RDB servers, pass relationships by references (i.e. by index of OKS object in some global table) instead of value (class and object ID strings); this reduces total size of data passed between servers on 30-40% for ATLAS partition.
  * add command line option to read name of master server from environment, that simplifies build of multi-layer hierarchy of RDB servers using template applications.


## tdaq-03-00-00

#### RDB Writer  

There are changes in area of updating OKS using RDB package. Since this release there are two RDB servers:  

* the **rdb_server** is used for read-only operations and supports master and mirror modes (inherits most of the code from old rdb_server);  
* the **rdb_writer** is used for read-write operations.  

The rdb_server inherits the code from old rdb_server and remove all write functions. The rdb_writer is new application with own IDL and has the following differences from old rdb_server:  
* Each RDB client updating database works in scope of _session_:
  * at start a client opens session and at the end it has to close it;
  * sessions are not shared between clients and changes made or committed by a client are not visible to other clients till the server is reloaded;
  * any read or write request to writer server must have session id (unique 64 bits integer number);  
 * each session allowed to have own OKS server commit credentials.
* All sessions are supported by single RDB writer server.
* The RDB writer has default OKS kernel used for some read-only requests and fast kernel copy operations.  
* Each session may have own OKS kernel:
  * the kernel is not initialised until client makes first update operation (i.e. while a client is only reading data, the overhead is minimal);
  * when session is closed, the corresponding OKS kernel is destroyed and all associated resources are released;
  * the server periodically sends ping to open sessions; on second ping failure it forces session close (for crashed clients and those who does not close session on exit).

Despite of _rdb_server_ and _rdb_writer_ have different IDLs, their equal names are not allowed in a scope of partition. The configuration plugins and utilities dealing with RDB IDLs detect which actual interface is used for given name. No changes in previous config plugin names and parameters are needed, e.g. the "rdbconfig:RDB@ATLAS" parameter can be used to read data from rdb_server with name "DRB" running in partition "ATLAS" and "rdbconfig:RDB_RW@ATLAS" can be used to read/write data from rdb_writer with name "DRB_RW" running in partition "ATLAS".  

The _rdb_admin_ utility was modified to support rdb_writer and has new options:  
* "-S session-id" - provide id for read-write server session this is required for some other options (see help for more);
* "-L" - print list of sessions of read-write server.

### Patches Summary (since tdaq-02-00-03)  

*  Fix problem with mirrow server, that only holds schema files. In such case auto-pushing mechanism was not working (see patch [4400](https://savannah.cern.ch/patch/?4400)).  
*  Fix problem with slow processing of client subscription on changes in case of big database updates (see patch [4400](https://savannah.cern.ch/patch/?4400)).

### Performance Improvements  

* Improve performance of server, when calculate references on given object used by _get_partition()_ DAL algorithm, is visible on scale of combined ATLAS partition (since tdaq-02-01-00).
* Improve performance of server, when reload files with big number of updated objects (see comment in patch [4400](https://savannah.cern.ch/patch/?4400)).


## tdaq-02-01-00

### tdaq-02-00-03 Patches Summary

* Fix problem with RCR updates and objects removal on the mirror and sub-system RDB servers (see patches [3473](https://savannah.cern.ch/patch/?3473) and [3586](https://savannah.cern.ch/patch/?3586)).  
* Fix bug when mirror is busy and main RDB is restarted (see patch [3586](https://savannah.cern.ch/patch/?3586)).  
* If segment RDB server cannot get an update from master one, the master reports fatal error. The Run Control shifter or expert system action to restart such segment server is needed (see patch [3586](https://savannah.cern.ch/patch/?3586)).
* Fix problem with slow reload by slave servers in case if changes contain many removed objects (see patch [3798](https://savannah.cern.ch/patch/?3798)).
* Add modified schemes to the list of modified files, e.g. to be shown by IGUI (see patch [3799](https://savannah.cern.ch/patch/?3799)).
* Add timestamp to the temporal file name since the PID range is not enough since previous RDB instances may stop without clean exit, that results error during temporal file creation (see patch [3832](https://savannah.cern.ch/patch/?3832)).
* RDB servers replaces colons in backup filenames by underscore to fix problem such backup file load using rdbconfig (see patch [3832](https://savannah.cern.ch/patch/?3832)).
* Avoid bug caused by simultaneous backup files creation when unsubscribe non-responding server and client subscribers on database reload (see patch [3993](https://savannah.cern.ch/patch/?3993)).
* Release check out files on abort and test existence of user repository when restore from backup (see patch [3993](https://savannah.cern.ch/patch/?3993)).
* Fix "RDB RW server and names of local files" bug (see patch [4104](https://savannah.cern.ch/patch/?4104)): when RDB RW server is requested to return object's file name, it may return name of temporal file used by OKS server. Such name will not be recognized by read-only RDB server, so reload of such file will fail. When OKS server is used, the RDB RW server must to return short repository name for files from TDAQ_DB_USER_REPOSITORY (which may only be visible to RW RDB server), that can be recognized by other servers and to reload checked out files located in TDAQ_DB_USER_REPOSITORY even if names of reloading files contain TDAQ_USER_REPOSITORY prefix.

### Improvements  

* Improve performance of server, when calculate references on given object (is used by _get_partition()_ DAL algorithm, is visible on scale of combined ATLAS partition).  


## tdaq-01-09-01

### IDL Changes

* the _subscribe()_ and _unsubscribe()_ methods pass structure ProcessInfo providing details of subscriber process (pid, host, user, application-name)
* add method _merge_database()_ to merge loaded schema and data files (to be used by OKS archivers)
* add methods _push_complete_db()_ and _push_db_changes(_) needed by remote monitoring
* the subscriber has to support _server_exits()_ method invoking on subscribers when RDB server exits  

### RDB Server

* in case of problems with notification on changes the RDB server reports details of problematic subscriber
* on unsubscription request the RDB server checks validity of process info (process id, host and user have to match to the subscription info); this prevents a situation when one subscriber unsubscribes another one using wrong subscription id
* when exits, the RDB server notifies remain subscribers  

### New RDB Server Command Line Options

#### Remote Monitoring

To run RDB server in push mode run it with option -M. In such case the RDB server pushes complete database to the mirror RDB server, when it starts and pushes incremental update on any reload or unload/load operations.  

#### Backup Server for templated servers

If value of option -r or -b points to the directory and option -a is used, then the name of the backup file is generated concatenating the directory name and the server name.


## tdaq-01-08-04

### IDL Changes (as required by new Config API)  

* add _initValue_, _isNotNull_ and _IntegerFormat_ enumeration to properties of the _RDBAttribute_
* add _isNotNull_ to properties of the _RDBRelationship_
* add _get_direct_sub_classes()_ method
* implement new method _get_file_of_object(in RDBObject o, out string file)_ to return file an object belongs
* change _refereneced_by()_ to work with composite and weak relations

### Externally Modified Databases

During M5 one of the issue was the inconsistent update of configuration by RDB server: the shifter modifies partition file using IGUI "Segment & Resources" panel and presses "Commit" button; if after start or last reload of the RDB server the partition file was modified by somebody else (e.g. via OKS Data Editor), then these changes were silently overwritten by RDB server.  

To solve the problem following solution is implemented:  

* before locking any file for writing, the RDB server checks the file's timestamp;
  * if the file was modified after it was loaded by the RDB server, the lock operation fails and the RDB server throws exception, that it cannot lock externally modified file;
  * then shifter has two choices:
* reload database, that will read modifications made by users  
* force re-writing of the externally modified databases by using rdb_admin utility or integrating such functionality into IGUI

The RDB IDL has new function:  

```
void update_database_status( in string db_name ) raises (NotAllowed, CannotProceed)
```

The rdb_admin has new command line option "-t filename" (only works for RDB server running in read-write mode), that updates timestamp of the given file on the RDB server and allows it's commit using above mentioned function.  

### List Modified and Reload Selected Files  

Another M5 issue was connected with lack of control from shifter side on the list of files reloaded by RDB server: when "Reload DB" button is pressed, the RDB server reloads all modified files; some modified files contain changes, which are not supposed to be used with current run, or the modified files are not in consistent state at all; the run cannot continue in this case at all.  

Similarly, when shifter presses "Commit" button from "Segment & Resources" panel, RDB server commits changes in partition files and undesirably reloads all modified files.  

To solve the problem following solution is implemented:  

* provide _get_modified_databases()_ function to know externally modified and uncommitted files;  
* add new optional argument to _RDBServer::reload_database()_ method: the list of files to be reloaded (if list is empty, reload all modified files as it was before);
* when IGUI commits changes (e.g. from "Segments & Resources" panel), it gets list of updated datafiles from _get_modified_databases()_ and passes only this list (i.e. the partition file) to _reload_database()_ command;
* when IGUI needs to reload externally modified database, it shows to shifter list of externally modified files obtained from the _get_modified_databases()_ and the shifter selects which ones need to be reloaded (e.g. filling some checkboxes) after consultation with experts, or select all files, as it is now.

New IDL function to list modified files is available:  

```
void get_modified_databases(  
  out RDBNameList updated_datafiles,  
  out RDBNameList externally_updated_datafiles,  
  out RDBNameList externally_removed_datafiles  
)
```

The reload database IDL function was changed to optionally select which files need to be reloaded:  

```
void reload_database( in ReloadParam param ) raises (CannotProceed);
```

is replaced by:  

```
void reload_database( in ReloadParam param, in RDBNameList names ) raises (CannotProceed);
```

The rdb_admin has new command line option "-m" to list modified files.  
The command line option "-r" of the rdb_admin was changed and now supports optional list of files to be reloaded, e.g.:  

* "rdb_admin -d RDB -r this"  => reload all modified files on the server RDB
* "rdb_admin -p x -d RDB -r partition file1 file2" => reload files file1 and file2 on all servers running in the partition X

### New RDB Server Command Line Option

Add command line option "-a" to get name of the server via process environment variable. This is required for automatically generated variables, when the server is running as infrastructure application.


## tdaq-01-08-03

* support new OKS data types (i.e. 64 bits integers and class reference)
* add more verbosity and profiling messages on commit() and notify() commands
* provide more diagnostics on errors (e.g. cobra-loc of client, when notify() command fails)
* run each notify() and reload() commands in a separate thread; the threads are executed in parallel
* redirect to debug stream non-server bugs before reported to error stream; the issue is thrown anyway and can be processed properly by client


## tdaq-01-07-00

### Performance Improvements

*  use compact packing to pass referenced objects (names of attributes and relationships are put once per class);
*  cache results of most of the calls.

### Other Improvements and Bug Fixes

*  avoid overlap of messages printed from different threads;
*  print out time per call using high precision timers, when server is running with verbose level > 3;
*  move to ERS-based IPC and get rid of _IPCCacheExpired_ exception.


## tdaq-01-06-00

### IDL Changes

* _NotFound_ exception has new _item_ member (points to what object, class, attribute, etc. was not found)
* add method _get_inheritance_hierarchy()_ to return complete inheritance graph for all schema classes in a single call (is used for performance optimisation)
* for performance optimisation add new methods which allow to read the requested objects and objects referenced by them as it is defined by the _recursion_depth_ and _referenced_class_names_ parameters:
  * _xget_object()_ is similar to existing _get_object()_
  * _xget_all_objects()_ is similar to existing _get_all_objects()_
  * _xget_objects_by_query()_ is similar to existing _get_objects_by_query()_
  * _xget_objects_by_path()_ is similar to existing _get_objects_by_path()_

### RDB Library

* _RDBInfoReceiver_ class is storing name of the server (in addition to CORBA reference) to be able to reconnect with server if it has been restarted
* catch ipc cache-expired and try to reconnect with rdb server

### RDB Server

Reuse value of _--restore-from-file_ command line parameter as backup file if it is not defined explicitly


## tdaq-01-02-00

### RDB Server Backup File

The backup facility has been implemented. The server is able to store information about loaded databases files and made notification subscriptions to the backup file. The backup file is updated each time when database files are opened / closed, or a subscription is changed. The backup file can be read to load database files and restore all subscriptions, when the server is restarted.

To make backup file use _--backup_ option, e.g.:
```
rdb_server ... -b /tmp/my-rdb.backup.xml
```

To restore server state from backup file use _--restore_ option:
```
rdb_server ... -r /tmp/my-rdb.backup.xml
```

If the backup file can not be read successfully, it is ignored.

### RDB Server Interface Changes

#### Path Queries

To support oks path queries new idl method _get_objects_by_path()_ has been added to the RDB interface:

It is integrated with _rdb_dump_ application. The _object-from_ is defined by the _class-name_ and the _object-id_ options and the query itself is defined by the _path-query_ option. See example below:
```
rdb_dump -d test -c Partition -o onlsw_test_3x3_lxlpus -y '(path-to "lxplus-3x3-21-ctrl@RunControlApplication" (direct "Segments" "OnlineInfrastructure" (nested "Segments" (direct "Applications" "IsControlledBy" "Resources"))))'  
Found 3 objects in the path:  
[1] Object "onlsw_test_3x3_lxlpus@Partition" ...  
[2] Object "lxplus-3x3-2@Segment" ...  
[3] Object "lxplus-3x3-21@Segment" ..._
```

#### Reload Servers

In accordance with current working model the user modifies databases, commit changes and then send command to the rdb server to reload the database files. Since multiple rdb servers can be started simultaneously, it is necessary to have a simple way to inform all the servers by single command. To do this the _reload_database()_ idl method has been modified to accept new reload parameter, which can be set to one of the following values:
* **this_server** - reload database files on given server
* **partition_servers** - reload database files on all rdb servers running in the partition of given server
* **all_servers** - reload database files on all rdb servers

The _--reload_ command line option of the rdb_admin application was changed to accept information what servers need to reload database files. Use "_-r this_" to reload files on given server only, "_-r partition_" to reload files on all partition rdb servers and "_-r all_" to reload files on all rdb servers.

#### 64-bits Compilers

Modify idl files by replacing _long_ type by _CORBA::Long_ required by tested 64-bits compiler.

### Bugs Fixes

The rdb information subscriber does not call _run()_ method anymore, since it blocks the user's thread. The _enter_loop_ parameter was removed from the _RDBInfoReceiver::subscribe()_ method.


## tdaq-01-01-00

### Changes

*   Add method _get_referenced_by()_ to return list of objects referencing given one.
*   Allow to run empty server, i.e. without any files to be loaded.
