#ifndef __RDB_IDL__
#define __RDB_IDL__

#include "ipc/ipc.idl"
#include "ers2idl/ers2idl.idl"

module rdb {

    /** Information about process. */

  struct ProcessInfo {
    string user_id;       ///< User ID
    string host;          ///< Host name
    long process_id;      ///< ID of user's process
    string app_name;      ///< application name
  };


    // base programming types

  enum PrimitiveKind{
    pk_unknown,
    pk_boolean,
    pk_u8_int,
    pk_s8_int,
    pk_s16_int,
    pk_u16_int,
    pk_s32_int,
    pk_u32_int,
    pk_s64_int,
    pk_u64_int,
    pk_float,
    pk_double,
    pk_string
  };


    // database object

  struct RDBObject{
    string name;
    string classid;
  };


    // single-value attribute

  union DataUnion switch(PrimitiveKind) { 
    case pk_s8_int:  char                du_s8_int;
    case pk_u8_int:  octet               du_u8_int;
    case pk_s16_int: short               du_s16_int;
    case pk_u16_int: unsigned short      du_u16_int;
    case pk_s32_int: long                du_s32_int;
    case pk_u32_int: unsigned long       du_u32_int;
    case pk_s64_int: long long           du_s64_int;
    case pk_u64_int: unsigned long long  du_u64_int;
    case pk_float:   float               du_float;
    case pk_double:  double              du_double;
    case pk_boolean: boolean             du_bool;
    case pk_string:  string              du_string;
  };


    // multi-value attribute

  union DataListUnion switch(PrimitiveKind) {
    case pk_s8_int:  sequence<char>                du_s8_ints;
    case pk_u8_int:  sequence<octet>               du_u8_ints;
    case pk_s16_int: sequence<short>               du_s16_ints;
    case pk_u16_int: sequence<unsigned short>      du_u16_ints;
    case pk_s32_int: sequence<long>                du_s32_ints;
    case pk_u32_int: sequence<unsigned long>       du_u32_ints;
    case pk_s64_int: sequence<long long>           du_s64_ints;
    case pk_u64_int: sequence<unsigned long long>  du_u64_ints;
    case pk_float:   sequence<float>               du_floats;
    case pk_double:  sequence<double>              du_doubles;
    case pk_boolean: sequence<boolean>             du_bools;
    case pk_string:  sequence<string>              du_strings;
  };


    // the numeration format to be used for integer value representation

  enum IntegerFormat {
    int_format_oct,  // use octal numeration for attribute value
    int_format_dec,  // use decimal numeration for attribute value
    int_format_hex,  // use hexadecimal numeration for attribute value
    int_format_na    // not applicable, the attribute type is not integer
  };

  struct RDBAttribute {
    string         name;
    string         type;
    string         range;
    string         initValue;
    IntegerFormat  intFormat;
    boolean        isMultiValue;
    boolean        isNotNull;
    boolean        isDirect;
    string         description;
  };

  struct RDBRelationship {
    string   name;
    string   classid;
    boolean  isMultiValue;
    boolean  isNotNull;
    boolean  isDirect;
    boolean  isAggregation;
    boolean  isExclusive;
    boolean  isDependent;
    boolean  isOrdered;
    string   description;    
  };


  struct RDBClass {
    string         name;
    boolean        isAbstract;
    string         description;
    unsigned long  objectNumber;
    unsigned long  attributeNumber;
    unsigned long  directAttributeNumber;
    unsigned long  relationshipNumber;
    unsigned long  directRelationshipNumber;
  };

  typedef sequence<RDBObject>        RDBObjectList;
  typedef sequence<RDBAttribute>     RDBAttributeList;
  typedef sequence<RDBRelationship>  RDBRelationshipList;
  typedef sequence<RDBClass>         RDBClassList;

    /// RDBNameList defines sequence of strings
    /// which is used for names of classes, object-identities, etc.

  typedef sequence<string> RDBNameList;



    // attribute/relationship value(s) and their sequences are used
    // to pass whole object in one go using get_object_values() method

  struct RDBAttributeValue {
    string     name;
    DataUnion  data;
  };

  struct RDBAttributeValues {
    string         name;
    DataListUnion  data;
  };

  struct RDBRelationshipValue {
    string         name;
    boolean        isMultiValue;
    RDBObjectList  data;
  };

  typedef sequence<RDBAttributeValue>     RDBAttributeValueList;
  typedef sequence<RDBAttributeValues>    RDBAttributeValuesList;
  typedef sequence<RDBRelationshipValue>  RDBRelationshipValueList;

  struct RDBObjectValue {
    string name;
    string classid;
    RDBAttributeValueList av;
    RDBAttributeValuesList avs;
    RDBRelationshipValueList rv;
  };

  typedef sequence<RDBObjectValue> RDBObjectValueList;



    // compact attribute/relationship value(s) and their sequences are used
    // to pass complete objects in one go using xget_...() methods

  typedef sequence<DataUnion>        RDBAttributeCompactValueList;
  typedef sequence<DataListUnion>    RDBAttributeCompactValuesList;
  typedef sequence<RDBObjectList>    RDBRelationshipCompactValueList;


    // object and values of it's attributes and relationships

  struct RDBObjectCompactValue {
    string name;
    RDBAttributeCompactValueList av;
    RDBAttributeCompactValuesList avs;
    RDBRelationshipCompactValueList rv;
  };

  typedef sequence<RDBObjectCompactValue> RDBObjectCompactValueList;


    // class and values of it's objects

  struct RDBClassValue {
    string name;
    RDBNameList sv_attributes;
    RDBNameList mv_attributes;
    RDBNameList relationships;
    RDBObjectCompactValueList objects;
  };

  typedef sequence<RDBClassValue> RDBClassValueList;


    // complete description of class

  struct RDBClassDescription {
    string                     name;
    boolean                    isAbstract;
    string                     description;
    RDBNameList                direct_superclasses;
    RDBNameList                all_superclasses;
    RDBNameList                direct_subclasses;
    RDBNameList                all_subclasses;
    RDBAttributeList           attributes;
    RDBRelationshipList        relationships;
  };

  typedef sequence<RDBClassDescription> RDBClassDescriptionList;


    // complete description of DB in compact format

  typedef sequence<boolean>             RDB_booleans;
  typedef sequence<char>                RDB_s8_ints;
  typedef sequence<octet>               RDB_u8_ints;
  typedef sequence<short>               RDB_s16_ints;
  typedef sequence<unsigned short>      RDB_u16_ints;
  typedef sequence<long>                RDB_s32_ints;
  typedef sequence<unsigned long>       RDB_u32_ints;
  typedef sequence<long long>           RDB_s64_ints;
  typedef sequence<unsigned long long>  RDB_u64_ints;
  typedef sequence<float>               RDB_floats;
  typedef sequence<double>              RDB_doubles;

  struct RDBClassData {
    RDBClassDescription schema;
    RDBNameList objects;
  };

  struct RDBCompleteDBi {

      // all strings used in attribute values; the values are passed as index in this dictionary

    RDBNameList             strings_dictionary;

      // values of all attributes and relationships

    RDB_booleans            booleans;
    RDB_s8_ints             s8_ints;
    RDB_u8_ints             u8_ints;
    RDB_s16_ints            s16_ints;
    RDB_u16_ints            u16_ints;         // u16 ints and enums
    RDB_s32_ints            s32_ints;         // s32 ints and single-value relationships (-1 means NULL for relationship)
    RDB_u32_ints            u32_ints;         // u32 ints and strings (by index from strings_dictionary)
    RDB_s64_ints            s64_ints;
    RDB_u64_ints            u64_ints;
    RDB_floats              floats;
    RDB_doubles             doubles;
    
    sequence<RDB_booleans>  boolean_lists;
    sequence<RDB_s8_ints>   s8_int_lists;
    sequence<RDB_u8_ints>   u8_int_lists;
    sequence<RDB_s16_ints>  s16_int_lists;
    sequence<RDB_u16_ints>  u16_int_lists;    // arrays of u16 ints and lists-of-enums
    sequence<RDB_s32_ints>  s32_int_lists;    // arrays of s32 ints and multi-value relationships
    sequence<RDB_u32_ints>  u32_int_lists;    // arrays of u32 ints and lists-of-strings  (by index from strings_dictionary)
    sequence<RDB_s64_ints>  s64_int_lists;
    sequence<RDB_u64_ints>  u64_int_lists;
    sequence<RDB_floats>    float_lists;
    sequence<RDB_doubles>   double_lists;
    
      // schema of classes and IDs of their objects

    sequence<RDBClassData>  classes;

  };

  typedef sequence<RDBCompleteDBi> RDBCompleteDB;


    /// RDBClassChanges describes changes occurred in a single class
    ///  * name     - is name of the class
    ///  * created  - sequence of created objects identities
    ///  * modified - sequence of modified objects identities
    ///  * removed  - sequence of removed objects identities

  struct RDBClassChanges {
    string       name;
    RDBNameList  created;
    RDBNameList  modified;
    RDBNameList  removed;
  };


    /// RDBClassChangesList defines changes occurred in several classes

  typedef sequence<RDBClassChanges> RDBClassChangesList;


    /// RDBBaseClasses defines base class and all it's supeclasses

  struct RDBBaseClasses {
    string       name;
    RDBNameList  all_parents;
  };

    /// RDBInheritanceHierarchyList defines supeclasses hierarchy

  typedef sequence<RDBBaseClasses> RDBInheritanceHierarchyList;


    /// the callback notifies subscriber about occurred changes

  interface callback
  {
      // returns number of microseconds spent by client to process callback
      // NOTE: the callback returns a value to avoid lost callback because of OMNI bug

    long inform( in RDBClassChangesList changes_list, in long long parameter );

      // is called by server on exit; client has to remove subscription

    void server_exits(in long long parameter);
  };



    /// RDBChanges defines values of created and updated objects and list of removed objects for given class

  struct RDBChanges {
    string                     class_name;
    RDBObjectCompactValueList  created_objects;
    RDBObjectCompactValueList  updated_objects;
    RDBNameList                removed_objects;
  };

  typedef sequence<RDBChanges> RDBDataChanges;

    /// the callback notifies RDB server subscriber about occurred changes

  interface rdb_callback
  {
      // returns number of microseconds spent by client to process callback
      // the data_changes contains created, updated and removed objects for each class

    long inform( in RDBDataChanges data_changes );
  };



    /// RDBFileObjects stores information about objects per data file
    
  struct RDBFileObjects {
    string         filename;
    RDBObjectList  objects;
  };

  typedef sequence<RDBFileObjects>        RDBFileObjectsList;


   /// RDBVersion

  struct RDBRepositoryVersion {
    string id;
    string user;
    unsigned long timestamp;
    string comment;
    RDBNameList files;
  };

  typedef sequence<RDBRepositoryVersion> RDBRepositoryVersionList;

  enum VersionsQueryType {
    query_by_date,
    query_by_id,
    query_by_tag
  };

  exception NotFound {
    ers2idl::Issue issue;
  };

  exception CannotProceed {
    ers2idl::Issue issue;
  };


    ////////////////////////////////////////////////////////////
    // below there is interface implemented by the rdb_server //
    ////////////////////////////////////////////////////////////

  interface cursor : ipc::servant {

      /// methods for databases manipulations

    void open_database( in ProcessInfo info, in RDBNameList names ) raises (CannotProceed);
    void close_database( in ProcessInfo info, in RDBNameList names ) raises (CannotProceed);
    void get_databases( out RDBNameList schemafiles, out RDBNameList datafiles );
    void get_modified_databases( out RDBNameList externally_updated_datafiles, out RDBNameList externally_removed_datafiles );
    void update_database_status( in ProcessInfo info, in string db_name ) raises (CannotProceed );
    void reload_database( in ProcessInfo info, in RDBNameList names ) raises (CannotProceed);
    void merge_database( in string data_file_name, in string schema_file_name, in string class_name, in string query, in long recursion_depth, in boolean ignore_dangling ) raises (CannotProceed);


      /// methods for database repository versions

    RDBRepositoryVersionList get_changes() raises (CannotProceed);
    RDBRepositoryVersionList get_versions(in boolean skip_irrelevant, in VersionsQueryType query_type, in string since, in string until) raises (CannotProceed);

      // methods for dynamic configuration

    void request_parent(in string name, in string host, out string parent) raises (CannotProceed);
    void register_parent(in string name, in string host) raises (CannotProceed);

      // read complete db and subscribe on changes (return subscription id)

    void get_complete_db( out RDBCompleteDB db, in string server_name, in rdb_callback cb );
    void unsubscribe_rdb ( in string server_name ) raises (CannotProceed);

      // push complete db and changes (mirroring servers)

    void push_complete_db ( in string db_name, in string partition_name, in RDBCompleteDB db ) raises (CannotProceed);
    void push_db_changes ( in string db_name, in string partition_name, in RDBDataChanges data_changes ) raises (CannotProceed);

      /// methods for database files info

    void database_get_files( in string db_name, out RDBNameList file_names ) raises (CannotProceed);

      /// method used by rdbproxy to check if information on server was updated

    long get_validity_key();

      /// methods to read schema (cid = class-identity or class-name)

    void get_all_classes( out RDBClassList l );
    void get_class( in string cid, out RDBClass c ) raises (NotFound);
    void get_direct_super_classes( in string cid, out RDBClassList l ) raises (NotFound);
    void get_all_super_classes( in string cid, out RDBClassList l ) raises (NotFound);
    void get_sub_classes( in string cid, out RDBClassList l ) raises (NotFound);
    void get_direct_sub_classes( in string cid, out RDBClassList l ) raises (NotFound);
    void get_attributes( in string cid, out RDBAttributeList l ) raises (NotFound);
    void get_relationships( in string cid, out RDBRelationshipList l ) raises (NotFound);
    long get_inheritance_hierarchy(out RDBInheritanceHierarchyList l );


      /// methods to read data (oid = object-identity, lis = look-in-subclasses)

    void get_object ( in string cid, in boolean lis, in string oid, out RDBObject o ) raises (NotFound);
    void get_file_of_object ( in RDBObject o, out string file ) raises (NotFound);
    void get_files_of_all_objects ( out RDBFileObjectsList info );
    void get_all_objects ( in string cid, in boolean lis, out RDBObjectList l ) raises (NotFound);
    void get_objects_by_query ( in string cid, in string query, out RDBObjectList l ) raises (NotFound, CannotProceed);
    void get_objects_by_path ( in RDBObject o, in string query, out RDBObjectList l ) raises (NotFound, CannotProceed);
    void get_object_values ( in string cid, in string oid, out RDBAttributeValueList la, out RDBAttributeValuesList las, out RDBRelationshipValueList lr) raises (NotFound);
    void cget_object_values ( in string cid, in string oid, out RDBAttributeCompactValueList la, out RDBAttributeCompactValuesList las, out RDBRelationshipCompactValueList lr) raises (NotFound);
    void xget_object_values ( in string cid, in string oid, out RDBNameList sv_an, out RDBNameList mv_an, out RDBNameList rn, out RDBAttributeCompactValueList la, out RDBAttributeCompactValuesList las, out RDBRelationshipCompactValueList lr) raises (NotFound);
    void get_objects_of_relationship ( in RDBObject o, in string r_name, out RDBObjectList objs) raises (NotFound);
    void get_referenced_by ( in RDBObject o, in string r_name, in boolean composite_only, out RDBObjectList objs) raises (NotFound);
    void get_attribute_value ( in RDBObject o, in string a_name, out DataUnion d) raises (NotFound, CannotProceed);
    void get_attribute_values ( in RDBObject o, in string a_name, out DataListUnion l) raises (NotFound, CannotProceed);


      /// same as above, but also read values of objects and referenced objects (if recursion_depth > 0),
      /// types of read objects can be limited by the names of classes parameter 'rcls' (if empty, read all objects)

    long xget_object ( in string cid, in boolean lis, in string oid, out RDBObject o, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values) raises (NotFound, CannotProceed);
    long xget_all_objects ( in string cid, in boolean lis, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (NotFound, CannotProceed);
    long xget_objects_by_query ( in string cid, in string query, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (NotFound, CannotProceed);
    void xget_objects_by_path ( in RDBObject o, in string query, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (NotFound, CannotProceed);


      /// prefetch all data into rdbconfig client cache (used by DBE)

    void prefetch_data(out RDBClassValueList values) raises (CannotProceed);
    void prefetch_schema(out RDBClassDescriptionList values) raises (CannotProceed);


      /// subscribes on changes occurred in the classes and returns handle
      ///  * the RDBNameList defines list of classes
      ///  * if the boolean parameter 'lis' is true, also look in subclasses
      ///  * the RDBObjectList 'l2' defines list of objects
      ///  * the callback is called when changes occurred
      ///  * the long is a user-defined parameter
      /// if both RDBNameList and RDBObjectList are empty, subscribe on all classes

    long subscribe ( in ProcessInfo info, in RDBNameList l1, in boolean lis, in RDBObjectList l2, in callback cb, in long long parameter) raises (CannotProceed);


      /// removes previous subscription
      ///  * use handle returned by the subscribe() method

    void unsubscribe ( in ProcessInfo info, in long id) raises (CannotProceed);

  };


  ///////////////////////////////////////////////////////////////////////////////


  typedef long long RDBSessionId;

  typedef sequence<long long> RDBSessionIdList;

  exception SessionNotFound {
    RDBSessionId session_id;
  };

    // the interface is used to check the client is alive

  interface ping
  {
      // return client's session ID

    RDBSessionId get_session_id();
  };

  struct SessionInfo {
    RDBSessionId  id;
    string        user_id;
    string        host;
    long          process_id;
    string        app_name;
    string        creation_ts;
  };

  typedef sequence<SessionInfo> SessionInfoList;


    ////////////////////////////////////////////////////////////
    // below there is interface implemented by the rdb_writer //
    ////////////////////////////////////////////////////////////

  interface writer : ipc::servant {

      /// session management

    RDBSessionId open_session( in ProcessInfo info, in rdb::ping cb ) raises (CannotProceed);
    void set_commit_credentials( in RDBSessionId session_id, in string user, in string password ) raises (SessionNotFound, CannotProceed);
    void close_session( in RDBSessionId session_id, in ProcessInfo info ) raises (SessionNotFound);
    void list_sessions( in ProcessInfo info, out SessionInfoList data) raises (CannotProceed);


      /// methods for databases manipulations

    void create_database( in RDBSessionId session_id, in string db_name ) raises (SessionNotFound, CannotProceed);
    void get_databases( in RDBSessionId session_id, out RDBNameList schemafiles, out RDBNameList datafiles ) raises (SessionNotFound);
    void get_modified_databases( in RDBSessionId session_id, out RDBNameList updated_datafiles, out RDBNameList externally_updated_datafiles, out RDBNameList externally_removed_datafiles ) raises (SessionNotFound);
    void update_database_status( in ProcessInfo info, in RDBSessionId session_id, in string db_name ) raises (SessionNotFound, CannotProceed);
    void commit_changes( in RDBSessionId session_id, in string token, in string log ) raises (SessionNotFound, CannotProceed);
    void abort_changes( in RDBSessionId session_id ) raises (SessionNotFound, CannotProceed);
    void reload_database( in ProcessInfo info, in RDBNameList names ) raises (CannotProceed);


      /// methods for database repository versions

    RDBRepositoryVersionList get_changes() raises (CannotProceed);
    RDBRepositoryVersionList get_versions(in boolean skip_irrelevant, in VersionsQueryType query_type, in string since, in string until) raises (CannotProceed);

      /// methods for database files manipulations

    void database_add_file( in RDBSessionId session_id, in string db_name, in string file_name ) raises (SessionNotFound, CannotProceed);
    void database_remove_file( in RDBSessionId session_id, in string db_name, in string file_name, out RDBClassChangesList removed_objects ) raises (SessionNotFound, CannotProceed);
    void database_get_files( in RDBSessionId session_id, in string db_name, out RDBNameList file_names ) raises (SessionNotFound, CannotProceed);
    boolean is_database_writable( in RDBSessionId session_id, in string db_name ) raises (SessionNotFound, CannotProceed);


      /// methods to read schema (cid = class-identity or class-name)

    void get_all_classes( in RDBSessionId session_id, out RDBClassList l ) raises (SessionNotFound);
    void get_class( in RDBSessionId session_id, in string cid, out RDBClass c ) raises (SessionNotFound, NotFound);
    void get_direct_super_classes( in RDBSessionId session_id, in string cid, out RDBClassList l ) raises (SessionNotFound, NotFound);
    void get_all_super_classes( in RDBSessionId session_id, in string cid, out RDBClassList l ) raises (SessionNotFound, NotFound);
    void get_sub_classes( in RDBSessionId session_id, in string cid, out RDBClassList l ) raises (SessionNotFound, NotFound);
    void get_direct_sub_classes( in RDBSessionId session_id, in string cid, out RDBClassList l ) raises (SessionNotFound, NotFound);
    void get_attributes( in RDBSessionId session_id, in string cid, out RDBAttributeList l ) raises (SessionNotFound, NotFound);
    void get_relationships( in RDBSessionId session_id, in string cid, out RDBRelationshipList l ) raises (SessionNotFound, NotFound);
    void get_inheritance_hierarchy( in RDBSessionId session_id, out RDBInheritanceHierarchyList l ) raises (SessionNotFound);


      /// methods to read data (oid = object-identity, lis = look-in-subclasses)

    void get_object ( in RDBSessionId session_id, in string cid, in boolean lis, in string oid, out RDBObject o ) raises (SessionNotFound, NotFound);
    void get_file_of_object ( in RDBSessionId session_id, in RDBObject o, out string file ) raises (SessionNotFound, NotFound);
    void get_files_of_all_objects ( in RDBSessionId session_id, out RDBFileObjectsList info ) raises (SessionNotFound);
    void get_all_objects ( in RDBSessionId session_id, in string cid, in boolean lis, out RDBObjectList l ) raises (SessionNotFound, NotFound);
    void get_objects_by_query ( in RDBSessionId session_id, in string cid, in string query, out RDBObjectList l ) raises (SessionNotFound, NotFound, CannotProceed);
    void get_objects_by_path ( in RDBSessionId session_id, in RDBObject o, in string query, out RDBObjectList l ) raises (SessionNotFound, NotFound, CannotProceed);
    void get_object_values ( in RDBSessionId session_id, in string cid, in string oid, out RDBAttributeValueList la, out RDBAttributeValuesList las, out RDBRelationshipValueList lr) raises (SessionNotFound, NotFound);
    void cget_object_values ( in RDBSessionId session_id, in string cid, in string oid, out RDBAttributeCompactValueList la, out RDBAttributeCompactValuesList las, out RDBRelationshipCompactValueList lr) raises (SessionNotFound, NotFound);
    void xget_object_values ( in RDBSessionId session_id, in string cid, in string oid, out RDBNameList sv_an, out RDBNameList mv_an, out RDBNameList rn, out RDBAttributeCompactValueList la, out RDBAttributeCompactValuesList las, out RDBRelationshipCompactValueList lr) raises (SessionNotFound, NotFound);
    void get_objects_of_relationship ( in RDBSessionId session_id, in RDBObject o, in string r_name, out RDBObjectList objs) raises (SessionNotFound, NotFound);
    void get_referenced_by ( in RDBSessionId session_id, in RDBObject o, in string r_name, in boolean composite_only, out RDBObjectList objs) raises (SessionNotFound, NotFound);
    void get_attribute_value ( in RDBSessionId session_id, in RDBObject o, in string a_name, out DataUnion d) raises (SessionNotFound, NotFound, CannotProceed);
    void get_attribute_values ( in RDBSessionId session_id, in RDBObject o, in string a_name, out DataListUnion l) raises (SessionNotFound, NotFound, CannotProceed);


      /// same as above, but also read values of objects and referenced objects (if recursion_depth > 0),
      /// types of read objects can be limited by the names of classes parameter 'rcls' (if empty, read all objects)

    void xget_object ( in RDBSessionId session_id, in string cid, in boolean lis, in string oid, out RDBObject o, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values) raises (SessionNotFound, NotFound, CannotProceed);
    void xget_all_objects ( in RDBSessionId session_id, in string cid, in boolean lis, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (SessionNotFound, NotFound, CannotProceed);
    void xget_objects_by_query ( in RDBSessionId session_id, in string cid, in string query, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (SessionNotFound, NotFound, CannotProceed);
    void xget_objects_by_path ( in RDBSessionId session_id, in RDBObject o, in string query, out RDBObjectList l, in long recursion_depth, in RDBNameList rcls, out RDBClassValueList values ) raises (SessionNotFound, NotFound, CannotProceed);


      /// prefetch data and schema into rdbconfig client cache

    void prefetch_data( in RDBSessionId session_id, out RDBClassValueList values ) raises (SessionNotFound, CannotProceed);
    void prefetch_schema( in RDBSessionId session_id, out RDBClassDescriptionList values ) raises (SessionNotFound, CannotProceed);


      /// methods to write data

    void create_object( in RDBSessionId session_id, in string cid, in string oid, in string file_name ) raises (SessionNotFound, NotFound, CannotProceed);
    void new_object( in RDBSessionId session_id, in string cid, in string oid, in RDBObject o ) raises (SessionNotFound, NotFound, CannotProceed);
    void delete_object( in RDBSessionId session_id, in RDBObject o, out RDBClassChangesList removed_objects ) raises (SessionNotFound, NotFound, CannotProceed);
    void move_object( in RDBSessionId session_id, in RDBObject o, in string file_name ) raises (SessionNotFound, NotFound, CannotProceed);
    void rename_object( in RDBSessionId session_id, in RDBObject o, in string new_name ) raises (SessionNotFound, NotFound, CannotProceed);
    void set_objects_of_relationship ( in RDBSessionId session_id, in RDBObject o, in string r_name, in RDBObjectList objs, in boolean skip_non_null_check) raises (SessionNotFound, NotFound, CannotProceed);
    void set_attribute_value ( in RDBSessionId session_id, in RDBObject o, in string a_name, in DataUnion d) raises (SessionNotFound, NotFound, CannotProceed);
    void set_attribute_values ( in RDBSessionId session_id, in RDBObject o, in string a_name, in DataListUnion l) raises (SessionNotFound, NotFound, CannotProceed);


      /// subscribes on changes occurred in the classes and returns handle
      ///  * the RDBNameList defines list of classes
      ///  * if the boolean parameter 'lis' is true, also look in subclasses
      ///  * the RDBObjectList 'l2' defines list of objects
      ///  * the callback is called when changes occurred
      ///  * the long is a user-defined parameter
      /// if both RDBNameList and RDBObjectList are empty, subscribe on all classes

    void subscribe ( in RDBSessionId session_id, in ProcessInfo info, in RDBNameList l1, in boolean lis, in RDBObjectList l2, in callback cb, in long long parameter) raises (SessionNotFound, CannotProceed);


      /// removes previous subscription

    void unsubscribe ( in RDBSessionId session_id, in ProcessInfo info) raises (SessionNotFound, CannotProceed);

  };



};

#endif
